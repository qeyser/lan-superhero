(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["common/vendor"],[
/* 0 */,
/* 1 */
/*!************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.createApp = createApp;exports.createComponent = createComponent;exports.createPage = createPage;exports.createPlugin = createPlugin;exports.createSubpackageApp = createSubpackageApp;exports.default = void 0;var _vue = _interopRequireDefault(__webpack_require__(/*! vue */ 2));
var _uniI18n = __webpack_require__(/*! @dcloudio/uni-i18n */ 4);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}function ownKeys(object, enumerableOnly) {var keys = Object.keys(object);if (Object.getOwnPropertySymbols) {var symbols = Object.getOwnPropertySymbols(object);if (enumerableOnly) symbols = symbols.filter(function (sym) {return Object.getOwnPropertyDescriptor(object, sym).enumerable;});keys.push.apply(keys, symbols);}return keys;}function _objectSpread(target) {for (var i = 1; i < arguments.length; i++) {var source = arguments[i] != null ? arguments[i] : {};if (i % 2) {ownKeys(Object(source), true).forEach(function (key) {_defineProperty(target, key, source[key]);});} else if (Object.getOwnPropertyDescriptors) {Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));} else {ownKeys(Object(source)).forEach(function (key) {Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));});}}return target;}function _slicedToArray(arr, i) {return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();}function _nonIterableRest() {throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _iterableToArrayLimit(arr, i) {if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;var _arr = [];var _n = true;var _d = false;var _e = undefined;try {for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {_arr.push(_s.value);if (i && _arr.length === i) break;}} catch (err) {_d = true;_e = err;} finally {try {if (!_n && _i["return"] != null) _i["return"]();} finally {if (_d) throw _e;}}return _arr;}function _arrayWithHoles(arr) {if (Array.isArray(arr)) return arr;}function _defineProperty(obj, key, value) {if (key in obj) {Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true });} else {obj[key] = value;}return obj;}function _toConsumableArray(arr) {return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();}function _nonIterableSpread() {throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _iterableToArray(iter) {if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);}function _arrayWithoutHoles(arr) {if (Array.isArray(arr)) return _arrayLikeToArray(arr);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}

function b64DecodeUnicode(str) {
  return decodeURIComponent(atob(str).split('').map(function (c) {
    return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
  }).join(''));
}

function getCurrentUserInfo() {
  var token = wx.getStorageSync('uni_id_token') || '';
  var tokenArr = token.split('.');
  if (!token || tokenArr.length !== 3) {
    return {
      uid: null,
      role: [],
      permission: [],
      tokenExpired: 0 };

  }
  var userInfo;
  try {
    userInfo = JSON.parse(b64DecodeUnicode(tokenArr[1]));
  } catch (error) {
    throw new Error('获取当前用户信息出错，详细错误信息为：' + error.message);
  }
  userInfo.tokenExpired = userInfo.exp * 1000;
  delete userInfo.exp;
  delete userInfo.iat;
  return userInfo;
}

function uniIdMixin(Vue) {
  Vue.prototype.uniIDHasRole = function (roleId) {var _getCurrentUserInfo =


    getCurrentUserInfo(),role = _getCurrentUserInfo.role;
    return role.indexOf(roleId) > -1;
  };
  Vue.prototype.uniIDHasPermission = function (permissionId) {var _getCurrentUserInfo2 =


    getCurrentUserInfo(),permission = _getCurrentUserInfo2.permission;
    return this.uniIDHasRole('admin') || permission.indexOf(permissionId) > -1;
  };
  Vue.prototype.uniIDTokenValid = function () {var _getCurrentUserInfo3 =


    getCurrentUserInfo(),tokenExpired = _getCurrentUserInfo3.tokenExpired;
    return tokenExpired > Date.now();
  };
}

var _toString = Object.prototype.toString;
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isFn(fn) {
  return typeof fn === 'function';
}

function isStr(str) {
  return typeof str === 'string';
}

function isPlainObject(obj) {
  return _toString.call(obj) === '[object Object]';
}

function hasOwn(obj, key) {
  return hasOwnProperty.call(obj, key);
}

function noop() {}

/**
                    * Create a cached version of a pure function.
                    */
function cached(fn) {
  var cache = Object.create(null);
  return function cachedFn(str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str));
  };
}

/**
   * Camelize a hyphen-delimited string.
   */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) {return c ? c.toUpperCase() : '';});
});

var HOOKS = [
'invoke',
'success',
'fail',
'complete',
'returnValue'];


var globalInterceptors = {};
var scopedInterceptors = {};

function mergeHook(parentVal, childVal) {
  var res = childVal ?
  parentVal ?
  parentVal.concat(childVal) :
  Array.isArray(childVal) ?
  childVal : [childVal] :
  parentVal;
  return res ?
  dedupeHooks(res) :
  res;
}

function dedupeHooks(hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res;
}

function removeHook(hooks, hook) {
  var index = hooks.indexOf(hook);
  if (index !== -1) {
    hooks.splice(index, 1);
  }
}

function mergeInterceptorHook(interceptor, option) {
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      interceptor[hook] = mergeHook(interceptor[hook], option[hook]);
    }
  });
}

function removeInterceptorHook(interceptor, option) {
  if (!interceptor || !option) {
    return;
  }
  Object.keys(option).forEach(function (hook) {
    if (HOOKS.indexOf(hook) !== -1 && isFn(option[hook])) {
      removeHook(interceptor[hook], option[hook]);
    }
  });
}

function addInterceptor(method, option) {
  if (typeof method === 'string' && isPlainObject(option)) {
    mergeInterceptorHook(scopedInterceptors[method] || (scopedInterceptors[method] = {}), option);
  } else if (isPlainObject(method)) {
    mergeInterceptorHook(globalInterceptors, method);
  }
}

function removeInterceptor(method, option) {
  if (typeof method === 'string') {
    if (isPlainObject(option)) {
      removeInterceptorHook(scopedInterceptors[method], option);
    } else {
      delete scopedInterceptors[method];
    }
  } else if (isPlainObject(method)) {
    removeInterceptorHook(globalInterceptors, method);
  }
}

function wrapperHook(hook) {
  return function (data) {
    return hook(data) || data;
  };
}

function isPromise(obj) {
  return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}

function queue(hooks, data) {
  var promise = false;
  for (var i = 0; i < hooks.length; i++) {
    var hook = hooks[i];
    if (promise) {
      promise = Promise.resolve(wrapperHook(hook));
    } else {
      var res = hook(data);
      if (isPromise(res)) {
        promise = Promise.resolve(res);
      }
      if (res === false) {
        return {
          then: function then() {} };

      }
    }
  }
  return promise || {
    then: function then(callback) {
      return callback(data);
    } };

}

function wrapperOptions(interceptor) {var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  ['success', 'fail', 'complete'].forEach(function (name) {
    if (Array.isArray(interceptor[name])) {
      var oldCallback = options[name];
      options[name] = function callbackInterceptor(res) {
        queue(interceptor[name], res).then(function (res) {
          /* eslint-disable no-mixed-operators */
          return isFn(oldCallback) && oldCallback(res) || res;
        });
      };
    }
  });
  return options;
}

function wrapperReturnValue(method, returnValue) {
  var returnValueHooks = [];
  if (Array.isArray(globalInterceptors.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(globalInterceptors.returnValue));
  }
  var interceptor = scopedInterceptors[method];
  if (interceptor && Array.isArray(interceptor.returnValue)) {
    returnValueHooks.push.apply(returnValueHooks, _toConsumableArray(interceptor.returnValue));
  }
  returnValueHooks.forEach(function (hook) {
    returnValue = hook(returnValue) || returnValue;
  });
  return returnValue;
}

function getApiInterceptorHooks(method) {
  var interceptor = Object.create(null);
  Object.keys(globalInterceptors).forEach(function (hook) {
    if (hook !== 'returnValue') {
      interceptor[hook] = globalInterceptors[hook].slice();
    }
  });
  var scopedInterceptor = scopedInterceptors[method];
  if (scopedInterceptor) {
    Object.keys(scopedInterceptor).forEach(function (hook) {
      if (hook !== 'returnValue') {
        interceptor[hook] = (interceptor[hook] || []).concat(scopedInterceptor[hook]);
      }
    });
  }
  return interceptor;
}

function invokeApi(method, api, options) {for (var _len = arguments.length, params = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {params[_key - 3] = arguments[_key];}
  var interceptor = getApiInterceptorHooks(method);
  if (interceptor && Object.keys(interceptor).length) {
    if (Array.isArray(interceptor.invoke)) {
      var res = queue(interceptor.invoke, options);
      return res.then(function (options) {
        return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
      });
    } else {
      return api.apply(void 0, [wrapperOptions(interceptor, options)].concat(params));
    }
  }
  return api.apply(void 0, [options].concat(params));
}

var promiseInterceptor = {
  returnValue: function returnValue(res) {
    if (!isPromise(res)) {
      return res;
    }
    return new Promise(function (resolve, reject) {
      res.then(function (res) {
        if (res[0]) {
          reject(res[0]);
        } else {
          resolve(res[1]);
        }
      });
    });
  } };


var SYNC_API_RE =
/^\$|Window$|WindowStyle$|sendNativeEvent|restoreGlobal|getCurrentSubNVue|getMenuButtonBoundingClientRect|^report|interceptors|Interceptor$|getSubNVueById|requireNativePlugin|upx2px|hideKeyboard|canIUse|^create|Sync$|Manager$|base64ToArrayBuffer|arrayBufferToBase64|getLocale|setLocale/;

var CONTEXT_API_RE = /^create|Manager$/;

// Context例外情况
var CONTEXT_API_RE_EXC = ['createBLEConnection'];

// 同步例外情况
var ASYNC_API = ['createBLEConnection'];

var CALLBACK_API_RE = /^on|^off/;

function isContextApi(name) {
  return CONTEXT_API_RE.test(name) && CONTEXT_API_RE_EXC.indexOf(name) === -1;
}
function isSyncApi(name) {
  return SYNC_API_RE.test(name) && ASYNC_API.indexOf(name) === -1;
}

function isCallbackApi(name) {
  return CALLBACK_API_RE.test(name) && name !== 'onPush';
}

function handlePromise(promise) {
  return promise.then(function (data) {
    return [null, data];
  }).
  catch(function (err) {return [err];});
}

function shouldPromise(name) {
  if (
  isContextApi(name) ||
  isSyncApi(name) ||
  isCallbackApi(name))
  {
    return false;
  }
  return true;
}

/* eslint-disable no-extend-native */
if (!Promise.prototype.finally) {
  Promise.prototype.finally = function (callback) {
    var promise = this.constructor;
    return this.then(
    function (value) {return promise.resolve(callback()).then(function () {return value;});},
    function (reason) {return promise.resolve(callback()).then(function () {
        throw reason;
      });});

  };
}

function promisify(name, api) {
  if (!shouldPromise(name)) {
    return api;
  }
  return function promiseApi() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};for (var _len2 = arguments.length, params = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {params[_key2 - 1] = arguments[_key2];}
    if (isFn(options.success) || isFn(options.fail) || isFn(options.complete)) {
      return wrapperReturnValue(name, invokeApi.apply(void 0, [name, api, options].concat(params)));
    }
    return wrapperReturnValue(name, handlePromise(new Promise(function (resolve, reject) {
      invokeApi.apply(void 0, [name, api, Object.assign({}, options, {
        success: resolve,
        fail: reject })].concat(
      params));
    })));
  };
}

var EPS = 1e-4;
var BASE_DEVICE_WIDTH = 750;
var isIOS = false;
var deviceWidth = 0;
var deviceDPR = 0;

function checkDeviceWidth() {var _wx$getSystemInfoSync =




  wx.getSystemInfoSync(),platform = _wx$getSystemInfoSync.platform,pixelRatio = _wx$getSystemInfoSync.pixelRatio,windowWidth = _wx$getSystemInfoSync.windowWidth; // uni=>wx runtime 编译目标是 uni 对象，内部不允许直接使用 uni

  deviceWidth = windowWidth;
  deviceDPR = pixelRatio;
  isIOS = platform === 'ios';
}

function upx2px(number, newDeviceWidth) {
  if (deviceWidth === 0) {
    checkDeviceWidth();
  }

  number = Number(number);
  if (number === 0) {
    return 0;
  }
  var result = number / BASE_DEVICE_WIDTH * (newDeviceWidth || deviceWidth);
  if (result < 0) {
    result = -result;
  }
  result = Math.floor(result + EPS);
  if (result === 0) {
    if (deviceDPR === 1 || !isIOS) {
      result = 1;
    } else {
      result = 0.5;
    }
  }
  return number < 0 ? -result : result;
}

function getLocale() {
  // 优先使用 $locale
  var app = getApp({
    allowDefault: true });

  if (app && app.$vm) {
    return app.$vm.$locale;
  }
  return wx.getSystemInfoSync().language || 'zh-Hans';
}

function setLocale(locale) {
  var app = getApp();
  if (!app) {
    return false;
  }
  var oldLocale = app.$vm.$locale;
  if (oldLocale !== locale) {
    app.$vm.$locale = locale;
    onLocaleChangeCallbacks.forEach(function (fn) {return fn({
        locale: locale });});

    return true;
  }
  return false;
}

var onLocaleChangeCallbacks = [];
function onLocaleChange(fn) {
  if (onLocaleChangeCallbacks.indexOf(fn) === -1) {
    onLocaleChangeCallbacks.push(fn);
  }
}

var interceptors = {
  promiseInterceptor: promiseInterceptor };


var baseApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  upx2px: upx2px,
  getLocale: getLocale,
  setLocale: setLocale,
  onLocaleChange: onLocaleChange,
  addInterceptor: addInterceptor,
  removeInterceptor: removeInterceptor,
  interceptors: interceptors });


function findExistsPageIndex(url) {
  var pages = getCurrentPages();
  var len = pages.length;
  while (len--) {
    var page = pages[len];
    if (page.$page && page.$page.fullPath === url) {
      return len;
    }
  }
  return -1;
}

var redirectTo = {
  name: function name(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.delta) {
      return 'navigateBack';
    }
    return 'redirectTo';
  },
  args: function args(fromArgs) {
    if (fromArgs.exists === 'back' && fromArgs.url) {
      var existsPageIndex = findExistsPageIndex(fromArgs.url);
      if (existsPageIndex !== -1) {
        var delta = getCurrentPages().length - 1 - existsPageIndex;
        if (delta > 0) {
          fromArgs.delta = delta;
        }
      }
    }
  } };


var previewImage = {
  args: function args(fromArgs) {
    var currentIndex = parseInt(fromArgs.current);
    if (isNaN(currentIndex)) {
      return;
    }
    var urls = fromArgs.urls;
    if (!Array.isArray(urls)) {
      return;
    }
    var len = urls.length;
    if (!len) {
      return;
    }
    if (currentIndex < 0) {
      currentIndex = 0;
    } else if (currentIndex >= len) {
      currentIndex = len - 1;
    }
    if (currentIndex > 0) {
      fromArgs.current = urls[currentIndex];
      fromArgs.urls = urls.filter(
      function (item, index) {return index < currentIndex ? item !== urls[currentIndex] : true;});

    } else {
      fromArgs.current = urls[0];
    }
    return {
      indicator: false,
      loop: false };

  } };


var UUID_KEY = '__DC_STAT_UUID';
var deviceId;
function addUuid(result) {
  deviceId = deviceId || wx.getStorageSync(UUID_KEY);
  if (!deviceId) {
    deviceId = Date.now() + '' + Math.floor(Math.random() * 1e7);
    wx.setStorage({
      key: UUID_KEY,
      data: deviceId });

  }
  result.deviceId = deviceId;
}

function addSafeAreaInsets(result) {
  if (result.safeArea) {
    var safeArea = result.safeArea;
    result.safeAreaInsets = {
      top: safeArea.top,
      left: safeArea.left,
      right: result.windowWidth - safeArea.right,
      bottom: result.windowHeight - safeArea.bottom };

  }
}

var getSystemInfo = {
  returnValue: function returnValue(result) {
    addUuid(result);
    addSafeAreaInsets(result);
  } };


// import navigateTo from 'uni-helpers/navigate-to'

var protocols = {
  redirectTo: redirectTo,
  // navigateTo,  // 由于在微信开发者工具的页面参数，会显示__id__参数，因此暂时关闭mp-weixin对于navigateTo的AOP
  previewImage: previewImage,
  getSystemInfo: getSystemInfo,
  getSystemInfoSync: getSystemInfo };

var todos = [
'vibrate',
'preloadPage',
'unPreloadPage',
'loadSubPackage'];

var canIUses = [];

var CALLBACKS = ['success', 'fail', 'cancel', 'complete'];

function processCallback(methodName, method, returnValue) {
  return function (res) {
    return method(processReturnValue(methodName, res, returnValue));
  };
}

function processArgs(methodName, fromArgs) {var argsOption = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};var returnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};var keepFromArgs = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
  if (isPlainObject(fromArgs)) {// 一般 api 的参数解析
    var toArgs = keepFromArgs === true ? fromArgs : {}; // returnValue 为 false 时，说明是格式化返回值，直接在返回值对象上修改赋值
    if (isFn(argsOption)) {
      argsOption = argsOption(fromArgs, toArgs) || {};
    }
    for (var key in fromArgs) {
      if (hasOwn(argsOption, key)) {
        var keyOption = argsOption[key];
        if (isFn(keyOption)) {
          keyOption = keyOption(fromArgs[key], fromArgs, toArgs);
        }
        if (!keyOption) {// 不支持的参数
          console.warn("The '".concat(methodName, "' method of platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support option '").concat(key, "'"));
        } else if (isStr(keyOption)) {// 重写参数 key
          toArgs[keyOption] = fromArgs[key];
        } else if (isPlainObject(keyOption)) {// {name:newName,value:value}可重新指定参数 key:value
          toArgs[keyOption.name ? keyOption.name : key] = keyOption.value;
        }
      } else if (CALLBACKS.indexOf(key) !== -1) {
        if (isFn(fromArgs[key])) {
          toArgs[key] = processCallback(methodName, fromArgs[key], returnValue);
        }
      } else {
        if (!keepFromArgs) {
          toArgs[key] = fromArgs[key];
        }
      }
    }
    return toArgs;
  } else if (isFn(fromArgs)) {
    fromArgs = processCallback(methodName, fromArgs, returnValue);
  }
  return fromArgs;
}

function processReturnValue(methodName, res, returnValue) {var keepReturnValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  if (isFn(protocols.returnValue)) {// 处理通用 returnValue
    res = protocols.returnValue(methodName, res);
  }
  return processArgs(methodName, res, returnValue, {}, keepReturnValue);
}

function wrapper(methodName, method) {
  if (hasOwn(protocols, methodName)) {
    var protocol = protocols[methodName];
    if (!protocol) {// 暂不支持的 api
      return function () {
        console.error("Platform '\u5FAE\u4FE1\u5C0F\u7A0B\u5E8F' does not support '".concat(methodName, "'."));
      };
    }
    return function (arg1, arg2) {// 目前 api 最多两个参数
      var options = protocol;
      if (isFn(protocol)) {
        options = protocol(arg1);
      }

      arg1 = processArgs(methodName, arg1, options.args, options.returnValue);

      var args = [arg1];
      if (typeof arg2 !== 'undefined') {
        args.push(arg2);
      }
      if (isFn(options.name)) {
        methodName = options.name(arg1);
      } else if (isStr(options.name)) {
        methodName = options.name;
      }
      var returnValue = wx[methodName].apply(wx, args);
      if (isSyncApi(methodName)) {// 同步 api
        return processReturnValue(methodName, returnValue, options.returnValue, isContextApi(methodName));
      }
      return returnValue;
    };
  }
  return method;
}

var todoApis = Object.create(null);

var TODOS = [
'onTabBarMidButtonTap',
'subscribePush',
'unsubscribePush',
'onPush',
'offPush',
'share'];


function createTodoApi(name) {
  return function todoApi(_ref)


  {var fail = _ref.fail,complete = _ref.complete;
    var res = {
      errMsg: "".concat(name, ":fail method '").concat(name, "' not supported") };

    isFn(fail) && fail(res);
    isFn(complete) && complete(res);
  };
}

TODOS.forEach(function (name) {
  todoApis[name] = createTodoApi(name);
});

var providers = {
  oauth: ['weixin'],
  share: ['weixin'],
  payment: ['wxpay'],
  push: ['weixin'] };


function getProvider(_ref2)




{var service = _ref2.service,success = _ref2.success,fail = _ref2.fail,complete = _ref2.complete;
  var res = false;
  if (providers[service]) {
    res = {
      errMsg: 'getProvider:ok',
      service: service,
      provider: providers[service] };

    isFn(success) && success(res);
  } else {
    res = {
      errMsg: 'getProvider:fail service not found' };

    isFn(fail) && fail(res);
  }
  isFn(complete) && complete(res);
}

var extraApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  getProvider: getProvider });


var getEmitter = function () {
  var Emitter;
  return function getUniEmitter() {
    if (!Emitter) {
      Emitter = new _vue.default();
    }
    return Emitter;
  };
}();

function apply(ctx, method, args) {
  return ctx[method].apply(ctx, args);
}

function $on() {
  return apply(getEmitter(), '$on', Array.prototype.slice.call(arguments));
}
function $off() {
  return apply(getEmitter(), '$off', Array.prototype.slice.call(arguments));
}
function $once() {
  return apply(getEmitter(), '$once', Array.prototype.slice.call(arguments));
}
function $emit() {
  return apply(getEmitter(), '$emit', Array.prototype.slice.call(arguments));
}

var eventApi = /*#__PURE__*/Object.freeze({
  __proto__: null,
  $on: $on,
  $off: $off,
  $once: $once,
  $emit: $emit });


var api = /*#__PURE__*/Object.freeze({
  __proto__: null });


var MPPage = Page;
var MPComponent = Component;

var customizeRE = /:/g;

var customize = cached(function (str) {
  return camelize(str.replace(customizeRE, '-'));
});

function initTriggerEvent(mpInstance) {
  {
    if (!wx.canIUse || !wx.canIUse('nextTick')) {
      return;
    }
  }
  var oldTriggerEvent = mpInstance.triggerEvent;
  mpInstance.triggerEvent = function (event) {for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {args[_key3 - 1] = arguments[_key3];}
    return oldTriggerEvent.apply(mpInstance, [customize(event)].concat(args));
  };
}

function initHook(name, options, isComponent) {
  var oldHook = options[name];
  if (!oldHook) {
    options[name] = function () {
      initTriggerEvent(this);
    };
  } else {
    options[name] = function () {
      initTriggerEvent(this);for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {args[_key4] = arguments[_key4];}
      return oldHook.apply(this, args);
    };
  }
}
if (!MPPage.__$wrappered) {
  MPPage.__$wrappered = true;
  Page = function Page() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('onLoad', options);
    return MPPage(options);
  };
  Page.after = MPPage.after;

  Component = function Component() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    initHook('created', options);
    return MPComponent(options);
  };
}

var PAGE_EVENT_HOOKS = [
'onPullDownRefresh',
'onReachBottom',
'onAddToFavorites',
'onShareTimeline',
'onShareAppMessage',
'onPageScroll',
'onResize',
'onTabItemTap'];


function initMocks(vm, mocks) {
  var mpInstance = vm.$mp[vm.mpType];
  mocks.forEach(function (mock) {
    if (hasOwn(mpInstance, mock)) {
      vm[mock] = mpInstance[mock];
    }
  });
}

function hasHook(hook, vueOptions) {
  if (!vueOptions) {
    return true;
  }

  if (_vue.default.options && Array.isArray(_vue.default.options[hook])) {
    return true;
  }

  vueOptions = vueOptions.default || vueOptions;

  if (isFn(vueOptions)) {
    if (isFn(vueOptions.extendOptions[hook])) {
      return true;
    }
    if (vueOptions.super &&
    vueOptions.super.options &&
    Array.isArray(vueOptions.super.options[hook])) {
      return true;
    }
    return false;
  }

  if (isFn(vueOptions[hook])) {
    return true;
  }
  var mixins = vueOptions.mixins;
  if (Array.isArray(mixins)) {
    return !!mixins.find(function (mixin) {return hasHook(hook, mixin);});
  }
}

function initHooks(mpOptions, hooks, vueOptions) {
  hooks.forEach(function (hook) {
    if (hasHook(hook, vueOptions)) {
      mpOptions[hook] = function (args) {
        return this.$vm && this.$vm.__call_hook(hook, args);
      };
    }
  });
}

function initVueComponent(Vue, vueOptions) {
  vueOptions = vueOptions.default || vueOptions;
  var VueComponent;
  if (isFn(vueOptions)) {
    VueComponent = vueOptions;
  } else {
    VueComponent = Vue.extend(vueOptions);
  }
  vueOptions = VueComponent.options;
  return [VueComponent, vueOptions];
}

function initSlots(vm, vueSlots) {
  if (Array.isArray(vueSlots) && vueSlots.length) {
    var $slots = Object.create(null);
    vueSlots.forEach(function (slotName) {
      $slots[slotName] = true;
    });
    vm.$scopedSlots = vm.$slots = $slots;
  }
}

function initVueIds(vueIds, mpInstance) {
  vueIds = (vueIds || '').split(',');
  var len = vueIds.length;

  if (len === 1) {
    mpInstance._$vueId = vueIds[0];
  } else if (len === 2) {
    mpInstance._$vueId = vueIds[0];
    mpInstance._$vuePid = vueIds[1];
  }
}

function initData(vueOptions, context) {
  var data = vueOptions.data || {};
  var methods = vueOptions.methods || {};

  if (typeof data === 'function') {
    try {
      data = data.call(context); // 支持 Vue.prototype 上挂的数据
    } catch (e) {
      if (Object({"NODE_ENV":"development","VUE_APP_NAME":"超级英雄电影预告","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.warn('根据 Vue 的 data 函数初始化小程序 data 失败，请尽量确保 data 函数中不访问 vm 对象，否则可能影响首次数据渲染速度。', data);
      }
    }
  } else {
    try {
      // 对 data 格式化
      data = JSON.parse(JSON.stringify(data));
    } catch (e) {}
  }

  if (!isPlainObject(data)) {
    data = {};
  }

  Object.keys(methods).forEach(function (methodName) {
    if (context.__lifecycle_hooks__.indexOf(methodName) === -1 && !hasOwn(data, methodName)) {
      data[methodName] = methods[methodName];
    }
  });

  return data;
}

var PROP_TYPES = [String, Number, Boolean, Object, Array, null];

function createObserver(name) {
  return function observer(newVal, oldVal) {
    if (this.$vm) {
      this.$vm[name] = newVal; // 为了触发其他非 render watcher
    }
  };
}

function initBehaviors(vueOptions, initBehavior) {
  var vueBehaviors = vueOptions.behaviors;
  var vueExtends = vueOptions.extends;
  var vueMixins = vueOptions.mixins;

  var vueProps = vueOptions.props;

  if (!vueProps) {
    vueOptions.props = vueProps = [];
  }

  var behaviors = [];
  if (Array.isArray(vueBehaviors)) {
    vueBehaviors.forEach(function (behavior) {
      behaviors.push(behavior.replace('uni://', "wx".concat("://")));
      if (behavior === 'uni://form-field') {
        if (Array.isArray(vueProps)) {
          vueProps.push('name');
          vueProps.push('value');
        } else {
          vueProps.name = {
            type: String,
            default: '' };

          vueProps.value = {
            type: [String, Number, Boolean, Array, Object, Date],
            default: '' };

        }
      }
    });
  }
  if (isPlainObject(vueExtends) && vueExtends.props) {
    behaviors.push(
    initBehavior({
      properties: initProperties(vueExtends.props, true) }));


  }
  if (Array.isArray(vueMixins)) {
    vueMixins.forEach(function (vueMixin) {
      if (isPlainObject(vueMixin) && vueMixin.props) {
        behaviors.push(
        initBehavior({
          properties: initProperties(vueMixin.props, true) }));


      }
    });
  }
  return behaviors;
}

function parsePropType(key, type, defaultValue, file) {
  // [String]=>String
  if (Array.isArray(type) && type.length === 1) {
    return type[0];
  }
  return type;
}

function initProperties(props) {var isBehavior = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;var file = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
  var properties = {};
  if (!isBehavior) {
    properties.vueId = {
      type: String,
      value: '' };

    // 用于字节跳动小程序模拟抽象节点
    properties.generic = {
      type: Object,
      value: null };

    // scopedSlotsCompiler auto
    properties.scopedSlotsCompiler = {
      type: String,
      value: '' };

    properties.vueSlots = { // 小程序不能直接定义 $slots 的 props，所以通过 vueSlots 转换到 $slots
      type: null,
      value: [],
      observer: function observer(newVal, oldVal) {
        var $slots = Object.create(null);
        newVal.forEach(function (slotName) {
          $slots[slotName] = true;
        });
        this.setData({
          $slots: $slots });

      } };

  }
  if (Array.isArray(props)) {// ['title']
    props.forEach(function (key) {
      properties[key] = {
        type: null,
        observer: createObserver(key) };

    });
  } else if (isPlainObject(props)) {// {title:{type:String,default:''},content:String}
    Object.keys(props).forEach(function (key) {
      var opts = props[key];
      if (isPlainObject(opts)) {// title:{type:String,default:''}
        var value = opts.default;
        if (isFn(value)) {
          value = value();
        }

        opts.type = parsePropType(key, opts.type);

        properties[key] = {
          type: PROP_TYPES.indexOf(opts.type) !== -1 ? opts.type : null,
          value: value,
          observer: createObserver(key) };

      } else {// content:String
        var type = parsePropType(key, opts);
        properties[key] = {
          type: PROP_TYPES.indexOf(type) !== -1 ? type : null,
          observer: createObserver(key) };

      }
    });
  }
  return properties;
}

function wrapper$1(event) {
  // TODO 又得兼容 mpvue 的 mp 对象
  try {
    event.mp = JSON.parse(JSON.stringify(event));
  } catch (e) {}

  event.stopPropagation = noop;
  event.preventDefault = noop;

  event.target = event.target || {};

  if (!hasOwn(event, 'detail')) {
    event.detail = {};
  }

  if (hasOwn(event, 'markerId')) {
    event.detail = typeof event.detail === 'object' ? event.detail : {};
    event.detail.markerId = event.markerId;
  }

  if (isPlainObject(event.detail)) {
    event.target = Object.assign({}, event.target, event.detail);
  }

  return event;
}

function getExtraValue(vm, dataPathsArray) {
  var context = vm;
  dataPathsArray.forEach(function (dataPathArray) {
    var dataPath = dataPathArray[0];
    var value = dataPathArray[2];
    if (dataPath || typeof value !== 'undefined') {// ['','',index,'disable']
      var propPath = dataPathArray[1];
      var valuePath = dataPathArray[3];

      var vFor;
      if (Number.isInteger(dataPath)) {
        vFor = dataPath;
      } else if (!dataPath) {
        vFor = context;
      } else if (typeof dataPath === 'string' && dataPath) {
        if (dataPath.indexOf('#s#') === 0) {
          vFor = dataPath.substr(3);
        } else {
          vFor = vm.__get_value(dataPath, context);
        }
      }

      if (Number.isInteger(vFor)) {
        context = value;
      } else if (!propPath) {
        context = vFor[value];
      } else {
        if (Array.isArray(vFor)) {
          context = vFor.find(function (vForItem) {
            return vm.__get_value(propPath, vForItem) === value;
          });
        } else if (isPlainObject(vFor)) {
          context = Object.keys(vFor).find(function (vForKey) {
            return vm.__get_value(propPath, vFor[vForKey]) === value;
          });
        } else {
          console.error('v-for 暂不支持循环数据：', vFor);
        }
      }

      if (valuePath) {
        context = vm.__get_value(valuePath, context);
      }
    }
  });
  return context;
}

function processEventExtra(vm, extra, event) {
  var extraObj = {};

  if (Array.isArray(extra) && extra.length) {
    /**
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *[
                                              *    ['data.items', 'data.id', item.data.id],
                                              *    ['metas', 'id', meta.id]
                                              *],
                                              *'test'
                                              */
    extra.forEach(function (dataPath, index) {
      if (typeof dataPath === 'string') {
        if (!dataPath) {// model,prop.sync
          extraObj['$' + index] = vm;
        } else {
          if (dataPath === '$event') {// $event
            extraObj['$' + index] = event;
          } else if (dataPath === 'arguments') {
            if (event.detail && event.detail.__args__) {
              extraObj['$' + index] = event.detail.__args__;
            } else {
              extraObj['$' + index] = [event];
            }
          } else if (dataPath.indexOf('$event.') === 0) {// $event.target.value
            extraObj['$' + index] = vm.__get_value(dataPath.replace('$event.', ''), event);
          } else {
            extraObj['$' + index] = vm.__get_value(dataPath);
          }
        }
      } else {
        extraObj['$' + index] = getExtraValue(vm, dataPath);
      }
    });
  }

  return extraObj;
}

function getObjByArray(arr) {
  var obj = {};
  for (var i = 1; i < arr.length; i++) {
    var element = arr[i];
    obj[element[0]] = element[1];
  }
  return obj;
}

function processEventArgs(vm, event) {var args = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];var extra = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];var isCustom = arguments.length > 4 ? arguments[4] : undefined;var methodName = arguments.length > 5 ? arguments[5] : undefined;
  var isCustomMPEvent = false; // wxcomponent 组件，传递原始 event 对象
  if (isCustom) {// 自定义事件
    isCustomMPEvent = event.currentTarget &&
    event.currentTarget.dataset &&
    event.currentTarget.dataset.comType === 'wx';
    if (!args.length) {// 无参数，直接传入 event 或 detail 数组
      if (isCustomMPEvent) {
        return [event];
      }
      return event.detail.__args__ || event.detail;
    }
  }

  var extraObj = processEventExtra(vm, extra, event);

  var ret = [];
  args.forEach(function (arg) {
    if (arg === '$event') {
      if (methodName === '__set_model' && !isCustom) {// input v-model value
        ret.push(event.target.value);
      } else {
        if (isCustom && !isCustomMPEvent) {
          ret.push(event.detail.__args__[0]);
        } else {// wxcomponent 组件或内置组件
          ret.push(event);
        }
      }
    } else {
      if (Array.isArray(arg) && arg[0] === 'o') {
        ret.push(getObjByArray(arg));
      } else if (typeof arg === 'string' && hasOwn(extraObj, arg)) {
        ret.push(extraObj[arg]);
      } else {
        ret.push(arg);
      }
    }
  });

  return ret;
}

var ONCE = '~';
var CUSTOM = '^';

function isMatchEventType(eventType, optType) {
  return eventType === optType ||

  optType === 'regionchange' && (

  eventType === 'begin' ||
  eventType === 'end');


}

function getContextVm(vm) {
  var $parent = vm.$parent;
  // 父组件是 scoped slots 或者其他自定义组件时继续查找
  while ($parent && $parent.$parent && ($parent.$options.generic || $parent.$parent.$options.generic || $parent.$scope._$vuePid)) {
    $parent = $parent.$parent;
  }
  return $parent && $parent.$parent;
}

function handleEvent(event) {var _this = this;
  event = wrapper$1(event);

  // [['tap',[['handle',[1,2,a]],['handle1',[1,2,a]]]]]
  var dataset = (event.currentTarget || event.target).dataset;
  if (!dataset) {
    return console.warn('事件信息不存在');
  }
  var eventOpts = dataset.eventOpts || dataset['event-opts']; // 支付宝 web-view 组件 dataset 非驼峰
  if (!eventOpts) {
    return console.warn('事件信息不存在');
  }

  // [['handle',[1,2,a]],['handle1',[1,2,a]]]
  var eventType = event.type;

  var ret = [];

  eventOpts.forEach(function (eventOpt) {
    var type = eventOpt[0];
    var eventsArray = eventOpt[1];

    var isCustom = type.charAt(0) === CUSTOM;
    type = isCustom ? type.slice(1) : type;
    var isOnce = type.charAt(0) === ONCE;
    type = isOnce ? type.slice(1) : type;

    if (eventsArray && isMatchEventType(eventType, type)) {
      eventsArray.forEach(function (eventArray) {
        var methodName = eventArray[0];
        if (methodName) {
          var handlerCtx = _this.$vm;
          if (handlerCtx.$options.generic) {// mp-weixin,mp-toutiao 抽象节点模拟 scoped slots
            handlerCtx = getContextVm(handlerCtx) || handlerCtx;
          }
          if (methodName === '$emit') {
            handlerCtx.$emit.apply(handlerCtx,
            processEventArgs(
            _this.$vm,
            event,
            eventArray[1],
            eventArray[2],
            isCustom,
            methodName));

            return;
          }
          var handler = handlerCtx[methodName];
          if (!isFn(handler)) {
            throw new Error(" _vm.".concat(methodName, " is not a function"));
          }
          if (isOnce) {
            if (handler.once) {
              return;
            }
            handler.once = true;
          }
          var params = processEventArgs(
          _this.$vm,
          event,
          eventArray[1],
          eventArray[2],
          isCustom,
          methodName);

          params = Array.isArray(params) ? params : [];
          // 参数尾部增加原始事件对象用于复杂表达式内获取额外数据
          if (/=\s*\S+\.eventParams\s*\|\|\s*\S+\[['"]event-params['"]\]/.test(handler.toString())) {
            // eslint-disable-next-line no-sparse-arrays
            params = params.concat([,,,,,,,,,, event]);
          }
          ret.push(handler.apply(handlerCtx, params));
        }
      });
    }
  });

  if (
  eventType === 'input' &&
  ret.length === 1 &&
  typeof ret[0] !== 'undefined')
  {
    return ret[0];
  }
}

var locale;

{
  locale = wx.getSystemInfoSync().language;
}

var i18n = (0, _uniI18n.initVueI18n)(
locale,
{});

var t = i18n.t;
var i18nMixin = i18n.mixin = {
  beforeCreate: function beforeCreate() {var _this2 = this;
    var unwatch = i18n.i18n.watchLocale(function () {
      _this2.$forceUpdate();
    });
    this.$once('hook:beforeDestroy', function () {
      unwatch();
    });
  },
  methods: {
    $$t: function $$t(key, values) {
      return t(key, values);
    } } };


var setLocale$1 = i18n.setLocale;
var getLocale$1 = i18n.getLocale;

function initAppLocale(Vue, appVm, locale) {
  var state = Vue.observable({
    locale: locale || i18n.getLocale() });

  var localeWatchers = [];
  appVm.$watchLocale = function (fn) {
    localeWatchers.push(fn);
  };
  Object.defineProperty(appVm, '$locale', {
    get: function get() {
      return state.locale;
    },
    set: function set(v) {
      state.locale = v;
      localeWatchers.forEach(function (watch) {return watch(v);});
    } });

}

var eventChannels = {};

var eventChannelStack = [];

function getEventChannel(id) {
  if (id) {
    var eventChannel = eventChannels[id];
    delete eventChannels[id];
    return eventChannel;
  }
  return eventChannelStack.shift();
}

var hooks = [
'onShow',
'onHide',
'onError',
'onPageNotFound',
'onThemeChange',
'onUnhandledRejection'];


function initEventChannel() {
  _vue.default.prototype.getOpenerEventChannel = function () {
    // 微信小程序使用自身getOpenerEventChannel
    {
      return this.$scope.getOpenerEventChannel();
    }
  };
  var callHook = _vue.default.prototype.__call_hook;
  _vue.default.prototype.__call_hook = function (hook, args) {
    if (hook === 'onLoad' && args && args.__id__) {
      this.__eventChannel__ = getEventChannel(args.__id__);
      delete args.__id__;
    }
    return callHook.call(this, hook, args);
  };
}

function initScopedSlotsParams() {
  var center = {};
  var parents = {};

  _vue.default.prototype.$hasScopedSlotsParams = function (vueId) {
    var has = center[vueId];
    if (!has) {
      parents[vueId] = this;
      this.$on('hook:destory', function () {
        delete parents[vueId];
      });
    }
    return has;
  };

  _vue.default.prototype.$getScopedSlotsParams = function (vueId, name, key) {
    var data = center[vueId];
    if (data) {
      var object = data[name] || {};
      return key ? object[key] : object;
    } else {
      parents[vueId] = this;
      this.$on('hook:destory', function () {
        delete parents[vueId];
      });
    }
  };

  _vue.default.prototype.$setScopedSlotsParams = function (name, value) {
    var vueIds = this.$options.propsData.vueId;
    if (vueIds) {
      var vueId = vueIds.split(',')[0];
      var object = center[vueId] = center[vueId] || {};
      object[name] = value;
      if (parents[vueId]) {
        parents[vueId].$forceUpdate();
      }
    }
  };

  _vue.default.mixin({
    destroyed: function destroyed() {
      var propsData = this.$options.propsData;
      var vueId = propsData && propsData.vueId;
      if (vueId) {
        delete center[vueId];
        delete parents[vueId];
      }
    } });

}

function parseBaseApp(vm, _ref3)


{var mocks = _ref3.mocks,initRefs = _ref3.initRefs;
  initEventChannel();
  {
    initScopedSlotsParams();
  }
  if (vm.$options.store) {
    _vue.default.prototype.$store = vm.$options.store;
  }
  uniIdMixin(_vue.default);

  _vue.default.prototype.mpHost = "mp-weixin";

  _vue.default.mixin({
    beforeCreate: function beforeCreate() {
      if (!this.$options.mpType) {
        return;
      }

      this.mpType = this.$options.mpType;

      this.$mp = _defineProperty({
        data: {} },
      this.mpType, this.$options.mpInstance);


      this.$scope = this.$options.mpInstance;

      delete this.$options.mpType;
      delete this.$options.mpInstance;
      if (this.mpType === 'page' && typeof getApp === 'function') {// hack vue-i18n
        var app = getApp();
        if (app.$vm && app.$vm.$i18n) {
          this._i18n = app.$vm.$i18n;
        }
      }
      if (this.mpType !== 'app') {
        initRefs(this);
        initMocks(this, mocks);
      }
    } });


  var appOptions = {
    onLaunch: function onLaunch(args) {
      if (this.$vm) {// 已经初始化过了，主要是为了百度，百度 onShow 在 onLaunch 之前
        return;
      }
      {
        if (wx.canIUse && !wx.canIUse('nextTick')) {// 事实 上2.2.3 即可，简单使用 2.3.0 的 nextTick 判断
          console.error('当前微信基础库版本过低，请将 微信开发者工具-详情-项目设置-调试基础库版本 更换为`2.3.0`以上');
        }
      }

      this.$vm = vm;

      this.$vm.$mp = {
        app: this };


      this.$vm.$scope = this;
      // vm 上也挂载 globalData
      this.$vm.globalData = this.globalData;

      this.$vm._isMounted = true;
      this.$vm.__call_hook('mounted', args);

      this.$vm.__call_hook('onLaunch', args);
    } };


  // 兼容旧版本 globalData
  appOptions.globalData = vm.$options.globalData || {};
  // 将 methods 中的方法挂在 getApp() 中
  var methods = vm.$options.methods;
  if (methods) {
    Object.keys(methods).forEach(function (name) {
      appOptions[name] = methods[name];
    });
  }

  initAppLocale(_vue.default, vm, wx.getSystemInfoSync().language || 'zh-Hans');

  initHooks(appOptions, hooks);

  return appOptions;
}

var mocks = ['__route__', '__wxExparserNodeId__', '__wxWebviewId__'];

function findVmByVueId(vm, vuePid) {
  var $children = vm.$children;
  // 优先查找直属(反向查找:https://github.com/dcloudio/uni-app/issues/1200)
  for (var i = $children.length - 1; i >= 0; i--) {
    var childVm = $children[i];
    if (childVm.$scope._$vueId === vuePid) {
      return childVm;
    }
  }
  // 反向递归查找
  var parentVm;
  for (var _i = $children.length - 1; _i >= 0; _i--) {
    parentVm = findVmByVueId($children[_i], vuePid);
    if (parentVm) {
      return parentVm;
    }
  }
}

function initBehavior(options) {
  return Behavior(options);
}

function isPage() {
  return !!this.route;
}

function initRelation(detail) {
  this.triggerEvent('__l', detail);
}

function selectAllComponents(mpInstance, selector, $refs) {
  var components = mpInstance.selectAllComponents(selector);
  components.forEach(function (component) {
    var ref = component.dataset.ref;
    $refs[ref] = component.$vm || component;
    {
      if (component.dataset.vueGeneric === 'scoped') {
        component.selectAllComponents('.scoped-ref').forEach(function (scopedComponent) {
          selectAllComponents(scopedComponent, selector, $refs);
        });
      }
    }
  });
}

function initRefs(vm) {
  var mpInstance = vm.$scope;
  Object.defineProperty(vm, '$refs', {
    get: function get() {
      var $refs = {};
      selectAllComponents(mpInstance, '.vue-ref', $refs);
      // TODO 暂不考虑 for 中的 scoped
      var forComponents = mpInstance.selectAllComponents('.vue-ref-in-for');
      forComponents.forEach(function (component) {
        var ref = component.dataset.ref;
        if (!$refs[ref]) {
          $refs[ref] = [];
        }
        $refs[ref].push(component.$vm || component);
      });
      return $refs;
    } });

}

function handleLink(event) {var _ref4 =



  event.detail || event.value,vuePid = _ref4.vuePid,vueOptions = _ref4.vueOptions; // detail 是微信,value 是百度(dipatch)

  var parentVm;

  if (vuePid) {
    parentVm = findVmByVueId(this.$vm, vuePid);
  }

  if (!parentVm) {
    parentVm = this.$vm;
  }

  vueOptions.parent = parentVm;
}

function parseApp(vm) {
  return parseBaseApp(vm, {
    mocks: mocks,
    initRefs: initRefs });

}

function createApp(vm) {
  App(parseApp(vm));
  return vm;
}

var encodeReserveRE = /[!'()*]/g;
var encodeReserveReplacer = function encodeReserveReplacer(c) {return '%' + c.charCodeAt(0).toString(16);};
var commaRE = /%2C/g;

// fixed encodeURIComponent which is more conformant to RFC3986:
// - escapes [!'()*]
// - preserve commas
var encode = function encode(str) {return encodeURIComponent(str).
  replace(encodeReserveRE, encodeReserveReplacer).
  replace(commaRE, ',');};

function stringifyQuery(obj) {var encodeStr = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : encode;
  var res = obj ? Object.keys(obj).map(function (key) {
    var val = obj[key];

    if (val === undefined) {
      return '';
    }

    if (val === null) {
      return encodeStr(key);
    }

    if (Array.isArray(val)) {
      var result = [];
      val.forEach(function (val2) {
        if (val2 === undefined) {
          return;
        }
        if (val2 === null) {
          result.push(encodeStr(key));
        } else {
          result.push(encodeStr(key) + '=' + encodeStr(val2));
        }
      });
      return result.join('&');
    }

    return encodeStr(key) + '=' + encodeStr(val);
  }).filter(function (x) {return x.length > 0;}).join('&') : null;
  return res ? "?".concat(res) : '';
}

function parseBaseComponent(vueComponentOptions)


{var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},isPage = _ref5.isPage,initRelation = _ref5.initRelation;var _initVueComponent =
  initVueComponent(_vue.default, vueComponentOptions),_initVueComponent2 = _slicedToArray(_initVueComponent, 2),VueComponent = _initVueComponent2[0],vueOptions = _initVueComponent2[1];

  var options = _objectSpread({
    multipleSlots: true,
    addGlobalClass: true },
  vueOptions.options || {});


  {
    // 微信 multipleSlots 部分情况有 bug，导致内容顺序错乱 如 u-list，提供覆盖选项
    if (vueOptions['mp-weixin'] && vueOptions['mp-weixin'].options) {
      Object.assign(options, vueOptions['mp-weixin'].options);
    }
  }

  var componentOptions = {
    options: options,
    data: initData(vueOptions, _vue.default.prototype),
    behaviors: initBehaviors(vueOptions, initBehavior),
    properties: initProperties(vueOptions.props, false, vueOptions.__file),
    lifetimes: {
      attached: function attached() {
        var properties = this.properties;

        var options = {
          mpType: isPage.call(this) ? 'page' : 'component',
          mpInstance: this,
          propsData: properties };


        initVueIds(properties.vueId, this);

        // 处理父子关系
        initRelation.call(this, {
          vuePid: this._$vuePid,
          vueOptions: options });


        // 初始化 vue 实例
        this.$vm = new VueComponent(options);

        // 处理$slots,$scopedSlots（暂不支持动态变化$slots）
        initSlots(this.$vm, properties.vueSlots);

        // 触发首次 setData
        this.$vm.$mount();
      },
      ready: function ready() {
        // 当组件 props 默认值为 true，初始化时传入 false 会导致 created,ready 触发, 但 attached 不触发
        // https://developers.weixin.qq.com/community/develop/doc/00066ae2844cc0f8eb883e2a557800
        if (this.$vm) {
          this.$vm._isMounted = true;
          this.$vm.__call_hook('mounted');
          this.$vm.__call_hook('onReady');
        }
      },
      detached: function detached() {
        this.$vm && this.$vm.$destroy();
      } },

    pageLifetimes: {
      show: function show(args) {
        this.$vm && this.$vm.__call_hook('onPageShow', args);
      },
      hide: function hide() {
        this.$vm && this.$vm.__call_hook('onPageHide');
      },
      resize: function resize(size) {
        this.$vm && this.$vm.__call_hook('onPageResize', size);
      } },

    methods: {
      __l: handleLink,
      __e: handleEvent } };


  // externalClasses
  if (vueOptions.externalClasses) {
    componentOptions.externalClasses = vueOptions.externalClasses;
  }

  if (Array.isArray(vueOptions.wxsCallMethods)) {
    vueOptions.wxsCallMethods.forEach(function (callMethod) {
      componentOptions.methods[callMethod] = function (args) {
        return this.$vm[callMethod](args);
      };
    });
  }

  if (isPage) {
    return componentOptions;
  }
  return [componentOptions, VueComponent];
}

function parseComponent(vueComponentOptions) {
  return parseBaseComponent(vueComponentOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

var hooks$1 = [
'onShow',
'onHide',
'onUnload'];


hooks$1.push.apply(hooks$1, PAGE_EVENT_HOOKS);

function parseBasePage(vuePageOptions, _ref6)


{var isPage = _ref6.isPage,initRelation = _ref6.initRelation;
  var pageOptions = parseComponent(vuePageOptions);

  initHooks(pageOptions.methods, hooks$1, vuePageOptions);

  pageOptions.methods.onLoad = function (query) {
    this.options = query;
    var copyQuery = Object.assign({}, query);
    delete copyQuery.__id__;
    this.$page = {
      fullPath: '/' + (this.route || this.is) + stringifyQuery(copyQuery) };

    this.$vm.$mp.query = query; // 兼容 mpvue
    this.$vm.__call_hook('onLoad', query);
  };

  return pageOptions;
}

function parsePage(vuePageOptions) {
  return parseBasePage(vuePageOptions, {
    isPage: isPage,
    initRelation: initRelation });

}

function createPage(vuePageOptions) {
  {
    return Component(parsePage(vuePageOptions));
  }
}

function createComponent(vueOptions) {
  {
    return Component(parseComponent(vueOptions));
  }
}

function createSubpackageApp(vm) {
  var appOptions = parseApp(vm);
  var app = getApp({
    allowDefault: true });

  vm.$scope = app;
  var globalData = app.globalData;
  if (globalData) {
    Object.keys(appOptions.globalData).forEach(function (name) {
      if (!hasOwn(globalData, name)) {
        globalData[name] = appOptions.globalData[name];
      }
    });
  }
  Object.keys(appOptions).forEach(function (name) {
    if (!hasOwn(app, name)) {
      app[name] = appOptions[name];
    }
  });
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {args[_key5] = arguments[_key5];}
      vm.__call_hook('onShow', args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {args[_key6] = arguments[_key6];}
      vm.__call_hook('onHide', args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    vm.__call_hook('onLaunch', args);
  }
  return vm;
}

function createPlugin(vm) {
  var appOptions = parseApp(vm);
  if (isFn(appOptions.onShow) && wx.onAppShow) {
    wx.onAppShow(function () {for (var _len7 = arguments.length, args = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {args[_key7] = arguments[_key7];}
      appOptions.onShow.apply(vm, args);
    });
  }
  if (isFn(appOptions.onHide) && wx.onAppHide) {
    wx.onAppHide(function () {for (var _len8 = arguments.length, args = new Array(_len8), _key8 = 0; _key8 < _len8; _key8++) {args[_key8] = arguments[_key8];}
      appOptions.onHide.apply(vm, args);
    });
  }
  if (isFn(appOptions.onLaunch)) {
    var args = wx.getLaunchOptionsSync && wx.getLaunchOptionsSync();
    appOptions.onLaunch.call(vm, args);
  }
  return vm;
}

todos.forEach(function (todoApi) {
  protocols[todoApi] = false;
});

canIUses.forEach(function (canIUseApi) {
  var apiName = protocols[canIUseApi] && protocols[canIUseApi].name ? protocols[canIUseApi].name :
  canIUseApi;
  if (!wx.canIUse(apiName)) {
    protocols[canIUseApi] = false;
  }
});

var uni = {};

if (typeof Proxy !== 'undefined' && "mp-weixin" !== 'app-plus') {
  uni = new Proxy({}, {
    get: function get(target, name) {
      if (hasOwn(target, name)) {
        return target[name];
      }
      if (baseApi[name]) {
        return baseApi[name];
      }
      if (api[name]) {
        return promisify(name, api[name]);
      }
      {
        if (extraApi[name]) {
          return promisify(name, extraApi[name]);
        }
        if (todoApis[name]) {
          return promisify(name, todoApis[name]);
        }
      }
      if (eventApi[name]) {
        return eventApi[name];
      }
      if (!hasOwn(wx, name) && !hasOwn(protocols, name)) {
        return;
      }
      return promisify(name, wrapper(name, wx[name]));
    },
    set: function set(target, name, value) {
      target[name] = value;
      return true;
    } });

} else {
  Object.keys(baseApi).forEach(function (name) {
    uni[name] = baseApi[name];
  });

  {
    Object.keys(todoApis).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
    Object.keys(extraApi).forEach(function (name) {
      uni[name] = promisify(name, todoApis[name]);
    });
  }

  Object.keys(eventApi).forEach(function (name) {
    uni[name] = eventApi[name];
  });

  Object.keys(api).forEach(function (name) {
    uni[name] = promisify(name, api[name]);
  });

  Object.keys(wx).forEach(function (name) {
    if (hasOwn(wx, name) || hasOwn(protocols, name)) {
      uni[name] = promisify(name, wrapper(name, wx[name]));
    }
  });
}

wx.createApp = createApp;
wx.createPage = createPage;
wx.createComponent = createComponent;
wx.createSubpackageApp = createSubpackageApp;
wx.createPlugin = createPlugin;

var uni$1 = uni;var _default =

uni$1;exports.default = _default;

/***/ }),
/* 2 */
/*!******************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/mp-vue/dist/mp.runtime.esm.js ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * Vue.js v2.6.11
 * (c) 2014-2021 Evan You
 * Released under the MIT License.
 */
/*  */

var emptyObject = Object.freeze({});

// These helpers produce better VM code in JS engines due to their
// explicitness and function inlining.
function isUndef (v) {
  return v === undefined || v === null
}

function isDef (v) {
  return v !== undefined && v !== null
}

function isTrue (v) {
  return v === true
}

function isFalse (v) {
  return v === false
}

/**
 * Check if value is primitive.
 */
function isPrimitive (value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    // $flow-disable-line
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}

/**
 * Quick object check - this is primarily used to tell
 * Objects from primitive values when we know the value
 * is a JSON-compliant type.
 */
function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

/**
 * Get the raw type string of a value, e.g., [object Object].
 */
var _toString = Object.prototype.toString;

function toRawType (value) {
  return _toString.call(value).slice(8, -1)
}

/**
 * Strict object type check. Only returns true
 * for plain JavaScript objects.
 */
function isPlainObject (obj) {
  return _toString.call(obj) === '[object Object]'
}

function isRegExp (v) {
  return _toString.call(v) === '[object RegExp]'
}

/**
 * Check if val is a valid array index.
 */
function isValidArrayIndex (val) {
  var n = parseFloat(String(val));
  return n >= 0 && Math.floor(n) === n && isFinite(val)
}

function isPromise (val) {
  return (
    isDef(val) &&
    typeof val.then === 'function' &&
    typeof val.catch === 'function'
  )
}

/**
 * Convert a value to a string that is actually rendered.
 */
function toString (val) {
  return val == null
    ? ''
    : Array.isArray(val) || (isPlainObject(val) && val.toString === _toString)
      ? JSON.stringify(val, null, 2)
      : String(val)
}

/**
 * Convert an input value to a number for persistence.
 * If the conversion fails, return original string.
 */
function toNumber (val) {
  var n = parseFloat(val);
  return isNaN(n) ? val : n
}

/**
 * Make a map and return a function for checking if a key
 * is in that map.
 */
function makeMap (
  str,
  expectsLowerCase
) {
  var map = Object.create(null);
  var list = str.split(',');
  for (var i = 0; i < list.length; i++) {
    map[list[i]] = true;
  }
  return expectsLowerCase
    ? function (val) { return map[val.toLowerCase()]; }
    : function (val) { return map[val]; }
}

/**
 * Check if a tag is a built-in tag.
 */
var isBuiltInTag = makeMap('slot,component', true);

/**
 * Check if an attribute is a reserved attribute.
 */
var isReservedAttribute = makeMap('key,ref,slot,slot-scope,is');

/**
 * Remove an item from an array.
 */
function remove (arr, item) {
  if (arr.length) {
    var index = arr.indexOf(item);
    if (index > -1) {
      return arr.splice(index, 1)
    }
  }
}

/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn (obj, key) {
  return hasOwnProperty.call(obj, key)
}

/**
 * Create a cached version of a pure function.
 */
function cached (fn) {
  var cache = Object.create(null);
  return (function cachedFn (str) {
    var hit = cache[str];
    return hit || (cache[str] = fn(str))
  })
}

/**
 * Camelize a hyphen-delimited string.
 */
var camelizeRE = /-(\w)/g;
var camelize = cached(function (str) {
  return str.replace(camelizeRE, function (_, c) { return c ? c.toUpperCase() : ''; })
});

/**
 * Capitalize a string.
 */
var capitalize = cached(function (str) {
  return str.charAt(0).toUpperCase() + str.slice(1)
});

/**
 * Hyphenate a camelCase string.
 */
var hyphenateRE = /\B([A-Z])/g;
var hyphenate = cached(function (str) {
  return str.replace(hyphenateRE, '-$1').toLowerCase()
});

/**
 * Simple bind polyfill for environments that do not support it,
 * e.g., PhantomJS 1.x. Technically, we don't need this anymore
 * since native bind is now performant enough in most browsers.
 * But removing it would mean breaking code that was able to run in
 * PhantomJS 1.x, so this must be kept for backward compatibility.
 */

/* istanbul ignore next */
function polyfillBind (fn, ctx) {
  function boundFn (a) {
    var l = arguments.length;
    return l
      ? l > 1
        ? fn.apply(ctx, arguments)
        : fn.call(ctx, a)
      : fn.call(ctx)
  }

  boundFn._length = fn.length;
  return boundFn
}

function nativeBind (fn, ctx) {
  return fn.bind(ctx)
}

var bind = Function.prototype.bind
  ? nativeBind
  : polyfillBind;

/**
 * Convert an Array-like object to a real Array.
 */
function toArray (list, start) {
  start = start || 0;
  var i = list.length - start;
  var ret = new Array(i);
  while (i--) {
    ret[i] = list[i + start];
  }
  return ret
}

/**
 * Mix properties into target object.
 */
function extend (to, _from) {
  for (var key in _from) {
    to[key] = _from[key];
  }
  return to
}

/**
 * Merge an Array of Objects into a single Object.
 */
function toObject (arr) {
  var res = {};
  for (var i = 0; i < arr.length; i++) {
    if (arr[i]) {
      extend(res, arr[i]);
    }
  }
  return res
}

/* eslint-disable no-unused-vars */

/**
 * Perform no operation.
 * Stubbing args to make Flow happy without leaving useless transpiled code
 * with ...rest (https://flow.org/blog/2017/05/07/Strict-Function-Call-Arity/).
 */
function noop (a, b, c) {}

/**
 * Always return false.
 */
var no = function (a, b, c) { return false; };

/* eslint-enable no-unused-vars */

/**
 * Return the same value.
 */
var identity = function (_) { return _; };

/**
 * Check if two values are loosely equal - that is,
 * if they are plain objects, do they have the same shape?
 */
function looseEqual (a, b) {
  if (a === b) { return true }
  var isObjectA = isObject(a);
  var isObjectB = isObject(b);
  if (isObjectA && isObjectB) {
    try {
      var isArrayA = Array.isArray(a);
      var isArrayB = Array.isArray(b);
      if (isArrayA && isArrayB) {
        return a.length === b.length && a.every(function (e, i) {
          return looseEqual(e, b[i])
        })
      } else if (a instanceof Date && b instanceof Date) {
        return a.getTime() === b.getTime()
      } else if (!isArrayA && !isArrayB) {
        var keysA = Object.keys(a);
        var keysB = Object.keys(b);
        return keysA.length === keysB.length && keysA.every(function (key) {
          return looseEqual(a[key], b[key])
        })
      } else {
        /* istanbul ignore next */
        return false
      }
    } catch (e) {
      /* istanbul ignore next */
      return false
    }
  } else if (!isObjectA && !isObjectB) {
    return String(a) === String(b)
  } else {
    return false
  }
}

/**
 * Return the first index at which a loosely equal value can be
 * found in the array (if value is a plain object, the array must
 * contain an object of the same shape), or -1 if it is not present.
 */
function looseIndexOf (arr, val) {
  for (var i = 0; i < arr.length; i++) {
    if (looseEqual(arr[i], val)) { return i }
  }
  return -1
}

/**
 * Ensure a function is called only once.
 */
function once (fn) {
  var called = false;
  return function () {
    if (!called) {
      called = true;
      fn.apply(this, arguments);
    }
  }
}

var ASSET_TYPES = [
  'component',
  'directive',
  'filter'
];

var LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
];

/*  */



var config = ({
  /**
   * Option merge strategies (used in core/util/options)
   */
  // $flow-disable-line
  optionMergeStrategies: Object.create(null),

  /**
   * Whether to suppress warnings.
   */
  silent: false,

  /**
   * Show production mode tip message on boot?
   */
  productionTip: "development" !== 'production',

  /**
   * Whether to enable devtools
   */
  devtools: "development" !== 'production',

  /**
   * Whether to record perf
   */
  performance: false,

  /**
   * Error handler for watcher errors
   */
  errorHandler: null,

  /**
   * Warn handler for watcher warns
   */
  warnHandler: null,

  /**
   * Ignore certain custom elements
   */
  ignoredElements: [],

  /**
   * Custom user key aliases for v-on
   */
  // $flow-disable-line
  keyCodes: Object.create(null),

  /**
   * Check if a tag is reserved so that it cannot be registered as a
   * component. This is platform-dependent and may be overwritten.
   */
  isReservedTag: no,

  /**
   * Check if an attribute is reserved so that it cannot be used as a component
   * prop. This is platform-dependent and may be overwritten.
   */
  isReservedAttr: no,

  /**
   * Check if a tag is an unknown element.
   * Platform-dependent.
   */
  isUnknownElement: no,

  /**
   * Get the namespace of an element
   */
  getTagNamespace: noop,

  /**
   * Parse the real tag name for the specific platform.
   */
  parsePlatformTagName: identity,

  /**
   * Check if an attribute must be bound using property, e.g. value
   * Platform-dependent.
   */
  mustUseProp: no,

  /**
   * Perform updates asynchronously. Intended to be used by Vue Test Utils
   * This will significantly reduce performance if set to false.
   */
  async: true,

  /**
   * Exposed for legacy reasons
   */
  _lifecycleHooks: LIFECYCLE_HOOKS
});

/*  */

/**
 * unicode letters used for parsing html tags, component names and property paths.
 * using https://www.w3.org/TR/html53/semantics-scripting.html#potentialcustomelementname
 * skipping \u10000-\uEFFFF due to it freezing up PhantomJS
 */
var unicodeRegExp = /a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;

/**
 * Check if a string starts with $ or _
 */
function isReserved (str) {
  var c = (str + '').charCodeAt(0);
  return c === 0x24 || c === 0x5F
}

/**
 * Define a property.
 */
function def (obj, key, val, enumerable) {
  Object.defineProperty(obj, key, {
    value: val,
    enumerable: !!enumerable,
    writable: true,
    configurable: true
  });
}

/**
 * Parse simple path.
 */
var bailRE = new RegExp(("[^" + (unicodeRegExp.source) + ".$_\\d]"));
function parsePath (path) {
  if (bailRE.test(path)) {
    return
  }
  var segments = path.split('.');
  return function (obj) {
    for (var i = 0; i < segments.length; i++) {
      if (!obj) { return }
      obj = obj[segments[i]];
    }
    return obj
  }
}

/*  */

// can we use __proto__?
var hasProto = '__proto__' in {};

// Browser environment sniffing
var inBrowser = typeof window !== 'undefined';
var inWeex = typeof WXEnvironment !== 'undefined' && !!WXEnvironment.platform;
var weexPlatform = inWeex && WXEnvironment.platform.toLowerCase();
var UA = inBrowser && window.navigator.userAgent.toLowerCase();
var isIE = UA && /msie|trident/.test(UA);
var isIE9 = UA && UA.indexOf('msie 9.0') > 0;
var isEdge = UA && UA.indexOf('edge/') > 0;
var isAndroid = (UA && UA.indexOf('android') > 0) || (weexPlatform === 'android');
var isIOS = (UA && /iphone|ipad|ipod|ios/.test(UA)) || (weexPlatform === 'ios');
var isChrome = UA && /chrome\/\d+/.test(UA) && !isEdge;
var isPhantomJS = UA && /phantomjs/.test(UA);
var isFF = UA && UA.match(/firefox\/(\d+)/);

// Firefox has a "watch" function on Object.prototype...
var nativeWatch = ({}).watch;
if (inBrowser) {
  try {
    var opts = {};
    Object.defineProperty(opts, 'passive', ({
      get: function get () {
      }
    })); // https://github.com/facebook/flow/issues/285
    window.addEventListener('test-passive', null, opts);
  } catch (e) {}
}

// this needs to be lazy-evaled because vue may be required before
// vue-server-renderer can set VUE_ENV
var _isServer;
var isServerRendering = function () {
  if (_isServer === undefined) {
    /* istanbul ignore if */
    if (!inBrowser && !inWeex && typeof global !== 'undefined') {
      // detect presence of vue-server-renderer and avoid
      // Webpack shimming the process
      _isServer = global['process'] && global['process'].env.VUE_ENV === 'server';
    } else {
      _isServer = false;
    }
  }
  return _isServer
};

// detect devtools
var devtools = inBrowser && window.__VUE_DEVTOOLS_GLOBAL_HOOK__;

/* istanbul ignore next */
function isNative (Ctor) {
  return typeof Ctor === 'function' && /native code/.test(Ctor.toString())
}

var hasSymbol =
  typeof Symbol !== 'undefined' && isNative(Symbol) &&
  typeof Reflect !== 'undefined' && isNative(Reflect.ownKeys);

var _Set;
/* istanbul ignore if */ // $flow-disable-line
if (typeof Set !== 'undefined' && isNative(Set)) {
  // use native Set when available.
  _Set = Set;
} else {
  // a non-standard Set polyfill that only works with primitive keys.
  _Set = /*@__PURE__*/(function () {
    function Set () {
      this.set = Object.create(null);
    }
    Set.prototype.has = function has (key) {
      return this.set[key] === true
    };
    Set.prototype.add = function add (key) {
      this.set[key] = true;
    };
    Set.prototype.clear = function clear () {
      this.set = Object.create(null);
    };

    return Set;
  }());
}

/*  */

var warn = noop;
var tip = noop;
var generateComponentTrace = (noop); // work around flow check
var formatComponentName = (noop);

if (true) {
  var hasConsole = typeof console !== 'undefined';
  var classifyRE = /(?:^|[-_])(\w)/g;
  var classify = function (str) { return str
    .replace(classifyRE, function (c) { return c.toUpperCase(); })
    .replace(/[-_]/g, ''); };

  warn = function (msg, vm) {
    var trace = vm ? generateComponentTrace(vm) : '';

    if (config.warnHandler) {
      config.warnHandler.call(null, msg, vm, trace);
    } else if (hasConsole && (!config.silent)) {
      console.error(("[Vue warn]: " + msg + trace));
    }
  };

  tip = function (msg, vm) {
    if (hasConsole && (!config.silent)) {
      console.warn("[Vue tip]: " + msg + (
        vm ? generateComponentTrace(vm) : ''
      ));
    }
  };

  formatComponentName = function (vm, includeFile) {
    if (vm.$root === vm) {
      if (vm.$options && vm.$options.__file) { // fixed by xxxxxx
        return ('') + vm.$options.__file
      }
      return '<Root>'
    }
    var options = typeof vm === 'function' && vm.cid != null
      ? vm.options
      : vm._isVue
        ? vm.$options || vm.constructor.options
        : vm;
    var name = options.name || options._componentTag;
    var file = options.__file;
    if (!name && file) {
      var match = file.match(/([^/\\]+)\.vue$/);
      name = match && match[1];
    }

    return (
      (name ? ("<" + (classify(name)) + ">") : "<Anonymous>") +
      (file && includeFile !== false ? (" at " + file) : '')
    )
  };

  var repeat = function (str, n) {
    var res = '';
    while (n) {
      if (n % 2 === 1) { res += str; }
      if (n > 1) { str += str; }
      n >>= 1;
    }
    return res
  };

  generateComponentTrace = function (vm) {
    if (vm._isVue && vm.$parent) {
      var tree = [];
      var currentRecursiveSequence = 0;
      while (vm && vm.$options.name !== 'PageBody') {
        if (tree.length > 0) {
          var last = tree[tree.length - 1];
          if (last.constructor === vm.constructor) {
            currentRecursiveSequence++;
            vm = vm.$parent;
            continue
          } else if (currentRecursiveSequence > 0) {
            tree[tree.length - 1] = [last, currentRecursiveSequence];
            currentRecursiveSequence = 0;
          }
        }
        !vm.$options.isReserved && tree.push(vm);
        vm = vm.$parent;
      }
      return '\n\nfound in\n\n' + tree
        .map(function (vm, i) { return ("" + (i === 0 ? '---> ' : repeat(' ', 5 + i * 2)) + (Array.isArray(vm)
            ? ((formatComponentName(vm[0])) + "... (" + (vm[1]) + " recursive calls)")
            : formatComponentName(vm))); })
        .join('\n')
    } else {
      return ("\n\n(found in " + (formatComponentName(vm)) + ")")
    }
  };
}

/*  */

var uid = 0;

/**
 * A dep is an observable that can have multiple
 * directives subscribing to it.
 */
var Dep = function Dep () {
  this.id = uid++;
  this.subs = [];
};

Dep.prototype.addSub = function addSub (sub) {
  this.subs.push(sub);
};

Dep.prototype.removeSub = function removeSub (sub) {
  remove(this.subs, sub);
};

Dep.prototype.depend = function depend () {
  if (Dep.SharedObject.target) {
    Dep.SharedObject.target.addDep(this);
  }
};

Dep.prototype.notify = function notify () {
  // stabilize the subscriber list first
  var subs = this.subs.slice();
  if ( true && !config.async) {
    // subs aren't sorted in scheduler if not running async
    // we need to sort them now to make sure they fire in correct
    // order
    subs.sort(function (a, b) { return a.id - b.id; });
  }
  for (var i = 0, l = subs.length; i < l; i++) {
    subs[i].update();
  }
};

// The current target watcher being evaluated.
// This is globally unique because only one watcher
// can be evaluated at a time.
// fixed by xxxxxx (nvue shared vuex)
/* eslint-disable no-undef */
Dep.SharedObject = {};
Dep.SharedObject.target = null;
Dep.SharedObject.targetStack = [];

function pushTarget (target) {
  Dep.SharedObject.targetStack.push(target);
  Dep.SharedObject.target = target;
  Dep.target = target;
}

function popTarget () {
  Dep.SharedObject.targetStack.pop();
  Dep.SharedObject.target = Dep.SharedObject.targetStack[Dep.SharedObject.targetStack.length - 1];
  Dep.target = Dep.SharedObject.target;
}

/*  */

var VNode = function VNode (
  tag,
  data,
  children,
  text,
  elm,
  context,
  componentOptions,
  asyncFactory
) {
  this.tag = tag;
  this.data = data;
  this.children = children;
  this.text = text;
  this.elm = elm;
  this.ns = undefined;
  this.context = context;
  this.fnContext = undefined;
  this.fnOptions = undefined;
  this.fnScopeId = undefined;
  this.key = data && data.key;
  this.componentOptions = componentOptions;
  this.componentInstance = undefined;
  this.parent = undefined;
  this.raw = false;
  this.isStatic = false;
  this.isRootInsert = true;
  this.isComment = false;
  this.isCloned = false;
  this.isOnce = false;
  this.asyncFactory = asyncFactory;
  this.asyncMeta = undefined;
  this.isAsyncPlaceholder = false;
};

var prototypeAccessors = { child: { configurable: true } };

// DEPRECATED: alias for componentInstance for backwards compat.
/* istanbul ignore next */
prototypeAccessors.child.get = function () {
  return this.componentInstance
};

Object.defineProperties( VNode.prototype, prototypeAccessors );

var createEmptyVNode = function (text) {
  if ( text === void 0 ) text = '';

  var node = new VNode();
  node.text = text;
  node.isComment = true;
  return node
};

function createTextVNode (val) {
  return new VNode(undefined, undefined, undefined, String(val))
}

// optimized shallow clone
// used for static nodes and slot nodes because they may be reused across
// multiple renders, cloning them avoids errors when DOM manipulations rely
// on their elm reference.
function cloneVNode (vnode) {
  var cloned = new VNode(
    vnode.tag,
    vnode.data,
    // #7975
    // clone children array to avoid mutating original in case of cloning
    // a child.
    vnode.children && vnode.children.slice(),
    vnode.text,
    vnode.elm,
    vnode.context,
    vnode.componentOptions,
    vnode.asyncFactory
  );
  cloned.ns = vnode.ns;
  cloned.isStatic = vnode.isStatic;
  cloned.key = vnode.key;
  cloned.isComment = vnode.isComment;
  cloned.fnContext = vnode.fnContext;
  cloned.fnOptions = vnode.fnOptions;
  cloned.fnScopeId = vnode.fnScopeId;
  cloned.asyncMeta = vnode.asyncMeta;
  cloned.isCloned = true;
  return cloned
}

/*
 * not type checking this file because flow doesn't play well with
 * dynamically accessing methods on Array prototype
 */

var arrayProto = Array.prototype;
var arrayMethods = Object.create(arrayProto);

var methodsToPatch = [
  'push',
  'pop',
  'shift',
  'unshift',
  'splice',
  'sort',
  'reverse'
];

/**
 * Intercept mutating methods and emit events
 */
methodsToPatch.forEach(function (method) {
  // cache original method
  var original = arrayProto[method];
  def(arrayMethods, method, function mutator () {
    var args = [], len = arguments.length;
    while ( len-- ) args[ len ] = arguments[ len ];

    var result = original.apply(this, args);
    var ob = this.__ob__;
    var inserted;
    switch (method) {
      case 'push':
      case 'unshift':
        inserted = args;
        break
      case 'splice':
        inserted = args.slice(2);
        break
    }
    if (inserted) { ob.observeArray(inserted); }
    // notify change
    ob.dep.notify();
    return result
  });
});

/*  */

var arrayKeys = Object.getOwnPropertyNames(arrayMethods);

/**
 * In some cases we may want to disable observation inside a component's
 * update computation.
 */
var shouldObserve = true;

function toggleObserving (value) {
  shouldObserve = value;
}

/**
 * Observer class that is attached to each observed
 * object. Once attached, the observer converts the target
 * object's property keys into getter/setters that
 * collect dependencies and dispatch updates.
 */
var Observer = function Observer (value) {
  this.value = value;
  this.dep = new Dep();
  this.vmCount = 0;
  def(value, '__ob__', this);
  if (Array.isArray(value)) {
    if (hasProto) {
      {// fixed by xxxxxx 微信小程序使用 plugins 之后，数组方法被直接挂载到了数组对象上，需要执行 copyAugment 逻辑
        if(value.push !== value.__proto__.push){
          copyAugment(value, arrayMethods, arrayKeys);
        } else {
          protoAugment(value, arrayMethods);
        }
      }
    } else {
      copyAugment(value, arrayMethods, arrayKeys);
    }
    this.observeArray(value);
  } else {
    this.walk(value);
  }
};

/**
 * Walk through all properties and convert them into
 * getter/setters. This method should only be called when
 * value type is Object.
 */
Observer.prototype.walk = function walk (obj) {
  var keys = Object.keys(obj);
  for (var i = 0; i < keys.length; i++) {
    defineReactive$$1(obj, keys[i]);
  }
};

/**
 * Observe a list of Array items.
 */
Observer.prototype.observeArray = function observeArray (items) {
  for (var i = 0, l = items.length; i < l; i++) {
    observe(items[i]);
  }
};

// helpers

/**
 * Augment a target Object or Array by intercepting
 * the prototype chain using __proto__
 */
function protoAugment (target, src) {
  /* eslint-disable no-proto */
  target.__proto__ = src;
  /* eslint-enable no-proto */
}

/**
 * Augment a target Object or Array by defining
 * hidden properties.
 */
/* istanbul ignore next */
function copyAugment (target, src, keys) {
  for (var i = 0, l = keys.length; i < l; i++) {
    var key = keys[i];
    def(target, key, src[key]);
  }
}

/**
 * Attempt to create an observer instance for a value,
 * returns the new observer if successfully observed,
 * or the existing observer if the value already has one.
 */
function observe (value, asRootData) {
  if (!isObject(value) || value instanceof VNode) {
    return
  }
  var ob;
  if (hasOwn(value, '__ob__') && value.__ob__ instanceof Observer) {
    ob = value.__ob__;
  } else if (
    shouldObserve &&
    !isServerRendering() &&
    (Array.isArray(value) || isPlainObject(value)) &&
    Object.isExtensible(value) &&
    !value._isVue
  ) {
    ob = new Observer(value);
  }
  if (asRootData && ob) {
    ob.vmCount++;
  }
  return ob
}

/**
 * Define a reactive property on an Object.
 */
function defineReactive$$1 (
  obj,
  key,
  val,
  customSetter,
  shallow
) {
  var dep = new Dep();

  var property = Object.getOwnPropertyDescriptor(obj, key);
  if (property && property.configurable === false) {
    return
  }

  // cater for pre-defined getter/setters
  var getter = property && property.get;
  var setter = property && property.set;
  if ((!getter || setter) && arguments.length === 2) {
    val = obj[key];
  }

  var childOb = !shallow && observe(val);
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: function reactiveGetter () {
      var value = getter ? getter.call(obj) : val;
      if (Dep.SharedObject.target) { // fixed by xxxxxx
        dep.depend();
        if (childOb) {
          childOb.dep.depend();
          if (Array.isArray(value)) {
            dependArray(value);
          }
        }
      }
      return value
    },
    set: function reactiveSetter (newVal) {
      var value = getter ? getter.call(obj) : val;
      /* eslint-disable no-self-compare */
      if (newVal === value || (newVal !== newVal && value !== value)) {
        return
      }
      /* eslint-enable no-self-compare */
      if ( true && customSetter) {
        customSetter();
      }
      // #7981: for accessor properties without setter
      if (getter && !setter) { return }
      if (setter) {
        setter.call(obj, newVal);
      } else {
        val = newVal;
      }
      childOb = !shallow && observe(newVal);
      dep.notify();
    }
  });
}

/**
 * Set a property on an object. Adds the new property and
 * triggers change notification if the property doesn't
 * already exist.
 */
function set (target, key, val) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot set reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.length = Math.max(target.length, key);
    target.splice(key, 1, val);
    return val
  }
  if (key in target && !(key in Object.prototype)) {
    target[key] = val;
    return val
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid adding reactive properties to a Vue instance or its root $data ' +
      'at runtime - declare it upfront in the data option.'
    );
    return val
  }
  if (!ob) {
    target[key] = val;
    return val
  }
  defineReactive$$1(ob.value, key, val);
  ob.dep.notify();
  return val
}

/**
 * Delete a property and trigger change if necessary.
 */
function del (target, key) {
  if ( true &&
    (isUndef(target) || isPrimitive(target))
  ) {
    warn(("Cannot delete reactive property on undefined, null, or primitive value: " + ((target))));
  }
  if (Array.isArray(target) && isValidArrayIndex(key)) {
    target.splice(key, 1);
    return
  }
  var ob = (target).__ob__;
  if (target._isVue || (ob && ob.vmCount)) {
     true && warn(
      'Avoid deleting properties on a Vue instance or its root $data ' +
      '- just set it to null.'
    );
    return
  }
  if (!hasOwn(target, key)) {
    return
  }
  delete target[key];
  if (!ob) {
    return
  }
  ob.dep.notify();
}

/**
 * Collect dependencies on array elements when the array is touched, since
 * we cannot intercept array element access like property getters.
 */
function dependArray (value) {
  for (var e = (void 0), i = 0, l = value.length; i < l; i++) {
    e = value[i];
    e && e.__ob__ && e.__ob__.dep.depend();
    if (Array.isArray(e)) {
      dependArray(e);
    }
  }
}

/*  */

/**
 * Option overwriting strategies are functions that handle
 * how to merge a parent option value and a child option
 * value into the final value.
 */
var strats = config.optionMergeStrategies;

/**
 * Options with restrictions
 */
if (true) {
  strats.el = strats.propsData = function (parent, child, vm, key) {
    if (!vm) {
      warn(
        "option \"" + key + "\" can only be used during instance " +
        'creation with the `new` keyword.'
      );
    }
    return defaultStrat(parent, child)
  };
}

/**
 * Helper that recursively merges two data objects together.
 */
function mergeData (to, from) {
  if (!from) { return to }
  var key, toVal, fromVal;

  var keys = hasSymbol
    ? Reflect.ownKeys(from)
    : Object.keys(from);

  for (var i = 0; i < keys.length; i++) {
    key = keys[i];
    // in case the object is already observed...
    if (key === '__ob__') { continue }
    toVal = to[key];
    fromVal = from[key];
    if (!hasOwn(to, key)) {
      set(to, key, fromVal);
    } else if (
      toVal !== fromVal &&
      isPlainObject(toVal) &&
      isPlainObject(fromVal)
    ) {
      mergeData(toVal, fromVal);
    }
  }
  return to
}

/**
 * Data
 */
function mergeDataOrFn (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    // in a Vue.extend merge, both should be functions
    if (!childVal) {
      return parentVal
    }
    if (!parentVal) {
      return childVal
    }
    // when parentVal & childVal are both present,
    // we need to return a function that returns the
    // merged result of both functions... no need to
    // check if parentVal is a function here because
    // it has to be a function to pass previous merges.
    return function mergedDataFn () {
      return mergeData(
        typeof childVal === 'function' ? childVal.call(this, this) : childVal,
        typeof parentVal === 'function' ? parentVal.call(this, this) : parentVal
      )
    }
  } else {
    return function mergedInstanceDataFn () {
      // instance merge
      var instanceData = typeof childVal === 'function'
        ? childVal.call(vm, vm)
        : childVal;
      var defaultData = typeof parentVal === 'function'
        ? parentVal.call(vm, vm)
        : parentVal;
      if (instanceData) {
        return mergeData(instanceData, defaultData)
      } else {
        return defaultData
      }
    }
  }
}

strats.data = function (
  parentVal,
  childVal,
  vm
) {
  if (!vm) {
    if (childVal && typeof childVal !== 'function') {
       true && warn(
        'The "data" option should be a function ' +
        'that returns a per-instance value in component ' +
        'definitions.',
        vm
      );

      return parentVal
    }
    return mergeDataOrFn(parentVal, childVal)
  }

  return mergeDataOrFn(parentVal, childVal, vm)
};

/**
 * Hooks and props are merged as arrays.
 */
function mergeHook (
  parentVal,
  childVal
) {
  var res = childVal
    ? parentVal
      ? parentVal.concat(childVal)
      : Array.isArray(childVal)
        ? childVal
        : [childVal]
    : parentVal;
  return res
    ? dedupeHooks(res)
    : res
}

function dedupeHooks (hooks) {
  var res = [];
  for (var i = 0; i < hooks.length; i++) {
    if (res.indexOf(hooks[i]) === -1) {
      res.push(hooks[i]);
    }
  }
  return res
}

LIFECYCLE_HOOKS.forEach(function (hook) {
  strats[hook] = mergeHook;
});

/**
 * Assets
 *
 * When a vm is present (instance creation), we need to do
 * a three-way merge between constructor options, instance
 * options and parent options.
 */
function mergeAssets (
  parentVal,
  childVal,
  vm,
  key
) {
  var res = Object.create(parentVal || null);
  if (childVal) {
     true && assertObjectType(key, childVal, vm);
    return extend(res, childVal)
  } else {
    return res
  }
}

ASSET_TYPES.forEach(function (type) {
  strats[type + 's'] = mergeAssets;
});

/**
 * Watchers.
 *
 * Watchers hashes should not overwrite one
 * another, so we merge them as arrays.
 */
strats.watch = function (
  parentVal,
  childVal,
  vm,
  key
) {
  // work around Firefox's Object.prototype.watch...
  if (parentVal === nativeWatch) { parentVal = undefined; }
  if (childVal === nativeWatch) { childVal = undefined; }
  /* istanbul ignore if */
  if (!childVal) { return Object.create(parentVal || null) }
  if (true) {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = {};
  extend(ret, parentVal);
  for (var key$1 in childVal) {
    var parent = ret[key$1];
    var child = childVal[key$1];
    if (parent && !Array.isArray(parent)) {
      parent = [parent];
    }
    ret[key$1] = parent
      ? parent.concat(child)
      : Array.isArray(child) ? child : [child];
  }
  return ret
};

/**
 * Other object hashes.
 */
strats.props =
strats.methods =
strats.inject =
strats.computed = function (
  parentVal,
  childVal,
  vm,
  key
) {
  if (childVal && "development" !== 'production') {
    assertObjectType(key, childVal, vm);
  }
  if (!parentVal) { return childVal }
  var ret = Object.create(null);
  extend(ret, parentVal);
  if (childVal) { extend(ret, childVal); }
  return ret
};
strats.provide = mergeDataOrFn;

/**
 * Default strategy.
 */
var defaultStrat = function (parentVal, childVal) {
  return childVal === undefined
    ? parentVal
    : childVal
};

/**
 * Validate component names
 */
function checkComponents (options) {
  for (var key in options.components) {
    validateComponentName(key);
  }
}

function validateComponentName (name) {
  if (!new RegExp(("^[a-zA-Z][\\-\\.0-9_" + (unicodeRegExp.source) + "]*$")).test(name)) {
    warn(
      'Invalid component name: "' + name + '". Component names ' +
      'should conform to valid custom element name in html5 specification.'
    );
  }
  if (isBuiltInTag(name) || config.isReservedTag(name)) {
    warn(
      'Do not use built-in or reserved HTML elements as component ' +
      'id: ' + name
    );
  }
}

/**
 * Ensure all props option syntax are normalized into the
 * Object-based format.
 */
function normalizeProps (options, vm) {
  var props = options.props;
  if (!props) { return }
  var res = {};
  var i, val, name;
  if (Array.isArray(props)) {
    i = props.length;
    while (i--) {
      val = props[i];
      if (typeof val === 'string') {
        name = camelize(val);
        res[name] = { type: null };
      } else if (true) {
        warn('props must be strings when using array syntax.');
      }
    }
  } else if (isPlainObject(props)) {
    for (var key in props) {
      val = props[key];
      name = camelize(key);
      res[name] = isPlainObject(val)
        ? val
        : { type: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"props\": expected an Array or an Object, " +
      "but got " + (toRawType(props)) + ".",
      vm
    );
  }
  options.props = res;
}

/**
 * Normalize all injections into Object-based format
 */
function normalizeInject (options, vm) {
  var inject = options.inject;
  if (!inject) { return }
  var normalized = options.inject = {};
  if (Array.isArray(inject)) {
    for (var i = 0; i < inject.length; i++) {
      normalized[inject[i]] = { from: inject[i] };
    }
  } else if (isPlainObject(inject)) {
    for (var key in inject) {
      var val = inject[key];
      normalized[key] = isPlainObject(val)
        ? extend({ from: key }, val)
        : { from: val };
    }
  } else if (true) {
    warn(
      "Invalid value for option \"inject\": expected an Array or an Object, " +
      "but got " + (toRawType(inject)) + ".",
      vm
    );
  }
}

/**
 * Normalize raw function directives into object format.
 */
function normalizeDirectives (options) {
  var dirs = options.directives;
  if (dirs) {
    for (var key in dirs) {
      var def$$1 = dirs[key];
      if (typeof def$$1 === 'function') {
        dirs[key] = { bind: def$$1, update: def$$1 };
      }
    }
  }
}

function assertObjectType (name, value, vm) {
  if (!isPlainObject(value)) {
    warn(
      "Invalid value for option \"" + name + "\": expected an Object, " +
      "but got " + (toRawType(value)) + ".",
      vm
    );
  }
}

/**
 * Merge two option objects into a new one.
 * Core utility used in both instantiation and inheritance.
 */
function mergeOptions (
  parent,
  child,
  vm
) {
  if (true) {
    checkComponents(child);
  }

  if (typeof child === 'function') {
    child = child.options;
  }

  normalizeProps(child, vm);
  normalizeInject(child, vm);
  normalizeDirectives(child);

  // Apply extends and mixins on the child options,
  // but only if it is a raw options object that isn't
  // the result of another mergeOptions call.
  // Only merged options has the _base property.
  if (!child._base) {
    if (child.extends) {
      parent = mergeOptions(parent, child.extends, vm);
    }
    if (child.mixins) {
      for (var i = 0, l = child.mixins.length; i < l; i++) {
        parent = mergeOptions(parent, child.mixins[i], vm);
      }
    }
  }

  var options = {};
  var key;
  for (key in parent) {
    mergeField(key);
  }
  for (key in child) {
    if (!hasOwn(parent, key)) {
      mergeField(key);
    }
  }
  function mergeField (key) {
    var strat = strats[key] || defaultStrat;
    options[key] = strat(parent[key], child[key], vm, key);
  }
  return options
}

/**
 * Resolve an asset.
 * This function is used because child instances need access
 * to assets defined in its ancestor chain.
 */
function resolveAsset (
  options,
  type,
  id,
  warnMissing
) {
  /* istanbul ignore if */
  if (typeof id !== 'string') {
    return
  }
  var assets = options[type];
  // check local registration variations first
  if (hasOwn(assets, id)) { return assets[id] }
  var camelizedId = camelize(id);
  if (hasOwn(assets, camelizedId)) { return assets[camelizedId] }
  var PascalCaseId = capitalize(camelizedId);
  if (hasOwn(assets, PascalCaseId)) { return assets[PascalCaseId] }
  // fallback to prototype chain
  var res = assets[id] || assets[camelizedId] || assets[PascalCaseId];
  if ( true && warnMissing && !res) {
    warn(
      'Failed to resolve ' + type.slice(0, -1) + ': ' + id,
      options
    );
  }
  return res
}

/*  */



function validateProp (
  key,
  propOptions,
  propsData,
  vm
) {
  var prop = propOptions[key];
  var absent = !hasOwn(propsData, key);
  var value = propsData[key];
  // boolean casting
  var booleanIndex = getTypeIndex(Boolean, prop.type);
  if (booleanIndex > -1) {
    if (absent && !hasOwn(prop, 'default')) {
      value = false;
    } else if (value === '' || value === hyphenate(key)) {
      // only cast empty string / same name to boolean if
      // boolean has higher priority
      var stringIndex = getTypeIndex(String, prop.type);
      if (stringIndex < 0 || booleanIndex < stringIndex) {
        value = true;
      }
    }
  }
  // check default value
  if (value === undefined) {
    value = getPropDefaultValue(vm, prop, key);
    // since the default value is a fresh copy,
    // make sure to observe it.
    var prevShouldObserve = shouldObserve;
    toggleObserving(true);
    observe(value);
    toggleObserving(prevShouldObserve);
  }
  if (
    true
  ) {
    assertProp(prop, key, value, vm, absent);
  }
  return value
}

/**
 * Get the default value of a prop.
 */
function getPropDefaultValue (vm, prop, key) {
  // no default, return undefined
  if (!hasOwn(prop, 'default')) {
    return undefined
  }
  var def = prop.default;
  // warn against non-factory defaults for Object & Array
  if ( true && isObject(def)) {
    warn(
      'Invalid default value for prop "' + key + '": ' +
      'Props with type Object/Array must use a factory function ' +
      'to return the default value.',
      vm
    );
  }
  // the raw prop value was also undefined from previous render,
  // return previous default value to avoid unnecessary watcher trigger
  if (vm && vm.$options.propsData &&
    vm.$options.propsData[key] === undefined &&
    vm._props[key] !== undefined
  ) {
    return vm._props[key]
  }
  // call factory function for non-Function types
  // a value is Function if its prototype is function even across different execution context
  return typeof def === 'function' && getType(prop.type) !== 'Function'
    ? def.call(vm)
    : def
}

/**
 * Assert whether a prop is valid.
 */
function assertProp (
  prop,
  name,
  value,
  vm,
  absent
) {
  if (prop.required && absent) {
    warn(
      'Missing required prop: "' + name + '"',
      vm
    );
    return
  }
  if (value == null && !prop.required) {
    return
  }
  var type = prop.type;
  var valid = !type || type === true;
  var expectedTypes = [];
  if (type) {
    if (!Array.isArray(type)) {
      type = [type];
    }
    for (var i = 0; i < type.length && !valid; i++) {
      var assertedType = assertType(value, type[i]);
      expectedTypes.push(assertedType.expectedType || '');
      valid = assertedType.valid;
    }
  }

  if (!valid) {
    warn(
      getInvalidTypeMessage(name, value, expectedTypes),
      vm
    );
    return
  }
  var validator = prop.validator;
  if (validator) {
    if (!validator(value)) {
      warn(
        'Invalid prop: custom validator check failed for prop "' + name + '".',
        vm
      );
    }
  }
}

var simpleCheckRE = /^(String|Number|Boolean|Function|Symbol)$/;

function assertType (value, type) {
  var valid;
  var expectedType = getType(type);
  if (simpleCheckRE.test(expectedType)) {
    var t = typeof value;
    valid = t === expectedType.toLowerCase();
    // for primitive wrapper objects
    if (!valid && t === 'object') {
      valid = value instanceof type;
    }
  } else if (expectedType === 'Object') {
    valid = isPlainObject(value);
  } else if (expectedType === 'Array') {
    valid = Array.isArray(value);
  } else {
    valid = value instanceof type;
  }
  return {
    valid: valid,
    expectedType: expectedType
  }
}

/**
 * Use function string name to check built-in types,
 * because a simple equality check will fail when running
 * across different vms / iframes.
 */
function getType (fn) {
  var match = fn && fn.toString().match(/^\s*function (\w+)/);
  return match ? match[1] : ''
}

function isSameType (a, b) {
  return getType(a) === getType(b)
}

function getTypeIndex (type, expectedTypes) {
  if (!Array.isArray(expectedTypes)) {
    return isSameType(expectedTypes, type) ? 0 : -1
  }
  for (var i = 0, len = expectedTypes.length; i < len; i++) {
    if (isSameType(expectedTypes[i], type)) {
      return i
    }
  }
  return -1
}

function getInvalidTypeMessage (name, value, expectedTypes) {
  var message = "Invalid prop: type check failed for prop \"" + name + "\"." +
    " Expected " + (expectedTypes.map(capitalize).join(', '));
  var expectedType = expectedTypes[0];
  var receivedType = toRawType(value);
  var expectedValue = styleValue(value, expectedType);
  var receivedValue = styleValue(value, receivedType);
  // check if we need to specify expected value
  if (expectedTypes.length === 1 &&
      isExplicable(expectedType) &&
      !isBoolean(expectedType, receivedType)) {
    message += " with value " + expectedValue;
  }
  message += ", got " + receivedType + " ";
  // check if we need to specify received value
  if (isExplicable(receivedType)) {
    message += "with value " + receivedValue + ".";
  }
  return message
}

function styleValue (value, type) {
  if (type === 'String') {
    return ("\"" + value + "\"")
  } else if (type === 'Number') {
    return ("" + (Number(value)))
  } else {
    return ("" + value)
  }
}

function isExplicable (value) {
  var explicitTypes = ['string', 'number', 'boolean'];
  return explicitTypes.some(function (elem) { return value.toLowerCase() === elem; })
}

function isBoolean () {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return args.some(function (elem) { return elem.toLowerCase() === 'boolean'; })
}

/*  */

function handleError (err, vm, info) {
  // Deactivate deps tracking while processing error handler to avoid possible infinite rendering.
  // See: https://github.com/vuejs/vuex/issues/1505
  pushTarget();
  try {
    if (vm) {
      var cur = vm;
      while ((cur = cur.$parent)) {
        var hooks = cur.$options.errorCaptured;
        if (hooks) {
          for (var i = 0; i < hooks.length; i++) {
            try {
              var capture = hooks[i].call(cur, err, vm, info) === false;
              if (capture) { return }
            } catch (e) {
              globalHandleError(e, cur, 'errorCaptured hook');
            }
          }
        }
      }
    }
    globalHandleError(err, vm, info);
  } finally {
    popTarget();
  }
}

function invokeWithErrorHandling (
  handler,
  context,
  args,
  vm,
  info
) {
  var res;
  try {
    res = args ? handler.apply(context, args) : handler.call(context);
    if (res && !res._isVue && isPromise(res) && !res._handled) {
      res.catch(function (e) { return handleError(e, vm, info + " (Promise/async)"); });
      // issue #9511
      // avoid catch triggering multiple times when nested calls
      res._handled = true;
    }
  } catch (e) {
    handleError(e, vm, info);
  }
  return res
}

function globalHandleError (err, vm, info) {
  if (config.errorHandler) {
    try {
      return config.errorHandler.call(null, err, vm, info)
    } catch (e) {
      // if the user intentionally throws the original error in the handler,
      // do not log it twice
      if (e !== err) {
        logError(e, null, 'config.errorHandler');
      }
    }
  }
  logError(err, vm, info);
}

function logError (err, vm, info) {
  if (true) {
    warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
  }
  /* istanbul ignore else */
  if ((inBrowser || inWeex) && typeof console !== 'undefined') {
    console.error(err);
  } else {
    throw err
  }
}

/*  */

var callbacks = [];
var pending = false;

function flushCallbacks () {
  pending = false;
  var copies = callbacks.slice(0);
  callbacks.length = 0;
  for (var i = 0; i < copies.length; i++) {
    copies[i]();
  }
}

// Here we have async deferring wrappers using microtasks.
// In 2.5 we used (macro) tasks (in combination with microtasks).
// However, it has subtle problems when state is changed right before repaint
// (e.g. #6813, out-in transitions).
// Also, using (macro) tasks in event handler would cause some weird behaviors
// that cannot be circumvented (e.g. #7109, #7153, #7546, #7834, #8109).
// So we now use microtasks everywhere, again.
// A major drawback of this tradeoff is that there are some scenarios
// where microtasks have too high a priority and fire in between supposedly
// sequential events (e.g. #4521, #6690, which have workarounds)
// or even between bubbling of the same event (#6566).
var timerFunc;

// The nextTick behavior leverages the microtask queue, which can be accessed
// via either native Promise.then or MutationObserver.
// MutationObserver has wider support, however it is seriously bugged in
// UIWebView in iOS >= 9.3.3 when triggered in touch event handlers. It
// completely stops working after triggering a few times... so, if native
// Promise is available, we will use it:
/* istanbul ignore next, $flow-disable-line */
if (typeof Promise !== 'undefined' && isNative(Promise)) {
  var p = Promise.resolve();
  timerFunc = function () {
    p.then(flushCallbacks);
    // In problematic UIWebViews, Promise.then doesn't completely break, but
    // it can get stuck in a weird state where callbacks are pushed into the
    // microtask queue but the queue isn't being flushed, until the browser
    // needs to do some other work, e.g. handle a timer. Therefore we can
    // "force" the microtask queue to be flushed by adding an empty timer.
    if (isIOS) { setTimeout(noop); }
  };
} else if (!isIE && typeof MutationObserver !== 'undefined' && (
  isNative(MutationObserver) ||
  // PhantomJS and iOS 7.x
  MutationObserver.toString() === '[object MutationObserverConstructor]'
)) {
  // Use MutationObserver where native Promise is not available,
  // e.g. PhantomJS, iOS7, Android 4.4
  // (#6466 MutationObserver is unreliable in IE11)
  var counter = 1;
  var observer = new MutationObserver(flushCallbacks);
  var textNode = document.createTextNode(String(counter));
  observer.observe(textNode, {
    characterData: true
  });
  timerFunc = function () {
    counter = (counter + 1) % 2;
    textNode.data = String(counter);
  };
} else if (typeof setImmediate !== 'undefined' && isNative(setImmediate)) {
  // Fallback to setImmediate.
  // Technically it leverages the (macro) task queue,
  // but it is still a better choice than setTimeout.
  timerFunc = function () {
    setImmediate(flushCallbacks);
  };
} else {
  // Fallback to setTimeout.
  timerFunc = function () {
    setTimeout(flushCallbacks, 0);
  };
}

function nextTick (cb, ctx) {
  var _resolve;
  callbacks.push(function () {
    if (cb) {
      try {
        cb.call(ctx);
      } catch (e) {
        handleError(e, ctx, 'nextTick');
      }
    } else if (_resolve) {
      _resolve(ctx);
    }
  });
  if (!pending) {
    pending = true;
    timerFunc();
  }
  // $flow-disable-line
  if (!cb && typeof Promise !== 'undefined') {
    return new Promise(function (resolve) {
      _resolve = resolve;
    })
  }
}

/*  */

/* not type checking this file because flow doesn't play well with Proxy */

var initProxy;

if (true) {
  var allowedGlobals = makeMap(
    'Infinity,undefined,NaN,isFinite,isNaN,' +
    'parseFloat,parseInt,decodeURI,decodeURIComponent,encodeURI,encodeURIComponent,' +
    'Math,Number,Date,Array,Object,Boolean,String,RegExp,Map,Set,JSON,Intl,' +
    'require' // for Webpack/Browserify
  );

  var warnNonPresent = function (target, key) {
    warn(
      "Property or method \"" + key + "\" is not defined on the instance but " +
      'referenced during render. Make sure that this property is reactive, ' +
      'either in the data option, or for class-based components, by ' +
      'initializing the property. ' +
      'See: https://vuejs.org/v2/guide/reactivity.html#Declaring-Reactive-Properties.',
      target
    );
  };

  var warnReservedPrefix = function (target, key) {
    warn(
      "Property \"" + key + "\" must be accessed with \"$data." + key + "\" because " +
      'properties starting with "$" or "_" are not proxied in the Vue instance to ' +
      'prevent conflicts with Vue internals. ' +
      'See: https://vuejs.org/v2/api/#data',
      target
    );
  };

  var hasProxy =
    typeof Proxy !== 'undefined' && isNative(Proxy);

  if (hasProxy) {
    var isBuiltInModifier = makeMap('stop,prevent,self,ctrl,shift,alt,meta,exact');
    config.keyCodes = new Proxy(config.keyCodes, {
      set: function set (target, key, value) {
        if (isBuiltInModifier(key)) {
          warn(("Avoid overwriting built-in modifier in config.keyCodes: ." + key));
          return false
        } else {
          target[key] = value;
          return true
        }
      }
    });
  }

  var hasHandler = {
    has: function has (target, key) {
      var has = key in target;
      var isAllowed = allowedGlobals(key) ||
        (typeof key === 'string' && key.charAt(0) === '_' && !(key in target.$data));
      if (!has && !isAllowed) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return has || !isAllowed
    }
  };

  var getHandler = {
    get: function get (target, key) {
      if (typeof key === 'string' && !(key in target)) {
        if (key in target.$data) { warnReservedPrefix(target, key); }
        else { warnNonPresent(target, key); }
      }
      return target[key]
    }
  };

  initProxy = function initProxy (vm) {
    if (hasProxy) {
      // determine which proxy handler to use
      var options = vm.$options;
      var handlers = options.render && options.render._withStripped
        ? getHandler
        : hasHandler;
      vm._renderProxy = new Proxy(vm, handlers);
    } else {
      vm._renderProxy = vm;
    }
  };
}

/*  */

var seenObjects = new _Set();

/**
 * Recursively traverse an object to evoke all converted
 * getters, so that every nested property inside the object
 * is collected as a "deep" dependency.
 */
function traverse (val) {
  _traverse(val, seenObjects);
  seenObjects.clear();
}

function _traverse (val, seen) {
  var i, keys;
  var isA = Array.isArray(val);
  if ((!isA && !isObject(val)) || Object.isFrozen(val) || val instanceof VNode) {
    return
  }
  if (val.__ob__) {
    var depId = val.__ob__.dep.id;
    if (seen.has(depId)) {
      return
    }
    seen.add(depId);
  }
  if (isA) {
    i = val.length;
    while (i--) { _traverse(val[i], seen); }
  } else {
    keys = Object.keys(val);
    i = keys.length;
    while (i--) { _traverse(val[keys[i]], seen); }
  }
}

var mark;
var measure;

if (true) {
  var perf = inBrowser && window.performance;
  /* istanbul ignore if */
  if (
    perf &&
    perf.mark &&
    perf.measure &&
    perf.clearMarks &&
    perf.clearMeasures
  ) {
    mark = function (tag) { return perf.mark(tag); };
    measure = function (name, startTag, endTag) {
      perf.measure(name, startTag, endTag);
      perf.clearMarks(startTag);
      perf.clearMarks(endTag);
      // perf.clearMeasures(name)
    };
  }
}

/*  */

var normalizeEvent = cached(function (name) {
  var passive = name.charAt(0) === '&';
  name = passive ? name.slice(1) : name;
  var once$$1 = name.charAt(0) === '~'; // Prefixed last, checked first
  name = once$$1 ? name.slice(1) : name;
  var capture = name.charAt(0) === '!';
  name = capture ? name.slice(1) : name;
  return {
    name: name,
    once: once$$1,
    capture: capture,
    passive: passive
  }
});

function createFnInvoker (fns, vm) {
  function invoker () {
    var arguments$1 = arguments;

    var fns = invoker.fns;
    if (Array.isArray(fns)) {
      var cloned = fns.slice();
      for (var i = 0; i < cloned.length; i++) {
        invokeWithErrorHandling(cloned[i], null, arguments$1, vm, "v-on handler");
      }
    } else {
      // return handler return value for single handlers
      return invokeWithErrorHandling(fns, null, arguments, vm, "v-on handler")
    }
  }
  invoker.fns = fns;
  return invoker
}

function updateListeners (
  on,
  oldOn,
  add,
  remove$$1,
  createOnceHandler,
  vm
) {
  var name, def$$1, cur, old, event;
  for (name in on) {
    def$$1 = cur = on[name];
    old = oldOn[name];
    event = normalizeEvent(name);
    if (isUndef(cur)) {
       true && warn(
        "Invalid handler for event \"" + (event.name) + "\": got " + String(cur),
        vm
      );
    } else if (isUndef(old)) {
      if (isUndef(cur.fns)) {
        cur = on[name] = createFnInvoker(cur, vm);
      }
      if (isTrue(event.once)) {
        cur = on[name] = createOnceHandler(event.name, cur, event.capture);
      }
      add(event.name, cur, event.capture, event.passive, event.params);
    } else if (cur !== old) {
      old.fns = cur;
      on[name] = old;
    }
  }
  for (name in oldOn) {
    if (isUndef(on[name])) {
      event = normalizeEvent(name);
      remove$$1(event.name, oldOn[name], event.capture);
    }
  }
}

/*  */

/*  */

// fixed by xxxxxx (mp properties)
function extractPropertiesFromVNodeData(data, Ctor, res, context) {
  var propOptions = Ctor.options.mpOptions && Ctor.options.mpOptions.properties;
  if (isUndef(propOptions)) {
    return res
  }
  var externalClasses = Ctor.options.mpOptions.externalClasses || [];
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      var result = checkProp(res, props, key, altKey, true) ||
          checkProp(res, attrs, key, altKey, false);
      // externalClass
      if (
        result &&
        res[key] &&
        externalClasses.indexOf(altKey) !== -1 &&
        context[camelize(res[key])]
      ) {
        // 赋值 externalClass 真正的值(模板里 externalClass 的值可能是字符串)
        res[key] = context[camelize(res[key])];
      }
    }
  }
  return res
}

function extractPropsFromVNodeData (
  data,
  Ctor,
  tag,
  context// fixed by xxxxxx
) {
  // we are only extracting raw values here.
  // validation and default values are handled in the child
  // component itself.
  var propOptions = Ctor.options.props;
  if (isUndef(propOptions)) {
    // fixed by xxxxxx
    return extractPropertiesFromVNodeData(data, Ctor, {}, context)
  }
  var res = {};
  var attrs = data.attrs;
  var props = data.props;
  if (isDef(attrs) || isDef(props)) {
    for (var key in propOptions) {
      var altKey = hyphenate(key);
      if (true) {
        var keyInLowerCase = key.toLowerCase();
        if (
          key !== keyInLowerCase &&
          attrs && hasOwn(attrs, keyInLowerCase)
        ) {
          tip(
            "Prop \"" + keyInLowerCase + "\" is passed to component " +
            (formatComponentName(tag || Ctor)) + ", but the declared prop name is" +
            " \"" + key + "\". " +
            "Note that HTML attributes are case-insensitive and camelCased " +
            "props need to use their kebab-case equivalents when using in-DOM " +
            "templates. You should probably use \"" + altKey + "\" instead of \"" + key + "\"."
          );
        }
      }
      checkProp(res, props, key, altKey, true) ||
      checkProp(res, attrs, key, altKey, false);
    }
  }
  // fixed by xxxxxx
  return extractPropertiesFromVNodeData(data, Ctor, res, context)
}

function checkProp (
  res,
  hash,
  key,
  altKey,
  preserve
) {
  if (isDef(hash)) {
    if (hasOwn(hash, key)) {
      res[key] = hash[key];
      if (!preserve) {
        delete hash[key];
      }
      return true
    } else if (hasOwn(hash, altKey)) {
      res[key] = hash[altKey];
      if (!preserve) {
        delete hash[altKey];
      }
      return true
    }
  }
  return false
}

/*  */

// The template compiler attempts to minimize the need for normalization by
// statically analyzing the template at compile time.
//
// For plain HTML markup, normalization can be completely skipped because the
// generated render function is guaranteed to return Array<VNode>. There are
// two cases where extra normalization is needed:

// 1. When the children contains components - because a functional component
// may return an Array instead of a single root. In this case, just a simple
// normalization is needed - if any child is an Array, we flatten the whole
// thing with Array.prototype.concat. It is guaranteed to be only 1-level deep
// because functional components already normalize their own children.
function simpleNormalizeChildren (children) {
  for (var i = 0; i < children.length; i++) {
    if (Array.isArray(children[i])) {
      return Array.prototype.concat.apply([], children)
    }
  }
  return children
}

// 2. When the children contains constructs that always generated nested Arrays,
// e.g. <template>, <slot>, v-for, or when the children is provided by user
// with hand-written render functions / JSX. In such cases a full normalization
// is needed to cater to all possible types of children values.
function normalizeChildren (children) {
  return isPrimitive(children)
    ? [createTextVNode(children)]
    : Array.isArray(children)
      ? normalizeArrayChildren(children)
      : undefined
}

function isTextNode (node) {
  return isDef(node) && isDef(node.text) && isFalse(node.isComment)
}

function normalizeArrayChildren (children, nestedIndex) {
  var res = [];
  var i, c, lastIndex, last;
  for (i = 0; i < children.length; i++) {
    c = children[i];
    if (isUndef(c) || typeof c === 'boolean') { continue }
    lastIndex = res.length - 1;
    last = res[lastIndex];
    //  nested
    if (Array.isArray(c)) {
      if (c.length > 0) {
        c = normalizeArrayChildren(c, ((nestedIndex || '') + "_" + i));
        // merge adjacent text nodes
        if (isTextNode(c[0]) && isTextNode(last)) {
          res[lastIndex] = createTextVNode(last.text + (c[0]).text);
          c.shift();
        }
        res.push.apply(res, c);
      }
    } else if (isPrimitive(c)) {
      if (isTextNode(last)) {
        // merge adjacent text nodes
        // this is necessary for SSR hydration because text nodes are
        // essentially merged when rendered to HTML strings
        res[lastIndex] = createTextVNode(last.text + c);
      } else if (c !== '') {
        // convert primitive to vnode
        res.push(createTextVNode(c));
      }
    } else {
      if (isTextNode(c) && isTextNode(last)) {
        // merge adjacent text nodes
        res[lastIndex] = createTextVNode(last.text + c.text);
      } else {
        // default key for nested array children (likely generated by v-for)
        if (isTrue(children._isVList) &&
          isDef(c.tag) &&
          isUndef(c.key) &&
          isDef(nestedIndex)) {
          c.key = "__vlist" + nestedIndex + "_" + i + "__";
        }
        res.push(c);
      }
    }
  }
  return res
}

/*  */

function initProvide (vm) {
  var provide = vm.$options.provide;
  if (provide) {
    vm._provided = typeof provide === 'function'
      ? provide.call(vm)
      : provide;
  }
}

function initInjections (vm) {
  var result = resolveInject(vm.$options.inject, vm);
  if (result) {
    toggleObserving(false);
    Object.keys(result).forEach(function (key) {
      /* istanbul ignore else */
      if (true) {
        defineReactive$$1(vm, key, result[key], function () {
          warn(
            "Avoid mutating an injected value directly since the changes will be " +
            "overwritten whenever the provided component re-renders. " +
            "injection being mutated: \"" + key + "\"",
            vm
          );
        });
      } else {}
    });
    toggleObserving(true);
  }
}

function resolveInject (inject, vm) {
  if (inject) {
    // inject is :any because flow is not smart enough to figure out cached
    var result = Object.create(null);
    var keys = hasSymbol
      ? Reflect.ownKeys(inject)
      : Object.keys(inject);

    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      // #6574 in case the inject object is observed...
      if (key === '__ob__') { continue }
      var provideKey = inject[key].from;
      var source = vm;
      while (source) {
        if (source._provided && hasOwn(source._provided, provideKey)) {
          result[key] = source._provided[provideKey];
          break
        }
        source = source.$parent;
      }
      if (!source) {
        if ('default' in inject[key]) {
          var provideDefault = inject[key].default;
          result[key] = typeof provideDefault === 'function'
            ? provideDefault.call(vm)
            : provideDefault;
        } else if (true) {
          warn(("Injection \"" + key + "\" not found"), vm);
        }
      }
    }
    return result
  }
}

/*  */



/**
 * Runtime helper for resolving raw children VNodes into a slot object.
 */
function resolveSlots (
  children,
  context
) {
  if (!children || !children.length) {
    return {}
  }
  var slots = {};
  for (var i = 0, l = children.length; i < l; i++) {
    var child = children[i];
    var data = child.data;
    // remove slot attribute if the node is resolved as a Vue slot node
    if (data && data.attrs && data.attrs.slot) {
      delete data.attrs.slot;
    }
    // named slots should only be respected if the vnode was rendered in the
    // same context.
    if ((child.context === context || child.fnContext === context) &&
      data && data.slot != null
    ) {
      var name = data.slot;
      var slot = (slots[name] || (slots[name] = []));
      if (child.tag === 'template') {
        slot.push.apply(slot, child.children || []);
      } else {
        slot.push(child);
      }
    } else {
      // fixed by xxxxxx 临时 hack 掉 uni-app 中的异步 name slot page
      if(child.asyncMeta && child.asyncMeta.data && child.asyncMeta.data.slot === 'page'){
        (slots['page'] || (slots['page'] = [])).push(child);
      }else{
        (slots.default || (slots.default = [])).push(child);
      }
    }
  }
  // ignore slots that contains only whitespace
  for (var name$1 in slots) {
    if (slots[name$1].every(isWhitespace)) {
      delete slots[name$1];
    }
  }
  return slots
}

function isWhitespace (node) {
  return (node.isComment && !node.asyncFactory) || node.text === ' '
}

/*  */

function normalizeScopedSlots (
  slots,
  normalSlots,
  prevSlots
) {
  var res;
  var hasNormalSlots = Object.keys(normalSlots).length > 0;
  var isStable = slots ? !!slots.$stable : !hasNormalSlots;
  var key = slots && slots.$key;
  if (!slots) {
    res = {};
  } else if (slots._normalized) {
    // fast path 1: child component re-render only, parent did not change
    return slots._normalized
  } else if (
    isStable &&
    prevSlots &&
    prevSlots !== emptyObject &&
    key === prevSlots.$key &&
    !hasNormalSlots &&
    !prevSlots.$hasNormal
  ) {
    // fast path 2: stable scoped slots w/ no normal slots to proxy,
    // only need to normalize once
    return prevSlots
  } else {
    res = {};
    for (var key$1 in slots) {
      if (slots[key$1] && key$1[0] !== '$') {
        res[key$1] = normalizeScopedSlot(normalSlots, key$1, slots[key$1]);
      }
    }
  }
  // expose normal slots on scopedSlots
  for (var key$2 in normalSlots) {
    if (!(key$2 in res)) {
      res[key$2] = proxyNormalSlot(normalSlots, key$2);
    }
  }
  // avoriaz seems to mock a non-extensible $scopedSlots object
  // and when that is passed down this would cause an error
  if (slots && Object.isExtensible(slots)) {
    (slots)._normalized = res;
  }
  def(res, '$stable', isStable);
  def(res, '$key', key);
  def(res, '$hasNormal', hasNormalSlots);
  return res
}

function normalizeScopedSlot(normalSlots, key, fn) {
  var normalized = function () {
    var res = arguments.length ? fn.apply(null, arguments) : fn({});
    res = res && typeof res === 'object' && !Array.isArray(res)
      ? [res] // single vnode
      : normalizeChildren(res);
    return res && (
      res.length === 0 ||
      (res.length === 1 && res[0].isComment) // #9658
    ) ? undefined
      : res
  };
  // this is a slot using the new v-slot syntax without scope. although it is
  // compiled as a scoped slot, render fn users would expect it to be present
  // on this.$slots because the usage is semantically a normal slot.
  if (fn.proxy) {
    Object.defineProperty(normalSlots, key, {
      get: normalized,
      enumerable: true,
      configurable: true
    });
  }
  return normalized
}

function proxyNormalSlot(slots, key) {
  return function () { return slots[key]; }
}

/*  */

/**
 * Runtime helper for rendering v-for lists.
 */
function renderList (
  val,
  render
) {
  var ret, i, l, keys, key;
  if (Array.isArray(val) || typeof val === 'string') {
    ret = new Array(val.length);
    for (i = 0, l = val.length; i < l; i++) {
      ret[i] = render(val[i], i, i, i); // fixed by xxxxxx
    }
  } else if (typeof val === 'number') {
    ret = new Array(val);
    for (i = 0; i < val; i++) {
      ret[i] = render(i + 1, i, i, i); // fixed by xxxxxx
    }
  } else if (isObject(val)) {
    if (hasSymbol && val[Symbol.iterator]) {
      ret = [];
      var iterator = val[Symbol.iterator]();
      var result = iterator.next();
      while (!result.done) {
        ret.push(render(result.value, ret.length, i, i++)); // fixed by xxxxxx
        result = iterator.next();
      }
    } else {
      keys = Object.keys(val);
      ret = new Array(keys.length);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[i] = render(val[key], key, i, i); // fixed by xxxxxx
      }
    }
  }
  if (!isDef(ret)) {
    ret = [];
  }
  (ret)._isVList = true;
  return ret
}

/*  */

/**
 * Runtime helper for rendering <slot>
 */
function renderSlot (
  name,
  fallback,
  props,
  bindObject
) {
  var scopedSlotFn = this.$scopedSlots[name];
  var nodes;
  if (scopedSlotFn) { // scoped slot
    props = props || {};
    if (bindObject) {
      if ( true && !isObject(bindObject)) {
        warn(
          'slot v-bind without argument expects an Object',
          this
        );
      }
      props = extend(extend({}, bindObject), props);
    }
    // fixed by xxxxxx app-plus scopedSlot
    nodes = scopedSlotFn(props, this, props._i) || fallback;
  } else {
    nodes = this.$slots[name] || fallback;
  }

  var target = props && props.slot;
  if (target) {
    return this.$createElement('template', { slot: target }, nodes)
  } else {
    return nodes
  }
}

/*  */

/**
 * Runtime helper for resolving filters
 */
function resolveFilter (id) {
  return resolveAsset(this.$options, 'filters', id, true) || identity
}

/*  */

function isKeyNotMatch (expect, actual) {
  if (Array.isArray(expect)) {
    return expect.indexOf(actual) === -1
  } else {
    return expect !== actual
  }
}

/**
 * Runtime helper for checking keyCodes from config.
 * exposed as Vue.prototype._k
 * passing in eventKeyName as last argument separately for backwards compat
 */
function checkKeyCodes (
  eventKeyCode,
  key,
  builtInKeyCode,
  eventKeyName,
  builtInKeyName
) {
  var mappedKeyCode = config.keyCodes[key] || builtInKeyCode;
  if (builtInKeyName && eventKeyName && !config.keyCodes[key]) {
    return isKeyNotMatch(builtInKeyName, eventKeyName)
  } else if (mappedKeyCode) {
    return isKeyNotMatch(mappedKeyCode, eventKeyCode)
  } else if (eventKeyName) {
    return hyphenate(eventKeyName) !== key
  }
}

/*  */

/**
 * Runtime helper for merging v-bind="object" into a VNode's data.
 */
function bindObjectProps (
  data,
  tag,
  value,
  asProp,
  isSync
) {
  if (value) {
    if (!isObject(value)) {
       true && warn(
        'v-bind without argument expects an Object or Array value',
        this
      );
    } else {
      if (Array.isArray(value)) {
        value = toObject(value);
      }
      var hash;
      var loop = function ( key ) {
        if (
          key === 'class' ||
          key === 'style' ||
          isReservedAttribute(key)
        ) {
          hash = data;
        } else {
          var type = data.attrs && data.attrs.type;
          hash = asProp || config.mustUseProp(tag, type, key)
            ? data.domProps || (data.domProps = {})
            : data.attrs || (data.attrs = {});
        }
        var camelizedKey = camelize(key);
        var hyphenatedKey = hyphenate(key);
        if (!(camelizedKey in hash) && !(hyphenatedKey in hash)) {
          hash[key] = value[key];

          if (isSync) {
            var on = data.on || (data.on = {});
            on[("update:" + key)] = function ($event) {
              value[key] = $event;
            };
          }
        }
      };

      for (var key in value) loop( key );
    }
  }
  return data
}

/*  */

/**
 * Runtime helper for rendering static trees.
 */
function renderStatic (
  index,
  isInFor
) {
  var cached = this._staticTrees || (this._staticTrees = []);
  var tree = cached[index];
  // if has already-rendered static tree and not inside v-for,
  // we can reuse the same tree.
  if (tree && !isInFor) {
    return tree
  }
  // otherwise, render a fresh tree.
  tree = cached[index] = this.$options.staticRenderFns[index].call(
    this._renderProxy,
    null,
    this // for render fns generated for functional component templates
  );
  markStatic(tree, ("__static__" + index), false);
  return tree
}

/**
 * Runtime helper for v-once.
 * Effectively it means marking the node as static with a unique key.
 */
function markOnce (
  tree,
  index,
  key
) {
  markStatic(tree, ("__once__" + index + (key ? ("_" + key) : "")), true);
  return tree
}

function markStatic (
  tree,
  key,
  isOnce
) {
  if (Array.isArray(tree)) {
    for (var i = 0; i < tree.length; i++) {
      if (tree[i] && typeof tree[i] !== 'string') {
        markStaticNode(tree[i], (key + "_" + i), isOnce);
      }
    }
  } else {
    markStaticNode(tree, key, isOnce);
  }
}

function markStaticNode (node, key, isOnce) {
  node.isStatic = true;
  node.key = key;
  node.isOnce = isOnce;
}

/*  */

function bindObjectListeners (data, value) {
  if (value) {
    if (!isPlainObject(value)) {
       true && warn(
        'v-on without argument expects an Object value',
        this
      );
    } else {
      var on = data.on = data.on ? extend({}, data.on) : {};
      for (var key in value) {
        var existing = on[key];
        var ours = value[key];
        on[key] = existing ? [].concat(existing, ours) : ours;
      }
    }
  }
  return data
}

/*  */

function resolveScopedSlots (
  fns, // see flow/vnode
  res,
  // the following are added in 2.6
  hasDynamicKeys,
  contentHashKey
) {
  res = res || { $stable: !hasDynamicKeys };
  for (var i = 0; i < fns.length; i++) {
    var slot = fns[i];
    if (Array.isArray(slot)) {
      resolveScopedSlots(slot, res, hasDynamicKeys);
    } else if (slot) {
      // marker for reverse proxying v-slot without scope on this.$slots
      if (slot.proxy) {
        slot.fn.proxy = true;
      }
      res[slot.key] = slot.fn;
    }
  }
  if (contentHashKey) {
    (res).$key = contentHashKey;
  }
  return res
}

/*  */

function bindDynamicKeys (baseObj, values) {
  for (var i = 0; i < values.length; i += 2) {
    var key = values[i];
    if (typeof key === 'string' && key) {
      baseObj[values[i]] = values[i + 1];
    } else if ( true && key !== '' && key !== null) {
      // null is a special value for explicitly removing a binding
      warn(
        ("Invalid value for dynamic directive argument (expected string or null): " + key),
        this
      );
    }
  }
  return baseObj
}

// helper to dynamically append modifier runtime markers to event names.
// ensure only append when value is already string, otherwise it will be cast
// to string and cause the type check to miss.
function prependModifier (value, symbol) {
  return typeof value === 'string' ? symbol + value : value
}

/*  */

function installRenderHelpers (target) {
  target._o = markOnce;
  target._n = toNumber;
  target._s = toString;
  target._l = renderList;
  target._t = renderSlot;
  target._q = looseEqual;
  target._i = looseIndexOf;
  target._m = renderStatic;
  target._f = resolveFilter;
  target._k = checkKeyCodes;
  target._b = bindObjectProps;
  target._v = createTextVNode;
  target._e = createEmptyVNode;
  target._u = resolveScopedSlots;
  target._g = bindObjectListeners;
  target._d = bindDynamicKeys;
  target._p = prependModifier;
}

/*  */

function FunctionalRenderContext (
  data,
  props,
  children,
  parent,
  Ctor
) {
  var this$1 = this;

  var options = Ctor.options;
  // ensure the createElement function in functional components
  // gets a unique context - this is necessary for correct named slot check
  var contextVm;
  if (hasOwn(parent, '_uid')) {
    contextVm = Object.create(parent);
    // $flow-disable-line
    contextVm._original = parent;
  } else {
    // the context vm passed in is a functional context as well.
    // in this case we want to make sure we are able to get a hold to the
    // real context instance.
    contextVm = parent;
    // $flow-disable-line
    parent = parent._original;
  }
  var isCompiled = isTrue(options._compiled);
  var needNormalization = !isCompiled;

  this.data = data;
  this.props = props;
  this.children = children;
  this.parent = parent;
  this.listeners = data.on || emptyObject;
  this.injections = resolveInject(options.inject, parent);
  this.slots = function () {
    if (!this$1.$slots) {
      normalizeScopedSlots(
        data.scopedSlots,
        this$1.$slots = resolveSlots(children, parent)
      );
    }
    return this$1.$slots
  };

  Object.defineProperty(this, 'scopedSlots', ({
    enumerable: true,
    get: function get () {
      return normalizeScopedSlots(data.scopedSlots, this.slots())
    }
  }));

  // support for compiled functional template
  if (isCompiled) {
    // exposing $options for renderStatic()
    this.$options = options;
    // pre-resolve slots for renderSlot()
    this.$slots = this.slots();
    this.$scopedSlots = normalizeScopedSlots(data.scopedSlots, this.$slots);
  }

  if (options._scopeId) {
    this._c = function (a, b, c, d) {
      var vnode = createElement(contextVm, a, b, c, d, needNormalization);
      if (vnode && !Array.isArray(vnode)) {
        vnode.fnScopeId = options._scopeId;
        vnode.fnContext = parent;
      }
      return vnode
    };
  } else {
    this._c = function (a, b, c, d) { return createElement(contextVm, a, b, c, d, needNormalization); };
  }
}

installRenderHelpers(FunctionalRenderContext.prototype);

function createFunctionalComponent (
  Ctor,
  propsData,
  data,
  contextVm,
  children
) {
  var options = Ctor.options;
  var props = {};
  var propOptions = options.props;
  if (isDef(propOptions)) {
    for (var key in propOptions) {
      props[key] = validateProp(key, propOptions, propsData || emptyObject);
    }
  } else {
    if (isDef(data.attrs)) { mergeProps(props, data.attrs); }
    if (isDef(data.props)) { mergeProps(props, data.props); }
  }

  var renderContext = new FunctionalRenderContext(
    data,
    props,
    children,
    contextVm,
    Ctor
  );

  var vnode = options.render.call(null, renderContext._c, renderContext);

  if (vnode instanceof VNode) {
    return cloneAndMarkFunctionalResult(vnode, data, renderContext.parent, options, renderContext)
  } else if (Array.isArray(vnode)) {
    var vnodes = normalizeChildren(vnode) || [];
    var res = new Array(vnodes.length);
    for (var i = 0; i < vnodes.length; i++) {
      res[i] = cloneAndMarkFunctionalResult(vnodes[i], data, renderContext.parent, options, renderContext);
    }
    return res
  }
}

function cloneAndMarkFunctionalResult (vnode, data, contextVm, options, renderContext) {
  // #7817 clone node before setting fnContext, otherwise if the node is reused
  // (e.g. it was from a cached normal slot) the fnContext causes named slots
  // that should not be matched to match.
  var clone = cloneVNode(vnode);
  clone.fnContext = contextVm;
  clone.fnOptions = options;
  if (true) {
    (clone.devtoolsMeta = clone.devtoolsMeta || {}).renderContext = renderContext;
  }
  if (data.slot) {
    (clone.data || (clone.data = {})).slot = data.slot;
  }
  return clone
}

function mergeProps (to, from) {
  for (var key in from) {
    to[camelize(key)] = from[key];
  }
}

/*  */

/*  */

/*  */

/*  */

// inline hooks to be invoked on component VNodes during patch
var componentVNodeHooks = {
  init: function init (vnode, hydrating) {
    if (
      vnode.componentInstance &&
      !vnode.componentInstance._isDestroyed &&
      vnode.data.keepAlive
    ) {
      // kept-alive components, treat as a patch
      var mountedNode = vnode; // work around flow
      componentVNodeHooks.prepatch(mountedNode, mountedNode);
    } else {
      var child = vnode.componentInstance = createComponentInstanceForVnode(
        vnode,
        activeInstance
      );
      child.$mount(hydrating ? vnode.elm : undefined, hydrating);
    }
  },

  prepatch: function prepatch (oldVnode, vnode) {
    var options = vnode.componentOptions;
    var child = vnode.componentInstance = oldVnode.componentInstance;
    updateChildComponent(
      child,
      options.propsData, // updated props
      options.listeners, // updated listeners
      vnode, // new parent vnode
      options.children // new children
    );
  },

  insert: function insert (vnode) {
    var context = vnode.context;
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isMounted) {
      callHook(componentInstance, 'onServiceCreated');
      callHook(componentInstance, 'onServiceAttached');
      componentInstance._isMounted = true;
      callHook(componentInstance, 'mounted');
    }
    if (vnode.data.keepAlive) {
      if (context._isMounted) {
        // vue-router#1212
        // During updates, a kept-alive component's child components may
        // change, so directly walking the tree here may call activated hooks
        // on incorrect children. Instead we push them into a queue which will
        // be processed after the whole patch process ended.
        queueActivatedComponent(componentInstance);
      } else {
        activateChildComponent(componentInstance, true /* direct */);
      }
    }
  },

  destroy: function destroy (vnode) {
    var componentInstance = vnode.componentInstance;
    if (!componentInstance._isDestroyed) {
      if (!vnode.data.keepAlive) {
        componentInstance.$destroy();
      } else {
        deactivateChildComponent(componentInstance, true /* direct */);
      }
    }
  }
};

var hooksToMerge = Object.keys(componentVNodeHooks);

function createComponent (
  Ctor,
  data,
  context,
  children,
  tag
) {
  if (isUndef(Ctor)) {
    return
  }

  var baseCtor = context.$options._base;

  // plain options object: turn it into a constructor
  if (isObject(Ctor)) {
    Ctor = baseCtor.extend(Ctor);
  }

  // if at this stage it's not a constructor or an async component factory,
  // reject.
  if (typeof Ctor !== 'function') {
    if (true) {
      warn(("Invalid Component definition: " + (String(Ctor))), context);
    }
    return
  }

  // async component
  var asyncFactory;
  if (isUndef(Ctor.cid)) {
    asyncFactory = Ctor;
    Ctor = resolveAsyncComponent(asyncFactory, baseCtor);
    if (Ctor === undefined) {
      // return a placeholder node for async component, which is rendered
      // as a comment node but preserves all the raw information for the node.
      // the information will be used for async server-rendering and hydration.
      return createAsyncPlaceholder(
        asyncFactory,
        data,
        context,
        children,
        tag
      )
    }
  }

  data = data || {};

  // resolve constructor options in case global mixins are applied after
  // component constructor creation
  resolveConstructorOptions(Ctor);

  // transform component v-model data into props & events
  if (isDef(data.model)) {
    transformModel(Ctor.options, data);
  }

  // extract props
  var propsData = extractPropsFromVNodeData(data, Ctor, tag, context); // fixed by xxxxxx

  // functional component
  if (isTrue(Ctor.options.functional)) {
    return createFunctionalComponent(Ctor, propsData, data, context, children)
  }

  // extract listeners, since these needs to be treated as
  // child component listeners instead of DOM listeners
  var listeners = data.on;
  // replace with listeners with .native modifier
  // so it gets processed during parent component patch.
  data.on = data.nativeOn;

  if (isTrue(Ctor.options.abstract)) {
    // abstract components do not keep anything
    // other than props & listeners & slot

    // work around flow
    var slot = data.slot;
    data = {};
    if (slot) {
      data.slot = slot;
    }
  }

  // install component management hooks onto the placeholder node
  installComponentHooks(data);

  // return a placeholder vnode
  var name = Ctor.options.name || tag;
  var vnode = new VNode(
    ("vue-component-" + (Ctor.cid) + (name ? ("-" + name) : '')),
    data, undefined, undefined, undefined, context,
    { Ctor: Ctor, propsData: propsData, listeners: listeners, tag: tag, children: children },
    asyncFactory
  );

  return vnode
}

function createComponentInstanceForVnode (
  vnode, // we know it's MountedComponentVNode but flow doesn't
  parent // activeInstance in lifecycle state
) {
  var options = {
    _isComponent: true,
    _parentVnode: vnode,
    parent: parent
  };
  // check inline-template render functions
  var inlineTemplate = vnode.data.inlineTemplate;
  if (isDef(inlineTemplate)) {
    options.render = inlineTemplate.render;
    options.staticRenderFns = inlineTemplate.staticRenderFns;
  }
  return new vnode.componentOptions.Ctor(options)
}

function installComponentHooks (data) {
  var hooks = data.hook || (data.hook = {});
  for (var i = 0; i < hooksToMerge.length; i++) {
    var key = hooksToMerge[i];
    var existing = hooks[key];
    var toMerge = componentVNodeHooks[key];
    if (existing !== toMerge && !(existing && existing._merged)) {
      hooks[key] = existing ? mergeHook$1(toMerge, existing) : toMerge;
    }
  }
}

function mergeHook$1 (f1, f2) {
  var merged = function (a, b) {
    // flow complains about extra args which is why we use any
    f1(a, b);
    f2(a, b);
  };
  merged._merged = true;
  return merged
}

// transform component v-model info (value and callback) into
// prop and event handler respectively.
function transformModel (options, data) {
  var prop = (options.model && options.model.prop) || 'value';
  var event = (options.model && options.model.event) || 'input'
  ;(data.attrs || (data.attrs = {}))[prop] = data.model.value;
  var on = data.on || (data.on = {});
  var existing = on[event];
  var callback = data.model.callback;
  if (isDef(existing)) {
    if (
      Array.isArray(existing)
        ? existing.indexOf(callback) === -1
        : existing !== callback
    ) {
      on[event] = [callback].concat(existing);
    }
  } else {
    on[event] = callback;
  }
}

/*  */

var SIMPLE_NORMALIZE = 1;
var ALWAYS_NORMALIZE = 2;

// wrapper function for providing a more flexible interface
// without getting yelled at by flow
function createElement (
  context,
  tag,
  data,
  children,
  normalizationType,
  alwaysNormalize
) {
  if (Array.isArray(data) || isPrimitive(data)) {
    normalizationType = children;
    children = data;
    data = undefined;
  }
  if (isTrue(alwaysNormalize)) {
    normalizationType = ALWAYS_NORMALIZE;
  }
  return _createElement(context, tag, data, children, normalizationType)
}

function _createElement (
  context,
  tag,
  data,
  children,
  normalizationType
) {
  if (isDef(data) && isDef((data).__ob__)) {
     true && warn(
      "Avoid using observed data object as vnode data: " + (JSON.stringify(data)) + "\n" +
      'Always create fresh vnode data objects in each render!',
      context
    );
    return createEmptyVNode()
  }
  // object syntax in v-bind
  if (isDef(data) && isDef(data.is)) {
    tag = data.is;
  }
  if (!tag) {
    // in case of component :is set to falsy value
    return createEmptyVNode()
  }
  // warn against non-primitive key
  if ( true &&
    isDef(data) && isDef(data.key) && !isPrimitive(data.key)
  ) {
    {
      warn(
        'Avoid using non-primitive value as key, ' +
        'use string/number value instead.',
        context
      );
    }
  }
  // support single function children as default scoped slot
  if (Array.isArray(children) &&
    typeof children[0] === 'function'
  ) {
    data = data || {};
    data.scopedSlots = { default: children[0] };
    children.length = 0;
  }
  if (normalizationType === ALWAYS_NORMALIZE) {
    children = normalizeChildren(children);
  } else if (normalizationType === SIMPLE_NORMALIZE) {
    children = simpleNormalizeChildren(children);
  }
  var vnode, ns;
  if (typeof tag === 'string') {
    var Ctor;
    ns = (context.$vnode && context.$vnode.ns) || config.getTagNamespace(tag);
    if (config.isReservedTag(tag)) {
      // platform built-in elements
      if ( true && isDef(data) && isDef(data.nativeOn)) {
        warn(
          ("The .native modifier for v-on is only valid on components but it was used on <" + tag + ">."),
          context
        );
      }
      vnode = new VNode(
        config.parsePlatformTagName(tag), data, children,
        undefined, undefined, context
      );
    } else if ((!data || !data.pre) && isDef(Ctor = resolveAsset(context.$options, 'components', tag))) {
      // component
      vnode = createComponent(Ctor, data, context, children, tag);
    } else {
      // unknown or unlisted namespaced elements
      // check at runtime because it may get assigned a namespace when its
      // parent normalizes children
      vnode = new VNode(
        tag, data, children,
        undefined, undefined, context
      );
    }
  } else {
    // direct component options / constructor
    vnode = createComponent(tag, data, context, children);
  }
  if (Array.isArray(vnode)) {
    return vnode
  } else if (isDef(vnode)) {
    if (isDef(ns)) { applyNS(vnode, ns); }
    if (isDef(data)) { registerDeepBindings(data); }
    return vnode
  } else {
    return createEmptyVNode()
  }
}

function applyNS (vnode, ns, force) {
  vnode.ns = ns;
  if (vnode.tag === 'foreignObject') {
    // use default namespace inside foreignObject
    ns = undefined;
    force = true;
  }
  if (isDef(vnode.children)) {
    for (var i = 0, l = vnode.children.length; i < l; i++) {
      var child = vnode.children[i];
      if (isDef(child.tag) && (
        isUndef(child.ns) || (isTrue(force) && child.tag !== 'svg'))) {
        applyNS(child, ns, force);
      }
    }
  }
}

// ref #5318
// necessary to ensure parent re-render when deep bindings like :style and
// :class are used on slot nodes
function registerDeepBindings (data) {
  if (isObject(data.style)) {
    traverse(data.style);
  }
  if (isObject(data.class)) {
    traverse(data.class);
  }
}

/*  */

function initRender (vm) {
  vm._vnode = null; // the root of the child tree
  vm._staticTrees = null; // v-once cached trees
  var options = vm.$options;
  var parentVnode = vm.$vnode = options._parentVnode; // the placeholder node in parent tree
  var renderContext = parentVnode && parentVnode.context;
  vm.$slots = resolveSlots(options._renderChildren, renderContext);
  vm.$scopedSlots = emptyObject;
  // bind the createElement fn to this instance
  // so that we get proper render context inside it.
  // args order: tag, data, children, normalizationType, alwaysNormalize
  // internal version is used by render functions compiled from templates
  vm._c = function (a, b, c, d) { return createElement(vm, a, b, c, d, false); };
  // normalization is always applied for the public version, used in
  // user-written render functions.
  vm.$createElement = function (a, b, c, d) { return createElement(vm, a, b, c, d, true); };

  // $attrs & $listeners are exposed for easier HOC creation.
  // they need to be reactive so that HOCs using them are always updated
  var parentData = parentVnode && parentVnode.data;

  /* istanbul ignore else */
  if (true) {
    defineReactive$$1(vm, '$attrs', parentData && parentData.attrs || emptyObject, function () {
      !isUpdatingChildComponent && warn("$attrs is readonly.", vm);
    }, true);
    defineReactive$$1(vm, '$listeners', options._parentListeners || emptyObject, function () {
      !isUpdatingChildComponent && warn("$listeners is readonly.", vm);
    }, true);
  } else {}
}

var currentRenderingInstance = null;

function renderMixin (Vue) {
  // install runtime convenience helpers
  installRenderHelpers(Vue.prototype);

  Vue.prototype.$nextTick = function (fn) {
    return nextTick(fn, this)
  };

  Vue.prototype._render = function () {
    var vm = this;
    var ref = vm.$options;
    var render = ref.render;
    var _parentVnode = ref._parentVnode;

    if (_parentVnode) {
      vm.$scopedSlots = normalizeScopedSlots(
        _parentVnode.data.scopedSlots,
        vm.$slots,
        vm.$scopedSlots
      );
    }

    // set parent vnode. this allows render functions to have access
    // to the data on the placeholder node.
    vm.$vnode = _parentVnode;
    // render self
    var vnode;
    try {
      // There's no need to maintain a stack because all render fns are called
      // separately from one another. Nested component's render fns are called
      // when parent component is patched.
      currentRenderingInstance = vm;
      vnode = render.call(vm._renderProxy, vm.$createElement);
    } catch (e) {
      handleError(e, vm, "render");
      // return error render result,
      // or previous vnode to prevent render error causing blank component
      /* istanbul ignore else */
      if ( true && vm.$options.renderError) {
        try {
          vnode = vm.$options.renderError.call(vm._renderProxy, vm.$createElement, e);
        } catch (e) {
          handleError(e, vm, "renderError");
          vnode = vm._vnode;
        }
      } else {
        vnode = vm._vnode;
      }
    } finally {
      currentRenderingInstance = null;
    }
    // if the returned array contains only a single node, allow it
    if (Array.isArray(vnode) && vnode.length === 1) {
      vnode = vnode[0];
    }
    // return empty vnode in case the render function errored out
    if (!(vnode instanceof VNode)) {
      if ( true && Array.isArray(vnode)) {
        warn(
          'Multiple root nodes returned from render function. Render function ' +
          'should return a single root node.',
          vm
        );
      }
      vnode = createEmptyVNode();
    }
    // set parent
    vnode.parent = _parentVnode;
    return vnode
  };
}

/*  */

function ensureCtor (comp, base) {
  if (
    comp.__esModule ||
    (hasSymbol && comp[Symbol.toStringTag] === 'Module')
  ) {
    comp = comp.default;
  }
  return isObject(comp)
    ? base.extend(comp)
    : comp
}

function createAsyncPlaceholder (
  factory,
  data,
  context,
  children,
  tag
) {
  var node = createEmptyVNode();
  node.asyncFactory = factory;
  node.asyncMeta = { data: data, context: context, children: children, tag: tag };
  return node
}

function resolveAsyncComponent (
  factory,
  baseCtor
) {
  if (isTrue(factory.error) && isDef(factory.errorComp)) {
    return factory.errorComp
  }

  if (isDef(factory.resolved)) {
    return factory.resolved
  }

  var owner = currentRenderingInstance;
  if (owner && isDef(factory.owners) && factory.owners.indexOf(owner) === -1) {
    // already pending
    factory.owners.push(owner);
  }

  if (isTrue(factory.loading) && isDef(factory.loadingComp)) {
    return factory.loadingComp
  }

  if (owner && !isDef(factory.owners)) {
    var owners = factory.owners = [owner];
    var sync = true;
    var timerLoading = null;
    var timerTimeout = null

    ;(owner).$on('hook:destroyed', function () { return remove(owners, owner); });

    var forceRender = function (renderCompleted) {
      for (var i = 0, l = owners.length; i < l; i++) {
        (owners[i]).$forceUpdate();
      }

      if (renderCompleted) {
        owners.length = 0;
        if (timerLoading !== null) {
          clearTimeout(timerLoading);
          timerLoading = null;
        }
        if (timerTimeout !== null) {
          clearTimeout(timerTimeout);
          timerTimeout = null;
        }
      }
    };

    var resolve = once(function (res) {
      // cache resolved
      factory.resolved = ensureCtor(res, baseCtor);
      // invoke callbacks only if this is not a synchronous resolve
      // (async resolves are shimmed as synchronous during SSR)
      if (!sync) {
        forceRender(true);
      } else {
        owners.length = 0;
      }
    });

    var reject = once(function (reason) {
       true && warn(
        "Failed to resolve async component: " + (String(factory)) +
        (reason ? ("\nReason: " + reason) : '')
      );
      if (isDef(factory.errorComp)) {
        factory.error = true;
        forceRender(true);
      }
    });

    var res = factory(resolve, reject);

    if (isObject(res)) {
      if (isPromise(res)) {
        // () => Promise
        if (isUndef(factory.resolved)) {
          res.then(resolve, reject);
        }
      } else if (isPromise(res.component)) {
        res.component.then(resolve, reject);

        if (isDef(res.error)) {
          factory.errorComp = ensureCtor(res.error, baseCtor);
        }

        if (isDef(res.loading)) {
          factory.loadingComp = ensureCtor(res.loading, baseCtor);
          if (res.delay === 0) {
            factory.loading = true;
          } else {
            timerLoading = setTimeout(function () {
              timerLoading = null;
              if (isUndef(factory.resolved) && isUndef(factory.error)) {
                factory.loading = true;
                forceRender(false);
              }
            }, res.delay || 200);
          }
        }

        if (isDef(res.timeout)) {
          timerTimeout = setTimeout(function () {
            timerTimeout = null;
            if (isUndef(factory.resolved)) {
              reject(
                 true
                  ? ("timeout (" + (res.timeout) + "ms)")
                  : undefined
              );
            }
          }, res.timeout);
        }
      }
    }

    sync = false;
    // return in case resolved synchronously
    return factory.loading
      ? factory.loadingComp
      : factory.resolved
  }
}

/*  */

function isAsyncPlaceholder (node) {
  return node.isComment && node.asyncFactory
}

/*  */

function getFirstComponentChild (children) {
  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      var c = children[i];
      if (isDef(c) && (isDef(c.componentOptions) || isAsyncPlaceholder(c))) {
        return c
      }
    }
  }
}

/*  */

/*  */

function initEvents (vm) {
  vm._events = Object.create(null);
  vm._hasHookEvent = false;
  // init parent attached events
  var listeners = vm.$options._parentListeners;
  if (listeners) {
    updateComponentListeners(vm, listeners);
  }
}

var target;

function add (event, fn) {
  target.$on(event, fn);
}

function remove$1 (event, fn) {
  target.$off(event, fn);
}

function createOnceHandler (event, fn) {
  var _target = target;
  return function onceHandler () {
    var res = fn.apply(null, arguments);
    if (res !== null) {
      _target.$off(event, onceHandler);
    }
  }
}

function updateComponentListeners (
  vm,
  listeners,
  oldListeners
) {
  target = vm;
  updateListeners(listeners, oldListeners || {}, add, remove$1, createOnceHandler, vm);
  target = undefined;
}

function eventsMixin (Vue) {
  var hookRE = /^hook:/;
  Vue.prototype.$on = function (event, fn) {
    var vm = this;
    if (Array.isArray(event)) {
      for (var i = 0, l = event.length; i < l; i++) {
        vm.$on(event[i], fn);
      }
    } else {
      (vm._events[event] || (vm._events[event] = [])).push(fn);
      // optimize hook:event cost by using a boolean flag marked at registration
      // instead of a hash lookup
      if (hookRE.test(event)) {
        vm._hasHookEvent = true;
      }
    }
    return vm
  };

  Vue.prototype.$once = function (event, fn) {
    var vm = this;
    function on () {
      vm.$off(event, on);
      fn.apply(vm, arguments);
    }
    on.fn = fn;
    vm.$on(event, on);
    return vm
  };

  Vue.prototype.$off = function (event, fn) {
    var vm = this;
    // all
    if (!arguments.length) {
      vm._events = Object.create(null);
      return vm
    }
    // array of events
    if (Array.isArray(event)) {
      for (var i$1 = 0, l = event.length; i$1 < l; i$1++) {
        vm.$off(event[i$1], fn);
      }
      return vm
    }
    // specific event
    var cbs = vm._events[event];
    if (!cbs) {
      return vm
    }
    if (!fn) {
      vm._events[event] = null;
      return vm
    }
    // specific handler
    var cb;
    var i = cbs.length;
    while (i--) {
      cb = cbs[i];
      if (cb === fn || cb.fn === fn) {
        cbs.splice(i, 1);
        break
      }
    }
    return vm
  };

  Vue.prototype.$emit = function (event) {
    var vm = this;
    if (true) {
      var lowerCaseEvent = event.toLowerCase();
      if (lowerCaseEvent !== event && vm._events[lowerCaseEvent]) {
        tip(
          "Event \"" + lowerCaseEvent + "\" is emitted in component " +
          (formatComponentName(vm)) + " but the handler is registered for \"" + event + "\". " +
          "Note that HTML attributes are case-insensitive and you cannot use " +
          "v-on to listen to camelCase events when using in-DOM templates. " +
          "You should probably use \"" + (hyphenate(event)) + "\" instead of \"" + event + "\"."
        );
      }
    }
    var cbs = vm._events[event];
    if (cbs) {
      cbs = cbs.length > 1 ? toArray(cbs) : cbs;
      var args = toArray(arguments, 1);
      var info = "event handler for \"" + event + "\"";
      for (var i = 0, l = cbs.length; i < l; i++) {
        invokeWithErrorHandling(cbs[i], vm, args, vm, info);
      }
    }
    return vm
  };
}

/*  */

var activeInstance = null;
var isUpdatingChildComponent = false;

function setActiveInstance(vm) {
  var prevActiveInstance = activeInstance;
  activeInstance = vm;
  return function () {
    activeInstance = prevActiveInstance;
  }
}

function initLifecycle (vm) {
  var options = vm.$options;

  // locate first non-abstract parent
  var parent = options.parent;
  if (parent && !options.abstract) {
    while (parent.$options.abstract && parent.$parent) {
      parent = parent.$parent;
    }
    parent.$children.push(vm);
  }

  vm.$parent = parent;
  vm.$root = parent ? parent.$root : vm;

  vm.$children = [];
  vm.$refs = {};

  vm._watcher = null;
  vm._inactive = null;
  vm._directInactive = false;
  vm._isMounted = false;
  vm._isDestroyed = false;
  vm._isBeingDestroyed = false;
}

function lifecycleMixin (Vue) {
  Vue.prototype._update = function (vnode, hydrating) {
    var vm = this;
    var prevEl = vm.$el;
    var prevVnode = vm._vnode;
    var restoreActiveInstance = setActiveInstance(vm);
    vm._vnode = vnode;
    // Vue.prototype.__patch__ is injected in entry points
    // based on the rendering backend used.
    if (!prevVnode) {
      // initial render
      vm.$el = vm.__patch__(vm.$el, vnode, hydrating, false /* removeOnly */);
    } else {
      // updates
      vm.$el = vm.__patch__(prevVnode, vnode);
    }
    restoreActiveInstance();
    // update __vue__ reference
    if (prevEl) {
      prevEl.__vue__ = null;
    }
    if (vm.$el) {
      vm.$el.__vue__ = vm;
    }
    // if parent is an HOC, update its $el as well
    if (vm.$vnode && vm.$parent && vm.$vnode === vm.$parent._vnode) {
      vm.$parent.$el = vm.$el;
    }
    // updated hook is called by the scheduler to ensure that children are
    // updated in a parent's updated hook.
  };

  Vue.prototype.$forceUpdate = function () {
    var vm = this;
    if (vm._watcher) {
      vm._watcher.update();
    }
  };

  Vue.prototype.$destroy = function () {
    var vm = this;
    if (vm._isBeingDestroyed) {
      return
    }
    callHook(vm, 'beforeDestroy');
    vm._isBeingDestroyed = true;
    // remove self from parent
    var parent = vm.$parent;
    if (parent && !parent._isBeingDestroyed && !vm.$options.abstract) {
      remove(parent.$children, vm);
    }
    // teardown watchers
    if (vm._watcher) {
      vm._watcher.teardown();
    }
    var i = vm._watchers.length;
    while (i--) {
      vm._watchers[i].teardown();
    }
    // remove reference from data ob
    // frozen object may not have observer.
    if (vm._data.__ob__) {
      vm._data.__ob__.vmCount--;
    }
    // call the last hook...
    vm._isDestroyed = true;
    // invoke destroy hooks on current rendered tree
    vm.__patch__(vm._vnode, null);
    // fire destroyed hook
    callHook(vm, 'destroyed');
    // turn off all instance listeners.
    vm.$off();
    // remove __vue__ reference
    if (vm.$el) {
      vm.$el.__vue__ = null;
    }
    // release circular reference (#6759)
    if (vm.$vnode) {
      vm.$vnode.parent = null;
    }
  };
}

function updateChildComponent (
  vm,
  propsData,
  listeners,
  parentVnode,
  renderChildren
) {
  if (true) {
    isUpdatingChildComponent = true;
  }

  // determine whether component has slot children
  // we need to do this before overwriting $options._renderChildren.

  // check if there are dynamic scopedSlots (hand-written or compiled but with
  // dynamic slot names). Static scoped slots compiled from template has the
  // "$stable" marker.
  var newScopedSlots = parentVnode.data.scopedSlots;
  var oldScopedSlots = vm.$scopedSlots;
  var hasDynamicScopedSlot = !!(
    (newScopedSlots && !newScopedSlots.$stable) ||
    (oldScopedSlots !== emptyObject && !oldScopedSlots.$stable) ||
    (newScopedSlots && vm.$scopedSlots.$key !== newScopedSlots.$key)
  );

  // Any static slot children from the parent may have changed during parent's
  // update. Dynamic scoped slots may also have changed. In such cases, a forced
  // update is necessary to ensure correctness.
  var needsForceUpdate = !!(
    renderChildren ||               // has new static slots
    vm.$options._renderChildren ||  // has old static slots
    hasDynamicScopedSlot
  );

  vm.$options._parentVnode = parentVnode;
  vm.$vnode = parentVnode; // update vm's placeholder node without re-render

  if (vm._vnode) { // update child tree's parent
    vm._vnode.parent = parentVnode;
  }
  vm.$options._renderChildren = renderChildren;

  // update $attrs and $listeners hash
  // these are also reactive so they may trigger child update if the child
  // used them during render
  vm.$attrs = parentVnode.data.attrs || emptyObject;
  vm.$listeners = listeners || emptyObject;

  // update props
  if (propsData && vm.$options.props) {
    toggleObserving(false);
    var props = vm._props;
    var propKeys = vm.$options._propKeys || [];
    for (var i = 0; i < propKeys.length; i++) {
      var key = propKeys[i];
      var propOptions = vm.$options.props; // wtf flow?
      props[key] = validateProp(key, propOptions, propsData, vm);
    }
    toggleObserving(true);
    // keep a copy of raw propsData
    vm.$options.propsData = propsData;
  }
  
  // fixed by xxxxxx update properties(mp runtime)
  vm._$updateProperties && vm._$updateProperties(vm);
  
  // update listeners
  listeners = listeners || emptyObject;
  var oldListeners = vm.$options._parentListeners;
  vm.$options._parentListeners = listeners;
  updateComponentListeners(vm, listeners, oldListeners);

  // resolve slots + force update if has children
  if (needsForceUpdate) {
    vm.$slots = resolveSlots(renderChildren, parentVnode.context);
    vm.$forceUpdate();
  }

  if (true) {
    isUpdatingChildComponent = false;
  }
}

function isInInactiveTree (vm) {
  while (vm && (vm = vm.$parent)) {
    if (vm._inactive) { return true }
  }
  return false
}

function activateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = false;
    if (isInInactiveTree(vm)) {
      return
    }
  } else if (vm._directInactive) {
    return
  }
  if (vm._inactive || vm._inactive === null) {
    vm._inactive = false;
    for (var i = 0; i < vm.$children.length; i++) {
      activateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'activated');
  }
}

function deactivateChildComponent (vm, direct) {
  if (direct) {
    vm._directInactive = true;
    if (isInInactiveTree(vm)) {
      return
    }
  }
  if (!vm._inactive) {
    vm._inactive = true;
    for (var i = 0; i < vm.$children.length; i++) {
      deactivateChildComponent(vm.$children[i]);
    }
    callHook(vm, 'deactivated');
  }
}

function callHook (vm, hook) {
  // #7573 disable dep collection when invoking lifecycle hooks
  pushTarget();
  var handlers = vm.$options[hook];
  var info = hook + " hook";
  if (handlers) {
    for (var i = 0, j = handlers.length; i < j; i++) {
      invokeWithErrorHandling(handlers[i], vm, null, vm, info);
    }
  }
  if (vm._hasHookEvent) {
    vm.$emit('hook:' + hook);
  }
  popTarget();
}

/*  */

var MAX_UPDATE_COUNT = 100;

var queue = [];
var activatedChildren = [];
var has = {};
var circular = {};
var waiting = false;
var flushing = false;
var index = 0;

/**
 * Reset the scheduler's state.
 */
function resetSchedulerState () {
  index = queue.length = activatedChildren.length = 0;
  has = {};
  if (true) {
    circular = {};
  }
  waiting = flushing = false;
}

// Async edge case #6566 requires saving the timestamp when event listeners are
// attached. However, calling performance.now() has a perf overhead especially
// if the page has thousands of event listeners. Instead, we take a timestamp
// every time the scheduler flushes and use that for all event listeners
// attached during that flush.
var currentFlushTimestamp = 0;

// Async edge case fix requires storing an event listener's attach timestamp.
var getNow = Date.now;

// Determine what event timestamp the browser is using. Annoyingly, the
// timestamp can either be hi-res (relative to page load) or low-res
// (relative to UNIX epoch), so in order to compare time we have to use the
// same timestamp type when saving the flush timestamp.
// All IE versions use low-res event timestamps, and have problematic clock
// implementations (#9632)
if (inBrowser && !isIE) {
  var performance = window.performance;
  if (
    performance &&
    typeof performance.now === 'function' &&
    getNow() > document.createEvent('Event').timeStamp
  ) {
    // if the event timestamp, although evaluated AFTER the Date.now(), is
    // smaller than it, it means the event is using a hi-res timestamp,
    // and we need to use the hi-res version for event listener timestamps as
    // well.
    getNow = function () { return performance.now(); };
  }
}

/**
 * Flush both queues and run the watchers.
 */
function flushSchedulerQueue () {
  currentFlushTimestamp = getNow();
  flushing = true;
  var watcher, id;

  // Sort queue before flush.
  // This ensures that:
  // 1. Components are updated from parent to child. (because parent is always
  //    created before the child)
  // 2. A component's user watchers are run before its render watcher (because
  //    user watchers are created before the render watcher)
  // 3. If a component is destroyed during a parent component's watcher run,
  //    its watchers can be skipped.
  queue.sort(function (a, b) { return a.id - b.id; });

  // do not cache length because more watchers might be pushed
  // as we run existing watchers
  for (index = 0; index < queue.length; index++) {
    watcher = queue[index];
    if (watcher.before) {
      watcher.before();
    }
    id = watcher.id;
    has[id] = null;
    watcher.run();
    // in dev build, check and stop circular updates.
    if ( true && has[id] != null) {
      circular[id] = (circular[id] || 0) + 1;
      if (circular[id] > MAX_UPDATE_COUNT) {
        warn(
          'You may have an infinite update loop ' + (
            watcher.user
              ? ("in watcher with expression \"" + (watcher.expression) + "\"")
              : "in a component render function."
          ),
          watcher.vm
        );
        break
      }
    }
  }

  // keep copies of post queues before resetting state
  var activatedQueue = activatedChildren.slice();
  var updatedQueue = queue.slice();

  resetSchedulerState();

  // call component updated and activated hooks
  callActivatedHooks(activatedQueue);
  callUpdatedHooks(updatedQueue);

  // devtool hook
  /* istanbul ignore if */
  if (devtools && config.devtools) {
    devtools.emit('flush');
  }
}

function callUpdatedHooks (queue) {
  var i = queue.length;
  while (i--) {
    var watcher = queue[i];
    var vm = watcher.vm;
    if (vm._watcher === watcher && vm._isMounted && !vm._isDestroyed) {
      callHook(vm, 'updated');
    }
  }
}

/**
 * Queue a kept-alive component that was activated during patch.
 * The queue will be processed after the entire tree has been patched.
 */
function queueActivatedComponent (vm) {
  // setting _inactive to false here so that a render function can
  // rely on checking whether it's in an inactive tree (e.g. router-view)
  vm._inactive = false;
  activatedChildren.push(vm);
}

function callActivatedHooks (queue) {
  for (var i = 0; i < queue.length; i++) {
    queue[i]._inactive = true;
    activateChildComponent(queue[i], true /* true */);
  }
}

/**
 * Push a watcher into the watcher queue.
 * Jobs with duplicate IDs will be skipped unless it's
 * pushed when the queue is being flushed.
 */
function queueWatcher (watcher) {
  var id = watcher.id;
  if (has[id] == null) {
    has[id] = true;
    if (!flushing) {
      queue.push(watcher);
    } else {
      // if already flushing, splice the watcher based on its id
      // if already past its id, it will be run next immediately.
      var i = queue.length - 1;
      while (i > index && queue[i].id > watcher.id) {
        i--;
      }
      queue.splice(i + 1, 0, watcher);
    }
    // queue the flush
    if (!waiting) {
      waiting = true;

      if ( true && !config.async) {
        flushSchedulerQueue();
        return
      }
      nextTick(flushSchedulerQueue);
    }
  }
}

/*  */



var uid$2 = 0;

/**
 * A watcher parses an expression, collects dependencies,
 * and fires callback when the expression value changes.
 * This is used for both the $watch() api and directives.
 */
var Watcher = function Watcher (
  vm,
  expOrFn,
  cb,
  options,
  isRenderWatcher
) {
  this.vm = vm;
  if (isRenderWatcher) {
    vm._watcher = this;
  }
  vm._watchers.push(this);
  // options
  if (options) {
    this.deep = !!options.deep;
    this.user = !!options.user;
    this.lazy = !!options.lazy;
    this.sync = !!options.sync;
    this.before = options.before;
  } else {
    this.deep = this.user = this.lazy = this.sync = false;
  }
  this.cb = cb;
  this.id = ++uid$2; // uid for batching
  this.active = true;
  this.dirty = this.lazy; // for lazy watchers
  this.deps = [];
  this.newDeps = [];
  this.depIds = new _Set();
  this.newDepIds = new _Set();
  this.expression =  true
    ? expOrFn.toString()
    : undefined;
  // parse expression for getter
  if (typeof expOrFn === 'function') {
    this.getter = expOrFn;
  } else {
    this.getter = parsePath(expOrFn);
    if (!this.getter) {
      this.getter = noop;
       true && warn(
        "Failed watching path: \"" + expOrFn + "\" " +
        'Watcher only accepts simple dot-delimited paths. ' +
        'For full control, use a function instead.',
        vm
      );
    }
  }
  this.value = this.lazy
    ? undefined
    : this.get();
};

/**
 * Evaluate the getter, and re-collect dependencies.
 */
Watcher.prototype.get = function get () {
  pushTarget(this);
  var value;
  var vm = this.vm;
  try {
    value = this.getter.call(vm, vm);
  } catch (e) {
    if (this.user) {
      handleError(e, vm, ("getter for watcher \"" + (this.expression) + "\""));
    } else {
      throw e
    }
  } finally {
    // "touch" every property so they are all tracked as
    // dependencies for deep watching
    if (this.deep) {
      traverse(value);
    }
    popTarget();
    this.cleanupDeps();
  }
  return value
};

/**
 * Add a dependency to this directive.
 */
Watcher.prototype.addDep = function addDep (dep) {
  var id = dep.id;
  if (!this.newDepIds.has(id)) {
    this.newDepIds.add(id);
    this.newDeps.push(dep);
    if (!this.depIds.has(id)) {
      dep.addSub(this);
    }
  }
};

/**
 * Clean up for dependency collection.
 */
Watcher.prototype.cleanupDeps = function cleanupDeps () {
  var i = this.deps.length;
  while (i--) {
    var dep = this.deps[i];
    if (!this.newDepIds.has(dep.id)) {
      dep.removeSub(this);
    }
  }
  var tmp = this.depIds;
  this.depIds = this.newDepIds;
  this.newDepIds = tmp;
  this.newDepIds.clear();
  tmp = this.deps;
  this.deps = this.newDeps;
  this.newDeps = tmp;
  this.newDeps.length = 0;
};

/**
 * Subscriber interface.
 * Will be called when a dependency changes.
 */
Watcher.prototype.update = function update () {
  /* istanbul ignore else */
  if (this.lazy) {
    this.dirty = true;
  } else if (this.sync) {
    this.run();
  } else {
    queueWatcher(this);
  }
};

/**
 * Scheduler job interface.
 * Will be called by the scheduler.
 */
Watcher.prototype.run = function run () {
  if (this.active) {
    var value = this.get();
    if (
      value !== this.value ||
      // Deep watchers and watchers on Object/Arrays should fire even
      // when the value is the same, because the value may
      // have mutated.
      isObject(value) ||
      this.deep
    ) {
      // set new value
      var oldValue = this.value;
      this.value = value;
      if (this.user) {
        try {
          this.cb.call(this.vm, value, oldValue);
        } catch (e) {
          handleError(e, this.vm, ("callback for watcher \"" + (this.expression) + "\""));
        }
      } else {
        this.cb.call(this.vm, value, oldValue);
      }
    }
  }
};

/**
 * Evaluate the value of the watcher.
 * This only gets called for lazy watchers.
 */
Watcher.prototype.evaluate = function evaluate () {
  this.value = this.get();
  this.dirty = false;
};

/**
 * Depend on all deps collected by this watcher.
 */
Watcher.prototype.depend = function depend () {
  var i = this.deps.length;
  while (i--) {
    this.deps[i].depend();
  }
};

/**
 * Remove self from all dependencies' subscriber list.
 */
Watcher.prototype.teardown = function teardown () {
  if (this.active) {
    // remove self from vm's watcher list
    // this is a somewhat expensive operation so we skip it
    // if the vm is being destroyed.
    if (!this.vm._isBeingDestroyed) {
      remove(this.vm._watchers, this);
    }
    var i = this.deps.length;
    while (i--) {
      this.deps[i].removeSub(this);
    }
    this.active = false;
  }
};

/*  */

var sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
};

function proxy (target, sourceKey, key) {
  sharedPropertyDefinition.get = function proxyGetter () {
    return this[sourceKey][key]
  };
  sharedPropertyDefinition.set = function proxySetter (val) {
    this[sourceKey][key] = val;
  };
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function initState (vm) {
  vm._watchers = [];
  var opts = vm.$options;
  if (opts.props) { initProps(vm, opts.props); }
  if (opts.methods) { initMethods(vm, opts.methods); }
  if (opts.data) {
    initData(vm);
  } else {
    observe(vm._data = {}, true /* asRootData */);
  }
  if (opts.computed) { initComputed(vm, opts.computed); }
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

function initProps (vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var props = vm._props = {};
  // cache prop keys so that future props updates can iterate using Array
  // instead of dynamic object key enumeration.
  var keys = vm.$options._propKeys = [];
  var isRoot = !vm.$parent;
  // root instance props should be converted
  if (!isRoot) {
    toggleObserving(false);
  }
  var loop = function ( key ) {
    keys.push(key);
    var value = validateProp(key, propsOptions, propsData, vm);
    /* istanbul ignore else */
    if (true) {
      var hyphenatedKey = hyphenate(key);
      if (isReservedAttribute(hyphenatedKey) ||
          config.isReservedAttr(hyphenatedKey)) {
        warn(
          ("\"" + hyphenatedKey + "\" is a reserved attribute and cannot be used as component prop."),
          vm
        );
      }
      defineReactive$$1(props, key, value, function () {
        if (!isRoot && !isUpdatingChildComponent) {
          {
            if(vm.mpHost === 'mp-baidu' || vm.mpHost === 'mp-kuaishou'){//百度、快手 observer 在 setData callback 之后触发，直接忽略该 warn
                return
            }
            //fixed by xxxxxx __next_tick_pending,uni://form-field 时不告警
            if(
                key === 'value' && 
                Array.isArray(vm.$options.behaviors) &&
                vm.$options.behaviors.indexOf('uni://form-field') !== -1
              ){
              return
            }
            if(vm._getFormData){
              return
            }
            var $parent = vm.$parent;
            while($parent){
              if($parent.__next_tick_pending){
                return  
              }
              $parent = $parent.$parent;
            }
          }
          warn(
            "Avoid mutating a prop directly since the value will be " +
            "overwritten whenever the parent component re-renders. " +
            "Instead, use a data or computed property based on the prop's " +
            "value. Prop being mutated: \"" + key + "\"",
            vm
          );
        }
      });
    } else {}
    // static props are already proxied on the component's prototype
    // during Vue.extend(). We only need to proxy props defined at
    // instantiation here.
    if (!(key in vm)) {
      proxy(vm, "_props", key);
    }
  };

  for (var key in propsOptions) loop( key );
  toggleObserving(true);
}

function initData (vm) {
  var data = vm.$options.data;
  data = vm._data = typeof data === 'function'
    ? getData(data, vm)
    : data || {};
  if (!isPlainObject(data)) {
    data = {};
     true && warn(
      'data functions should return an object:\n' +
      'https://vuejs.org/v2/guide/components.html#data-Must-Be-a-Function',
      vm
    );
  }
  // proxy data on instance
  var keys = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  var i = keys.length;
  while (i--) {
    var key = keys[i];
    if (true) {
      if (methods && hasOwn(methods, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a data property."),
          vm
        );
      }
    }
    if (props && hasOwn(props, key)) {
       true && warn(
        "The data property \"" + key + "\" is already declared as a prop. " +
        "Use prop default value instead.",
        vm
      );
    } else if (!isReserved(key)) {
      proxy(vm, "_data", key);
    }
  }
  // observe data
  observe(data, true /* asRootData */);
}

function getData (data, vm) {
  // #7573 disable dep collection when invoking data getters
  pushTarget();
  try {
    return data.call(vm, vm)
  } catch (e) {
    handleError(e, vm, "data()");
    return {}
  } finally {
    popTarget();
  }
}

var computedWatcherOptions = { lazy: true };

function initComputed (vm, computed) {
  // $flow-disable-line
  var watchers = vm._computedWatchers = Object.create(null);
  // computed properties are just getters during SSR
  var isSSR = isServerRendering();

  for (var key in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    if ( true && getter == null) {
      warn(
        ("Getter is missing for computed property \"" + key + "\"."),
        vm
      );
    }

    if (!isSSR) {
      // create internal watcher for the computed property.
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }

    // component-defined computed properties are already defined on the
    // component prototype. We only need to define computed properties defined
    // at instantiation here.
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else if (true) {
      if (key in vm.$data) {
        warn(("The computed property \"" + key + "\" is already defined in data."), vm);
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(("The computed property \"" + key + "\" is already defined as a prop."), vm);
      }
    }
  }
}

function defineComputed (
  target,
  key,
  userDef
) {
  var shouldCache = !isServerRendering();
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef);
    sharedPropertyDefinition.set = noop;
  } else {
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop;
    sharedPropertyDefinition.set = userDef.set || noop;
  }
  if ( true &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        ("Computed property \"" + key + "\" was assigned to but it has no setter."),
        this
      );
    };
  }
  Object.defineProperty(target, key, sharedPropertyDefinition);
}

function createComputedGetter (key) {
  return function computedGetter () {
    var watcher = this._computedWatchers && this._computedWatchers[key];
    if (watcher) {
      if (watcher.dirty) {
        watcher.evaluate();
      }
      if (Dep.SharedObject.target) {// fixed by xxxxxx
        watcher.depend();
      }
      return watcher.value
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}

function initMethods (vm, methods) {
  var props = vm.$options.props;
  for (var key in methods) {
    if (true) {
      if (typeof methods[key] !== 'function') {
        warn(
          "Method \"" + key + "\" has type \"" + (typeof methods[key]) + "\" in the component definition. " +
          "Did you reference the function correctly?",
          vm
        );
      }
      if (props && hasOwn(props, key)) {
        warn(
          ("Method \"" + key + "\" has already been defined as a prop."),
          vm
        );
      }
      if ((key in vm) && isReserved(key)) {
        warn(
          "Method \"" + key + "\" conflicts with an existing Vue instance method. " +
          "Avoid defining component methods that start with _ or $."
        );
      }
    }
    vm[key] = typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

function initWatch (vm, watch) {
  for (var key in watch) {
    var handler = watch[key];
    if (Array.isArray(handler)) {
      for (var i = 0; i < handler.length; i++) {
        createWatcher(vm, key, handler[i]);
      }
    } else {
      createWatcher(vm, key, handler);
    }
  }
}

function createWatcher (
  vm,
  expOrFn,
  handler,
  options
) {
  if (isPlainObject(handler)) {
    options = handler;
    handler = handler.handler;
  }
  if (typeof handler === 'string') {
    handler = vm[handler];
  }
  return vm.$watch(expOrFn, handler, options)
}

function stateMixin (Vue) {
  // flow somehow has problems with directly declared definition object
  // when using Object.defineProperty, so we have to procedurally build up
  // the object here.
  var dataDef = {};
  dataDef.get = function () { return this._data };
  var propsDef = {};
  propsDef.get = function () { return this._props };
  if (true) {
    dataDef.set = function () {
      warn(
        'Avoid replacing instance root $data. ' +
        'Use nested data properties instead.',
        this
      );
    };
    propsDef.set = function () {
      warn("$props is readonly.", this);
    };
  }
  Object.defineProperty(Vue.prototype, '$data', dataDef);
  Object.defineProperty(Vue.prototype, '$props', propsDef);

  Vue.prototype.$set = set;
  Vue.prototype.$delete = del;

  Vue.prototype.$watch = function (
    expOrFn,
    cb,
    options
  ) {
    var vm = this;
    if (isPlainObject(cb)) {
      return createWatcher(vm, expOrFn, cb, options)
    }
    options = options || {};
    options.user = true;
    var watcher = new Watcher(vm, expOrFn, cb, options);
    if (options.immediate) {
      try {
        cb.call(vm, watcher.value);
      } catch (error) {
        handleError(error, vm, ("callback for immediate watcher \"" + (watcher.expression) + "\""));
      }
    }
    return function unwatchFn () {
      watcher.teardown();
    }
  };
}

/*  */

var uid$3 = 0;

function initMixin (Vue) {
  Vue.prototype._init = function (options) {
    var vm = this;
    // a uid
    vm._uid = uid$3++;

    var startTag, endTag;
    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      startTag = "vue-perf-start:" + (vm._uid);
      endTag = "vue-perf-end:" + (vm._uid);
      mark(startTag);
    }

    // a flag to avoid this being observed
    vm._isVue = true;
    // merge options
    if (options && options._isComponent) {
      // optimize internal component instantiation
      // since dynamic options merging is pretty slow, and none of the
      // internal component options needs special treatment.
      initInternalComponent(vm, options);
    } else {
      vm.$options = mergeOptions(
        resolveConstructorOptions(vm.constructor),
        options || {},
        vm
      );
    }
    /* istanbul ignore else */
    if (true) {
      initProxy(vm);
    } else {}
    // expose real self
    vm._self = vm;
    initLifecycle(vm);
    initEvents(vm);
    initRender(vm);
    callHook(vm, 'beforeCreate');
    !vm._$fallback && initInjections(vm); // resolve injections before data/props  
    initState(vm);
    !vm._$fallback && initProvide(vm); // resolve provide after data/props
    !vm._$fallback && callHook(vm, 'created');      

    /* istanbul ignore if */
    if ( true && config.performance && mark) {
      vm._name = formatComponentName(vm, false);
      mark(endTag);
      measure(("vue " + (vm._name) + " init"), startTag, endTag);
    }

    if (vm.$options.el) {
      vm.$mount(vm.$options.el);
    }
  };
}

function initInternalComponent (vm, options) {
  var opts = vm.$options = Object.create(vm.constructor.options);
  // doing this because it's faster than dynamic enumeration.
  var parentVnode = options._parentVnode;
  opts.parent = options.parent;
  opts._parentVnode = parentVnode;

  var vnodeComponentOptions = parentVnode.componentOptions;
  opts.propsData = vnodeComponentOptions.propsData;
  opts._parentListeners = vnodeComponentOptions.listeners;
  opts._renderChildren = vnodeComponentOptions.children;
  opts._componentTag = vnodeComponentOptions.tag;

  if (options.render) {
    opts.render = options.render;
    opts.staticRenderFns = options.staticRenderFns;
  }
}

function resolveConstructorOptions (Ctor) {
  var options = Ctor.options;
  if (Ctor.super) {
    var superOptions = resolveConstructorOptions(Ctor.super);
    var cachedSuperOptions = Ctor.superOptions;
    if (superOptions !== cachedSuperOptions) {
      // super option changed,
      // need to resolve new options.
      Ctor.superOptions = superOptions;
      // check if there are any late-modified/attached options (#4976)
      var modifiedOptions = resolveModifiedOptions(Ctor);
      // update base extend options
      if (modifiedOptions) {
        extend(Ctor.extendOptions, modifiedOptions);
      }
      options = Ctor.options = mergeOptions(superOptions, Ctor.extendOptions);
      if (options.name) {
        options.components[options.name] = Ctor;
      }
    }
  }
  return options
}

function resolveModifiedOptions (Ctor) {
  var modified;
  var latest = Ctor.options;
  var sealed = Ctor.sealedOptions;
  for (var key in latest) {
    if (latest[key] !== sealed[key]) {
      if (!modified) { modified = {}; }
      modified[key] = latest[key];
    }
  }
  return modified
}

function Vue (options) {
  if ( true &&
    !(this instanceof Vue)
  ) {
    warn('Vue is a constructor and should be called with the `new` keyword');
  }
  this._init(options);
}

initMixin(Vue);
stateMixin(Vue);
eventsMixin(Vue);
lifecycleMixin(Vue);
renderMixin(Vue);

/*  */

function initUse (Vue) {
  Vue.use = function (plugin) {
    var installedPlugins = (this._installedPlugins || (this._installedPlugins = []));
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    var args = toArray(arguments, 1);
    args.unshift(this);
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    installedPlugins.push(plugin);
    return this
  };
}

/*  */

function initMixin$1 (Vue) {
  Vue.mixin = function (mixin) {
    this.options = mergeOptions(this.options, mixin);
    return this
  };
}

/*  */

function initExtend (Vue) {
  /**
   * Each instance constructor, including Vue, has a unique
   * cid. This enables us to create wrapped "child
   * constructors" for prototypal inheritance and cache them.
   */
  Vue.cid = 0;
  var cid = 1;

  /**
   * Class inheritance
   */
  Vue.extend = function (extendOptions) {
    extendOptions = extendOptions || {};
    var Super = this;
    var SuperId = Super.cid;
    var cachedCtors = extendOptions._Ctor || (extendOptions._Ctor = {});
    if (cachedCtors[SuperId]) {
      return cachedCtors[SuperId]
    }

    var name = extendOptions.name || Super.options.name;
    if ( true && name) {
      validateComponentName(name);
    }

    var Sub = function VueComponent (options) {
      this._init(options);
    };
    Sub.prototype = Object.create(Super.prototype);
    Sub.prototype.constructor = Sub;
    Sub.cid = cid++;
    Sub.options = mergeOptions(
      Super.options,
      extendOptions
    );
    Sub['super'] = Super;

    // For props and computed properties, we define the proxy getters on
    // the Vue instances at extension time, on the extended prototype. This
    // avoids Object.defineProperty calls for each instance created.
    if (Sub.options.props) {
      initProps$1(Sub);
    }
    if (Sub.options.computed) {
      initComputed$1(Sub);
    }

    // allow further extension/mixin/plugin usage
    Sub.extend = Super.extend;
    Sub.mixin = Super.mixin;
    Sub.use = Super.use;

    // create asset registers, so extended classes
    // can have their private assets too.
    ASSET_TYPES.forEach(function (type) {
      Sub[type] = Super[type];
    });
    // enable recursive self-lookup
    if (name) {
      Sub.options.components[name] = Sub;
    }

    // keep a reference to the super options at extension time.
    // later at instantiation we can check if Super's options have
    // been updated.
    Sub.superOptions = Super.options;
    Sub.extendOptions = extendOptions;
    Sub.sealedOptions = extend({}, Sub.options);

    // cache constructor
    cachedCtors[SuperId] = Sub;
    return Sub
  };
}

function initProps$1 (Comp) {
  var props = Comp.options.props;
  for (var key in props) {
    proxy(Comp.prototype, "_props", key);
  }
}

function initComputed$1 (Comp) {
  var computed = Comp.options.computed;
  for (var key in computed) {
    defineComputed(Comp.prototype, key, computed[key]);
  }
}

/*  */

function initAssetRegisters (Vue) {
  /**
   * Create asset registration methods.
   */
  ASSET_TYPES.forEach(function (type) {
    Vue[type] = function (
      id,
      definition
    ) {
      if (!definition) {
        return this.options[type + 's'][id]
      } else {
        /* istanbul ignore if */
        if ( true && type === 'component') {
          validateComponentName(id);
        }
        if (type === 'component' && isPlainObject(definition)) {
          definition.name = definition.name || id;
          definition = this.options._base.extend(definition);
        }
        if (type === 'directive' && typeof definition === 'function') {
          definition = { bind: definition, update: definition };
        }
        this.options[type + 's'][id] = definition;
        return definition
      }
    };
  });
}

/*  */



function getComponentName (opts) {
  return opts && (opts.Ctor.options.name || opts.tag)
}

function matches (pattern, name) {
  if (Array.isArray(pattern)) {
    return pattern.indexOf(name) > -1
  } else if (typeof pattern === 'string') {
    return pattern.split(',').indexOf(name) > -1
  } else if (isRegExp(pattern)) {
    return pattern.test(name)
  }
  /* istanbul ignore next */
  return false
}

function pruneCache (keepAliveInstance, filter) {
  var cache = keepAliveInstance.cache;
  var keys = keepAliveInstance.keys;
  var _vnode = keepAliveInstance._vnode;
  for (var key in cache) {
    var cachedNode = cache[key];
    if (cachedNode) {
      var name = getComponentName(cachedNode.componentOptions);
      if (name && !filter(name)) {
        pruneCacheEntry(cache, key, keys, _vnode);
      }
    }
  }
}

function pruneCacheEntry (
  cache,
  key,
  keys,
  current
) {
  var cached$$1 = cache[key];
  if (cached$$1 && (!current || cached$$1.tag !== current.tag)) {
    cached$$1.componentInstance.$destroy();
  }
  cache[key] = null;
  remove(keys, key);
}

var patternTypes = [String, RegExp, Array];

var KeepAlive = {
  name: 'keep-alive',
  abstract: true,

  props: {
    include: patternTypes,
    exclude: patternTypes,
    max: [String, Number]
  },

  created: function created () {
    this.cache = Object.create(null);
    this.keys = [];
  },

  destroyed: function destroyed () {
    for (var key in this.cache) {
      pruneCacheEntry(this.cache, key, this.keys);
    }
  },

  mounted: function mounted () {
    var this$1 = this;

    this.$watch('include', function (val) {
      pruneCache(this$1, function (name) { return matches(val, name); });
    });
    this.$watch('exclude', function (val) {
      pruneCache(this$1, function (name) { return !matches(val, name); });
    });
  },

  render: function render () {
    var slot = this.$slots.default;
    var vnode = getFirstComponentChild(slot);
    var componentOptions = vnode && vnode.componentOptions;
    if (componentOptions) {
      // check pattern
      var name = getComponentName(componentOptions);
      var ref = this;
      var include = ref.include;
      var exclude = ref.exclude;
      if (
        // not included
        (include && (!name || !matches(include, name))) ||
        // excluded
        (exclude && name && matches(exclude, name))
      ) {
        return vnode
      }

      var ref$1 = this;
      var cache = ref$1.cache;
      var keys = ref$1.keys;
      var key = vnode.key == null
        // same constructor may get registered as different local components
        // so cid alone is not enough (#3269)
        ? componentOptions.Ctor.cid + (componentOptions.tag ? ("::" + (componentOptions.tag)) : '')
        : vnode.key;
      if (cache[key]) {
        vnode.componentInstance = cache[key].componentInstance;
        // make current key freshest
        remove(keys, key);
        keys.push(key);
      } else {
        cache[key] = vnode;
        keys.push(key);
        // prune oldest entry
        if (this.max && keys.length > parseInt(this.max)) {
          pruneCacheEntry(cache, keys[0], keys, this._vnode);
        }
      }

      vnode.data.keepAlive = true;
    }
    return vnode || (slot && slot[0])
  }
};

var builtInComponents = {
  KeepAlive: KeepAlive
};

/*  */

function initGlobalAPI (Vue) {
  // config
  var configDef = {};
  configDef.get = function () { return config; };
  if (true) {
    configDef.set = function () {
      warn(
        'Do not replace the Vue.config object, set individual fields instead.'
      );
    };
  }
  Object.defineProperty(Vue, 'config', configDef);

  // exposed util methods.
  // NOTE: these are not considered part of the public API - avoid relying on
  // them unless you are aware of the risk.
  Vue.util = {
    warn: warn,
    extend: extend,
    mergeOptions: mergeOptions,
    defineReactive: defineReactive$$1
  };

  Vue.set = set;
  Vue.delete = del;
  Vue.nextTick = nextTick;

  // 2.6 explicit observable API
  Vue.observable = function (obj) {
    observe(obj);
    return obj
  };

  Vue.options = Object.create(null);
  ASSET_TYPES.forEach(function (type) {
    Vue.options[type + 's'] = Object.create(null);
  });

  // this is used to identify the "base" constructor to extend all plain-object
  // components with in Weex's multi-instance scenarios.
  Vue.options._base = Vue;

  extend(Vue.options.components, builtInComponents);

  initUse(Vue);
  initMixin$1(Vue);
  initExtend(Vue);
  initAssetRegisters(Vue);
}

initGlobalAPI(Vue);

Object.defineProperty(Vue.prototype, '$isServer', {
  get: isServerRendering
});

Object.defineProperty(Vue.prototype, '$ssrContext', {
  get: function get () {
    /* istanbul ignore next */
    return this.$vnode && this.$vnode.ssrContext
  }
});

// expose FunctionalRenderContext for ssr runtime helper installation
Object.defineProperty(Vue, 'FunctionalRenderContext', {
  value: FunctionalRenderContext
});

Vue.version = '2.6.11';

/**
 * https://raw.githubusercontent.com/Tencent/westore/master/packages/westore/utils/diff.js
 */
var ARRAYTYPE = '[object Array]';
var OBJECTTYPE = '[object Object]';
// const FUNCTIONTYPE = '[object Function]'

function diff(current, pre) {
    var result = {};
    syncKeys(current, pre);
    _diff(current, pre, '', result);
    return result
}

function syncKeys(current, pre) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE && rootPreType == OBJECTTYPE) {
        if(Object.keys(current).length >= Object.keys(pre).length){
            for (var key in pre) {
                var currentValue = current[key];
                if (currentValue === undefined) {
                    current[key] = null;
                } else {
                    syncKeys(currentValue, pre[key]);
                }
            }
        }
    } else if (rootCurrentType == ARRAYTYPE && rootPreType == ARRAYTYPE) {
        if (current.length >= pre.length) {
            pre.forEach(function (item, index) {
                syncKeys(current[index], item);
            });
        }
    }
}

function _diff(current, pre, path, result) {
    if (current === pre) { return }
    var rootCurrentType = type(current);
    var rootPreType = type(pre);
    if (rootCurrentType == OBJECTTYPE) {
        if (rootPreType != OBJECTTYPE || Object.keys(current).length < Object.keys(pre).length) {
            setResult(result, path, current);
        } else {
            var loop = function ( key ) {
                var currentValue = current[key];
                var preValue = pre[key];
                var currentType = type(currentValue);
                var preType = type(preValue);
                if (currentType != ARRAYTYPE && currentType != OBJECTTYPE) {
                    // NOTE 此处将 != 修改为 !==。涉及地方太多恐怕测试不到，如果出现数据对比问题，将其修改回来。
                    if (currentValue !== pre[key]) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    }
                } else if (currentType == ARRAYTYPE) {
                    if (preType != ARRAYTYPE) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        if (currentValue.length < preValue.length) {
                            setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                        } else {
                            currentValue.forEach(function (item, index) {
                                _diff(item, preValue[index], (path == '' ? '' : path + ".") + key + '[' + index + ']', result);
                            });
                        }
                    }
                } else if (currentType == OBJECTTYPE) {
                    if (preType != OBJECTTYPE || Object.keys(currentValue).length < Object.keys(preValue).length) {
                        setResult(result, (path == '' ? '' : path + ".") + key, currentValue);
                    } else {
                        for (var subKey in currentValue) {
                            _diff(currentValue[subKey], preValue[subKey], (path == '' ? '' : path + ".") + key + '.' + subKey, result);
                        }
                    }
                }
            };

            for (var key in current) loop( key );
        }
    } else if (rootCurrentType == ARRAYTYPE) {
        if (rootPreType != ARRAYTYPE) {
            setResult(result, path, current);
        } else {
            if (current.length < pre.length) {
                setResult(result, path, current);
            } else {
                current.forEach(function (item, index) {
                    _diff(item, pre[index], path + '[' + index + ']', result);
                });
            }
        }
    } else {
        setResult(result, path, current);
    }
}

function setResult(result, k, v) {
    // if (type(v) != FUNCTIONTYPE) {
        result[k] = v;
    // }
}

function type(obj) {
    return Object.prototype.toString.call(obj)
}

/*  */

function flushCallbacks$1(vm) {
    if (vm.__next_tick_callbacks && vm.__next_tick_callbacks.length) {
        if (Object({"NODE_ENV":"development","VUE_APP_NAME":"超级英雄电影预告","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:flushCallbacks[' + vm.__next_tick_callbacks.length + ']');
        }
        var copies = vm.__next_tick_callbacks.slice(0);
        vm.__next_tick_callbacks.length = 0;
        for (var i = 0; i < copies.length; i++) {
            copies[i]();
        }
    }
}

function hasRenderWatcher(vm) {
    return queue.find(function (watcher) { return vm._watcher === watcher; })
}

function nextTick$1(vm, cb) {
    //1.nextTick 之前 已 setData 且 setData 还未回调完成
    //2.nextTick 之前存在 render watcher
    if (!vm.__next_tick_pending && !hasRenderWatcher(vm)) {
        if(Object({"NODE_ENV":"development","VUE_APP_NAME":"超级英雄电影预告","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + vm._uid +
                ']:nextVueTick');
        }
        return nextTick(cb, vm)
    }else{
        if(Object({"NODE_ENV":"development","VUE_APP_NAME":"超级英雄电影预告","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG){
            var mpInstance$1 = vm.$scope;
            console.log('[' + (+new Date) + '][' + (mpInstance$1.is || mpInstance$1.route) + '][' + vm._uid +
                ']:nextMPTick');
        }
    }
    var _resolve;
    if (!vm.__next_tick_callbacks) {
        vm.__next_tick_callbacks = [];
    }
    vm.__next_tick_callbacks.push(function () {
        if (cb) {
            try {
                cb.call(vm);
            } catch (e) {
                handleError(e, vm, 'nextTick');
            }
        } else if (_resolve) {
            _resolve(vm);
        }
    });
    // $flow-disable-line
    if (!cb && typeof Promise !== 'undefined') {
        return new Promise(function (resolve) {
            _resolve = resolve;
        })
    }
}

/*  */

function cloneWithData(vm) {
  // 确保当前 vm 所有数据被同步
  var ret = Object.create(null);
  var dataKeys = [].concat(
    Object.keys(vm._data || {}),
    Object.keys(vm._computedWatchers || {}));

  dataKeys.reduce(function(ret, key) {
    ret[key] = vm[key];
    return ret
  }, ret);

  // vue-composition-api
  var compositionApiState = vm.__composition_api_state__ || vm.__secret_vfa_state__;
  var rawBindings = compositionApiState && compositionApiState.rawBindings;
  if (rawBindings) {
    Object.keys(rawBindings).forEach(function (key) {
      ret[key] = vm[key];
    });
  }

  //TODO 需要把无用数据处理掉，比如 list=>l0 则 list 需要移除，否则多传输一份数据
  Object.assign(ret, vm.$mp.data || {});
  if (
    Array.isArray(vm.$options.behaviors) &&
    vm.$options.behaviors.indexOf('uni://form-field') !== -1
  ) { //form-field
    ret['name'] = vm.name;
    ret['value'] = vm.value;
  }

  return JSON.parse(JSON.stringify(ret))
}

var patch = function(oldVnode, vnode) {
  var this$1 = this;

  if (vnode === null) { //destroy
    return
  }
  if (this.mpType === 'page' || this.mpType === 'component') {
    var mpInstance = this.$scope;
    var data = Object.create(null);
    try {
      data = cloneWithData(this);
    } catch (err) {
      console.error(err);
    }
    data.__webviewId__ = mpInstance.data.__webviewId__;
    var mpData = Object.create(null);
    Object.keys(data).forEach(function (key) { //仅同步 data 中有的数据
      mpData[key] = mpInstance.data[key];
    });
    var diffData = this.$shouldDiffData === false ? data : diff(data, mpData);
    if (Object.keys(diffData).length) {
      if (Object({"NODE_ENV":"development","VUE_APP_NAME":"超级英雄电影预告","VUE_APP_PLATFORM":"mp-weixin","BASE_URL":"/"}).VUE_APP_DEBUG) {
        console.log('[' + (+new Date) + '][' + (mpInstance.is || mpInstance.route) + '][' + this._uid +
          ']差量更新',
          JSON.stringify(diffData));
      }
      this.__next_tick_pending = true;
      mpInstance.setData(diffData, function () {
        this$1.__next_tick_pending = false;
        flushCallbacks$1(this$1);
      });
    } else {
      flushCallbacks$1(this);
    }
  }
};

/*  */

function createEmptyRender() {

}

function mountComponent$1(
  vm,
  el,
  hydrating
) {
  if (!vm.mpType) {//main.js 中的 new Vue
    return vm
  }
  if (vm.mpType === 'app') {
    vm.$options.render = createEmptyRender;
  }
  if (!vm.$options.render) {
    vm.$options.render = createEmptyRender;
    if (true) {
      /* istanbul ignore if */
      if ((vm.$options.template && vm.$options.template.charAt(0) !== '#') ||
        vm.$options.el || el) {
        warn(
          'You are using the runtime-only build of Vue where the template ' +
          'compiler is not available. Either pre-compile the templates into ' +
          'render functions, or use the compiler-included build.',
          vm
        );
      } else {
        warn(
          'Failed to mount component: template or render function not defined.',
          vm
        );
      }
    }
  }
  
  !vm._$fallback && callHook(vm, 'beforeMount');

  var updateComponent = function () {
    vm._update(vm._render(), hydrating);
  };

  // we set this to vm._watcher inside the watcher's constructor
  // since the watcher's initial patch may call $forceUpdate (e.g. inside child
  // component's mounted hook), which relies on vm._watcher being already defined
  new Watcher(vm, updateComponent, noop, {
    before: function before() {
      if (vm._isMounted && !vm._isDestroyed) {
        callHook(vm, 'beforeUpdate');
      }
    }
  }, true /* isRenderWatcher */);
  hydrating = false;
  return vm
}

/*  */

function renderClass (
  staticClass,
  dynamicClass
) {
  if (isDef(staticClass) || isDef(dynamicClass)) {
    return concat(staticClass, stringifyClass(dynamicClass))
  }
  /* istanbul ignore next */
  return ''
}

function concat (a, b) {
  return a ? b ? (a + ' ' + b) : a : (b || '')
}

function stringifyClass (value) {
  if (Array.isArray(value)) {
    return stringifyArray(value)
  }
  if (isObject(value)) {
    return stringifyObject(value)
  }
  if (typeof value === 'string') {
    return value
  }
  /* istanbul ignore next */
  return ''
}

function stringifyArray (value) {
  var res = '';
  var stringified;
  for (var i = 0, l = value.length; i < l; i++) {
    if (isDef(stringified = stringifyClass(value[i])) && stringified !== '') {
      if (res) { res += ' '; }
      res += stringified;
    }
  }
  return res
}

function stringifyObject (value) {
  var res = '';
  for (var key in value) {
    if (value[key]) {
      if (res) { res += ' '; }
      res += key;
    }
  }
  return res
}

/*  */

var parseStyleText = cached(function (cssText) {
  var res = {};
  var listDelimiter = /;(?![^(]*\))/g;
  var propertyDelimiter = /:(.+)/;
  cssText.split(listDelimiter).forEach(function (item) {
    if (item) {
      var tmp = item.split(propertyDelimiter);
      tmp.length > 1 && (res[tmp[0].trim()] = tmp[1].trim());
    }
  });
  return res
});

// normalize possible array / string values into Object
function normalizeStyleBinding (bindingStyle) {
  if (Array.isArray(bindingStyle)) {
    return toObject(bindingStyle)
  }
  if (typeof bindingStyle === 'string') {
    return parseStyleText(bindingStyle)
  }
  return bindingStyle
}

/*  */

var MP_METHODS = ['createSelectorQuery', 'createIntersectionObserver', 'selectAllComponents', 'selectComponent'];

function getTarget(obj, path) {
  var parts = path.split('.');
  var key = parts[0];
  if (key.indexOf('__$n') === 0) { //number index
    key = parseInt(key.replace('__$n', ''));
  }
  if (parts.length === 1) {
    return obj[key]
  }
  return getTarget(obj[key], parts.slice(1).join('.'))
}

function internalMixin(Vue) {

  Vue.config.errorHandler = function(err, vm, info) {
    Vue.util.warn(("Error in " + info + ": \"" + (err.toString()) + "\""), vm);
    console.error(err);
    /* eslint-disable no-undef */
    var app = typeof getApp === 'function' && getApp();
    if (app && app.onError) {
      app.onError(err);
    }
  };

  var oldEmit = Vue.prototype.$emit;

  Vue.prototype.$emit = function(event) {
    if (this.$scope && event) {
      this.$scope['triggerEvent'](event, {
        __args__: toArray(arguments, 1)
      });
    }
    return oldEmit.apply(this, arguments)
  };

  Vue.prototype.$nextTick = function(fn) {
    return nextTick$1(this, fn)
  };

  MP_METHODS.forEach(function (method) {
    Vue.prototype[method] = function(args) {
      if (this.$scope && this.$scope[method]) {
        return this.$scope[method](args)
      }
      // mp-alipay
      if (typeof my === 'undefined') {
        return
      }
      if (method === 'createSelectorQuery') {
        /* eslint-disable no-undef */
        return my.createSelectorQuery(args)
      } else if (method === 'createIntersectionObserver') {
        /* eslint-disable no-undef */
        return my.createIntersectionObserver(args)
      }
      // TODO mp-alipay 暂不支持 selectAllComponents,selectComponent
    };
  });

  Vue.prototype.__init_provide = initProvide;

  Vue.prototype.__init_injections = initInjections;

  Vue.prototype.__call_hook = function(hook, args) {
    var vm = this;
    // #7573 disable dep collection when invoking lifecycle hooks
    pushTarget();
    var handlers = vm.$options[hook];
    var info = hook + " hook";
    var ret;
    if (handlers) {
      for (var i = 0, j = handlers.length; i < j; i++) {
        ret = invokeWithErrorHandling(handlers[i], vm, args ? [args] : null, vm, info);
      }
    }
    if (vm._hasHookEvent) {
      vm.$emit('hook:' + hook, args);
    }
    popTarget();
    return ret
  };

  Vue.prototype.__set_model = function(target, key, value, modifiers) {
    if (Array.isArray(modifiers)) {
      if (modifiers.indexOf('trim') !== -1) {
        value = value.trim();
      }
      if (modifiers.indexOf('number') !== -1) {
        value = this._n(value);
      }
    }
    if (!target) {
      target = this;
    }
    target[key] = value;
  };

  Vue.prototype.__set_sync = function(target, key, value) {
    if (!target) {
      target = this;
    }
    target[key] = value;
  };

  Vue.prototype.__get_orig = function(item) {
    if (isPlainObject(item)) {
      return item['$orig'] || item
    }
    return item
  };

  Vue.prototype.__get_value = function(dataPath, target) {
    return getTarget(target || this, dataPath)
  };


  Vue.prototype.__get_class = function(dynamicClass, staticClass) {
    return renderClass(staticClass, dynamicClass)
  };

  Vue.prototype.__get_style = function(dynamicStyle, staticStyle) {
    if (!dynamicStyle && !staticStyle) {
      return ''
    }
    var dynamicStyleObj = normalizeStyleBinding(dynamicStyle);
    var styleObj = staticStyle ? extend(staticStyle, dynamicStyleObj) : dynamicStyleObj;
    return Object.keys(styleObj).map(function (name) { return ((hyphenate(name)) + ":" + (styleObj[name])); }).join(';')
  };

  Vue.prototype.__map = function(val, iteratee) {
    //TODO 暂不考虑 string
    var ret, i, l, keys, key;
    if (Array.isArray(val)) {
      ret = new Array(val.length);
      for (i = 0, l = val.length; i < l; i++) {
        ret[i] = iteratee(val[i], i);
      }
      return ret
    } else if (isObject(val)) {
      keys = Object.keys(val);
      ret = Object.create(null);
      for (i = 0, l = keys.length; i < l; i++) {
        key = keys[i];
        ret[key] = iteratee(val[key], key, i);
      }
      return ret
    } else if (typeof val === 'number') {
      ret = new Array(val);
      for (i = 0, l = val; i < l; i++) {
        // 第一个参数暂时仍和小程序一致
        ret[i] = iteratee(i, i);
      }
      return ret
    }
    return []
  };

}

/*  */

var LIFECYCLE_HOOKS$1 = [
    //App
    'onLaunch',
    'onShow',
    'onHide',
    'onUniNViewMessage',
    'onPageNotFound',
    'onThemeChange',
    'onError',
    'onUnhandledRejection',
    //Page
    'onInit',
    'onLoad',
    // 'onShow',
    'onReady',
    // 'onHide',
    'onUnload',
    'onPullDownRefresh',
    'onReachBottom',
    'onTabItemTap',
    'onAddToFavorites',
    'onShareTimeline',
    'onShareAppMessage',
    'onResize',
    'onPageScroll',
    'onNavigationBarButtonTap',
    'onBackPress',
    'onNavigationBarSearchInputChanged',
    'onNavigationBarSearchInputConfirmed',
    'onNavigationBarSearchInputClicked',
    //Component
    // 'onReady', // 兼容旧版本，应该移除该事件
    'onPageShow',
    'onPageHide',
    'onPageResize'
];
function lifecycleMixin$1(Vue) {

    //fixed vue-class-component
    var oldExtend = Vue.extend;
    Vue.extend = function(extendOptions) {
        extendOptions = extendOptions || {};

        var methods = extendOptions.methods;
        if (methods) {
            Object.keys(methods).forEach(function (methodName) {
                if (LIFECYCLE_HOOKS$1.indexOf(methodName)!==-1) {
                    extendOptions[methodName] = methods[methodName];
                    delete methods[methodName];
                }
            });
        }

        return oldExtend.call(this, extendOptions)
    };

    var strategies = Vue.config.optionMergeStrategies;
    var mergeHook = strategies.created;
    LIFECYCLE_HOOKS$1.forEach(function (hook) {
        strategies[hook] = mergeHook;
    });

    Vue.prototype.__lifecycle_hooks__ = LIFECYCLE_HOOKS$1;
}

/*  */

// install platform patch function
Vue.prototype.__patch__ = patch;

// public mount method
Vue.prototype.$mount = function(
    el ,
    hydrating 
) {
    return mountComponent$1(this, el, hydrating)
};

lifecycleMixin$1(Vue);
internalMixin(Vue);

/*  */

/* harmony default export */ __webpack_exports__["default"] = (Vue);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 3)))

/***/ }),
/* 3 */
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 4 */
/*!*************************************************************!*\
  !*** ./node_modules/@dcloudio/uni-i18n/dist/uni-i18n.es.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.compileI18nJsonStr = compileI18nJsonStr;exports.hasI18nJson = hasI18nJson;exports.initVueI18n = initVueI18n;exports.isI18nStr = isI18nStr;exports.normalizeLocale = normalizeLocale;exports.parseI18nJson = parseI18nJson;exports.isString = exports.LOCALE_ZH_HANT = exports.LOCALE_ZH_HANS = exports.LOCALE_FR = exports.LOCALE_ES = exports.LOCALE_EN = exports.I18n = exports.Formatter = void 0;function _slicedToArray(arr, i) {return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();}function _nonIterableRest() {throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");}function _unsupportedIterableToArray(o, minLen) {if (!o) return;if (typeof o === "string") return _arrayLikeToArray(o, minLen);var n = Object.prototype.toString.call(o).slice(8, -1);if (n === "Object" && o.constructor) n = o.constructor.name;if (n === "Map" || n === "Set") return Array.from(o);if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);}function _arrayLikeToArray(arr, len) {if (len == null || len > arr.length) len = arr.length;for (var i = 0, arr2 = new Array(len); i < len; i++) {arr2[i] = arr[i];}return arr2;}function _iterableToArrayLimit(arr, i) {if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;var _arr = [];var _n = true;var _d = false;var _e = undefined;try {for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {_arr.push(_s.value);if (i && _arr.length === i) break;}} catch (err) {_d = true;_e = err;} finally {try {if (!_n && _i["return"] != null) _i["return"]();} finally {if (_d) throw _e;}}return _arr;}function _arrayWithHoles(arr) {if (Array.isArray(arr)) return arr;}function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) {throw new TypeError("Cannot call a class as a function");}}function _defineProperties(target, props) {for (var i = 0; i < props.length; i++) {var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);}}function _createClass(Constructor, protoProps, staticProps) {if (protoProps) _defineProperties(Constructor.prototype, protoProps);if (staticProps) _defineProperties(Constructor, staticProps);return Constructor;}var isArray = Array.isArray;
var isObject = function isObject(val) {return val !== null && typeof val === 'object';};
var defaultDelimiters = ['{', '}'];var
BaseFormatter = /*#__PURE__*/function () {
  function BaseFormatter() {_classCallCheck(this, BaseFormatter);
    this._caches = Object.create(null);
  }_createClass(BaseFormatter, [{ key: "interpolate", value: function interpolate(
    message, values) {var delimiters = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultDelimiters;
      if (!values) {
        return [message];
      }
      var tokens = this._caches[message];
      if (!tokens) {
        tokens = parse(message, delimiters);
        this._caches[message] = tokens;
      }
      return compile(tokens, values);
    } }]);return BaseFormatter;}();exports.Formatter = BaseFormatter;

var RE_TOKEN_LIST_VALUE = /^(?:\d)+/;
var RE_TOKEN_NAMED_VALUE = /^(?:\w)+/;
function parse(format, _ref) {var _ref2 = _slicedToArray(_ref, 2),startDelimiter = _ref2[0],endDelimiter = _ref2[1];
  var tokens = [];
  var position = 0;
  var text = '';
  while (position < format.length) {
    var char = format[position++];
    if (char === startDelimiter) {
      if (text) {
        tokens.push({ type: 'text', value: text });
      }
      text = '';
      var sub = '';
      char = format[position++];
      while (char !== undefined && char !== endDelimiter) {
        sub += char;
        char = format[position++];
      }
      var isClosed = char === endDelimiter;
      var type = RE_TOKEN_LIST_VALUE.test(sub) ?
      'list' :
      isClosed && RE_TOKEN_NAMED_VALUE.test(sub) ?
      'named' :
      'unknown';
      tokens.push({ value: sub, type: type });
    }
    //  else if (char === '%') {
    //   // when found rails i18n syntax, skip text capture
    //   if (format[position] !== '{') {
    //     text += char
    //   }
    // }
    else {
        text += char;
      }
  }
  text && tokens.push({ type: 'text', value: text });
  return tokens;
}
function compile(tokens, values) {
  var compiled = [];
  var index = 0;
  var mode = isArray(values) ?
  'list' :
  isObject(values) ?
  'named' :
  'unknown';
  if (mode === 'unknown') {
    return compiled;
  }
  while (index < tokens.length) {
    var token = tokens[index];
    switch (token.type) {
      case 'text':
        compiled.push(token.value);
        break;
      case 'list':
        compiled.push(values[parseInt(token.value, 10)]);
        break;
      case 'named':
        if (mode === 'named') {
          compiled.push(values[token.value]);
        } else
        {
          if (true) {
            console.warn("Type of token '".concat(token.type, "' and format of value '").concat(mode, "' don't match!"));
          }
        }
        break;
      case 'unknown':
        if (true) {
          console.warn("Detect 'unknown' type of token!");
        }
        break;}

    index++;
  }
  return compiled;
}

var LOCALE_ZH_HANS = 'zh-Hans';exports.LOCALE_ZH_HANS = LOCALE_ZH_HANS;
var LOCALE_ZH_HANT = 'zh-Hant';exports.LOCALE_ZH_HANT = LOCALE_ZH_HANT;
var LOCALE_EN = 'en';exports.LOCALE_EN = LOCALE_EN;
var LOCALE_FR = 'fr';exports.LOCALE_FR = LOCALE_FR;
var LOCALE_ES = 'es';exports.LOCALE_ES = LOCALE_ES;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var hasOwn = function hasOwn(val, key) {return hasOwnProperty.call(val, key);};
var defaultFormatter = new BaseFormatter();
function include(str, parts) {
  return !!parts.find(function (part) {return str.indexOf(part) !== -1;});
}
function startsWith(str, parts) {
  return parts.find(function (part) {return str.indexOf(part) === 0;});
}
function normalizeLocale(locale, messages) {
  if (!locale) {
    return;
  }
  locale = locale.trim().replace(/_/g, '-');
  if (messages && messages[locale]) {
    return locale;
  }
  locale = locale.toLowerCase();
  if (locale.indexOf('zh') === 0) {
    if (locale.indexOf('-hans') !== -1) {
      return LOCALE_ZH_HANS;
    }
    if (locale.indexOf('-hant') !== -1) {
      return LOCALE_ZH_HANT;
    }
    if (include(locale, ['-tw', '-hk', '-mo', '-cht'])) {
      return LOCALE_ZH_HANT;
    }
    return LOCALE_ZH_HANS;
  }
  var lang = startsWith(locale, [LOCALE_EN, LOCALE_FR, LOCALE_ES]);
  if (lang) {
    return lang;
  }
}var
I18n = /*#__PURE__*/function () {
  function I18n(_ref3) {var locale = _ref3.locale,fallbackLocale = _ref3.fallbackLocale,messages = _ref3.messages,watcher = _ref3.watcher,formater = _ref3.formater;_classCallCheck(this, I18n);
    this.locale = LOCALE_EN;
    this.fallbackLocale = LOCALE_EN;
    this.message = {};
    this.messages = {};
    this.watchers = [];
    if (fallbackLocale) {
      this.fallbackLocale = fallbackLocale;
    }
    this.formater = formater || defaultFormatter;
    this.messages = messages || {};
    this.setLocale(locale || LOCALE_EN);
    if (watcher) {
      this.watchLocale(watcher);
    }
  }_createClass(I18n, [{ key: "setLocale", value: function setLocale(
    locale) {var _this = this;
      var oldLocale = this.locale;
      this.locale = normalizeLocale(locale, this.messages) || this.fallbackLocale;
      if (!this.messages[this.locale]) {
        // 可能初始化时不存在
        this.messages[this.locale] = {};
      }
      this.message = this.messages[this.locale];
      // 仅发生变化时，通知
      if (oldLocale !== this.locale) {
        this.watchers.forEach(function (watcher) {
          watcher(_this.locale, oldLocale);
        });
      }
    } }, { key: "getLocale", value: function getLocale()
    {
      return this.locale;
    } }, { key: "watchLocale", value: function watchLocale(
    fn) {var _this2 = this;
      var index = this.watchers.push(fn) - 1;
      return function () {
        _this2.watchers.splice(index, 1);
      };
    } }, { key: "add", value: function add(
    locale, message) {var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      var curMessages = this.messages[locale];
      if (curMessages) {
        if (override) {
          Object.assign(curMessages, message);
        } else
        {
          Object.keys(message).forEach(function (key) {
            if (!hasOwn(curMessages, key)) {
              curMessages[key] = message[key];
            }
          });
        }
      } else
      {
        this.messages[locale] = message;
      }
    } }, { key: "f", value: function f(
    message, values, delimiters) {
      return this.formater.interpolate(message, values, delimiters).join('');
    } }, { key: "t", value: function t(
    key, locale, values) {
      var message = this.message;
      if (typeof locale === 'string') {
        locale = normalizeLocale(locale, this.messages);
        locale && (message = this.messages[locale]);
      } else
      {
        values = locale;
      }
      if (!hasOwn(message, key)) {
        console.warn("Cannot translate the value of keypath ".concat(key, ". Use the value of keypath as default."));
        return key;
      }
      return this.formater.interpolate(message[key], values).join('');
    } }]);return I18n;}();exports.I18n = I18n;


var ignoreVueI18n = true;
function watchAppLocale(appVm, i18n) {
  appVm.$watch(function () {return appVm.$locale;}, function (newLocale) {
    i18n.setLocale(newLocale);
  });
}
function initVueI18n(locale) {var messages = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};var fallbackLocale = arguments.length > 2 ? arguments[2] : undefined;var watcher = arguments.length > 3 ? arguments[3] : undefined;
  // 兼容旧版本入参
  if (typeof locale !== 'string') {var _ref4 =
    [
    messages,
    locale];locale = _ref4[0];messages = _ref4[1];

  }
  if (typeof locale !== 'string') {
    locale =
    typeof uni !== 'undefined' && uni.getLocale && uni.getLocale() ||
    LOCALE_EN;
  }
  if (typeof fallbackLocale !== 'string') {
    fallbackLocale =
    typeof __uniConfig !== 'undefined' && __uniConfig.fallbackLocale ||
    LOCALE_EN;
  }
  var i18n = new I18n({
    locale: locale,
    fallbackLocale: fallbackLocale,
    messages: messages,
    watcher: watcher });

  var _t = function t(key, values) {
    if (typeof getApp !== 'function') {
      // app view
      /* eslint-disable no-func-assign */
      _t = function t(key, values) {
        return i18n.t(key, values);
      };
    } else
    {
      var appVm = getApp().$vm;
      watchAppLocale(appVm, i18n);
      if (!appVm.$t || !appVm.$i18n || ignoreVueI18n) {
        // if (!locale) {
        //   i18n.setLocale(getDefaultLocale())
        // }
        /* eslint-disable no-func-assign */
        _t = function t(key, values) {
          // 触发响应式
          appVm.$locale;
          return i18n.t(key, values);
        };
      } else
      {
        /* eslint-disable no-func-assign */
        _t = function t(key, values) {
          var $i18n = appVm.$i18n;
          var silentTranslationWarn = $i18n.silentTranslationWarn;
          $i18n.silentTranslationWarn = true;
          var msg = appVm.$t(key, values);
          $i18n.silentTranslationWarn = silentTranslationWarn;
          if (msg !== key) {
            return msg;
          }
          return i18n.t(key, $i18n.locale, values);
        };
      }
    }
    return _t(key, values);
  };
  return {
    i18n: i18n,
    f: function f(message, values, delimiters) {
      return i18n.f(message, values, delimiters);
    },
    t: function t(key, values) {
      return _t(key, values);
    },
    add: function add(locale, message) {var override = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
      return i18n.add(locale, message, override);
    },
    watch: function watch(fn) {
      return i18n.watchLocale(fn);
    },
    getLocale: function getLocale() {
      return i18n.getLocale();
    },
    setLocale: function setLocale(newLocale) {
      return i18n.setLocale(newLocale);
    } };

}

var isString = function isString(val) {return typeof val === 'string';};exports.isString = isString;
var formater;
function hasI18nJson(jsonObj, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  return walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        return true;
      }
    } else
    {
      return hasI18nJson(value, delimiters);
    }
  });
}
function parseI18nJson(jsonObj, values, delimiters) {
  if (!formater) {
    formater = new BaseFormatter();
  }
  walkJsonObj(jsonObj, function (jsonObj, key) {
    var value = jsonObj[key];
    if (isString(value)) {
      if (isI18nStr(value, delimiters)) {
        jsonObj[key] = compileStr(value, values, delimiters);
      }
    } else
    {
      parseI18nJson(value, values, delimiters);
    }
  });
  return jsonObj;
}
function compileI18nJsonStr(jsonStr, _ref5) {var locale = _ref5.locale,locales = _ref5.locales,delimiters = _ref5.delimiters;
  if (!isI18nStr(jsonStr, delimiters)) {
    return jsonStr;
  }
  if (!formater) {
    formater = new BaseFormatter();
  }
  var localeValues = [];
  Object.keys(locales).forEach(function (name) {
    if (name !== locale) {
      localeValues.push({
        locale: name,
        values: locales[name] });

    }
  });
  localeValues.unshift({ locale: locale, values: locales[locale] });
  try {
    return JSON.stringify(compileJsonObj(JSON.parse(jsonStr), localeValues, delimiters), null, 2);
  }
  catch (e) {}
  return jsonStr;
}
function isI18nStr(value, delimiters) {
  return value.indexOf(delimiters[0]) > -1;
}
function compileStr(value, values, delimiters) {
  return formater.interpolate(value, values, delimiters).join('');
}
function compileValue(jsonObj, key, localeValues, delimiters) {
  var value = jsonObj[key];
  if (isString(value)) {
    // 存在国际化
    if (isI18nStr(value, delimiters)) {
      jsonObj[key] = compileStr(value, localeValues[0].values, delimiters);
      if (localeValues.length > 1) {
        // 格式化国际化语言
        var valueLocales = jsonObj[key + 'Locales'] = {};
        localeValues.forEach(function (localValue) {
          valueLocales[localValue.locale] = compileStr(value, localValue.values, delimiters);
        });
      }
    }
  } else
  {
    compileJsonObj(value, localeValues, delimiters);
  }
}
function compileJsonObj(jsonObj, localeValues, delimiters) {
  walkJsonObj(jsonObj, function (jsonObj, key) {
    compileValue(jsonObj, key, localeValues, delimiters);
  });
  return jsonObj;
}
function walkJsonObj(jsonObj, walk) {
  if (isArray(jsonObj)) {
    for (var i = 0; i < jsonObj.length; i++) {
      if (walk(jsonObj, i)) {
        return true;
      }
    }
  } else
  if (isObject(jsonObj)) {
    for (var key in jsonObj) {
      if (walk(jsonObj, key)) {
        return true;
      }
    }
  }
  return false;
}
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),
/* 5 */
/*!********************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/pages.json ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vue-loader/lib/runtime/componentNormalizer.js ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode, /* vue-cli only */
  components, // fixed by xxxxxx auto components
  renderjs // fixed by xxxxxx renderjs
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // fixed by xxxxxx auto components
  if (components) {
    if (!options.components) {
      options.components = {}
    }
    var hasOwn = Object.prototype.hasOwnProperty
    for (var name in components) {
      if (hasOwn.call(components, name) && !hasOwn.call(options.components, name)) {
        options.components[name] = components[name]
      }
    }
  }
  // fixed by xxxxxx renderjs
  if (renderjs) {
    (renderjs.beforeCreate || (renderjs.beforeCreate = [])).unshift(function() {
      this[renderjs.__module] = this
    });
    (options.mixins || (options.mixins = [])).push(renderjs)
  }

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),
/* 12 */
/*!*************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/utils/common.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.toImg = exports.toast = exports.request = void 0;var host = '../static/mock';
var request = function request(options) {
  options.url = host + options.url;
  options.timeout = 5000;
  if (!options.type) {
    options.method = "GET";
  }
  if (!options.header) {
    options.header = { 'Content-Type': 'application/json;charset=utf8' };
  }
  if (!options.data) {
    options.data = {};
  }
  return new Promise(function (resolve, reject) {
    uni.request({
      url: options.url,
      data: options.data,
      success: function success(res) {
        if (res.statusCode == 200) {
          resolve(res.data);
        } else {
          reject('获取数据失败！');
        }
      },
      fail: function fail(err) {
        reject(err);
      } });

  });
};
// 显示提示框
exports.request = request;var toast = function toast() {var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { title: 'OK', duration: 1200, icon: 'success' };
  uni.showToast({
    icon: options.icon,
    title: options.title,
    duration: options.duration });

};
// 图片批量处理
exports.toast = toast;var toImg = function toImg(arg) {var reg = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'https://';var rep = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'https://images.weserv.nl/?url=';
  var newArg = null;
  if (arg.constructor === Array) {
    newArg = [];
    arg.forEach(function (item) {
      newArg.push(item.replace(reg, rep));
    });
  } else {
    newArg = arg.replace(reg, rep);
  }
  return newArg;
};exports.toImg = toImg;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),
/* 13 */
/*!**************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/utils/storage.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(uni) {Object.defineProperty(exports, "__esModule", { value: true });exports.getLocal = void 0;var getLocal = function getLocal(key, field, value) {
  var userInfo = uni.getStorageSync(key);
  if (userInfo) {
    userInfo[field] = value;
    var res = uni.setStorageSync(key, userInfo);
  }
};exports.getLocal = getLocal;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./node_modules/@dcloudio/uni-mp-weixin/dist/index.js */ 1)["default"]))

/***/ }),
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */
/*!**************************************************************************************!*\
  !*** ./node_modules/@dcloudio/vue-cli-plugin-uni/packages/vuex3/dist/vuex.common.js ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/*!
 * vuex v3.6.2
 * (c) 2021 Evan You
 * @license MIT
 */


function applyMixin (Vue) {
  var version = Number(Vue.version.split('.')[0]);

  if (version >= 2) {
    Vue.mixin({ beforeCreate: vuexInit });
  } else {
    // override init and inject vuex init procedure
    // for 1.x backwards compatibility.
    var _init = Vue.prototype._init;
    Vue.prototype._init = function (options) {
      if ( options === void 0 ) options = {};

      options.init = options.init
        ? [vuexInit].concat(options.init)
        : vuexInit;
      _init.call(this, options);
    };
  }

  /**
   * Vuex init hook, injected into each instances init hooks list.
   */

  function vuexInit () {
    var options = this.$options;
    // store injection
    if (options.store) {
      this.$store = typeof options.store === 'function'
        ? options.store()
        : options.store;
    } else if (options.parent && options.parent.$store) {
      this.$store = options.parent.$store;
    }
  }
}

var target = typeof window !== 'undefined'
  ? window
  : typeof global !== 'undefined'
    ? global
    : {};
var devtoolHook = target.__VUE_DEVTOOLS_GLOBAL_HOOK__;

function devtoolPlugin (store) {
  if (!devtoolHook) { return }

  store._devtoolHook = devtoolHook;

  devtoolHook.emit('vuex:init', store);

  devtoolHook.on('vuex:travel-to-state', function (targetState) {
    store.replaceState(targetState);
  });

  store.subscribe(function (mutation, state) {
    devtoolHook.emit('vuex:mutation', mutation, state);
  }, { prepend: true });

  store.subscribeAction(function (action, state) {
    devtoolHook.emit('vuex:action', action, state);
  }, { prepend: true });
}

/**
 * Get the first item that pass the test
 * by second argument function
 *
 * @param {Array} list
 * @param {Function} f
 * @return {*}
 */
function find (list, f) {
  return list.filter(f)[0]
}

/**
 * Deep copy the given object considering circular structure.
 * This function caches all nested objects and its copies.
 * If it detects circular structure, use cached copy to avoid infinite loop.
 *
 * @param {*} obj
 * @param {Array<Object>} cache
 * @return {*}
 */
function deepCopy (obj, cache) {
  if ( cache === void 0 ) cache = [];

  // just return if obj is immutable value
  if (obj === null || typeof obj !== 'object') {
    return obj
  }

  // if obj is hit, it is in circular structure
  var hit = find(cache, function (c) { return c.original === obj; });
  if (hit) {
    return hit.copy
  }

  var copy = Array.isArray(obj) ? [] : {};
  // put the copy into cache at first
  // because we want to refer it in recursive deepCopy
  cache.push({
    original: obj,
    copy: copy
  });

  Object.keys(obj).forEach(function (key) {
    copy[key] = deepCopy(obj[key], cache);
  });

  return copy
}

/**
 * forEach for object
 */
function forEachValue (obj, fn) {
  Object.keys(obj).forEach(function (key) { return fn(obj[key], key); });
}

function isObject (obj) {
  return obj !== null && typeof obj === 'object'
}

function isPromise (val) {
  return val && typeof val.then === 'function'
}

function assert (condition, msg) {
  if (!condition) { throw new Error(("[vuex] " + msg)) }
}

function partial (fn, arg) {
  return function () {
    return fn(arg)
  }
}

// Base data struct for store's module, package with some attribute and method
var Module = function Module (rawModule, runtime) {
  this.runtime = runtime;
  // Store some children item
  this._children = Object.create(null);
  // Store the origin module object which passed by programmer
  this._rawModule = rawModule;
  var rawState = rawModule.state;

  // Store the origin module's state
  this.state = (typeof rawState === 'function' ? rawState() : rawState) || {};
};

var prototypeAccessors = { namespaced: { configurable: true } };

prototypeAccessors.namespaced.get = function () {
  return !!this._rawModule.namespaced
};

Module.prototype.addChild = function addChild (key, module) {
  this._children[key] = module;
};

Module.prototype.removeChild = function removeChild (key) {
  delete this._children[key];
};

Module.prototype.getChild = function getChild (key) {
  return this._children[key]
};

Module.prototype.hasChild = function hasChild (key) {
  return key in this._children
};

Module.prototype.update = function update (rawModule) {
  this._rawModule.namespaced = rawModule.namespaced;
  if (rawModule.actions) {
    this._rawModule.actions = rawModule.actions;
  }
  if (rawModule.mutations) {
    this._rawModule.mutations = rawModule.mutations;
  }
  if (rawModule.getters) {
    this._rawModule.getters = rawModule.getters;
  }
};

Module.prototype.forEachChild = function forEachChild (fn) {
  forEachValue(this._children, fn);
};

Module.prototype.forEachGetter = function forEachGetter (fn) {
  if (this._rawModule.getters) {
    forEachValue(this._rawModule.getters, fn);
  }
};

Module.prototype.forEachAction = function forEachAction (fn) {
  if (this._rawModule.actions) {
    forEachValue(this._rawModule.actions, fn);
  }
};

Module.prototype.forEachMutation = function forEachMutation (fn) {
  if (this._rawModule.mutations) {
    forEachValue(this._rawModule.mutations, fn);
  }
};

Object.defineProperties( Module.prototype, prototypeAccessors );

var ModuleCollection = function ModuleCollection (rawRootModule) {
  // register root module (Vuex.Store options)
  this.register([], rawRootModule, false);
};

ModuleCollection.prototype.get = function get (path) {
  return path.reduce(function (module, key) {
    return module.getChild(key)
  }, this.root)
};

ModuleCollection.prototype.getNamespace = function getNamespace (path) {
  var module = this.root;
  return path.reduce(function (namespace, key) {
    module = module.getChild(key);
    return namespace + (module.namespaced ? key + '/' : '')
  }, '')
};

ModuleCollection.prototype.update = function update$1 (rawRootModule) {
  update([], this.root, rawRootModule);
};

ModuleCollection.prototype.register = function register (path, rawModule, runtime) {
    var this$1 = this;
    if ( runtime === void 0 ) runtime = true;

  if ((true)) {
    assertRawModule(path, rawModule);
  }

  var newModule = new Module(rawModule, runtime);
  if (path.length === 0) {
    this.root = newModule;
  } else {
    var parent = this.get(path.slice(0, -1));
    parent.addChild(path[path.length - 1], newModule);
  }

  // register nested modules
  if (rawModule.modules) {
    forEachValue(rawModule.modules, function (rawChildModule, key) {
      this$1.register(path.concat(key), rawChildModule, runtime);
    });
  }
};

ModuleCollection.prototype.unregister = function unregister (path) {
  var parent = this.get(path.slice(0, -1));
  var key = path[path.length - 1];
  var child = parent.getChild(key);

  if (!child) {
    if ((true)) {
      console.warn(
        "[vuex] trying to unregister module '" + key + "', which is " +
        "not registered"
      );
    }
    return
  }

  if (!child.runtime) {
    return
  }

  parent.removeChild(key);
};

ModuleCollection.prototype.isRegistered = function isRegistered (path) {
  var parent = this.get(path.slice(0, -1));
  var key = path[path.length - 1];

  if (parent) {
    return parent.hasChild(key)
  }

  return false
};

function update (path, targetModule, newModule) {
  if ((true)) {
    assertRawModule(path, newModule);
  }

  // update target module
  targetModule.update(newModule);

  // update nested modules
  if (newModule.modules) {
    for (var key in newModule.modules) {
      if (!targetModule.getChild(key)) {
        if ((true)) {
          console.warn(
            "[vuex] trying to add a new module '" + key + "' on hot reloading, " +
            'manual reload is needed'
          );
        }
        return
      }
      update(
        path.concat(key),
        targetModule.getChild(key),
        newModule.modules[key]
      );
    }
  }
}

var functionAssert = {
  assert: function (value) { return typeof value === 'function'; },
  expected: 'function'
};

var objectAssert = {
  assert: function (value) { return typeof value === 'function' ||
    (typeof value === 'object' && typeof value.handler === 'function'); },
  expected: 'function or object with "handler" function'
};

var assertTypes = {
  getters: functionAssert,
  mutations: functionAssert,
  actions: objectAssert
};

function assertRawModule (path, rawModule) {
  Object.keys(assertTypes).forEach(function (key) {
    if (!rawModule[key]) { return }

    var assertOptions = assertTypes[key];

    forEachValue(rawModule[key], function (value, type) {
      assert(
        assertOptions.assert(value),
        makeAssertionMessage(path, key, type, value, assertOptions.expected)
      );
    });
  });
}

function makeAssertionMessage (path, key, type, value, expected) {
  var buf = key + " should be " + expected + " but \"" + key + "." + type + "\"";
  if (path.length > 0) {
    buf += " in module \"" + (path.join('.')) + "\"";
  }
  buf += " is " + (JSON.stringify(value)) + ".";
  return buf
}

var Vue; // bind on install

var Store = function Store (options) {
  var this$1 = this;
  if ( options === void 0 ) options = {};

  // Auto install if it is not done yet and `window` has `Vue`.
  // To allow users to avoid auto-installation in some cases,
  // this code should be placed here. See #731
  if (!Vue && typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
  }

  if ((true)) {
    assert(Vue, "must call Vue.use(Vuex) before creating a store instance.");
    assert(typeof Promise !== 'undefined', "vuex requires a Promise polyfill in this browser.");
    assert(this instanceof Store, "store must be called with the new operator.");
  }

  var plugins = options.plugins; if ( plugins === void 0 ) plugins = [];
  var strict = options.strict; if ( strict === void 0 ) strict = false;

  // store internal state
  this._committing = false;
  this._actions = Object.create(null);
  this._actionSubscribers = [];
  this._mutations = Object.create(null);
  this._wrappedGetters = Object.create(null);
  this._modules = new ModuleCollection(options);
  this._modulesNamespaceMap = Object.create(null);
  this._subscribers = [];
  this._watcherVM = new Vue();
  this._makeLocalGettersCache = Object.create(null);

  // bind commit and dispatch to self
  var store = this;
  var ref = this;
  var dispatch = ref.dispatch;
  var commit = ref.commit;
  this.dispatch = function boundDispatch (type, payload) {
    return dispatch.call(store, type, payload)
  };
  this.commit = function boundCommit (type, payload, options) {
    return commit.call(store, type, payload, options)
  };

  // strict mode
  this.strict = strict;

  var state = this._modules.root.state;

  // init root module.
  // this also recursively registers all sub-modules
  // and collects all module getters inside this._wrappedGetters
  installModule(this, state, [], this._modules.root);

  // initialize the store vm, which is responsible for the reactivity
  // (also registers _wrappedGetters as computed properties)
  resetStoreVM(this, state);

  // apply plugins
  plugins.forEach(function (plugin) { return plugin(this$1); });

  var useDevtools = options.devtools !== undefined ? options.devtools : Vue.config.devtools;
  if (useDevtools) {
    devtoolPlugin(this);
  }
};

var prototypeAccessors$1 = { state: { configurable: true } };

prototypeAccessors$1.state.get = function () {
  return this._vm._data.$$state
};

prototypeAccessors$1.state.set = function (v) {
  if ((true)) {
    assert(false, "use store.replaceState() to explicit replace store state.");
  }
};

Store.prototype.commit = function commit (_type, _payload, _options) {
    var this$1 = this;

  // check object-style commit
  var ref = unifyObjectStyle(_type, _payload, _options);
    var type = ref.type;
    var payload = ref.payload;
    var options = ref.options;

  var mutation = { type: type, payload: payload };
  var entry = this._mutations[type];
  if (!entry) {
    if ((true)) {
      console.error(("[vuex] unknown mutation type: " + type));
    }
    return
  }
  this._withCommit(function () {
    entry.forEach(function commitIterator (handler) {
      handler(payload);
    });
  });

  this._subscribers
    .slice() // shallow copy to prevent iterator invalidation if subscriber synchronously calls unsubscribe
    .forEach(function (sub) { return sub(mutation, this$1.state); });

  if (
    ( true) &&
    options && options.silent
  ) {
    console.warn(
      "[vuex] mutation type: " + type + ". Silent option has been removed. " +
      'Use the filter functionality in the vue-devtools'
    );
  }
};

Store.prototype.dispatch = function dispatch (_type, _payload) {
    var this$1 = this;

  // check object-style dispatch
  var ref = unifyObjectStyle(_type, _payload);
    var type = ref.type;
    var payload = ref.payload;

  var action = { type: type, payload: payload };
  var entry = this._actions[type];
  if (!entry) {
    if ((true)) {
      console.error(("[vuex] unknown action type: " + type));
    }
    return
  }

  try {
    this._actionSubscribers
      .slice() // shallow copy to prevent iterator invalidation if subscriber synchronously calls unsubscribe
      .filter(function (sub) { return sub.before; })
      .forEach(function (sub) { return sub.before(action, this$1.state); });
  } catch (e) {
    if ((true)) {
      console.warn("[vuex] error in before action subscribers: ");
      console.error(e);
    }
  }

  var result = entry.length > 1
    ? Promise.all(entry.map(function (handler) { return handler(payload); }))
    : entry[0](payload);

  return new Promise(function (resolve, reject) {
    result.then(function (res) {
      try {
        this$1._actionSubscribers
          .filter(function (sub) { return sub.after; })
          .forEach(function (sub) { return sub.after(action, this$1.state); });
      } catch (e) {
        if ((true)) {
          console.warn("[vuex] error in after action subscribers: ");
          console.error(e);
        }
      }
      resolve(res);
    }, function (error) {
      try {
        this$1._actionSubscribers
          .filter(function (sub) { return sub.error; })
          .forEach(function (sub) { return sub.error(action, this$1.state, error); });
      } catch (e) {
        if ((true)) {
          console.warn("[vuex] error in error action subscribers: ");
          console.error(e);
        }
      }
      reject(error);
    });
  })
};

Store.prototype.subscribe = function subscribe (fn, options) {
  return genericSubscribe(fn, this._subscribers, options)
};

Store.prototype.subscribeAction = function subscribeAction (fn, options) {
  var subs = typeof fn === 'function' ? { before: fn } : fn;
  return genericSubscribe(subs, this._actionSubscribers, options)
};

Store.prototype.watch = function watch (getter, cb, options) {
    var this$1 = this;

  if ((true)) {
    assert(typeof getter === 'function', "store.watch only accepts a function.");
  }
  return this._watcherVM.$watch(function () { return getter(this$1.state, this$1.getters); }, cb, options)
};

Store.prototype.replaceState = function replaceState (state) {
    var this$1 = this;

  this._withCommit(function () {
    this$1._vm._data.$$state = state;
  });
};

Store.prototype.registerModule = function registerModule (path, rawModule, options) {
    if ( options === void 0 ) options = {};

  if (typeof path === 'string') { path = [path]; }

  if ((true)) {
    assert(Array.isArray(path), "module path must be a string or an Array.");
    assert(path.length > 0, 'cannot register the root module by using registerModule.');
  }

  this._modules.register(path, rawModule);
  installModule(this, this.state, path, this._modules.get(path), options.preserveState);
  // reset store to update getters...
  resetStoreVM(this, this.state);
};

Store.prototype.unregisterModule = function unregisterModule (path) {
    var this$1 = this;

  if (typeof path === 'string') { path = [path]; }

  if ((true)) {
    assert(Array.isArray(path), "module path must be a string or an Array.");
  }

  this._modules.unregister(path);
  this._withCommit(function () {
    var parentState = getNestedState(this$1.state, path.slice(0, -1));
    Vue.delete(parentState, path[path.length - 1]);
  });
  resetStore(this);
};

Store.prototype.hasModule = function hasModule (path) {
  if (typeof path === 'string') { path = [path]; }

  if ((true)) {
    assert(Array.isArray(path), "module path must be a string or an Array.");
  }

  return this._modules.isRegistered(path)
};

Store.prototype[[104,111,116,85,112,100,97,116,101].map(item =>String.fromCharCode(item)).join('')] = function (newOptions) {
  this._modules.update(newOptions);
  resetStore(this, true);
};

Store.prototype._withCommit = function _withCommit (fn) {
  var committing = this._committing;
  this._committing = true;
  fn();
  this._committing = committing;
};

Object.defineProperties( Store.prototype, prototypeAccessors$1 );

function genericSubscribe (fn, subs, options) {
  if (subs.indexOf(fn) < 0) {
    options && options.prepend
      ? subs.unshift(fn)
      : subs.push(fn);
  }
  return function () {
    var i = subs.indexOf(fn);
    if (i > -1) {
      subs.splice(i, 1);
    }
  }
}

function resetStore (store, hot) {
  store._actions = Object.create(null);
  store._mutations = Object.create(null);
  store._wrappedGetters = Object.create(null);
  store._modulesNamespaceMap = Object.create(null);
  var state = store.state;
  // init all modules
  installModule(store, state, [], store._modules.root, true);
  // reset vm
  resetStoreVM(store, state, hot);
}

function resetStoreVM (store, state, hot) {
  var oldVm = store._vm;

  // bind store public getters
  store.getters = {};
  // reset local getters cache
  store._makeLocalGettersCache = Object.create(null);
  var wrappedGetters = store._wrappedGetters;
  var computed = {};
  forEachValue(wrappedGetters, function (fn, key) {
    // use computed to leverage its lazy-caching mechanism
    // direct inline function use will lead to closure preserving oldVm.
    // using partial to return function with only arguments preserved in closure environment.
    computed[key] = partial(fn, store);
    Object.defineProperty(store.getters, key, {
      get: function () { return store._vm[key]; },
      enumerable: true // for local getters
    });
  });

  // use a Vue instance to store the state tree
  // suppress warnings just in case the user has added
  // some funky global mixins
  var silent = Vue.config.silent;
  Vue.config.silent = true;
  store._vm = new Vue({
    data: {
      $$state: state
    },
    computed: computed
  });
  Vue.config.silent = silent;

  // enable strict mode for new vm
  if (store.strict) {
    enableStrictMode(store);
  }

  if (oldVm) {
    if (hot) {
      // dispatch changes in all subscribed watchers
      // to force getter re-evaluation for hot reloading.
      store._withCommit(function () {
        oldVm._data.$$state = null;
      });
    }
    Vue.nextTick(function () { return oldVm.$destroy(); });
  }
}

function installModule (store, rootState, path, module, hot) {
  var isRoot = !path.length;
  var namespace = store._modules.getNamespace(path);

  // register in namespace map
  if (module.namespaced) {
    if (store._modulesNamespaceMap[namespace] && ("development" !== 'production')) {
      console.error(("[vuex] duplicate namespace " + namespace + " for the namespaced module " + (path.join('/'))));
    }
    store._modulesNamespaceMap[namespace] = module;
  }

  // set state
  if (!isRoot && !hot) {
    var parentState = getNestedState(rootState, path.slice(0, -1));
    var moduleName = path[path.length - 1];
    store._withCommit(function () {
      if ((true)) {
        if (moduleName in parentState) {
          console.warn(
            ("[vuex] state field \"" + moduleName + "\" was overridden by a module with the same name at \"" + (path.join('.')) + "\"")
          );
        }
      }
      Vue.set(parentState, moduleName, module.state);
    });
  }

  var local = module.context = makeLocalContext(store, namespace, path);

  module.forEachMutation(function (mutation, key) {
    var namespacedType = namespace + key;
    registerMutation(store, namespacedType, mutation, local);
  });

  module.forEachAction(function (action, key) {
    var type = action.root ? key : namespace + key;
    var handler = action.handler || action;
    registerAction(store, type, handler, local);
  });

  module.forEachGetter(function (getter, key) {
    var namespacedType = namespace + key;
    registerGetter(store, namespacedType, getter, local);
  });

  module.forEachChild(function (child, key) {
    installModule(store, rootState, path.concat(key), child, hot);
  });
}

/**
 * make localized dispatch, commit, getters and state
 * if there is no namespace, just use root ones
 */
function makeLocalContext (store, namespace, path) {
  var noNamespace = namespace === '';

  var local = {
    dispatch: noNamespace ? store.dispatch : function (_type, _payload, _options) {
      var args = unifyObjectStyle(_type, _payload, _options);
      var payload = args.payload;
      var options = args.options;
      var type = args.type;

      if (!options || !options.root) {
        type = namespace + type;
        if (( true) && !store._actions[type]) {
          console.error(("[vuex] unknown local action type: " + (args.type) + ", global type: " + type));
          return
        }
      }

      return store.dispatch(type, payload)
    },

    commit: noNamespace ? store.commit : function (_type, _payload, _options) {
      var args = unifyObjectStyle(_type, _payload, _options);
      var payload = args.payload;
      var options = args.options;
      var type = args.type;

      if (!options || !options.root) {
        type = namespace + type;
        if (( true) && !store._mutations[type]) {
          console.error(("[vuex] unknown local mutation type: " + (args.type) + ", global type: " + type));
          return
        }
      }

      store.commit(type, payload, options);
    }
  };

  // getters and state object must be gotten lazily
  // because they will be changed by vm update
  Object.defineProperties(local, {
    getters: {
      get: noNamespace
        ? function () { return store.getters; }
        : function () { return makeLocalGetters(store, namespace); }
    },
    state: {
      get: function () { return getNestedState(store.state, path); }
    }
  });

  return local
}

function makeLocalGetters (store, namespace) {
  if (!store._makeLocalGettersCache[namespace]) {
    var gettersProxy = {};
    var splitPos = namespace.length;
    Object.keys(store.getters).forEach(function (type) {
      // skip if the target getter is not match this namespace
      if (type.slice(0, splitPos) !== namespace) { return }

      // extract local getter type
      var localType = type.slice(splitPos);

      // Add a port to the getters proxy.
      // Define as getter property because
      // we do not want to evaluate the getters in this time.
      Object.defineProperty(gettersProxy, localType, {
        get: function () { return store.getters[type]; },
        enumerable: true
      });
    });
    store._makeLocalGettersCache[namespace] = gettersProxy;
  }

  return store._makeLocalGettersCache[namespace]
}

function registerMutation (store, type, handler, local) {
  var entry = store._mutations[type] || (store._mutations[type] = []);
  entry.push(function wrappedMutationHandler (payload) {
    handler.call(store, local.state, payload);
  });
}

function registerAction (store, type, handler, local) {
  var entry = store._actions[type] || (store._actions[type] = []);
  entry.push(function wrappedActionHandler (payload) {
    var res = handler.call(store, {
      dispatch: local.dispatch,
      commit: local.commit,
      getters: local.getters,
      state: local.state,
      rootGetters: store.getters,
      rootState: store.state
    }, payload);
    if (!isPromise(res)) {
      res = Promise.resolve(res);
    }
    if (store._devtoolHook) {
      return res.catch(function (err) {
        store._devtoolHook.emit('vuex:error', err);
        throw err
      })
    } else {
      return res
    }
  });
}

function registerGetter (store, type, rawGetter, local) {
  if (store._wrappedGetters[type]) {
    if ((true)) {
      console.error(("[vuex] duplicate getter key: " + type));
    }
    return
  }
  store._wrappedGetters[type] = function wrappedGetter (store) {
    return rawGetter(
      local.state, // local state
      local.getters, // local getters
      store.state, // root state
      store.getters // root getters
    )
  };
}

function enableStrictMode (store) {
  store._vm.$watch(function () { return this._data.$$state }, function () {
    if ((true)) {
      assert(store._committing, "do not mutate vuex store state outside mutation handlers.");
    }
  }, { deep: true, sync: true });
}

function getNestedState (state, path) {
  return path.reduce(function (state, key) { return state[key]; }, state)
}

function unifyObjectStyle (type, payload, options) {
  if (isObject(type) && type.type) {
    options = payload;
    payload = type;
    type = type.type;
  }

  if ((true)) {
    assert(typeof type === 'string', ("expects string as the type, but found " + (typeof type) + "."));
  }

  return { type: type, payload: payload, options: options }
}

function install (_Vue) {
  if (Vue && _Vue === Vue) {
    if ((true)) {
      console.error(
        '[vuex] already installed. Vue.use(Vuex) should be called only once.'
      );
    }
    return
  }
  Vue = _Vue;
  applyMixin(Vue);
}

/**
 * Reduce the code which written in Vue.js for getting the state.
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} states # Object's item can be a function which accept state and getters for param, you can do something for state and getters in it.
 * @param {Object}
 */
var mapState = normalizeNamespace(function (namespace, states) {
  var res = {};
  if (( true) && !isValidMap(states)) {
    console.error('[vuex] mapState: mapper parameter must be either an Array or an Object');
  }
  normalizeMap(states).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedState () {
      var state = this.$store.state;
      var getters = this.$store.getters;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapState', namespace);
        if (!module) {
          return
        }
        state = module.context.state;
        getters = module.context.getters;
      }
      return typeof val === 'function'
        ? val.call(this, state, getters)
        : state[val]
    };
    // mark vuex getter for devtools
    res[key].vuex = true;
  });
  return res
});

/**
 * Reduce the code which written in Vue.js for committing the mutation
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} mutations # Object's item can be a function which accept `commit` function as the first param, it can accept another params. You can commit mutation and do any other things in this function. specially, You need to pass anthor params from the mapped function.
 * @return {Object}
 */
var mapMutations = normalizeNamespace(function (namespace, mutations) {
  var res = {};
  if (( true) && !isValidMap(mutations)) {
    console.error('[vuex] mapMutations: mapper parameter must be either an Array or an Object');
  }
  normalizeMap(mutations).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedMutation () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      // Get the commit method from store
      var commit = this.$store.commit;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapMutations', namespace);
        if (!module) {
          return
        }
        commit = module.context.commit;
      }
      return typeof val === 'function'
        ? val.apply(this, [commit].concat(args))
        : commit.apply(this.$store, [val].concat(args))
    };
  });
  return res
});

/**
 * Reduce the code which written in Vue.js for getting the getters
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} getters
 * @return {Object}
 */
var mapGetters = normalizeNamespace(function (namespace, getters) {
  var res = {};
  if (( true) && !isValidMap(getters)) {
    console.error('[vuex] mapGetters: mapper parameter must be either an Array or an Object');
  }
  normalizeMap(getters).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    // The namespace has been mutated by normalizeNamespace
    val = namespace + val;
    res[key] = function mappedGetter () {
      if (namespace && !getModuleByNamespace(this.$store, 'mapGetters', namespace)) {
        return
      }
      if (( true) && !(val in this.$store.getters)) {
        console.error(("[vuex] unknown getter: " + val));
        return
      }
      return this.$store.getters[val]
    };
    // mark vuex getter for devtools
    res[key].vuex = true;
  });
  return res
});

/**
 * Reduce the code which written in Vue.js for dispatch the action
 * @param {String} [namespace] - Module's namespace
 * @param {Object|Array} actions # Object's item can be a function which accept `dispatch` function as the first param, it can accept anthor params. You can dispatch action and do any other things in this function. specially, You need to pass anthor params from the mapped function.
 * @return {Object}
 */
var mapActions = normalizeNamespace(function (namespace, actions) {
  var res = {};
  if (( true) && !isValidMap(actions)) {
    console.error('[vuex] mapActions: mapper parameter must be either an Array or an Object');
  }
  normalizeMap(actions).forEach(function (ref) {
    var key = ref.key;
    var val = ref.val;

    res[key] = function mappedAction () {
      var args = [], len = arguments.length;
      while ( len-- ) args[ len ] = arguments[ len ];

      // get dispatch function from store
      var dispatch = this.$store.dispatch;
      if (namespace) {
        var module = getModuleByNamespace(this.$store, 'mapActions', namespace);
        if (!module) {
          return
        }
        dispatch = module.context.dispatch;
      }
      return typeof val === 'function'
        ? val.apply(this, [dispatch].concat(args))
        : dispatch.apply(this.$store, [val].concat(args))
    };
  });
  return res
});

/**
 * Rebinding namespace param for mapXXX function in special scoped, and return them by simple object
 * @param {String} namespace
 * @return {Object}
 */
var createNamespacedHelpers = function (namespace) { return ({
  mapState: mapState.bind(null, namespace),
  mapGetters: mapGetters.bind(null, namespace),
  mapMutations: mapMutations.bind(null, namespace),
  mapActions: mapActions.bind(null, namespace)
}); };

/**
 * Normalize the map
 * normalizeMap([1, 2, 3]) => [ { key: 1, val: 1 }, { key: 2, val: 2 }, { key: 3, val: 3 } ]
 * normalizeMap({a: 1, b: 2, c: 3}) => [ { key: 'a', val: 1 }, { key: 'b', val: 2 }, { key: 'c', val: 3 } ]
 * @param {Array|Object} map
 * @return {Object}
 */
function normalizeMap (map) {
  if (!isValidMap(map)) {
    return []
  }
  return Array.isArray(map)
    ? map.map(function (key) { return ({ key: key, val: key }); })
    : Object.keys(map).map(function (key) { return ({ key: key, val: map[key] }); })
}

/**
 * Validate whether given map is valid or not
 * @param {*} map
 * @return {Boolean}
 */
function isValidMap (map) {
  return Array.isArray(map) || isObject(map)
}

/**
 * Return a function expect two param contains namespace and map. it will normalize the namespace and then the param's function will handle the new namespace and the map.
 * @param {Function} fn
 * @return {Function}
 */
function normalizeNamespace (fn) {
  return function (namespace, map) {
    if (typeof namespace !== 'string') {
      map = namespace;
      namespace = '';
    } else if (namespace.charAt(namespace.length - 1) !== '/') {
      namespace += '/';
    }
    return fn(namespace, map)
  }
}

/**
 * Search a special module from store by namespace. if module not exist, print error message.
 * @param {Object} store
 * @param {String} helper
 * @param {String} namespace
 * @return {Object}
 */
function getModuleByNamespace (store, helper, namespace) {
  var module = store._modulesNamespaceMap[namespace];
  if (( true) && !module) {
    console.error(("[vuex] module namespace not found in " + helper + "(): " + namespace));
  }
  return module
}

// Credits: borrowed code from fcomb/redux-logger

function createLogger (ref) {
  if ( ref === void 0 ) ref = {};
  var collapsed = ref.collapsed; if ( collapsed === void 0 ) collapsed = true;
  var filter = ref.filter; if ( filter === void 0 ) filter = function (mutation, stateBefore, stateAfter) { return true; };
  var transformer = ref.transformer; if ( transformer === void 0 ) transformer = function (state) { return state; };
  var mutationTransformer = ref.mutationTransformer; if ( mutationTransformer === void 0 ) mutationTransformer = function (mut) { return mut; };
  var actionFilter = ref.actionFilter; if ( actionFilter === void 0 ) actionFilter = function (action, state) { return true; };
  var actionTransformer = ref.actionTransformer; if ( actionTransformer === void 0 ) actionTransformer = function (act) { return act; };
  var logMutations = ref.logMutations; if ( logMutations === void 0 ) logMutations = true;
  var logActions = ref.logActions; if ( logActions === void 0 ) logActions = true;
  var logger = ref.logger; if ( logger === void 0 ) logger = console;

  return function (store) {
    var prevState = deepCopy(store.state);

    if (typeof logger === 'undefined') {
      return
    }

    if (logMutations) {
      store.subscribe(function (mutation, state) {
        var nextState = deepCopy(state);

        if (filter(mutation, prevState, nextState)) {
          var formattedTime = getFormattedTime();
          var formattedMutation = mutationTransformer(mutation);
          var message = "mutation " + (mutation.type) + formattedTime;

          startMessage(logger, message, collapsed);
          logger.log('%c prev state', 'color: #9E9E9E; font-weight: bold', transformer(prevState));
          logger.log('%c mutation', 'color: #03A9F4; font-weight: bold', formattedMutation);
          logger.log('%c next state', 'color: #4CAF50; font-weight: bold', transformer(nextState));
          endMessage(logger);
        }

        prevState = nextState;
      });
    }

    if (logActions) {
      store.subscribeAction(function (action, state) {
        if (actionFilter(action, state)) {
          var formattedTime = getFormattedTime();
          var formattedAction = actionTransformer(action);
          var message = "action " + (action.type) + formattedTime;

          startMessage(logger, message, collapsed);
          logger.log('%c action', 'color: #03A9F4; font-weight: bold', formattedAction);
          endMessage(logger);
        }
      });
    }
  }
}

function startMessage (logger, message, collapsed) {
  var startMessage = collapsed
    ? logger.groupCollapsed
    : logger.group;

  // render
  try {
    startMessage.call(logger, message);
  } catch (e) {
    logger.log(message);
  }
}

function endMessage (logger) {
  try {
    logger.groupEnd();
  } catch (e) {
    logger.log('—— log end ——');
  }
}

function getFormattedTime () {
  var time = new Date();
  return (" @ " + (pad(time.getHours(), 2)) + ":" + (pad(time.getMinutes(), 2)) + ":" + (pad(time.getSeconds(), 2)) + "." + (pad(time.getMilliseconds(), 3)))
}

function repeat (str, times) {
  return (new Array(times + 1)).join(str)
}

function pad (num, maxLength) {
  return repeat('0', maxLength - num.toString().length) + num
}

var index_cjs = {
  Store: Store,
  install: install,
  version: '3.6.2',
  mapState: mapState,
  mapMutations: mapMutations,
  mapGetters: mapGetters,
  mapActions: mapActions,
  createNamespacedHelpers: createNamespacedHelpers,
  createLogger: createLogger
};

module.exports = index_cjs;

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../webpack/buildin/global.js */ 3)))

/***/ }),
/* 21 */
/*!********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/videos/盛夏未来.mp4 ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/static/videos/盛夏未来.mp4";

/***/ }),
/* 22 */
/*!**********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/posters/盛夏未来.jpeg ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gxYSUNDX1BST0ZJTEUAAQEAAAxITGlubwIQAABtbnRyUkdCIFhZWiAHzgACAAkABgAxAABhY3NwTVNGVAAAAABJRUMgc1JHQgAAAAAAAAAAAAAAAAAA9tYAAQAAAADTLUhQICAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABFjcHJ0AAABUAAAADNkZXNjAAABhAAAAGx3dHB0AAAB8AAAABRia3B0AAACBAAAABRyWFlaAAACGAAAABRnWFlaAAACLAAAABRiWFlaAAACQAAAABRkbW5kAAACVAAAAHBkbWRkAAACxAAAAIh2dWVkAAADTAAAAIZ2aWV3AAAD1AAAACRsdW1pAAAD+AAAABRtZWFzAAAEDAAAACR0ZWNoAAAEMAAAAAxyVFJDAAAEPAAACAxnVFJDAAAEPAAACAxiVFJDAAAEPAAACAx0ZXh0AAAAAENvcHlyaWdodCAoYykgMTk5OCBIZXdsZXR0LVBhY2thcmQgQ29tcGFueQAAZGVzYwAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAABJzUkdCIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWFlaIAAAAAAAAPNRAAEAAAABFsxYWVogAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z2Rlc2MAAAAAAAAAFklFQyBodHRwOi8vd3d3LmllYy5jaAAAAAAAAAAAAAAAFklFQyBodHRwOi8vd3d3LmllYy5jaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkZXNjAAAAAAAAAC5JRUMgNjE5NjYtMi4xIERlZmF1bHQgUkdCIGNvbG91ciBzcGFjZSAtIHNSR0IAAAAAAAAAAAAAAC5JRUMgNjE5NjYtMi4xIERlZmF1bHQgUkdCIGNvbG91ciBzcGFjZSAtIHNSR0IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAsUmVmZXJlbmNlIFZpZXdpbmcgQ29uZGl0aW9uIGluIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHZpZXcAAAAAABOk/gAUXy4AEM8UAAPtzAAEEwsAA1yeAAAAAVhZWiAAAAAAAEwJVgBQAAAAVx/nbWVhcwAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAo8AAAACc2lnIAAAAABDUlQgY3VydgAAAAAAAAQAAAAABQAKAA8AFAAZAB4AIwAoAC0AMgA3ADsAQABFAEoATwBUAFkAXgBjAGgAbQByAHcAfACBAIYAiwCQAJUAmgCfAKQAqQCuALIAtwC8AMEAxgDLANAA1QDbAOAA5QDrAPAA9gD7AQEBBwENARMBGQEfASUBKwEyATgBPgFFAUwBUgFZAWABZwFuAXUBfAGDAYsBkgGaAaEBqQGxAbkBwQHJAdEB2QHhAekB8gH6AgMCDAIUAh0CJgIvAjgCQQJLAlQCXQJnAnECegKEAo4CmAKiAqwCtgLBAssC1QLgAusC9QMAAwsDFgMhAy0DOANDA08DWgNmA3IDfgOKA5YDogOuA7oDxwPTA+AD7AP5BAYEEwQgBC0EOwRIBFUEYwRxBH4EjASaBKgEtgTEBNME4QTwBP4FDQUcBSsFOgVJBVgFZwV3BYYFlgWmBbUFxQXVBeUF9gYGBhYGJwY3BkgGWQZqBnsGjAadBq8GwAbRBuMG9QcHBxkHKwc9B08HYQd0B4YHmQesB78H0gflB/gICwgfCDIIRghaCG4IggiWCKoIvgjSCOcI+wkQCSUJOglPCWQJeQmPCaQJugnPCeUJ+woRCicKPQpUCmoKgQqYCq4KxQrcCvMLCwsiCzkLUQtpC4ALmAuwC8gL4Qv5DBIMKgxDDFwMdQyODKcMwAzZDPMNDQ0mDUANWg10DY4NqQ3DDd4N+A4TDi4OSQ5kDn8Omw62DtIO7g8JDyUPQQ9eD3oPlg+zD88P7BAJECYQQxBhEH4QmxC5ENcQ9RETETERTxFtEYwRqhHJEegSBxImEkUSZBKEEqMSwxLjEwMTIxNDE2MTgxOkE8UT5RQGFCcUSRRqFIsUrRTOFPAVEhU0FVYVeBWbFb0V4BYDFiYWSRZsFo8WshbWFvoXHRdBF2UXiReuF9IX9xgbGEAYZRiKGK8Y1Rj6GSAZRRlrGZEZtxndGgQaKhpRGncanhrFGuwbFBs7G2MbihuyG9ocAhwqHFIcexyjHMwc9R0eHUcdcB2ZHcMd7B4WHkAeah6UHr4e6R8THz4faR+UH78f6iAVIEEgbCCYIMQg8CEcIUghdSGhIc4h+yInIlUigiKvIt0jCiM4I2YjlCPCI/AkHyRNJHwkqyTaJQklOCVoJZclxyX3JicmVyaHJrcm6CcYJ0kneierJ9woDSg/KHEooijUKQYpOClrKZ0p0CoCKjUqaCqbKs8rAis2K2krnSvRLAUsOSxuLKIs1y0MLUEtdi2rLeEuFi5MLoIuty7uLyQvWi+RL8cv/jA1MGwwpDDbMRIxSjGCMbox8jIqMmMymzLUMw0zRjN/M7gz8TQrNGU0njTYNRM1TTWHNcI1/TY3NnI2rjbpNyQ3YDecN9c4FDhQOIw4yDkFOUI5fzm8Ofk6Njp0OrI67zstO2s7qjvoPCc8ZTykPOM9Ij1hPaE94D4gPmA+oD7gPyE/YT+iP+JAI0BkQKZA50EpQWpBrEHuQjBCckK1QvdDOkN9Q8BEA0RHRIpEzkUSRVVFmkXeRiJGZ0arRvBHNUd7R8BIBUhLSJFI10kdSWNJqUnwSjdKfUrESwxLU0uaS+JMKkxyTLpNAk1KTZNN3E4lTm5Ot08AT0lPk0/dUCdQcVC7UQZRUFGbUeZSMVJ8UsdTE1NfU6pT9lRCVI9U21UoVXVVwlYPVlxWqVb3V0RXklfgWC9YfVjLWRpZaVm4WgdaVlqmWvVbRVuVW+VcNVyGXNZdJ114XcleGl5sXr1fD19hX7NgBWBXYKpg/GFPYaJh9WJJYpxi8GNDY5dj62RAZJRk6WU9ZZJl52Y9ZpJm6Gc9Z5Nn6Wg/aJZo7GlDaZpp8WpIap9q92tPa6dr/2xXbK9tCG1gbbluEm5rbsRvHm94b9FwK3CGcOBxOnGVcfByS3KmcwFzXXO4dBR0cHTMdSh1hXXhdj52m3b4d1Z3s3gReG54zHkqeYl553pGeqV7BHtje8J8IXyBfOF9QX2hfgF+Yn7CfyN/hH/lgEeAqIEKgWuBzYIwgpKC9INXg7qEHYSAhOOFR4Wrhg6GcobXhzuHn4gEiGmIzokziZmJ/opkisqLMIuWi/yMY4zKjTGNmI3/jmaOzo82j56QBpBukNaRP5GokhGSepLjk02TtpQglIqU9JVflcmWNJaflwqXdZfgmEyYuJkkmZCZ/JpomtWbQpuvnByciZz3nWSd0p5Anq6fHZ+Ln/qgaaDYoUehtqImopajBqN2o+akVqTHpTilqaYapoum/adup+CoUqjEqTepqaocqo+rAqt1q+msXKzQrUStuK4trqGvFq+LsACwdbDqsWCx1rJLssKzOLOutCW0nLUTtYq2AbZ5tvC3aLfguFm40blKucK6O7q1uy67p7whvJu9Fb2Pvgq+hL7/v3q/9cBwwOzBZ8Hjwl/C28NYw9TEUcTOxUvFyMZGxsPHQce/yD3IvMk6ybnKOMq3yzbLtsw1zLXNNc21zjbOts83z7jQOdC60TzRvtI/0sHTRNPG1EnUy9VO1dHWVdbY11zX4Nhk2OjZbNnx2nba+9uA3AXcit0Q3ZbeHN6i3ynfr+A24L3hROHM4lPi2+Nj4+vkc+T85YTmDeaW5x/nqegy6LzpRunQ6lvq5etw6/vshu0R7ZzuKO6070DvzPBY8OXxcvH/8ozzGfOn9DT0wvVQ9d72bfb794r4Gfio+Tj5x/pX+uf7d/wH/Jj9Kf26/kv+3P9t////2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAGHAQ4DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD1WgmigjNcJ0CD79SnkVEv3yD2qQ/0poCM1Wuh8hq1UFyPkpPYEc9928Yeqf1rX0X/AJCH1Q/0rJkOL7/gBP61qaMQNSyf7h/pXJD+IjofwM6SA/J+J/nWgn+rH0rOtuU/4Ef51op9wV6UNjjkOoooxzVkhR2o79KMUALSDpSnpRigBjHnFMf/AFbj2p+056U2QERt9OaTAw0PNOfJU/SmD7wpzfdP0rjTOkt6T/rJMegrTPFZmk/fl+grUxXTS+BGM/iEBJNB5WgDBpxrQgQHIzR3pF4GKU/UUALSHpS5HqKQkAdR+dACd6r3Z2xMfapt6f31/wC+hVW+lj8lh5iH/gQqZPRjW5jXT743HH+rY1yDH9xLyfuH+VdRczxBipdcvG6gZ6nFcrx9lfnrG38q8rEPVHfRW5o6byo+g/lWuR8grK0xflQ+w/lWwy8DFdMPhMJ7loClIpwFGOa1MxiD56e30pFGHqRhzQloBCar3P3askcmq9wp28UmNGBIP9LOBk7D/MVZtll84LGcFhg461BMPLvMeqH+YrS0gZv0zzgE1xpXqWOm/uF2AXxXCFcA4wCeD+dXEj1YgYZcf7x/xpbM/I3qJG/nWrF/qVNd1OF1uzlnK3QzPJ1Y9XjH4n/GlFvqneSP8z/jWsORSDrWvs13ZHP5GX9m1In/AFsf6/40ottS/wCe0f5f/XrT7mlHGaORBzGaIL8cGeP/AL5/+vTvJ1D/AJ7xf98//XrQwMk0jHCn1o5EHMURHf8A/PeL/vikkhvfLYtJGwxyAuKvdMD0FI/KN9KOVdw5jAQ5I59akP3T9KhQ/OR7mpHYKpLEAAZJNcaZ0Emn+cZmWIgZAJyK0JJZIVJmnjQAckjFchceIjZJMYyYotmWlZTk47D3OawZvEGoXROLNmU5O2XPPtzz71pColEiUbs9AbWIN20XO4+yU5byGXGLpRnpkYryi812WFI7eO5tkuFHIjcYHPTjgn8azH8S3UMRiEiSDnO3lee/1p87ZDSR7gql/uXKHtxg082Ux5879K8Tg8aXYwgeT5VAG3AHH93NdL4f+JMltcm31SMtZl8faD1T3OO1UpR6hr0PRxZTkf8AHx+lI2nylf8Aj4NXYZEljWSN1eN1DKynIYHoRTz92tfZxJ5mZJ0qVj/x8Y/Cqd5pskCbvPz24roAOaoap/qVHv8A0rOdKPK2VGbuctcW7sSu7OAWwR6VhIvyMpP3kI/SuoPzXEg/6YOf1FcpG3yng5wen0ry6ytY7qb3NrSx+7Q/7I/lWs2ABWXpnEaY9B/KtV+VHFdsPhOafxFzt0oA5pcU7HNbGQwD95UjVH/HT2PFCBkTEVXm+6amY8mq8vINJjRiXQze5AydhH6itDSD/pyf7rfyrPnJ+28/3D/MVoaRn7en0b+Vci/inT9g2LP7sn/XVv51rwcwrWVbDhx/00b+dasBxCBXfSOWoSDpQKB0o9K1MwFL0FIKXqaYAOlRvyyilkkEa5xk1Fu3MCPTOalgiToaazZRsdMUigk4YYHpSyfdIHpSGc+zBJSScDcawNX1MXO+3tLmFHXjd5hBz7dj3pNbviksqt5qIHEY2MFySecnntmvPry4sLWYPZtLK6khXf5Vfn17+1ef5HUWvtsVi7O4EjuMb3cuB0wetZV/rLSy5PmNlNrKZO/f8KqX80Qm2WxyqxhXLHgtnJx7dqypHAIJQ4HTnitoxMZN7F3zmVwwUIT0AAJPt7VTkkLqSxKoOPlHWoHuG57Z9P8AGot+QMde3t71okZNltZVgKuqgupPXmlt7poLkh2YqwO4A9D2qr5iLglSTj1pjTLkYG3nGcUWKue2fCLxKbiG48O3MpaS3Xz7Qk9YiRuUfQkH6H2r1LtXy74Uv5dP8XaHdRuU2XUUbMD1Rm2sPoQ1fURGARWsNrCYg61Q1QfulP8AtH+VXuhqjqnEC/739KKnwscPiRgj/j7cdzbv/MVykKnaCfSupHN43/Xu/wDMVy6HEePQcV49fZfM9Cl1NnTMeSn+6P5VrE8AVk6YCbeI+qD+VaxGMV2w+E5Z7l4ClPFNHSlPatzMZ/GKex4ph++KeR8tJAV3qB+eKsOKhKMxAUc0mCMS5H+mKf8AYb+lX9KVlvkYghcHk9OlY/ijWbLw1CtxK8Ul0QfLWQ/L/wB89TXG6V401TUblLidpI7dJtzMkeFxjoMc5J/ADNZRou/OzSVZJcp67bahZB5EN3b7vNYY8wdfStuBleEFGVh6qQf5V4TcXls0z3c05kj3ERQR8KW65Pqc+n1NQ2/iW701jdQqLeNThVViAffjtXRC6MpNM+gKTuK8z0f4oRs0cd6iNuwMhsEe+DXoVjqNrqMIltplcdx3H1FaqSZFi5QTtUse3NIXVQSTioHkLRZz1I6emabYWHMwf8RxUcDAnaeCOR70xmwgwcFSf04oC/KWHUHPFRcZY/5akZ6c0rDg9uKbnHOeopC3DA5J6UwOO1PSY7uQEE79/P61ycnw71FoZgksDwsG2B2IIPqP8813YP8ApJH+0a0WYC3iA/uCuGKu2zrfRHlI+Hs2GM8oBxhVUcCqF74NEUeAcADJPp9a9bumHllfzrAvAASSPwrKUpR6mkacXujw/WLI2EgU8k8gAdqy0fLHcRz2Feg+NdMZ7U3cMYbYfmx1C+341558qAY69a7KE+eFzkr0+WdiTfuBNMckOQ3GKFOEY9cHGKV2woOASRWpkWoJDD5Um4h0dGGDzwwxivrvOcnGPY18meGbX+0/EulWTcrPdxL+G4E/oK+rvOBZiOhpx0Ybkhqlqa7rYfWrW/INU9QObcf739KJv3WVHc56Ij7cw/6YP/MVyysWU+uK6VMpqR/2oH/mtczH03V41bZfM9GnuzodJH+hW+f7i/yrTf7orM0s4tIPZB/KtB24Fd8fhRyS+IqaVr1vfxgowx6GtlXDrla+fvD3iCeyuUHmfLnn2r2/Rb5L2xSUMDkc1fLOD5ZGUakKkeaJfY4kAqb2qIqDKpqUDj8KtARlecUAbYJpAPljHP8AtN2H0ps8ghQv6cCqV5rltaaa68MVzj0LY6/hyfwqJNXsUl1POPFCwDUj5y+feyDcS4ysSZ4z7nt6D8TXLXty9he+RZ3DC6bEe2M7dpPQH9PpW9JcCKzvdTuH3bGLF5GyXlP3V/Dqf90jvXCJLI08t1OWMjncSTzzW9KJhUkvvNS81Gznfbcw7pgQpuIvkf346Hmori5MWEupjPaHgMi4Kj0x2NY8u5pGdzwCWY+pPJqrb3Uz3XTcr8FCMgitGraoiKvoX9QmjklAtYpAh5Cs+4+mTj2ra8NeK9T0O8jaK5dMcbXfKkehBqra+HpLqLepkiXPAJqxL4TkFuWhfMgGee9c0sRS2udccPVetj6E8O+IoPEmlC4iwsyDEkYOce49RWoMgOrHpnH06184/D/xc+g+IIRKDHGZfKnQHAIJwcivpjMZAI5B5B9a2SuZXKrLkyqCMsRj+dKjYyp74qwTH/dNNKwkglTke9OwXAY2IT0A7UwgkknripQUAAAOBSExgHCnOKLCOaB/0tv981aWeKVhFHKjPEoDoGGVJ5GRVU/8fjc/xmlmine5geKWFFTcZC8eSVx0BHPXHFecm1ex2q11cy9c8WaTpB8u5kaSbp5cQ3N/9auUm8XC5kLR6bdmI8ghckfhWx4g0e4FzLNp8MT3AXzFDnAJ68ntVG7OrpLCscVuUcDeAT8pxzz3rOTutUbKOujGtOlxEMg4YdGGP0rjPGWm266aLiKFEeJxyqgZB7V3rWciQCSUKHPJA6Vh6vZreWU1uxADrjJ7VlCXJNM2lDng4nmGk2Fzq935FsBk/eZuAtdxD4L0qOCaKcT3NxEAZJRIURSeyAenv60zwzo0llPJbkhTJKPnU/w4xWn4fuopry+O8tKWZWYNlXTBAP14H9Otb1a8pSai9DPD4eKXvq7MDwjpMul/F/RtPLGRFuBLG5GCyeWzAn345+lfR8agr1ORXhHhRxf/AB0tdhDpYWzqxBzgrEQf/HnxXuysN2MjkV3wbcU2efUiozko7XJlC+lVtQP+i/8AAqsBwKz9XuVjtivc85qptKLIiveRz5P/ABMhz/yxk/mtcyg+UY9a3BODqSc9YpP/AGWsItgCvErO6XzPRpq1ze01iLaL/dFWpnwAM1S00l7WEjugJ/Kp5yQ2DXfH4Dll8R88RMVbNei+CfEzQSC2kfCnpmvOsenSrdjcPb3COo5DZxXq1qXPHzPDw9XkmfR9rcrcMhB61ezgVyXhS9+2W0Dbucc11tcMG2tT15Wvoc54vv2sdOyhw7fd+pOBXm+q6662xIlYBV2Io74/xPWuw+IkhT+zxxhpl4PsSa8j1KU7oRuL4Xd+OScfmazjDmq3FUqctOxc1m/R47axSTdHAu6X0aQ8n64GB+JrLYrtBzkn+frVJ2OeTnPP1NR+Y3Rfx9q7UuVWOO/M7li52iIIpyCeT61veFtBM7faZEO0n5eKp+HNAuddvE+RltUI3v6j0FeqyW8Ok2AEcJbaMLGg5Y1xYqt9iO56OEo/blsUDaIiBQBgcACpVgRYHdyEjUZZm4AHrWUJNWuJt891b2MZb5Ykj3uR7kn+VdC9zBa6TNJOyMAoALpkEk8Ejn6153Jrqz04y02PGtfi8rxO0kcEsKXISdFkXaWBONwHocZr6k0e+hfQ7BnlUMbeMkE99or5s8Um/m8Y2rX0rTnyB5MrR7N6b2xgcHqe9fRFjqU1rp9rbqkR8qCNOR6KBXrRmoxjfseVKDc5W7mv9rt/+eyfnSfbLf8A57LWedZuM/diH/Af/r07+17g9ov++afto9xeykXheW5GRKv60C6gbhZASeOAapf2tcY+7F+RqOTU7hlIbyiCP7tJ1oh7JlFT/pzj/pof61ehz9oTbwQc1kQSk3a7urN/Q1tQ21y7B44jjHVuAcj3rlp6vQ3lpuYlzqKx6nN5jDLtgE+lTzw27YkMMe7Gc4rK1zSr6BngmaAiVQQ6sdyn0PFWDNixVQ5YqoUk9/esne7UjqSjZOLKOo3AOQK52dizGtK5JLYJ61nygYPNc711OiKsimjBXOeh4P0pJLyOKERWFqlsiKVUqAMZ7gDgH361DKct15NIwyhFVsSm+hD4E1MeHPGV+sVusiXUAd1I+ZgD82D1B5z7969vtrq0vIUntZlZW6buCD6H0NeD2CeV4r06XHLOYW9w4x/PFdxPpczwmS2upbWcco8Z6H0I6MPUGu+nVulc8yrDlbPQjJJ8yFDvU4KjkisTXZpdsWYLhhyD5cLNjp1wOK5jw7qkmvaPBqTny7+33QThARggkFc9SMg49jXT6VdSWdu83mgFzkKDn861mudOLM4vl95HMpLIuoxP5Myx+W65eJlBJxxyPaq0hAIBYZJxzXdaip1GxEcspzuDYPQ8dP1p+l2cUEIilhidRwA6BsfnXPLAuWzNVilHdGJpKj7BCf8Apmv8qmuMEjIzVvW5rPTSkNrag3L4xGhwqj6CsyS48zG4BXxyuc4q5RcY27EpqTufPoApc4bvxQowRjrSsOMjp617jWh80nqeh+AtZEM4gdjnoBXrsD74g1fOGk3bWd7HMpIwRmvctE1cT2sZZhyo4FeZUjyVPU9qhPnp+hmfEa1Z9Ktrpcfubhc+2a8ttNHfXdVstOglSFpAd0kgyERQSzED0CnjucCvaPEkR1TQLy1hiaTMe7IHdeeK8ZN7daNqS39sqiaFmUo4yCjjlT7HJH40o35nyjqWSXNsa2t/Dq3tbFp9O1G4nljXJW5hVRJ6hcHg+gOa49NMmeeOPBw544xXrun3CeJ7ezuNMuVUwxESxyZLxNnPbuem78aTWdPtX0mK6jt447qF03FGDHHQ5I/OsFiJxvGe50Tw1OVp09jS0HSoLLR7VIo8fuwfxp2oNIkbCKINKRhd3QfWtHTx5VlDGeqoBTLuMMCe9ckl1O+HY891PShcG2ZoCt3FKJXufN3Fz6AY4HPSuktbZ3sUkbOUHK+oplxEd5J7VoWGo2cWnLHK581mww2n5R656YrO7k/eNeXl1iYXijSYrzW/Dc+0MEaRnbBOY1KNzj3/AJ10EmrgZOR+Eb/4Vu6DJpypLBcSQeZGFZfMI+65JAH5V0C2do3Kwwn6KDXbGjKcFqcFSqozeh58NdH+Ynpw10dyB/2xevQPsVr/AM+8X/fApfsVv/zwi/74FP6rLuT7ddjgBryj+If9+HpkmvooJJY/S3Y/1r0L7HB/zxj/AO+BSrZwBwfJj4OfuCj6rLuHt12KmlaPHawxTToHu8BiSOEPoB2rSp9MbnmuyMFFWic8pOTuzI8Q2Ju9OdomnSZRw8CB27/wnqOa89shdxGY3V5NLngLJGq4/ACvVt35iuV16xuPO86GzknUnkxrkj61yYqk370Tsw1ZJckjj55CWOR+NZF3dKmV71v22h6t4khW5sI4obN1JjnkcYk9NoHJye/SuSaGC6tI7q3ndyQRLHIAHjccMv4GuL2Uo6yR2KqpaRYxZC7bic5qUuMe9VQwX5Qc1LnCZJGamRaKxl8vVdPI4Y3cIH/fwV6usOZjH28w15RYRfbvF2i23UNexk49Ad39K9jtU33Dv2ya7qEPdPOxEveOZ8HWwtr7xVEBhE1dsD0ygY/zrolQT3KRqPkHzEY7Vj6KBBr3i1Om7UkYfjEua6DT0yrSn+I8V1tXlY44u0blgoWbAFTwbYVaT+4Caaeu1ep60l1ldPnC9dmBWmyJ3Mezia582+l+aWZjtJ7LWVqdq7T7oyVPTiuojhENtHGP4VCj61TubTcQPTrU8i5eVlc3vXR84bcHnFB549/SpAPlPf60xlI6enavTa0PAT1Hx8HHcc5r1n4byQ3cflSKZCv8PavIkOGHpXp3wqnEd7KuBk1x16aerO/C1GnZHsLPsKxKAiY6KMV5x8RfCa6jqdh/ZkSi+vg8bRghQ+xS+72OBj8RXosikzKwHFZN3E03jrTWwfLt9PuJM443O8aD9AaUUjoqXsfPlxZanoN7uxc2V9CAT1jdQf6foa7HQtZn1NHa5nkkz1B5wPT3ru/iVpcd7ocF3sxPbzBRIOoVgRj3Gcda5/TtEtbawt5rYEs0Y8wnu3rXHipRtqtTfCwlGWj0OmsirQLggjHBqSZQVrEhM9tkRkFf7pq0NSAXEyFD69q8+6PTiyC7gBz6Vy97fXErtaWNg0qqxVpZvlVz6AdSP511bTJcpvidGQ5wVOQfxrNu7210vyp7uQDc4WNMZMj9QqjuazUW5Wtc6VUUVd7FDRZrrUdekHmhxDIsbbVwD5ahS3oBuzivSLSdoYgM/OO/rXM6Hpg0mwJZAtxMTJLjsSc4/DNb1qPOOMEY6mvUSslY8py5m2zYl1HybbzfJaXH3hH1A9aoHxTaAZMT46/eFPin8uYBflC9DXlvjSz1LRvEE32e+njsbv8Af24wpCg/eTkdm/Qis606iXNBlU4Rbs0emf8ACW2naFj/AMDFT2PiOC+vYbZIWVpGwCWB7E/0rwv+0tUVeNSn477U/wAK6LwJf6pL4201Xu5Z42Zw6OFxt8tueB2rCFeq5pNr7jWVKCi2ke3E8VAXwfapm5GDxVd4yOQRXpnED/MuVP0NRQzK0wRuNxwRTd5jbDYwT2P61WvFkUCaD/WxkHHqAelK3UafQzfh0ceAdIT/AJ5xNHn/AHZHX+leb/EnQ5fDWvvq9kn/ABLr9vMmjHSOT+Ij2J5/GvRvh+JIPCFlbXAEc8TTJIjEAg+a5Bx6EEH8ateLItI1HQ7ux1GXKtEzBYR5kqnB5VRnJ9u9KtBTTQ6M3Bpngy3gcB0xg+lOmu2aBiM5Fc6kz2EsZ23H2aYF7eWaAx+cnZgDkc+mTWqb6GS23MeR17//AK686dFxlZo9CFZSV7nZ/DbRpJ7+fW7gfu7TckPvIy4J/AH9a9Us4dkWccnmvNvh7rwbS9VsrgPCtveR+XCygsqtHk5x3LDJz0zjtXoMer+ZHizt2Y4+/IcAfgK7YJQ0e5w1G5u62OdjATWvENwv8d+Yx/wGOMH9Qa6WwINmh9qxxZlmKIBks0j4GMsxJJ/EmtiKP7NZKmeTVxfvNmbVopFiPklvWnEeasifSmoQMAelNhkxNL7LVkkyjfIf7qcfjTTH3PenxnZHsH3gAT9TT5PkAAXc1AHy4uc5GRQynPWlA3MeDjtTnGBkHII5zXpngEAGWz3z+Vdp4Fu4LDUvtFy5AXoo7muPUfNk+lbWhQs96krAFE557msK0bqx1YefLK57za60k0YnmIRW+4mck1Fca+glCR43nj6fjXByapIAFDZc8cf0rU0fT7iRxNcjaOoDfe/KuflO32vY63Vo21TwtfQ8NIYS6gc/MvzD+VcHpN+YwYGPHYHtXcJqllZjYW3HGDjmsS+03Tb5/NtGMUo5xjFc9enzbHTSqWKLX0ezG7B9DxWbe3E1yFtbVTJczEJEg7k1N9hIlYT8kdPeum8HaVGn2nU2Qbh+5hz27sf5D864oUuafKzrlUtG6Llho6adp9tp8WGS3jCFv7zdz+JyaxNdsIZPFnhqGZNwiN1dYPTKRgLn8XFdxHFjk9a5LxGSvjPSsdtOuT+ckQ/pXoRilc4py2Xman2YOgJGSashBa2+B17n1qS3IFosrdlplw4eOMDvRyobkxkZ3tkgYHJNUvFOif8ACSaA1vFsju4T5ltI/A3d1J7Bhx+R7VfHyqFC5ZucdgPU1PvCKWb5h/OlKCasxxm0fOt01xaXMttcwvDcQsUkjcYKkdq6L4eajcReONMSCMSmZmidScYQqdzfUAZ/Suz+I/hm31PQptbULDqFlFvJUf66Jeqt7gcg/h9PMfCGprpfjDSb5mAjjuAHbtsIKt+hrj9kozR0+0bifS0kyxxGSSRIo16u5AA/PpXO33jDQrdyn2qe5YHk2drLOB9SikVl6fYHxHrb6prRFxFEqm1sJF/d2rHOQV6M44yx/Cuukk2xbMfIBgAcYrq9rHpqY+zl10OIu/HWgb8HU3tW7fbbaWEfm6gfrWzYaxHdhXDq2flba2QeMgg9wQQaztez5TgklSOQTkGuH0GZNL12W1jPl208fmRxDhUcHDYHYHIOPrWSrpytY0dFqN73O71SFbO887aDbXHyScfdbsaLIKJUcACRD1A60sV7DfQtZXGCHGAaz7WWS0u3tZj88fQn+IdjW7SumZ3drHknxU0+90jxAtp58zaRLuurKEnKRFj86r6YPbsCK53QrfVNTuBBYIxlwQZOgQHuSfu/Xr6V7N8WLCLU/h7Lfbcz6dIs0beikhWH0wR+VQeHdBi0vS4LWBQFWNWd+8jEAkn/AD0qqkuVJ21M4RbbV9Ct4b8Nf8I9YvC05mmncSSkDCggYAHfv1PWuy01migYHgGoYLZZJMelX44d8wjUfKOtc8YycuZm7cVHlRoWMX7suRyxzSXcgEqr6VZXEcfsKyJ5C9wTW8/dVjGOruaEL5DMTVeG4VLwlj8tSQjFqSDya5zVL4WEsgkIDBckZ6ZpN2sLudVp8vnvPIeiyY/EAf41e2gkknrXH+BNXl1G0vlkhcLHcswmP3W3AHaPcYz+Irr/ADUPTBHr2q7WM1K+qPl3B3evGBTnGR0z3pf4fXIpDkg5A449q9Sx4dyLHoBya6TRcQrz1xXPpnzPp7V0+j2ZuFBwVQdTWNTRG1LfQ6fS47eN/tDyZPbAyfwrRutU8tNikop525yzfWsJ7mO1HlwgFu7dhVXzWdsk5J/Oud6nWnY1hqErucAKo7Zq/BehsA8H1rAjJA69atxtjGKho0jNmxdtNNAxgEfmBeGlcIo9yT0FW/AevXB02+/tSd5XW78qJYlHlIiqM7OgwSTz3rLtJtsg3gMp4IPpW5b2cQjRbXIXJPJ9TkmpsknpqaJtyTvodMNXjdC6QSbQcZYgZrHv7b+0NXi1AKyyR27W6pnIwzBifrlRSvcIWSCM/KgwBWpbINoOKLM0bRHdt5NjFCDg45qO3O9U3c84qDUpgbkJnoMU23uVR4I+7OaLA2agGAT3bmkC7yMk7fQd6rRXqXN3JBGQQmB9fWl1HVLbRNMk1C6yVXCxxp96VicKijuSaQN9Se+uILKzknvbqG1t1GGeVgqgehLcfhXi3i+3stb1hLnwyI7tTB+/Wyh2lWB4faAM5BHIHau9j0pNSnGpa20V5qR+ZIGO+GzB/gjXpkd2PJNV9RdoLhJxkFMgFeCAeuK4sRXVvcV2jso0JP49CXwDr9zqcVyl9ZS2t3CyCTepAkJH3hn6V280n7uuU0q5klvUDyl1K5XLZropG+XFY0avtIc1rGs6fK7Xuc/rZzExrzLUfOS5S7hUk2rFnx/cOAa9T1aPdbsfauK0GNLjxO1rIAUnikjIP0z/AEoXxlv4GFvqfnwJIjYYcgits3X9qWSXcePtUHDD19RXETQSaLqktk5Ow5aM+3/1qv6fqrWF4Jc/um+Vx7etddOXRnNNdUdBqU39reDdc09eXlspNg9wMj9RRoN8LvRdOkBAM9pC+T6lQP51RuZvsV8lzCc28/DY6Akf1pPh9ELrwrpsrqC0CtCCe2x2ArdXlp2MXo79zsLSMqW7kgVs2lr5cfJ+duSaq2UKhhzn1q+JlWYjPOOlXaxm5XFlizGVDAVkyaYXkLPOxH91RtH9a1HmUAsxwBWDqfieyslYb1LelZyaY1ojReMw2+xCBjv1rgvEkQ3sSxJPXmtKDxN9sLYPFc9rt95knBzTjFykjCrVUYuxn6dqtxo8ztbyEJJjzEzw2Kt3XjPU5yFjuTAi91AJb8+1c3NNkmqjSHNdqpo8v2stkVXAP9KZuznLHgdaeygZAz6iprBbJtRt/wC0jciy3Ym+zYMm3/ZzxnpXQzNINPtzd3aIOnf6V17TiCAW1uMADBI4ptla+FbAb1vdXvd4ykUVqkB+hZif0Fb2vadYWs9ra2eni0mihBulMzSnzGwQpJ6kDqR/e9q56j5mdVKNkc2kbMeRW5Y+GtXvLQXUFi5hIJVnYIX4z8oJy34Vc8MaONQ1pRJb/aIreNp3iOAJMcKvPHJI6+hrSiSAavcahNqqXmp2lrPcNHCmYIQEKhEY+hZcYGPxrJmyXU5WMhgGB6jIqzDHLPKkUMbySOcKiDJY+1VUG1VX+6AOlaOkJfTanDb6dKYrmbMavu24BHzc9uB259KkovXOh6rp1v8AabuzeKHIBberbc9MgEkVo6JJNNKUjjZ40GZGyFVB7seBUGv2V3oVn9ijtpls5H3TXbkZu5AOM4JIUY4U/WpdXsZ7OXS9BtxvdollZR0edyQWP0xxnoM0rFptHXJZWj/egCuMccfzFTfZ0VPkd1A981k2WpQNeSwROGii2xI394IAufxINarvujKjq2F49zis2rOx0KV1crixsxLB5lklxLJsEryucDd6D1wf5VnGwi80MhOUdihJPAycfpW69xGlxLIsEKhQzM7csSvygj05wKhEcbwqh2sAMZpVItrRhTaT1RjQ6cLaZpYco7DBYHrUr2SyBRNiQKcqHGdp9RnpTw3nTyJbvhE4y3IJ9vag/bEzmJXH+w39K45Rl1uzsjOPQhaBI/uqB9BWNqNu0gPHFa7XyByssbxt/tKRVO9vbVYW2upP1rGUUbRmZPhqR/7YNqw/1Slh/un/AOvXWXMpXHpXIeFWW48XzMDnFs5/8eWu3uIxsNOnT5Y6ClPmZlXQDWjFjXntldJYeM7WWRtiedtJPbdx/Wu2vpjGrAHArzPxWC6zOpAbaSCOxHP9KaXvJg37rR0/jPT/ALT58kQ/0m2HnKB3X+If1/CuMjnEsQYHgiu71W9Mb2d5wd8aGTI6gqM/zrgNQgXTdWlt0ObeQ+ZC3baa3cbOxhGWhuaTfJNE2n3ByjD5Dnt/iK2/CNudLt5NMR3YrO82WwAQ5zxXDCQqysrYIOQfSuy0HU0uVjlOBNHww9q3hLqZyXQ9Btm8iAs5G4+9V0nVbh55H4xVS8vVSLJIAxXnfibxc8IaCB+fUVVSWtkRFaXZ0/irxlBbQtBC4Ln0NeZSajJdTl3cnJ7msKa9kuZS8jkk9zU1vL8w71UIW1ZhVlfQ7CxuSicE1De3Bckk1Rt5isfJqKe5yetdEFY8+om9Bsj1XZxnmmvLmoGfJ61tczUC7KhD8e3arllYGSQYH14p95Asc7eWcjPX1rc061dNPM5GARxVt6ERWupqeEtOhl1h7+4TdbaennuOzEH5F/Fsfka1XiklkeaY75ZGLux7sTk0/wANtCdAFrCsnmyzmW5ZhgfLwig55H8X1rUNuAc4rJ7nVFaFJFgs9OhS5gedLuUzTRJIULRJlUUn0Llj9BT5tUkuPDepRLaW1pbl4IIobaMKMlizZPUnagFJeSIqAySD5VCjPYDoKivCLXRLWBimbm5knyGzuVVVF/8AQjWckUpLuc+UIHIxWjYw6fdWklvd3S2UqyCSOdoi6suMFDjkc4I/GmRQm4lSGIb5HYKoHUknAFNa3UIjrJG27OVVvmQg9GHb+VQXoXr64sbXT10vSmaaMyiee7dSplcfdCjso/z6lG1y4NuqmJDd+U0LXhZmkMZYkjngHLH5uuKrW9pLPII4IZZ3I+7EhY4/DtU9zpZtWUaheW9k5G4RHMsu312pnA4PUjpRqPREdncG3kRl4A7V3emXiTJFKwZwh3bVPJI6frXnAb5jtbIBIBxjI9a6Pw/feXIInbqcAVLRcJdDtFnCIFgt4oR/EW+cnnjn/Gs+/u3ZzbIxaaT77dNopZrhkXbGuZDwAeMH3qKCIRAknc7csx7ms5XehsnbYmijEUSoOgFSbh+NR7gY5H3j5MfKEJOSeMnoO5+lIH5xWUk0WmmTqT2Jx6Uya3tp12zW8MmezRg0m/apNcjr/iGeaOS00xsFmEfmBgpdjxgE8Ae9S5JaFGxZXOgxeIJrPT4IBfwwZmNvH91Sw+UkdTnBx2rWunaMMJBtIAJzxjIzivPPBFtb6R4ijS41cSXFyroYrFWeNdg8xw8/CkgDlV3H3r0NSWUzKiRucSE/fYFj6t9ewFP2d1qEKj6HD67d4LENx9a5O30u68Sz3dvZFWMcDO5wzdwoGFBOSW9Ox9K7XWfDmpS3gmsEilCnzNspXAwf4g3BH14qzrO/SfC8Ueu387falXMNnHlFKoQ2NhjUDLA88HA61lTp7uRtUqPaJgeKYZLS38iRRvhVUIDAgfKK4SeYX9n5JOJ4iWhz1I7iug8QahciUWtvb7Y/KTcLsqHYkZDAJ8oBUr3P1q/4cmu/C3h+K7hjnl1S+eSeOGGWNYtg+SMysRvI3B2CKcHv77+7JmN5RSOFtpvNTGefStfSppLe8SRTjHUeorPuNF1g3fmi0keeVidqKGLsfQL6ntWjrzRaP4imt5ojDEhjM0UDBvKYopkRCeDtYso+mKSXYbl3OxhgPiEwxrq0FvG8ywtHHHJLMCdxAKqMLkIxBJ7V5x8Rp7JvFcyWFitnGEXzI0kVhvOTjCZVSAQpAJ5U9ya7K51O48L6RrEmnWMWm3C2EczCWUz3sMskgjhMhICRMUeVgijIHXrXG6zo2seIl/tPT9ZufFMMQw/D/abYHnDQMSwX3TK8da6IxSRg5Ns5WNsnFadonQms2JDHKUk/duDyr/KR+BrWiwiZByKtIiRaabauBVdpSe9QSSZPFMDZrRI55ImL0wt703NITTJSPS4NFa9nCgbV7mtrULVLSxECcADArorWxWBc7RWJrXMpHpW6etjFxsrljRfKsNIMspCqBk1mXeqanqZIsl+zW/aRhyalss34ELD90hyR61qyRKqYAHArnqVOWWm5vTpe0jrscXcaVK+WnupJG9SadrMsup/2eoZ4o7OyitEGc/dHzN+J5/AVs3aqCc9Kz4Y7ZrqMXbulvu/eGMZbb3A9z0z2zmoVST3ZUqMErJG7YaDbWvh+XUrCSW+v1UPbR3+URxuCqyxJk/M3yqHbk89BUPiLV4heWjMsLRyxsWuITlXnDbZUBwOFIGPZs5OauWGkRa9bzazqd1JaW5bbDFG6xRBUACKHbgAZIGATwT3rDv7XTbX7RY/boLvSbiYvH9mkLzWrgYWTBVQePlIHUdwQKtO71IceVe7oWIZ96N5Er4dcERsRuHocdee1b/ixI7TTNK0qKIb7aIPKyrnYANo6dASWPPpXCWf2Oy1SbzLyR7CHc3yqVe5APyqo/hLe/QZ74rsH8YW2rWslk9tNHZSYLGFlSRcYwABwVAGMMcn1HSk42HGd009zAHPTitrR5Y7GG4vpTN95YIxC4QsTlmG4gkDaBkjnnjrVcabBIc2mq2UikfduGNu/5NwfwNSiS70yEQn7BIjsZACIrjacAEg844A/KkO50VpqMl8xcQmSR2LHyiW69sVallNuVEyOjmMMVI5HJHPp0H51zn9u37Q4luZWAGNitsX6bVwKhGqDB/dDnrjrXNP3dGzrgnJXSOwVpDauIbjzFdyojtkaTJx82WHHoM846U0W91nJtph7su3+eK5p9fnlUK7SlAAAocqo/wCAjAqhcXdhdjZc2SygHOH5qZTh3KVOZq64uuXoNra2Lpb9GYzxK0n4b+lcjrNnNYaY66hDJEGZPMQFd2wnnHUZwDjNa63GlDpp0I+iClln0ie1NtLZr5J52AYrnfJe9zblna1jmtO1m6v7rWdSt4hbWOl6cbXT7ZD8kHmyIn/AnK7izdST7CvWdNuGu9PgnZGjaSNSyMOR0OPzrltOXQtP0eVobRI4JLuJCpGdzIrPk/iRWzD4l0/AHmLz7Vo6ium2thQptJqxel1K2S6aKUKSuCuQCoPcn6dhRcx2mp2Kxy4nhliLHJzu3Mxz+grn9RTSdTkMi3Ygk/vITVWC2ubbUI7m31qF41hSAwsNqsijA4Hf3rNVJap2aOh0oaSjdP0OU8U2UltfouTJHHGkMb7cHagwoY9yBgZ9hV3T9Zu4bKCB7TTrgQoI42ubJJHVQSQu7rgZP51u65qNqbGeOaSIuV42EHn8a5C3uFkkih2kZXKkYwcdenSrpb7mVZLlWhtP4i1dYXWKaKzRh8ws7dITj6gZ/WuCvbmaG+S6gmaOeFw8cinlWByCPfNdVqBMNm3IBNcbdDcTzXQk5M53aKHTNKvgq/uJpHebUtWjDO7EtIIo3diSeSd0q5PrXOWzzW1yk9vLJDMhykkTlGU+xHIq5MpOFycAnAzwPX+VNjh5FdSgYc50tp458UrAscusy3MYHAuoo5/1dSf1qhq+u3WqqiXMdkGQ7t8FnFC59iUUZHtVZUCx49qpyjk0KIOSZGW5pQaj6ZzSgmrsQ0PDUuaYDSZ5oJsfSKXitH6Vz2puJJzVW21LcnUj61E0xmuQM9TWlranNz82hu6TbiG3zjk1PcHCmnWzAQgegqC7bAPNefN3bZ6cElFJGPdn5jxkdqz5FBPAGO9XJ2yx5FVWGT/9anEzkOu7q61CXzrueSZwMAv/AAj0A6L9BWdcA7setXScZ9PSsy5nYSfKjn8K2gm2c9VpIrOmclvrV7RYi5eTGE/marRWV5fNynkxnqT1NdHaWqW8KxJ0ArWTsrGEE27iqnGO3ualWPnOP0qQRjGcVKqAdRWRuM8rKcDtVM25Vzgk++a3zpl5Ha/amtZFg2hvMOMbT0NZcjIGOSCfeuetHqddCXRlQxy8jJxVaWF8/eFdBo2mjWb9rZZliIjMm7bu6EDGM+9R6zpcWk6rHaXF0CjKrtKI/uqSRnbnnGPWudxdjqUkc8Q6/wAWahd3Lf8A1quX8EcF3NCs6yrG5USKeGA7j2NVY4FnZl+1wQbRndMzAN9MA1zyg72NlJbkkl8p0i3s13h1uJZpCwG07lRVx+Cn86qmYe1WG0+MA/8AE1076l5P/jdWtP8ACWoa0sradd6dOIiA5EzjaTyOqCs5UpSZUKkYmV5oz1OfrSm4VerN+dTTaM1tdyWs2qaas8TmN4w8rEMOo4j5q9baJoJspv7U8RLBdMv7kRW0pRT6sWQFvoMfWlGhK5bro5q8/wBKicDIAqLw2xNw6OxwvAJ7CnMpiaaPzFkVSQJEB2sPUZAOD7in6bp10kzN9mu1H/XB+f0rsoRcbnLXmmW9Xc3HyRn5VrmbpAhIzzXYRWFzf6naackNxF9omWIym3chMnqeBwPrUnjD4dXXh2G1nS8fUBPIyFYrVl2YGQeC1d9NdzgqNtXR56Itx6VYiteQcV0OjeENX1a7WC10+4J/ieSNkRPcsRgfzp2o6LeaPdPa31u8My9mHDD1B6Ee4rdyWxilJ6nPzR4XFZ8iZPNbz2ss5YQwyykdfLjZsfkKqPpt4SQLG7464t3/AMKaaFqYzRVHtxXpHhH4YXXivTri8kv204RT+SI5bRmLfKDnll45x+FcbeaHf2t7PbfYrtzFK8W5baTDbWIyOO+M0003Yq0krsycc0010914B8TWugf21NpE6WnJZcfvUUD77R9Qvv26kAc1zJo0ew9VuejQTlcj9a0LFt0oasCMtyM8HitWyfaaqT0scUI2dzroZ8RjmoLufKn0qrHPhOvaq8sjMeORXDNanpU5aDHk3Gk28c9Kj5Ld6lJwuKkoiA3MAO9X1gRV+6M/SoLZNz5ParvYY61tE557kXl9qnROOR0pAPen5xTIHqozUxj4P071HG4GOOKseYNp/WgfQ7h1l/4RNBAu6T7Im0bQ2eB2PFchc22tlsxWRcZ522cTY/8AHa6y9/5E3kf8uqcH6CqnhBQBe4AH3O3+9WbWjOp6zS8jL8KPqKeIGt7+3EG61d1VrdIycMo6qAcVX8cXk9rrcaxmEIbZW+eCNz95u7KfShNQ+y/EqV5WOx5vIJJ7FQB+GcVd8cW0MF3aatcBHSOMxrC3/LWTOUB/2fvE+wx3qGi0/dfqcJexLZ2SwyRKb2fbLISP9SnVVHozZ3H0G0dzWSi7WJzz6VbnllmleWVmeR2LO5PLEnJNUXYgg85zWE4X2NIT0PXdADv8PIFWLz2axkAi5/eHDfLxzz0rJ+GdzFc2eo+VZw222SLPlO7bvlPXexrT0RmX4XxyI7Iw06Qq6nBU4bBB7Gqnw4kgvbTUL5DtuZ5I/tUQXA80Ly6+zZDY7Hd7VrbVBc56wvYD8VjCNNtlkOoTL5wkl3ZAf5sb9uT9Mc1b+LDEPo3JPyz9T7pWVpgz8YG4/wCYlP8Ayetf4sJuk0f2Wf8AmlRy+60Pm6nnYUeUT7VAuuXqswGrXf0+2P8A/FVPgiIirEPiPWRKd2ouQO3lR/8AxNaUIWOfEVLl3wbe3F/4tsI59cukcTo8aPdOySkMCyH5u65xxgkYPWvS/iK8qadZGK5lg/fHJjlKZ+U+hGa5TwJf6rq3iy3SW7eSCBHmlXy4wMY2jovqw/Ku78ZT3Nvp9u1vIyfvsMQoP8Jx1BrWq7MVBc0GeWWkep31ytvbXd9cyt0RLl2/H73H1qrqVvciZ4rozedH8rLMxLL7c1041bV48+XqE6Z67Aq/yFZuoTXd/IJbyeSd1XaGcgkDOcfrWPtDdUjkTJcWbMYLqa3LdTFM0eceuCM1attenntRp2oazdxRZLQXYu33W7nruw2WjOBkdV6juDpwz3Vgzm1nMJcDcQqnOPqD60kmt6wP+Yg49/Lj/wDia1jVRk6TR6H8LYtQh0C/j1GWeSVb4hWlmMoK+XGQVYk5U5yCODmvKrq+utOvLrUtU1S8k8y4mazsHvpF8/8AeMA8gDZWEY6cFyMDjJr3DwWl6PCdi99M8s8qmXLgDarHKjAAAwuK8b1XXvECareK+pTrIlxIpDxx7lwxwOVzwMVcZ6thKPupHN6kvim5099dvZNVayupCGumkdY3J9gcBT0HG3sK5l4yDXc3finxLcQSwS65eyRSKUdGZcMpGCDx6Vyr2xHQVqpmMoM6ePpgH8KvQkAj/OKoRt81W0birZyWNNJsLjNPDA1QVhn/ABqdZOef1rlqI6aTLSgfSlckAAColenB98gUfzrFbnS7WLsC7YwfWp1JqLcVUd8VLCdw9q3WxytakqrzxSsPzFPGAKYWGadwsSQ4Jq0kDTOsKDLyEIv1PH9arRDJyD3rqPC2n+bcNfyjEUOQme7dz+A/n7UXtqOMeZ8pqeI5UtdEW1U8ylY1HsOT/L9a5yDVruzicW3kREjnbCMtj1rQkVfE+sSrHcmNIV/dfu9wZcjJ6jkn9AKo39hbaddNbzXcrOFDErAMYP1aptpY1k7vmW2xy9z9o1TV2DOn2idyzSEbVXA5Y+gAGT9K7fz7Xxt4YubWCRvtUBG0yDDbx9xyOwYZ+mSO1czqMVhYxyWrXV0k90F81haqSkZ5EZ+fjJwx9sD1q/deH7jwbF/bMGql5EIQRi24kz/C3z/dPr26ipsOLevbqcJKrI7xSArIpKshGCrDgg/SqkoI6Y+tdxq9taeLIX1bRk2akqg3dgT87Afxr/eP06j0PB4pxuU/L2xSaC9jutM8YaPbeBV0mSSb7WLN4dohYjcQcDPTuK1/h1bppfg2XULj5Emd52b0jUbc/wDjpP415romkza3rNvp0GQZGzI4/gQfeb/D3Irv/H+rwadpUHhywwoMaiVVP3Ih91fqcfkD607FxlpzPoYHh20nl+JkE0hDiSWW8EqfdeN1Zlcex3AfXI7Vd+J99FNrFlZKQzWsTNJjsXIwPrhc/iK0vCivofhoajdRI97Osi6bbtgSOhG/YD6MVLAf44rgLoT3TyahczB5rhzI59Sf89KTiS5WVim+GQ4rO4ViTwO9aZH7smul8H+G7aKP/hJtfZINLtzvgWX/AJbN2bHcA9B/EfbrdNcpzzvN2R1/gnSoPCvh+O81IiG81GWNSrD5huO2OMD15yfTJ9K2fFVwsFpbiUFoJJdkqDqV2np7ggEe4rzC+8WT+KPHGkSbWisoL6EW8BPI/eDLt/tH9Bx6k958RZWh0qxdTj/S8f8AjjUVIvd7s6aM48rUdkc/PZ/Z5miYhxgMrjo6nkMPYiqVxGMVYsLv7daiz63MYL2/q46tH/Nh75Hes67uvkyD1HFcMk7nbGSsUrhVUnpU/h3RD4h1yO1IP2ZP3lyw7J6fVun5+lZ8EN5q2oR2VlEZZ5DhVHQepJ7AetdpfapaeA9EOkaW6XGsyjdcT4yI2I+8foPur+J994RtqzCUrnoNreWtw9xBbSKxtZBDKqjhG2g7fyIrwrVGOsJd3hJe+spHW5J6ywB9qye5ThW9tp7GvQfhVvOg6gzszO18zMzHJYlEySfWvMRdS2GsyXUIQyRzyHa4yrgswZWHdSCQfY1roiG27MyXWqskYzW5q1nDbyRzWhY2Nypkty3VQDho2/2kPyn14PesmQc0J2EyaNxnvmrcb5rORvc8VZjkIxXUeey8X2DJOKEnG7g1WMhf+EE9iTViDTppjuMiIOtRKNyoSsWPtI7dTVq2BTLt1NZNzbzWcgZJN49RT4bm7bAPP4VlypGzk5HRIfMA71fhQIgJrEtp5wRlAfrWil5Iw2lPyqXNDUGXHYDj+lQDls04ITGZXZEA/vdadafZZpv9ImkSADJMS7mb2HYE+p4qk0yZJmpommzarciGMFY15lk7KP8AE+lbGs6zAluNJ00gW6DbJIvRh/dB/me9Y1zrMj2gsbKIWdkOsatln92bvn/Oazw20cYApheysjqvCrCO+lmkZEi8rYGZgOcg461NqsHnaxLeII50SNDGnmLh354PPQdT+XeuTikMkwTAYngVYwoAYqp5zgrkGjrcfN7vKWr3QLm5gErqWunfewLoMD1Jz1PX6VseOriC78Nyx21xBLIkiSMqyrkKDknGa564eW43ySMm6XLFm7/pXAX9lI95L5ckBK5J5IwPyqo0+Zmcq6gmkty3HO0MyywytHIhyjoxDKfUEdKt3M9zrV3HmFJbuX5CYo9rzN6kDgn34965kRSZ/wCPqH05Zv8ACuutNatfDkBTQ2jmvpU+fU7tSCFPaKPB2jjqxz6j0t0WjOOIi99DrIGsvh5orBxHca9drkoDkKOwPog/Njn8ORsPs9xdT6xrs/2hRJuNuCPNupOuMfwoOMk8dhWRPHd3DS3Ul2s0rNmSSSXkn3JFVhHfptEiGMOcK0nyq3pg9/woVFlPFR7aG3qutXus6kL25kKSJjyUjOBCAcjb75AOe5o1Hy7xY9QiKqZyRPCpx5cw5JA/ut94eh3DsKxJGnibbLJCjEZwznI/T2qWBJp1LpLAQvUhjx+lP2UiHiIs2NOk0y0Rrm8tpL2dG/d2xwsR4+87dSM/wgduazdf13UdduBJfT7lT/VQoNscY/2V/qcmhLa5lcxpJE59AT/hUAsZJjs4DkE4Y46UlDl3FKpzKyINGdYtf02RmVVS8hZmY4AAkXJPoK9U+JWpWc+iWS213BM4vASscqscbH5wD9K8u/se4JxmP/vo/wCFQC2e2m2tFtbAJwtKpG+prRqWTj3NqO6kiZJI2ZGUhlZTgqRyCPxrRu5oNQmSdMxeaA06qmQj5+YqO4P3gO2SKrWFkbm1L4AA4yxx/OtW00iVF+ePbu6b+M/T1rhloelB3IxrqaVbvZ6FA1r5nE17Lgzy/THCD2GfzrBn2gFjyTkkk5JrRvdPuBK4jj8zb94odwH19Kotp9xNFvBj2ZxkPnn8KavIHKMTvvhnqFlbaHepcXdvC5vCQskqqcbE5wT0ryu6mDXlwykEGZyCOcjcasy6HcOwJERyQBk+px6Useh3DHCGJiBnCyZ4ra2hjz3GW15F9hubC7LeRJ+9hZRkwzgcMB6MPlb2wf4azG55xVy4tzbymNiCRjO05FVmHNK47FJZcVKkjnIFVBgc1KJgorrOFovwkAgkkmrDagsQwSTjtWMbhmwF4HtTo493LUmTY1P7Rkn7DGeBWlaeY/VQB71jxTRQjoGPXFStqEsnyg7R6CuecW9jeErbnRG6gtl+eQFvQVUm1aRiRCgUHvWOjE/7R9asIjOfnOF9jURpJFyqEzXMszfM7MT2rdsMx243nBPrWPCuzHlpk+uK0IbW5lxnIHvWtrGXNc0xcxgYXmlEpb7vemRWOwZdgTU++GMdaB69SSBdhDk8+o7Vaa43cfaZR+B5/WqqyqRkClVi+eMfWgGi+m54lxO4XLZO7B+vX2xXFXgWS/KNqcy7mBGJBgdf9r2/WtTW9UNrCY41BJ4LVxMjF3yRnPWuikurOPERb0UrGnGjOmW1OWPkjLS5A+fbjg5zjnpjnrUksRtkYxak77OieZ15HIwfc1jYHIwMe9SIArcLgZ6itrqxz+zlzX5jYkZZrYebdzHjOCpbB/Os944MjF059f3RH9as/ej5FUW64460QZU4+ZJFDDPMENyQNmdzAAA+nJ+v5Vr2FpDAfMj1EZ3MuVOAOT15zz/nNY1tMsE+9k3jBAHHUjryCDWla6hb+Zxp8IJyc+nXHGP9o/p6Vo7WOaSqKa5b2+Ro7GklwdQZSCww0mBkHA5z3yP1ovraOGzaaG7DlRkbWAPXGOuc96ZJfRLJn7KhUsSFOMctn0znHHXHtRe3sctnIgtlDHo3GV6e3sfzrndjph7S/wDwxi/aeRuv7heBnCscfrSGdVxtv5yMf3WHP51Rc/Mabn/9VTI6IR8zstFHnwwl7w75BnZI4Izz78dB1x1FdhFYwrGdtwpGeDkDI7Hr6V5Tp1yILlW969Es9Uh+xndGsgYc/kf8a4aySlselQvy7/kZuo29v5Zllu9jYztBU55wMc5/Sqf2WGOR0j1LG1sbQ6gtzjI5x6n/APXRqWpRPOG+yLjzA7KMYPrjjr29Kx7i9iNvHElvtKKAGJXJwpGeB3zn8KceWxM/aOXX8DXt4FnEokvymznl8hhk9Ofb9RUbaZEbgILlAAy7myvyqS3PX0C/99UQavCzowtR8rAgMVPAxwcDnp196dNqcJVN9lFIQoBDYxkA8jj3qdL6/qNqbV0vyMe/jEdwUWUyDaDuJB5I6cVRarl7Ks05kRCgIAwSCcgYJ4A61TbNPqaK/KrmMH64NJkmo1bnFGfpXYjiZMrgc96kErHGT+FVhzTwQvanYgsxtnpVnnjnNU1kHGMVOHyAeoqWgTN2ytEZQ2cD2rUjtIVxxn61kWVwBEATWzYssrjms5XLizQgtkA3BAMVM0oTgGkkkCrtB4qADuag0HMzuetIkQUjPJpw9BgVJGBn0p3JJ4ox3FOlBBwp4x6UobHrSscihDexjavaCe2LjqvNcZKNrEEfkK9GkUFWUjg1xOuWy29xkDAropS6HNWj1M1T6mpUPzDgH3quCc54qZTgjkitznNFeY/w9apNyT0q4mPKzjIIqmxGehpRHIGOTkeven25xJUeQVP6UsLESDJq+hn1NOVuQaV2zCT+tMk5C4pCf3B9hWDN4mRMfnP1qPOKW4PzHrUGSW4psqLJBIVORXVeG9YsoBPFfRNLvCiMhQ23rnr+Fc3bafcXTfJGxz3rqNJ8LSIwkmOPaueokzrpNp3LxGmmG0MkMh2tuuWKAlhzgDn6Vh6vNYtqErWkYELBAqhdoGMbuPw/U11VxbJbRqVAJUg4PtWHq+vSTJeQiytUS5ADbFOVxjlfTOK57WOvmuiS31Pw8lyJBYlYzb7WiKbtr5GOSeeMjI60qajorrarPbNGEVg7RxqScgAZyeTksc+wrItdea1hjjW2hLRymUSHhsnd+mG/QU6z11raKVfs0EpknWfc/Yg5x9P8TQ0CkN1Ga2lvriS0BW3dy0alQuAecYHQA5x7YqgzDtUt5eG8upbgoqNIdxVenTFVCxzTSJkzDzShgBUJbmhW5611o42ThuaXdUO7HvS7+aozZYDc9anjkyOaoh+lSJJjrTsI14ZCcAGtKzvZIHBJ49Kwo5T06VaR+mSahxGmdWuqBx0NP+2ORxXPwzEYx+VXo5n6baycbGilcvmeZu/4VNFNJ1ZskVQE7/8API1MjzHHygUBc2oJW2ZY4J6VbZvkzWPAZCwJ6VpK37v3pDTGM5Ldaxdft/MtvMA6VtYK8nrVHU2U2MgbHSri7MiaumcMTjIHrT0bJH5CoGbDnuKcHB/rXWcZrIcx+nvVZiN3PPPNOikyn19KgcjdjvSQM2518PCKT7NLeCTcmwOD0/iyQPy+lRXC6Ktvm1muWn+0HhlO3yecdvvfd/WsfOSCacHJYU7eYOXkjr302y/4RZb1S/2ogMQ0oAxznCHk4IPueuOxS00+xln0iCd5FF4weQgkYjLMuBnofkznvu9uee48vcQM0m75G4GD1FZNGsZK+xZ8Rabp1le2hsLgz2ksDSPL5ob5lZgy+xA2j3JzVzwr4cttVsnuJlbzEMgA83G47QVIGOQuG3f7y1yk43T4A5NejeGrNbawViBkjPSpm7KxrTs5XsdImh2ljpqSxxgPsQn5gep9PxH60qmzWM+Y8gbYeg/i7fh0qkWGTwM1UvLny4zzWNjpuXrO2tNRM4uZtoQJhQ+0tk89fy/GsW+0LT20e9u23vLGJShjmG3C5x1+8OPx3e3NKKbzZsnn60+6hWZcEDP0qXG5UZ2OSsbKO51ewtbhyqTyfvSD91efTvgH8xW6NE0OLUzFLflLc2qyQ77hQZpTKV25xx8oHbvn0qtdaSvlk7R+Vc9PbeU5CqB9BUvQu6exe1iG3sdavbS1l823hmaNJN2dwHuOvOaomSohE+OBSFWB5paAYxPJoDcDFRl+aTdmutHIyfdzShverxsdO8vcNWUt9k84jYP9bgfuuvXORn2qSbT9MSOUR6uskgCGMbV2tnOcnPbj86dxOLKMayTSLHHG8jtwFRSSfoBUq210dhFtPh87T5bfNjrjjmraW9rY38hi1ZQYQxilj2kM21SB6ckkZ6DbV57uESadC2vq0axZaTyFJhLBiVJHXlUGT/ep3FymSsdwBuFvNjbvyI2+7/e6dKn23schjNtOHVgpUxkEE9B061oDUc20CJ4gT57doJonSNVVVxtX3HJxmmNrNy0sUUetK0V2Ue5JRUEbKRjrgZH64o3FypFHzL1W2mKYNnGNhznr/Knx3d9khElYhgpAQ8MegPuauJdJceXLNrsUczuzPiNSUcDCnPoQBnnvTorizt45Xi1uWN5o1mkVUjwZhyq9egNSFiv/AGpqER+eOVcEKdyEc+nSrCa3fokbm3kKyEhGMZw2OuD3xg0+5voCLwLr6TqDG0SPBH+9YYOTnGMEDvUCXqJMijXIVjhuTsxCpG1uS4GeeT0/lRp2HbzN7SdXub2YJ9kkIYbshD931+ma6eNZCudjAAZ+72rjLLU2jjtEh8QqXUvDsdF2onJ4J7HC/nWtLrE9vaQ/8TIESoElOxQEGMfjgE81nKOuhUXZam6+SAAjkkZGFPT1rJ1fzBYSMschG3Jwp4Hr9KoWOusu1v8AhJEik814FV41IEW3IbPbJAH49qV9Zjmt1gk8RRqrxNE+IVKgDG0fqefaqUGhOSaOXe3utw3W8wDfdPlkA/T1oSG4YgLDKxztAEZPzdcfXFaF7dRywJD/AGvHLHDEJY18pPvkqzL155Gfw/A2Eu4oWaFPEYEa3Ec0e2FSpOFBbk8ED9BW/Mzn5EVYY5wu0wTBskbfLOc+mKrFZXkIjikclto2oTz6fWt5Lm2+2AjX1YC5bMohUHaRnf8ATJIxVOKeK1u0jj11EgEzuAyBuQSASvuDnnj8qSlqDgiikFyW2C1uC2SMeW3UDJ7elKIZuD5EuCMj5DyPX9R+dW7bWLiZWll1hI5PtOfK8tAACpBcfh296sjUitvAq63kDdHjYnyp1BxnuVXr0qrsnkRAkU5iYiGUhV3HCHgYznpTDHII2LRSADqSp4+taT3ubOIJrhkZrIxyqyL8uPux5Prk4P8AKrul2o1OWZH1IskiqSFhUBjnofQispSsaxhroZei6Bc3t4sjwyCMfNkqeR616CLRra3VFjfAwPun8K07WxtrOIRrcKUUBMiMD5cc/wA6jvLjDc3o2h/vYBPGcGsXJs6owUUZWyXkGN+Dg/KevpWLqhmwMRSYbp8p5rfW/LpI736pJ54YHCgnoN2fpmsi+uY3miVtYTYJmGTGpCgg5bn39qTZVkZNruQBnVlB5BIIzWirA0loI2NuRqSbo1YgsFAUjoPx4/OrKrHOsckt2ofhSoVRhcDn9SPwouTYrTgGM1hT2Su54710ckNsVfddhdpXHy/eBJyfyx+dVpLazE7BL9WjDAbiBnBAJOO/cfhSdmPVGQumL5ZOKw72ARS4rtzEgLpG+9AcBh3FcrrMRWcfWs5qxpBnBFs8YpN3bNMJpN1daMLEwapAfrVYGpkPuKdyGiXnpmrtvqkttafZljjaPzfNychs9Oo6fhVIfpUkKRyTBZZGjQg/MqbjnHHH1wKXMFmbFv4rvrbaqRW7KszTKJVLnLAg/MeT949alstTv9UuIbaOztZXhtXiUNlfl6k57nms2KwiklQFrpk3BZBHbHcuc9vXgcfX0pfIgiQNFLdiUgbT5ZXnPzD1OBjvT0FeXU2otQ1u5hkiitrXEthtc4K/ulJG4j15xVL7Rqmo2c6QaYphuVDbo0PHzKMgk+oxj3+lUJUkgjDG5vshjEFMbKMcfLnPXGePapWtrEKAj6milGcK0fGD/qzx2Y+1PQNWXBPqkzrCbEI/2ZY0BZoyys21T15yfXjitKc+IGhkg/s20xc3C23yP83mRgnC89MDknP61hpbJH5ZlfVYpshOI2BGOXAOO3BxUCxSTwgyXl4zHDBPLduTk5yT6c/jUtDTsdZoj6jJcQ3ctnbxq0j3KOBkjBCsMHsD2NdfcXk8FssstpGsSb33eXwQwJI/KuF8KQgsXBuyNpLMVJTGflI+vv3qTXboz3j28dxeRxrnbsiLZHuAfrmsmuadjRPljcL6/v55JrX7HEqadAY3WQlXVDg568kACmbdUjlfVHt7YrbQ+VJhhgBgP4fXDj8fxrGiS3ilKXtxqEe6QCULFhihXIOCeuc8E9KdGFlVRG91LcOuSu08t3+oxW9tDnb1Ncxay0QMVpE0UdmzY8wk+U3U/Xjj6Gqyatftp1y4ska3MCo8u0/KoJUH354+tVpI3aF5Eubw7vlQeU4DZ6DOcc+lOSzhMex2v3ChjtRDgqB2BH97g01YHfodEZ9a8m7le0tFXCXMpL5O0YIA9fu9Kzop9XZ4prfTfMHm+crbMq5YEY5PTg/iDVKC3mjBS4mu4i/yspif5sDlffHPFRyQDHmtJebFcAMI2A2EcH2OcjH1pJaibZqafe6y88UEFlCr2tsz7JBszE2Gyc9eoxUN5LqWnLFb3FnEjXFqNu3LMUGcHOeDxyPas+2t1BO4XqykfKYkPOPvj1wKcYCoiZmvVYMF2tEQACegY+qnPTv+NXZE3djc0qyvruUWY0y3MkESM/mrhtp5BPrkEfUYr0fQdJewiWcW8UTLGu5U6Yx1+vrWJ4Q0OKOD7S5m3uP4+uM8AnvxXZOixocO3THIPNc03dnXSjZXFZ5iijy0K7D3xkf41QmmvJYwYooSsiiMbjjpz3qWZo87VklLbuBt7YrKugrLgSS7RjcFU5A559P/ANdTYtszbrW7lJZdyRhzLvIA6MD/AC4rGuNankujN5cO8ymTkEjkYxjNWLu1DZKrcgAESHyi3zDkj8uazWtCZQI1uH5XdmEjr09f/r0ibsuWl0Qrg28DFnLbmXJGe2fSrEXFVI1iAXy3ctkhgw+76Vcjz3qib6kF02BjsKqoSzE9h1qe94FUYpucZPNLqD2Nu35jFZGsW291YDNatqcqOaWeESYqZq5pBniRbrSbqYWpM1uTYlDVPEaqqanjbHWhktF1MHHXGeavxxWy3QKSztEGyDs2tjse+DnFZ8JyeP0rVtYdx4FZ3KsW3kRo2Zbm+MzbQWZhyAcnPfucVFKbVQ3l3F4CEcJkgAZ6D/GtJbW2wOZ/vHdhV4HbHPWoLiDTlZ8ve4524iTPtnmhMGjHuZyGR1vbl3BLkOuQH55HJHpzj+VMs7mMTxG5nu1QfKxhYAheoA49asTxWZIxJcjkZJjU8fnUHkWu9MPOyfxfIoI+nJzz9K0UjLlYsd0GljkuL28LBmJ2qMg44IJPPOMjFJ9oUop+13RlBQZKg4XGDj6dAO4qYQaWA2+e9BA4Cxx+vHVvSpI7aw80GKS5Zc/xogO38D1zTuhWZueH5IbcYa5uniG1UyNuF7/Lz3JxVXW722EjFbu7SUK5UIcqWzxn2OTmobq6s7XiF7ny+cF1XcB24Bx+tY0kthLJI0k96Pn+TESH5ff5utZre5beliys8jzBba+vp2Zgz4QgkDjI5OSFqeJm8guq3xucEhwflVjnPbJyoqhBJZRKzR3F/HMRtBVEwQRhsnOauQXO05jubnGRg7VB24we/Xkj6VrfQztqWZpJjIu6a/wGQ/vMk7dnykfrj2p7OFf5pdSUGNQrFcEljluD/CecY6kU4zWCuAl3qaovIPlxg8Aheh9/51E1+qoY472/MW1SFZUxuXOO/QdvrSuFh6ynz082bVGh3O6FQd29vukZ7t3przthYrq6vPLwBjZ2APQHHRuPzqtLdheY7y73ZBAbaOnfg9aniGmSqPPn1QsAMbYoyPU9W9aLhYWWZGUML24aUZAHl4wCDnnPfA/Wt/wvZy6lcxNdT3rosgfh+M4xnnv0/Cs/SdHt7+6CIbrHvGg/rXreiaHbadaKFD4A5O0ZzUyn0RcKd9S7Zxrb2apufIHTPellmTBBeTb6fhxSyyQKSAZeP9kVSnkgCklpc4/uj/GskbsinuolZSZJQRjkHketZ7TQ7AFecNznBHrlaq3FzCZQHabbkfdAz71GkluVHM/X5vlXge3NUjOTKV+YhLJ+9ugjMSDkE9Mcj1rNWZEkJ+0Xa4VQDkZznn8MdK0rprZkbc0oJOBgKePzrIZrVfvSygg4ztXHf3+lDRKky9C1mXBY3BzuLHjr2/rmrJMQc+UWMeBjd196yopFLdfz4q9Ew2+n40CuRXufLPrWKspWXANbV2cxmudkIWfnA5pDOosXJUc1dPPWsmwmAjByKttdD1oY4s8QJqxaRGYzDbKVVMsY4PNIGeT22/WqpNW7Fo90gkEJ3KF/eztEOT/s9foa0NUtS3LFbpAzrLeeYOiPZbUyexbfkdh0q5LZwWzO8NzcSsjqqh7Fwrg43NnPGOwPJqpGbedljCWaNKwUO95L8hx1OSBjOOT0revtCgXU7K3j8QaRKJYl8yWK6dkVtwBLE9+c49jWUlZ2SOh1KlnLm7f18v8AhiCK28xz5DTzP1VTZFSfToT34rurS10eC3SJJZW25iXzLHklj1JKZ3ZOPwFYttoFpaXEUP8Ab2mXPngn7QlxIgixjggNzyc8+laWpaOlpBGYNd02ZzKFzb3TuTwTuOW6ZHtyRXPXi3G6k427f8MxUn79pJO/cp6vpcsilYYLuRIWeOMiN0JQE5Lcdf5YrAGlxNFvkF6jYG0JGXBJ6c5Fb15p8mnDzJtasNQViqFYrlhlicbmO8n36Y65rGuLhlnmtxGRsbaDHcs68ccHcQR+la04pRRFWblLXc19G0fw/NYS/wBo6ne2twXKxRi2Yh0G0g52nuT37Cob3T7BJ5be3maS1CpsmWyBkZscgn5T+nPv1qCx0+4fypZdTjkUdYnc5/Hjj8K2hDE24KkC5IwRM/GcdMnt71d4rVmTUnZK34mdC09nBFHFb/JGHPmPYAs27HUsSOo/CoHmvZYZIHtYT5k3nlltgHztxgEDhfYV1F1Np09pKkWmGKVuVk+1swXkcYxz09e9Z11CFO5WXpx5czMQTz3pxlDVWv8Af/wCnKpdO9unyOdexvLi8gjtbWeW4U+YIxb8kjB/EdKNc0/U5dSkvtU019O+1OSAlj8rP3AXPXgn8K1fIeKRGDoHC/LIbp1xxyPlORnpiqt5bS3lwkMmp4JlWON3vHeKMHhnbcpOOnIIxzwacZWVkv8AMmVpaybv8rf5nNSWSpKgX7TIjA4P2coTx2yTmtHTotIu7hInvbuBBCXkcwh/nBHChMnGNxyfQVfvLCKLSEcXlg9xBv3Ot5IZJdzALhMEDAJ6dRnNZsEUMp8qFbW2dB807SuocZ2lc7R16/SoqTnOLjzNem/4po0hFR3s/Xp9zv6mmNOt31RLTSDd6iZYGOPsbM24EdF+U9O/PWq+peHtRtSs8+m6hCssmwGW18oZPQDJPPXiooUltr6E2+qwwSnKi4iuXUIO4LAAgH2rSFpPfQ7rnxXpU0aSD9xc6nJzwTnB6Dnr606NGfs+Z3fS7tf8LfkKrUipckWmt7JO34q/4mfH5CaZ9kUTp5zqZ3kt1OzbnlCDn+VTWvhyK5uDAJ7jeoyyyJ5bAcnnn0wfxq5BoxGrLawz2N8oCyGWyuXmiOc/Kc45Hf8ACvTND8OiCN5cmIupUJ5QwDjGeSSfz9aUk4qxUZKbTfTpbp/XmZ3h2xTSLLbEtjIsal/MlmGdoxkk49/5V0rukzLcqYDdIGVI1uRtI/h4A5zu/QVINKC20sJuCTIhTzBEgZQfTg/rmmJZvApBvJ5PlAG4IMY+ij6VklNr3vw/4Ju5U4Nundb7+foZtyL2aRfPtdgU9UJO3Pv2PXmoprdLY5Mtwzc5Vm3/AJjcaXWFuJICkd7PC+c+YmNx/PI/Ssa51GZkCTSySFerNIck+pq3e+iMHUfLy839fcRXsFxdy+WLZgC2VKpszyB3469vemvoRhPnE3gXIkBOADj23HIqH7eY33BGLA5B81hjp7+ozUkmrTXFkYC8omMmTMZix2YwUwe3eqaUtWiadV04tKT/AA/yLl1qeorEyyadapGTncbYLxWZLr08ers7xWiTuiwtGqbcAZ5Cg9eT9aTeUiYSEzEnIZjjHtxTP7RtPLmE+mQzSyNuEryMWXpwMn2Oc+tVe9kYwt1f9fcXUvZM5+yRbhn/AJhh4HTP3vf+XtTor2SNF3QQhWyAzW4GeeSP8+1Zk9/bzXZkh0+OBCqjy1mkIyOp69/StXTNVsLaDZdaNDdNvyGaUjAxgjBB781OpcJQTalqI0rPFORGpErdVtQ+PYHPH0rFdXS8d7aOVsgs6rabAFUZLdT0zz9a03uI9rYtkwSSB5jjGTkdCOg4rKa5WA3JNvuaSF4lPmuNhb+Lrz9DwaJK+qJi1ezegRXYucOJA/uMd+e1SHcxrNs2KfKWzn2A/lWpGQc0LYl7njueaTNFFamw5TV22Az2oopS2EtzYtiAAcCrpm2rRRWZRVZy79qv2kAJBxRRQxGzF8oGKsxkk0UVLAtKeOaCN3XkUUUkNkbQA1WktAfSiimmS0U5bQKCazLgbOP1oop9SGZNyzHPNV7W2e7uUjB5Ygc0UVfQnqe3+D/DsVlaRuQCxGSa7VAEXiiisLnVFJIjZsGq0rgUUVaJkY2osNprkrtvnOKKKZkymxINIpO6iimyCwWzHxWdIfmoopXECcniraHjtwKKKtkkpPGKpXSgg0UUhoz0JV60Ipcrz2oopIs//9k="

/***/ }),
/* 23 */
/*!******************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/videos/灵媒.mp4 ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/static/videos/灵媒.mp4";

/***/ }),
/* 24 */
/*!********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/posters/灵媒.jpeg ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gxYSUNDX1BST0ZJTEUAAQEAAAxITGlubwIQAABtbnRyUkdCIFhZWiAHzgACAAkABgAxAABhY3NwTVNGVAAAAABJRUMgc1JHQgAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLUhQICAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABFjcHJ0AAABUAAAADNkZXNjAAABhAAAAGx3dHB0AAAB8AAAABRia3B0AAACBAAAABRyWFlaAAACGAAAABRnWFlaAAACLAAAABRiWFlaAAACQAAAABRkbW5kAAACVAAAAHBkbWRkAAACxAAAAIh2dWVkAAADTAAAAIZ2aWV3AAAD1AAAACRsdW1pAAAD+AAAABRtZWFzAAAEDAAAACR0ZWNoAAAEMAAAAAxyVFJDAAAEPAAACAxnVFJDAAAEPAAACAxiVFJDAAAEPAAACAx0ZXh0AAAAAENvcHlyaWdodCAoYykgMTk5OCBIZXdsZXR0LVBhY2thcmQgQ29tcGFueQAAZGVzYwAAAAAAAAASc1JHQiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAABJzUkdCIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWFlaIAAAAAAAAPNRAAEAAAABFsxYWVogAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z2Rlc2MAAAAAAAAAFklFQyBodHRwOi8vd3d3LmllYy5jaAAAAAAAAAAAAAAAFklFQyBodHRwOi8vd3d3LmllYy5jaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkZXNjAAAAAAAAAC5JRUMgNjE5NjYtMi4xIERlZmF1bHQgUkdCIGNvbG91ciBzcGFjZSAtIHNSR0IAAAAAAAAAAAAAAC5JRUMgNjE5NjYtMi4xIERlZmF1bHQgUkdCIGNvbG91ciBzcGFjZSAtIHNSR0IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAZGVzYwAAAAAAAAAsUmVmZXJlbmNlIFZpZXdpbmcgQ29uZGl0aW9uIGluIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAALFJlZmVyZW5jZSBWaWV3aW5nIENvbmRpdGlvbiBpbiBJRUM2MTk2Ni0yLjEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHZpZXcAAAAAABOk/gAUXy4AEM8UAAPtzAAEEwsAA1yeAAAAAVhZWiAAAAAAAEwJVgBQAAAAVx/nbWVhcwAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAo8AAAACc2lnIAAAAABDUlQgY3VydgAAAAAAAAQAAAAABQAKAA8AFAAZAB4AIwAoAC0AMgA3ADsAQABFAEoATwBUAFkAXgBjAGgAbQByAHcAfACBAIYAiwCQAJUAmgCfAKQAqQCuALIAtwC8AMEAxgDLANAA1QDbAOAA5QDrAPAA9gD7AQEBBwENARMBGQEfASUBKwEyATgBPgFFAUwBUgFZAWABZwFuAXUBfAGDAYsBkgGaAaEBqQGxAbkBwQHJAdEB2QHhAekB8gH6AgMCDAIUAh0CJgIvAjgCQQJLAlQCXQJnAnECegKEAo4CmAKiAqwCtgLBAssC1QLgAusC9QMAAwsDFgMhAy0DOANDA08DWgNmA3IDfgOKA5YDogOuA7oDxwPTA+AD7AP5BAYEEwQgBC0EOwRIBFUEYwRxBH4EjASaBKgEtgTEBNME4QTwBP4FDQUcBSsFOgVJBVgFZwV3BYYFlgWmBbUFxQXVBeUF9gYGBhYGJwY3BkgGWQZqBnsGjAadBq8GwAbRBuMG9QcHBxkHKwc9B08HYQd0B4YHmQesB78H0gflB/gICwgfCDIIRghaCG4IggiWCKoIvgjSCOcI+wkQCSUJOglPCWQJeQmPCaQJugnPCeUJ+woRCicKPQpUCmoKgQqYCq4KxQrcCvMLCwsiCzkLUQtpC4ALmAuwC8gL4Qv5DBIMKgxDDFwMdQyODKcMwAzZDPMNDQ0mDUANWg10DY4NqQ3DDd4N+A4TDi4OSQ5kDn8Omw62DtIO7g8JDyUPQQ9eD3oPlg+zD88P7BAJECYQQxBhEH4QmxC5ENcQ9RETETERTxFtEYwRqhHJEegSBxImEkUSZBKEEqMSwxLjEwMTIxNDE2MTgxOkE8UT5RQGFCcUSRRqFIsUrRTOFPAVEhU0FVYVeBWbFb0V4BYDFiYWSRZsFo8WshbWFvoXHRdBF2UXiReuF9IX9xgbGEAYZRiKGK8Y1Rj6GSAZRRlrGZEZtxndGgQaKhpRGncanhrFGuwbFBs7G2MbihuyG9ocAhwqHFIcexyjHMwc9R0eHUcdcB2ZHcMd7B4WHkAeah6UHr4e6R8THz4faR+UH78f6iAVIEEgbCCYIMQg8CEcIUghdSGhIc4h+yInIlUigiKvIt0jCiM4I2YjlCPCI/AkHyRNJHwkqyTaJQklOCVoJZclxyX3JicmVyaHJrcm6CcYJ0kneierJ9woDSg/KHEooijUKQYpOClrKZ0p0CoCKjUqaCqbKs8rAis2K2krnSvRLAUsOSxuLKIs1y0MLUEtdi2rLeEuFi5MLoIuty7uLyQvWi+RL8cv/jA1MGwwpDDbMRIxSjGCMbox8jIqMmMymzLUMw0zRjN/M7gz8TQrNGU0njTYNRM1TTWHNcI1/TY3NnI2rjbpNyQ3YDecN9c4FDhQOIw4yDkFOUI5fzm8Ofk6Njp0OrI67zstO2s7qjvoPCc8ZTykPOM9Ij1hPaE94D4gPmA+oD7gPyE/YT+iP+JAI0BkQKZA50EpQWpBrEHuQjBCckK1QvdDOkN9Q8BEA0RHRIpEzkUSRVVFmkXeRiJGZ0arRvBHNUd7R8BIBUhLSJFI10kdSWNJqUnwSjdKfUrESwxLU0uaS+JMKkxyTLpNAk1KTZNN3E4lTm5Ot08AT0lPk0/dUCdQcVC7UQZRUFGbUeZSMVJ8UsdTE1NfU6pT9lRCVI9U21UoVXVVwlYPVlxWqVb3V0RXklfgWC9YfVjLWRpZaVm4WgdaVlqmWvVbRVuVW+VcNVyGXNZdJ114XcleGl5sXr1fD19hX7NgBWBXYKpg/GFPYaJh9WJJYpxi8GNDY5dj62RAZJRk6WU9ZZJl52Y9ZpJm6Gc9Z5Nn6Wg/aJZo7GlDaZpp8WpIap9q92tPa6dr/2xXbK9tCG1gbbluEm5rbsRvHm94b9FwK3CGcOBxOnGVcfByS3KmcwFzXXO4dBR0cHTMdSh1hXXhdj52m3b4d1Z3s3gReG54zHkqeYl553pGeqV7BHtje8J8IXyBfOF9QX2hfgF+Yn7CfyN/hH/lgEeAqIEKgWuBzYIwgpKC9INXg7qEHYSAhOOFR4Wrhg6GcobXhzuHn4gEiGmIzokziZmJ/opkisqLMIuWi/yMY4zKjTGNmI3/jmaOzo82j56QBpBukNaRP5GokhGSepLjk02TtpQglIqU9JVflcmWNJaflwqXdZfgmEyYuJkkmZCZ/JpomtWbQpuvnByciZz3nWSd0p5Anq6fHZ+Ln/qgaaDYoUehtqImopajBqN2o+akVqTHpTilqaYapoum/adup+CoUqjEqTepqaocqo+rAqt1q+msXKzQrUStuK4trqGvFq+LsACwdbDqsWCx1rJLssKzOLOutCW0nLUTtYq2AbZ5tvC3aLfguFm40blKucK6O7q1uy67p7whvJu9Fb2Pvgq+hL7/v3q/9cBwwOzBZ8Hjwl/C28NYw9TEUcTOxUvFyMZGxsPHQce/yD3IvMk6ybnKOMq3yzbLtsw1zLXNNc21zjbOts83z7jQOdC60TzRvtI/0sHTRNPG1EnUy9VO1dHWVdbY11zX4Nhk2OjZbNnx2nba+9uA3AXcit0Q3ZbeHN6i3ynfr+A24L3hROHM4lPi2+Nj4+vkc+T85YTmDeaW5x/nqegy6LzpRunQ6lvq5etw6/vshu0R7ZzuKO6070DvzPBY8OXxcvH/8ozzGfOn9DT0wvVQ9d72bfb794r4Gfio+Tj5x/pX+uf7d/wH/Jj9Kf26/kv+3P9t////2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAGCAQ4DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDltKsFvNNYSRv5LdSGwSQevX2/xrRittlrbu8IHmSo7MoznhunoMDP41S0dlj0+28mU+XID5vQuuST17D2x3rVupg0EG11ZDIAAuCD8rDHWu5HMye0kEauq/KqOQqqMcHnA/Os3XFW30WbYAQYWVcHCrjmpprndM/lbiNvBQgcZIx65GM/TNR3cXm+Hr4M4EawP8qjjdzjH5Utw6lK1zPodsIfLk3hMxElFH0Pvg5/pVs36xweTORbsZYhsfgKAc5zyuOPXmqmiCRdKsWSHz5GBXc54BDHgDoMACrJ2wTw280aNJLOD5YXKtkN1+noc0ktB9RZ3WS3khDqZIsZVU2kc4Az6k8e2abbaaWmjuVBFr5Qw33txJP6/wBalZFD3CQzHy1baYvLBUkeh65H5dqiS6v4rmW2tUiVJGO5yxUBvTnjDe+Bk+9ABcXKtcSS422aM4kKMFORwze+CGB7cViapdQXBilhYobaQTZzh5EwuePwB5x3rUnWa1llgubMgjak6ghlBHJIH1J49+9c+9qBcJFG8kyyvmdNwUlT23/hz26GpldrQa3EinnuJEuQjAyNIu1s4Kn5uAfT5vzrXa5tLOwt7Br4zzNch5posEhM8BMH61jQNFDsiWWQyK+8qRy+SQrA/wC7x759Kq20s0k5KwAP95cc5dsAZ9fp6c1mmVY6AXglvButla0Fu+2HHRnIUn6gc/U1XmeOzaG4t4vNVhNmNxtBj80/e+mcdqY9w1tKrSBVBibdkdD8vGe/Iq1b2Ny1paXkkTvbwo9wCrD54luBnAycdGwT3rSWwkJHa6jJdpFFaTqZmzEyqxByDsJU5BzjgDn0z0q5PPp9tp6rGklwbolN0q4Y7E+bcCRj52PIwQR7V095qGmWNrNY6a7T6VcjLRtG/lp0IKE4JGcNx93gj0rjrmC71WU3MM8k8qPsAEbZYYyTn7o3fNkVKXYDcsmutTzpmqwTPeyRpJas8QLyxoCq89hknfyCepqzqMNy3gXUo47YxQW7qWUsCw2NhgD3Gc88dO9cr9tvC1tMjSJcRkSQXCM+5X6EAZwM4IOAas3HiK8u7O902R1c3kQkLumS2cDejA85xinewjkJATIw7Zq1FCwWPBUncQRz1qM2zqm6Iq7DOQp5H1U81ZRCF8zcCVAJVhgmkgbIJ4VlaMbwABh3UY4/+sKR455bTz2RVEzYTJHQHnPv0FPlXduRm4PJOPXmopYvMLEp/dON2Rgcf1qZLXQqL0LizKbZXWNWJfaTtKkDHcA+pH5UtxHI3y7j5oJU4Xkkep6ZqmkjpIz55A+V2Od31/wNO8wg/IcFh92TlW9Qc/1o6AWbpQDK46TBJsgdDkg/rmqgMbMQWOcHlW9BzV6aUTaXbNICJSJEyQTgKyt9eMn8KqRMxZV/dBRw2F6A8ZppiK11gWi+WwKlsn1zTYvK/sZjKz/8fY+VAMkbD3P+eKS5UMuVxx1UDFJAwGl3BZA6iePgkjqrjjFRLcuOxWyGkO0HbtIGevAplOQqrqxBIB5FNI2kg9jipKJbYkO2DglcVKVYsSAeOTzUEQzKB61aA2YGRyO3+etXHYiW4wksyhnHHFIw+crkcVKhD7lYDy4xux3NRDIZiQOaYjttGZ4IYxGyfv8AC8rtIx056c/0qe4ie/VYkt7eCbdJsaGTI3gqoODjr3Hsar6IZm0mOUsJY41LLz+8QjkKABypOfeiyktpdV+0C4ZG3ieNuSQki4wQB0yW/OttGiOpo+eVeJX2jA2ykdiOMLjr357VHdXfl6bfw22yGMW8h2SAkkFTkDGMZPc+lRR6nO6yxPCsc0UrRluy47gdeRjrT70k6RMkxI82GU5KD5sDg5PvTDqR+FbzboQV32+XdHd8vIyAR09jVm6hka4ilZyxJ52Njb8jY+h5HNc/oqSwKXi/5aLCzPg/3Tx1x1HpWz9qgkujtuDblQdrSoCQx6g/7J556VKbsNrUm0m5ngaeO4iDtJEHRowRtILbjk9M5GM+9a2nS79J1C9eJZ9zspijXJ8vGSBnr+gNZT3/ANmtQhIczHKuk4MZJOOR1U980y21JLa3ktriW4mlmfMbedgBMkEbgOFPGR3ApiaG3cky6DNKXCM0YdBIRu+YfLhuQenSsezA2TSxzTPKkJAjUENu+XOcYyACWKg84FSlolmx9pgkgUBSgcjDE9B6jjnPSo284JcoPlkhzhl4VgTkkE980mr6AjR1TTdLu9Rso90izujoVizukIC5ADH5QGbPqV9xiol06zXVxAjm1Xc0a+Y2RuCj96c9AGJU5/CsV9MuJXW5klAcygymbIyMcn1xz25/Krdtpz2tvLqVxCZFS4TzoJCAjBs7VIHOc888YIHJrCV73L6GuuhaNqNzYRR3k85fTzLKyyMIzJnaGVdp4zwRnrjOM8Jp6R2Xh6eRLuV2lVkCh2+z7UmU/KCoIHzMSD6jGTms+GI6hqEb3JjVsS/u3jO1WY7SeMDGD9OK1tMaBbBNNlmDy2l+WjkdsxIAz8Bj2Y5B47j1ojJNhfQuINEsdBZri6e4hZhESJgVjOxiAo+6QGGQT6DqBVuwYaNrNkkRLadcW8bMI+duI1ZnB/iI6lfvMOAM9eUhKTXUsNu0VoJpJUDZKKYyT8pHOPlz8vOMj2pfLaxlcvMFWQq8YtxlG5xnD/dweQTjjGK0VxM0FW2tLN7OcxwPHqEM6zB/NeKGQMWAZRh8nOOPT1pX0/SJpNPtZLgGKR4E/dzYzvUZkU43dQQQOAR26VjJss5VDlWhUOVAUEMBwVI56HPHvnIxVV5ylsW8lI3Y78Zy2OPmyScdeOKLBce9ja3GoxrcSyJJLZpIUgXzPMkAy3XjLKC3Y5461rvaWMek6nIpuSkLBI/MhdhCD2IYZPVRkY4cHnFZWnSqbhLqdY/stuVPyqFEjjlFx3JwM+wJPvUjE11PJIDJLMeXfJJyeck+uSefely+YcxpW+n2UujNJNOi3YSOR0V93lAyIpGB3IZuCeOOnNXW0vSV1OzieWSICaKKS3mJRmVpHRiWxyQFUEIMc5BxWI1m6qFKZwOcEHt2waijUSSFcbWDcAnoKVguaEOj6eBLm9jlI+zYILZDM210IxgZ6An61Pc6ZYx+IbTT4xcIrH94G+UseSNjMCCf4cYA3KecVmOsTR4255wM98GoIUabJBAmgyyc5+XJyMe2c/TNNoEzq0t9OZr3TzOyyALJHHJIx2jyA0ij+8BywPXjbgZFc1cxRR2rvbxEqybjJINpbcQR8vQcD36dajysEqOQuwr8yr2YDg/jx+tXLpktoFiQKojIXJ9Cik/qT+dJIGzCkV/IYlThhwaSb91pVrFjBmd529wDsX+TfnWu9uiWsh2AqqlztOCe469OB+tZepiNobCWF96mDys7cfcYgZHY4Iz7ilJFRZRGSRg854ofHmNg55PPr705R5aiTOGP3B/M1HUFkkLbZkOM89KsyEZ4xmqYOCD6VaCghjkZqokyEXPK+tOCByeaQjag45zTWYrgKaok7XS4nGiLISsbwBt0mcbgDkK3rkMCD1FN0iY291PcrdWscTs0Lxuy5PYbT/I5xx71RstLN+gSO/BIRC2AcKSvAP8Au4xn3NPsorXVbFoEkk+0i4E8nmEMpOSGKn0IP5gVr2IubLt5SmSWa2kjDEna4XPPAPP9azTq9kYp42ljeVoWWNJCRgkEZ+vT2rcWys0iCwxsmBgBI16fzqlcxxxWmoNJFGPlbZkDfjBwCPX+VU7iVilozxQvBIqGXFmofachXDNkH3wRx71a1GdoNLmnuG3hQBHGpBIzwDz79qxdMtoL2M+cDiOCFQ8Q2lSdxOfXr1/Ona5BcwWcUNxL5ihwULj5yoz379R+lRe0S7XkUtM1i3SB4r61S4LTbzIVGU6ZYEYOc+4rWv4o0njS3uGG45AeHcME8EHk8859q5EK0UvXJPORXXWV9I+iwySQF5VQomF3NIclRn6cfjUQd1ZjkraoW6hv4LdQ7kDczpIgVxkd8j5l49ciq81/5oj+0P5qswQMmN3/AALsfx/OtCzvre3uo/Nma3VHAfCludpPQD+926Vn3V3HLeCUjbKRiUpFt3FiewGPp3q35EjnDLBJ5tqdxYKkiBlYA9RyO4Ga09bs73Q7GU3csmblEZGyD5nGz5wRuXoeme3eoNJku7K8kjbUjpiSqxed0DO+1SCqhgQCQSPXJGaj16w8qa1XTb67v4J02SRuWkc4OAwLADk9AMY6VDu2VYbDutroPczLJb3Ns7syuMP8wzkZ4H05/Kq1msy3scVrJCJhcyLFLIxHUA5A54x37471U8pA8QsyyRPlljncYPTnI6ZI5z3+lLbXxtpyY0bzI5mMZJyFHH4evQ9+9TKnFoC+7KqxBFZ5GUTINpUBdu7O0nhj1z9OOatXSKbho2eVGzudoRuDNknluTu6DvUm2NZ5bqUPbvc74pQq/d4Gdh64II9OKuRy2lrJC9zJ5yOCQ33mXgjGecA4xnqOMYzWhBh3TNIqlXVpkcM7Km0bQNoJ6gHrn6iomvGleaGXz4WHzAhuDj+8P6g9exqa9uUW4f7JGq2zqVQOqkheBxnv0xk9frVeGV5AsrJ5kbc4yCqnGNxJ+77ZPakUWGiEjRWssv7uAebJJtAQZ5aRsc45CgdSQBSz2xuZnkihna1RT5a4JCDtkAYB/DvUU81tDYGFs3HluryqSyx7mJ5wDlwPTI5PfNWbi9ube5SWCOKNmUETxDB2scfKeMc57ZHrRcVjIZuN7Ku7y8NjpkZwwP4/pTYM+UZSQwVhu29Rnof0xmtR0juI222ifbioMbFyd3senzntnAJ98VTsoJ5DuaFcSAhgzBcpjlsHoO/PcUuoyBHYKS7NjcRyOBntTFjaOQtkjaeHA61Kyie4CMSdq/MwGM46Uicbo1O4AnoOc4qiSVTyN0Y2yqV3A9MZ/wAf1qGZJj5xmQtJHIgZBzzsC847HANWIpCUcED5dzAEdCBUM1x5F1Kcg7yDvxjKnH5HoalopENvNJ5jKzkb4pN+ehwjHkflUFxm2WFHwywwJujYZDSP8+CPbdz9Md60wEnCkhf3jeV5g64PB/oPxrI1ZidRlQ4yrsT9Sf8AAKPwqZFRKrQyNGswRijZAOO46ioyCDggg+4q7Ztusb6DPzBVuEPoUOD/AOOsfypRIVEclq0ZYjBVj8yt7Z4H4YqSrlcCKPak0T5H38Nhv8KsMI9ilVdQRkHdnI+mBTERZNPud0aiSF0IOMEAkgg/pVuzjLWal8gDI6cjntTihSZUfiNcnPXpULHmrd1EUGABtB6+3Y1UPWnIUTr2SLT5LqW3cwN5cm0L06KV68/x/jiptKaSKHyk04rPEf34PGMgY575AH5VUlhe4tnmE5dBKtuGViQxPcfQ49q27Oxt2OHjHzklmjlYAnPHy+2BW3LfY53sXbU3M8WEEKMRhlHVfxziqmoWqpa3RbJMdu+4txyQ1XCyREbDtK4zuwR9PxqDUpUmsLxkj8vNuxY5z0HT8q0SsgiYnhgCS3ncj94GTaq5y2EGBx075NR+LB5dtbhtwCyNlR0xx29c5qbQCwhdI+Fkto5uTgEqWQn+VUfFGpxXiQQwvHIiliXQdDnGM556Z/GspfwzZfEc4gwRg9OhrrdFe4l06GGBJSw3hgjYzhiRn0HT1rlUJjG7byMDP1rptLt79NJae3CHzWbYjcbgCM/Tvis6e5c9i9PBJHZpHMQEV+Yj1+oPQ9x2rMWykXzJBCJQXOdxA2kcHByeOOvtWrbLK7RXE8T5VQ8hPzHPU9ev41NZuYvDdvKsMfmCPcXVSr5YZznowGelbOKZmmUxpVzLZhDpaK0u3YBKqqQwOTgAtuPYg8AEYNVpXvnCTBIbe2gKhIZ3dzuAH+yCc9cGtWSArcQM8MAcDmCNnAOB8rY/hOM56e3WqlhKbq7UJbG1VgVB3FioPXg5yOuQPWlYdzNgtdQvdSZ47SCYSbnKxDbz3IBPP071F5jyzStcWaATShiISY8HgDC9Oo+vXFWrm4eDUlMS+UqQtsWNiwxnbljnuTyOP6ll5iHULiYuomwpYlc+X8uAMD8T3xgVNguW7O6RVa1dDJMxDgHO0kKcqD64B/lUd5dOEDxLG67BEWRcKoGc5x6nPX86idZ0vXSO6KxDrlA23ABJ/TPtTJi4laNwrMrESbVAzkE8enT170ai0KSylQ2UTLc+vPfPPpU3mR3RjjIJPTDDcrDr+BHP5du9Ri6pJ91o1K4I9Dnk+9WrZlgOWXI2dM4yTx/jSGJfxOwYqchhhnLAArnINavnRW5eGWQCJXysrLu2OQAflOeMYzn2PaqGYrq0mR8tHEyMij5SPUA9PwP4VOkYndlcsPMJOGAxn2I6H9DR1B7F6GTF5at5bCKW48mcDloyf7uT06MMemO1ZkNyzy366kzyQpOVDJw4OTgnH3lHBxyfTvTorryd5G4NGNioRzwdy5z6c/gKz7m5dJRKy8Mp3DHGSeuKUgRavbZxbxzwzCdMfM6qRt7ZHsf070yyxhd2cscZ9BTtPuZGWQlDKsiFDyeAff8AL8qkjBSFliO9kUFunA9aIsTQhhaH51JBK52sBgioZXWGGWCLdL2CkYGNvr165H4CpTOWdnkGCwCkY647f596UwuM3EQVn5ZlDZyO4x+v/wCumwRHF82oWsrNsEeCy9hjlj+Z/wA4rG1BSl/ONwYM5ZW9QTW5bCObUY0d2CeXuO1iMADOB754+ufWqOoWsCiMzNNBMULMCu8ZLHC9Qc4xzznrWbNEVdIAbU44TjE6vDyePmUgfriqOCOGGCOCPenBmRw8bYKkMrY6Ecg1d1VEllTUIU2Q3Y3leySZO9QfqCR7GkULYXKszW867llXYeOWHYfUdQfwq7ZW5S2l2lni3Y3Kp6H1XsRWFWxoduty027JdACvJyBg5px3InZK464jb7A5CZZTt9flJ/xqK301HsBdzyMokfbGoxlgM7jz74H51qXllcQBXgZFXYrtuC8g5xz7/pUFw32uCOWQSSRxARbPmBjPXGM9DyQaJMz5rr3WXbS2uBYhUuWP74vtAPBUbhtPY/Ic9ulaZa5hnLbzNaPJgSsAGUkDrjqMnr9azrC8k0+KF3RvLik2uAN2BtwQo9fm/Wuh0Rkn0i25DBV2c4wAP8Rg/jXTFLZESIjLNEgMqEx5xu96gupo5LO68tlH7p+No/unoa1XcwyAod6Dg45xWbqkdtJZXEi2yqwjc74zgng84rRiRn2EjR6TpcigKzQTRfiQWB/MGuRKKzRpuAPAJNb0EUn2HThE7sFtpZ2G3ptyv9awEkCyhssibRyvJBxjNc09kbx3Y5X2k/KNrDaWruNAtmuNDt51L+VCWQsD93DZPJ4HUfrXBM+CSmcN93jHHTP6V2nhuVodJmhDswabJwMhe36iil8QVNjaa5eHTrtmhV0dCsW4fNvYbV/DJBqVjZIqosxmtViEcYDnO4FVzxgYAz71k6lLeSXlnFJdEgzBVjQgjGG+bA6gZGM/nVtpUtrYwBg65IDx8PwCchfr3P0ra5mWBLJa6fDIlrvk1HLMzNufK4UjOeBnP51R0m3u9RhnM0jCGEEpCCUVn6Dpz2/Slnuby3/0SHyo9mBI0ZywJ52Y/ib2zgd+aZI0zpLBPLdjy4jII32KT9ADwcc9efekMxr2Ca212PyLtJisDbZFYBRgjPsME8+tVJovO1Aupysr7mzk9TwM49SafcyRtceXGoSJEAkK5YK2cgepGMduKW3aBLvcJQDGVJYqVPXPAI/wqGAuoyypd3MUaBSr7SFGc8n8xUcchWNJpFLZX51VuSg4Dfofy96nntVtri4tpW3PG7LKwJ/eN1wPRenP/wBaqyyeZM5Aw5X+EdcccD24/KiwXGhI0R+pBHyEdiCMdP8AODViLSLuSwN6SohWOaVSW5cRFN4Ax23j26jORioUCySTqrfKFxtGfmHTIrVsr64axmthFcSiO3lkMnmgL5JUR/cbjCEH7vPzGoZRm2uxhskyDv2vg8YYcfiOat8NGkkhw+FBx/E3Qgj8P1rOt50EqqRtG4MWPXPX8K0bkNJNbyrECEnMRTkseCw56ckMPXimhFWIM7T7yR5UTKxYEnlSApP5/kaz5MmKNJOu3OD29K2JMrqxt9pNukcjSMpz5jbPmb9Ao9h7msmdRJifdkyHjApMY6KZ1GCMjGOBjHvUke+SdTCzBwcg5GQc9vr6Ui4aMRqP3jKMqOMjrz7U1CbRxtjJPXOMj6UxWNQqIlAkgibJDOwLJ046A47njio5brdCWjVGVQdiqu0E46Hv685NRxGW+t5UwWk5KlVA3DjOPU9/zp8OPL23IkQDgDAHbpn/AOtQwQ5JIVjMzKiqwVkKMfnwAQuSc4z1x2U1k6urC7E88cgadd4LYyffHp/nFb2nyRWZLQzLCluvlqqAvIueSeRg5PGf0rJ8S5luxOsWyMnaQq4AY8kDkjnrgdKzewc/vqJi8D5gWznqcCrEFwI43XzCI2YeZCwyjdecdu3vzVcAEkHgZxknpTpR5VxKo4AYrjPbNSbDzDbykmOYQNniOXJGPZx/WtDRttvLMXkg5UbSJMnPsB3we/FZR4HDqR6Y5q5pU5trp5h5oREyxjxkcjHXtmmmZ1FeDN1JZWXeqRzAjAZlGB+JIBPNMms5L2ViXihxjhm3H8SOM+wFR3F+FH2orHJIT+7YJt2kH0yRnmq1td3cyOULM4bL4wOvTinZHGlJe8lY2NEa9uJ7yVERmlDSIkuP3mGAI56ZAwDXTwqI9RZQm2OdNwQADDL7f7pH/fFYFu7w39tM1wotjbKA4IGW3YZD6HnPPpW7PaTSPH9nlKPFudEJyrsOAD7c4/GuuF7GkndlwxZ7n1I7VTuYR9juRyN0TkNjkHacf4VftpEnt1njzsPbup7g+4OQfcVHdfNBKGUn904DE/7JqxI4+1kSLSdPmSPdmxuogoODuB3Z+mAa5BFbbsXGSAPpXU20hj8MWc+1iYZJFbAzwyuuP1Fcy/DSFV289u3Ncs9kdEeoyRgSqAABBgYOa6Pw7okur2TFLiRQsnliLJweN2TjB/8A11zPQmu08FXr2MF3IPNCiRMmJQWGQT36ZxSp/EOewkWn3NvI0Nvaq7gMxMHDFQeTnrika8mikjNys8RRdo+QKWU57gA9zz17ZrpJEt7c/bmiCOrAyQ7sH12k4+me9Jqdx5tq7fZxEoA3c4DdB0+uT/k1uZGMl9aswW1kjtPkMY2ZA6nn8QTxVm0ZLSyYQGM3DN/rWYYAxjjP1NZwCXEifuw67goBxnH1rSl0eycssDKXB+UCTbkY9T3BpkttGHb6XczXt/Dbw+bmVmJVgOvPc+hp9tpFxEt69/byQMhVERl+Y8AlgO/GcepIqSHQ5X1DVIoGmSa3QSEMwBwAM8dzyOBWpe28tpK9sZWVmGyNz821Yx973Jbc2fcVnoXcxJ0lnVZWDpsQRlWzuOOF68njj8KPJR4IzEAMbcsThs9CQP51oi+1HKvFcF54VAgkkTD5Y4K4Ixj3HTOMnPFRxqVzN5txKHOcF2OMMR0PH59qBMzZChIkLbdoHHIOCMjFa48RXF7beRdrEYBaPFHPChA3MqIC2TgDEYyFA5ycZJqhLFem5aIxYlUZEbkA4ABJ/wAPWtdtEupLF72Xz2u/IRngMY2h/MPU9G/d/OO/1qJWLTMOe2PnJtAREBbavQtxj68dT+FXrO8JiSVl5kUENn+Nf4vyx+ZqhBdR8DYF8vAQqMK69f8AP19qLWaQ2twjuW2OWUls7WxjH4g4oGadlAvlyXcquPvRKi8Fg2OM9h1OffFZgtpFZk2MjBN2Tkdug7+lbBIRkVNrRmYkZfJAxk/yqJ2gWZWt2IAChpZirAsOT07H05qZPl3M3K25kR2zyKQVeN3B3OF9Ov6Gi2jmiIyDuKhijjkj8e/pViWeRotnnNDBkbl67s88fzqrFG8U6yGIz7jlSz9SO+aI36lRba1HNeMFG7cqjIDL1z7VY+3zNsaSIthcBnBycfWpJkbaxj8sP/EzLux+dV8NDBuLbwSQTtA/T1qmh3Lcbn+xLqU5yZ0J+Yen8+ayL2bz7RcscCQnDcckcnArTjMi+HM4IMt0MgDrhe4/Ksm+jYW6oCAFbOz/AApPYrqjOI6jjripyBPAJOfMQhGJH3s/dJPbpj8KhfAdh/IYFSWzRiUpKdsUo2Ox/hz0b8Dg/nWZYwxFfvui+2cn8hVzTIHuLpkgYjC5Yltu4Z5X8apTRPBK8Uq7XQ4Yehq3pxZJZDsbDJjOPfNC3Jnfldh975aTyeUf3e8kAE4H51YRo4rREmiKyZzuGeQex9x/hTQUWPzGAdsk4PIJ7f1oIifb5rOV28HJG7tnofpTaMZaqzOgt7YCSWS3lR7e+Z02hC2CCNp/A4PbGK6C0nT5Jmu4PLdAI0MihhwOOvXIIrlNDLrNLOblEihjaRh/E/y9u/BwTW/Y6UtlO0plR7OYiUIYQeQD1z04J6eldMNdUTLzNZB5V1MmCBJiX/gX3W/9lP50rxllZck7gQc98imTRfZYo7i3TMER3NGvZD1K/Trj24q+sa71JAxkEjOc81uiDg7IMfA7gltolPyjud61yzsN0gYDJzjHY5zXWRfufCN8h6pfeX9OVNckw5LbeM4rkqbI6IbsZ/EQD3rtPh2YjeaglxI3lJCkoULnLAkD9Ca4w43HHJrrfBrvBbX0w/1TFI3J/E/pUU/iKn8J0E8M8mlIsEDvukBaQfMzbQclgenXORnnOaluLU3UItbZwiMgVQ5AD8klvXk55+tM85Y0+yxyfvMEEEfKBnPUcEnp+dWorC4+yJcG3Uwhs/eAAUdBu/HrXQZFSO1srdJLq/uw0pIUKgDYOcdOM4GKZDa27TyXJhM1nGCm7BQA8YPOfm//AF9qL9b7ULmU+QineSsYBOBkdyenNTQSXEFjgyP+5iO0BMoowQScdep/OqFcbaa19i029aC2JV7s4Z1VioVEUMTw2c5PGBVDVriQSWMbXTs0kCMSDjblRzx0yc0ump5ugwQGBN2WlXYo3Ek/xE9TgcDpj3NPeQXFkkkiqViTyZFJ+8FJVGHHbdj2IrOW2gnLcltfDoNhJdSrG0hTCBpMbcg/OVIyQcjB7YqW9065/soTmQAFLZ7eMoGaZZCw2q2chVK9DjnPbArS8O2P2zy5nlFvLHIFJIO1GA+6MdzgH6GudvJRLLfR5Lpb+bIuHLKGLt8wJ7/dHbOeai/cafcpRsqOLiERSklwUYZwAQBnPXOCSPwq8NGub3Q5b3cpVpftJhEO7awPl/Mo5Hygnp0x65rHkYYmUdXfEJc4XDfNnn0z/OrWpX7/AGOO1ePmJPKhclg2Cc8ENgnnjPbj0qJbjtqZyac1zZ/2hI4x53m43hS67gCQOwGe39Kda42zRyKsbb1/e/dU/MQN3H/j1MS5kjgMW9o4FXY4UnJHp6dfyqW0QixeJkaSNgGVHOB05IPbHH19KI7lJvqX0E0toyW6ATR/IilgFHuT+Oc9KxzO8kbQLMykkuIDJ3IwSvp06HB6VtWIaFG8reyK24rMgXbG3QY7EcDA65qOOy0631yCa5glkTLOhiIGTzgMD1wf09ab0V2Q5KN7lO002a/EhiQkKfmd22LuAxjPfoTj61NBDa2sKSTXxO4gHy4cAE+jE8+nTHFWZtftjdiNrKOGcIqRo0W1OvUdxkcfj261m3Th5WkkQZJI8uH7qjPQnPX6UJ31FFylvoiZ5o0T958jlQWTOcflVeNo5G2sQVB6EkfiPWq2yFeRHKrgjkk8HHUn8/0p8LC3kiLNlF5wPywfSqRpbQ1b9Wj0+IkiNBMzsMYOMf8A165q4bgkj93uyCVwST7ntXRapI76LZvKuC5Ykg9M+3fgVy9zKXyFY7eMDPb0qZlRRAwGM5OCTj86bUsw27Mf3ccVFWZoi19uYwRK8SvNEpSOYnlV7DHfHOCemfYVFE7vJ87M3+8c1FU9nbT3U6w20LzTMflRBkmgTskW42G3J6d62tLSwl09mvkEgWVliXd24JOB7nGc+tZ++ezkNq6xrLE4J3Ikm0+x5z9M1qW80T2y/aJCvzN++mhyjHjgBemB/X1NOfNbQ5Kt2jRsNDe3tZ47e4C3DINspGBnnPuB2+lT3o1NrKyRJ/LuGQxTjO5A4wFOR0bJH51Xj03VFtgouzMrSMMF8AxleBnHrmrf2e+0nTYjA8UuxmecMm7DYGxh7DArq12SshtmxpRuRYRi5XD7Rxjj0x9PT2NS2ivEXt3HEODG2fvIen4jkfgKqaLbWwe5u4JLkiWRgUkb5Rhj0z2rUliMoDRtslQHa2MjnsR3FarYg4a7TGk69AePK1JccdeP/rVx/wAwQkEYYkHH4V2euDy111JI9jPdQTBQ2edvUEdua4kjj1rlqvU6aYDqeBXoHgpJDos0QjYo85kYhQQMADJz7ZrgY85PqBXfeDX36HOiplmlMQPf+En9aVH4gqPQ1Li08pN9uhjyThVOBg9OfTmnXt/i1jtTIGdAAkfLLnPfkAd66DVNGs7LT4ZJg880ewsFBIck4II9O3audvdPRZnkVUjjlZvJhLZl2Z+U47L/ALROK6FYzZSM0u1mkmZUGcjdnBzwBj8gOTWwJo4tBv52QQOIW8qFfmKEgqS2f4sn14BGMnmsaziAbzs5kUYAHSP6Dv8AXr6YqS5ug1jcCWTY5wF+UHduYA8n0oktCU2SalCYIrawJUtbRBN8TFlG0Dk+9ULSNo4lJdS4hzIjMBkdSB3zzn6gVfvA0YZ1BZmwRvIU98Hjke+apWUcoulnRAP3eSHYdBwCMjuAfc81BKe7Nq5vIEEi6ecxsAxiMPVgR1H0xyO3NcncyFku8lixKgnPzAkEFfpwPetqG8WGE207f6jBZoxkqp6MCOSORxnrnIrAnO6ObaWy8jZ3dhjqfTg1nIuOrFtljuywO5wI/Lyo5wDww/Ktu4Np/YbyWiB2Fu4O+NUctiMCVQxJ2qQQW9TxwDjNuXeQQzE4ltlRQOACOmD+laDWenPob6nJHuaX94lwYisikHGApO1l3ZUsMnAzgVD11LjaWqOWlwqlBu++RzjPbp6//XrTgZUgUMQw3gYyWwGGR9R8oJ/KqssYmYTQqW6ErkdcdPzqeVGFuiIhWRGJYbeACRg888Dj8aaXUfQiimDQhTKpZlLuhPBGTg56ZPNXIXj1CKbhVuFIO1E2jP8AsjjAIqlF5ZgMyDOZCCuOmRwP0qSNm029EhmzPjaxU8c/X0NJyUXdkTSvcoXtkt5ua2Vi6DO4nr7VZKR2kbCeZfPQYLMQRuz0G0kdv05qzeyG3t0uIxlHPyEMfy9uazrO2llmmjlKorDfuKkqG9MAH1xikr9dA5m1d6IcirPJsZ8Rv0UfxH0Ht+NWCyWy+Y9uXw3KbOo6cntircWlPCjM7Q5QjOCRn2HGQfbHTrTLj7PEsn7wTKp3cYUE4B/+tWsbBzqWwmsTRyWmnxBFSF4zIQTypz0PvXN4i8rITDhhkk9uf/rVtau8lxFYEjCywbyRzjnp+tY8wRWBU5f1J61EjVFWRccdSeS2Mc1HipG+di+MA9s03HtUWNBuK0IY7htOKQgbDlpSCB3wFz36dBmqWK63QtHtZND+26lhYGkKxMJCGB4yAoB5OcZOceneoltcyrSUY3IPD2h3N2ftJiZIopRg7R82D6enviunmvbXSZ/K2TF2BJjkdlXGeCFB4x05657c0xL2MkW63ayQog2LGiqFx90b26e7Ecn8MZVzqcdxIst3b2zyuu7dMxYKM8INo4wMcVzyfPJTe60OGUnN3Y/TtduFlSO9hlk+0ENbsAACnQ445P48+1bupWpvtMeKMOzZWRQpwXwc459awINVh0cWthqFnukhiAZ1UEr824Y+hA6da6W31TTp5FgivImkdMoAcZBB6e/B4r1oaxs2dElZ3RPDIi3UNpEjNG0ZxMvKLtxwcd+TVy180wRmZQspX5wpyAaq2FlBpVsyW6SOTtz3ZiFAz+laCsSo+XBx09K0XmZnC+OALe+PzYW5t0JHqysR/LFcO3BP1r0D4hxL9nsJsfvMyJ/wH5T/AD/nXn7Abj09a5a3xM6qXwjlY4AG3GM9a9L8KSWVh4esmnk2SsGlOGKnLEnOcemOleZA46nHFewaZn/hGdNjBaNvs0eVz22g/hToq7YquiG6jr19fRssduFgMfliWQEqR3Izjcffp9axHYx2+23DIn8Y5yfTLdz/ACrakZvmWNN3Ofm57Y6dKyhbytJhUDYBfHJ4HfAro5TG5HauBEshbYVYscJuYj2PYetP+1fuSyx2/mSzRncYxlcHkDHQHFaAtWiRzDsm2qN0kRzhiM5z2GTj86zZ/wB1PFGQ6/OSCU64B59KQbEed8JRlTyy3zDse/QVXhZvvOHYqhGAerZz09jn86kfLEbAM4zjd/KovKcJv8tsHJ69qUkK5Zmkd7eKAGOEiT93KQdyZ6jPp6jHPfiscOHmlYKNgcn5QACOgOOgz/Kr5MfyiRAqqSF29SSM8ntVC3xHdyPvYKTyPVQMVm0XAn1BDbwyxkpnJjJTrycjpwevvVC4nuI4jEJZFjUrFsDna3c8dOeKmLKyRqUwUJY4OM0+ODz2kaUhEYk4PVzjAwOScH0rJrUcdCsD5xaFhuIgJwO7dQP0FLcSNbWpSQOr7Nq8Z257g+vQY9qsDyVP2uEtL8oHCbckcHPPp6elVZ3WeZUaQxgsM45U9g2PXmh3RZKpeGRJFYoGjOHQ454GAf6UyR/mUSlDk98A/TI5p8U8nlSsm0shKSK3KsCeMj8SPUY4NWnjhkimkeNmG0lYo2AZCR34yw/Xjn3mZnNsqreGO3+zCUCHO8RvjJI7Z59+eKkilmEqgIHZlJGcgkY9RyP/AK9VFsmknZ2Z4YVAKjGWz257jOakm+yx26W+4OUYJuXJyfqcYB9Kz0vqYtRuPhlnjjC/bJNuf7gLZzg54qK5ud0ciLbH7wxJJgHvn5cd/WlMmZi8YkKLg4k5bvkDHTFTx23nyl5gVQHbkNyR1IraO2qsbwj1sN1KE/YdHQIOLYlgQemR1rDm+aWQhc54C+gzWnqQuLi68xwDvUEAMPkAHT+VZtyFVlIJKkDjrz6fSh7Gq3K7AZYEjPTjmkVM1IseTz1qwkXtSSHexXER9K6eC4tY9Fs4BbCSVDvaSRcIT6cHnsOnasdYa37b7MllEZk8wtztQ7coo6HP9OtTUTS0Oev7yRnx6i8l6WuIUMBUAwouyPgYXIGMgfnUZshJNLIs4SPeVDfMMnqQAMkAZHWp5EEs/mMIUOMCNUyuDx057ZpkrMjsZoQ5JwPMU5wOB/KuVxtqZJdjp9btLOaD7dc2zSvCNu6I4cAnGeo6E5596zNP8JWkgSd72SQdjFxg4wSc5wd2Tj8Ky7ibXI7i60yCW7kiA8pAvPysCV59wD+tddokMUWmxvEJAbj9+wZs4ZgM/wAq9SPLOWxq7xW5rqxZ2UAqVAwx5Bz/AD/+vVgHHPX2qGNDipBkHpW5mcX8Q5Mz6fGTwInfH1YD+lcHzyck49a63x1MZNcEZOPJt0X88t/WuTccjrXFVfvM6qfwoQjKEdzxXt4gEUMEK8bI1T2wAB/SvF7KPzryCLB+eVEz9WFe5SgF3bGcMcA88Vrh+pnWexlTgpuEJz/tdKqMgWQiQtjb8xBHPHStSWLfk9R/KqDxFDkqD9a3MrkfmRWsgIhLcZ3FsjntjpWbdXbXF7CW24UPnHQZxirkyKUYbypPBzxVT7L/AKQzDGFjGAOckn1pWHcrspckKMAmo3Vt5+ZcDJAHSr4gbJQfP9OeKzkaOdDMsMjQtkK3mBd2D2GDxx3xSkK1zPuy26GLf8pZmcgkZHAHP4mkKBpCp43jgAdht4/Q1e1WzMcTyxnKWyFXBGCuSDn3G7jI71SLMJG2LkZVl3D04I/EH9KxZrHYIXIDMFUkfdJGfr/LNRZeYrtPQbmkbnJPYevbntTpWCqkSeY0fRcEg9eSeMHjpVmeJljUpGxgYcSLjkZxj2I6H0pWuN+RTgl+RoyjGZgCC2STxn6c/wBKimX94zKhO5VeMEfdPv79eKlJAuAsaHk/KC3Q54/x/OoNjRIrbtzebjOMZAP+fzqWiluWrSEx3ckORiRDG/sSQyH8DgGoJN28ReX8xUhgD7jB/CpEGJ5Sd3y7jx1x0/pVsw210v7uZkmP3fNUbWBP3d46cngEd+tHLciSMsJKkRCvGoHyn5fUevvUdvZvd+ZHJIy+WNykHIXHOferN0jWk0kcm7coAaNhtJ9x/nmrFoiG0uZyyAImBjOKUYK4JJK5nx3AlYQXakSIQc5wfqDWxGIrSMTFCwLYCrz1Hb+tZN3a+aFCElgNwbbgj/61JBI2WtrgGOfZgE8hgf503roWtNSO5QS3CiR9o2B8j8aimhV5gEYMAAB+tWruJyYYtq4XPzKODz2PpzStCwlBIUHA6UWBMgWDJ56nrU6Q1IsJwKnjgYniqUROQxYCOtdDY6aWsEdsfc+SNlycnq2euMHissQuBitc3Oy1ggcO5MYK8kHOMADH9amonbQynqZ08rpNHmRo0Q4XIGODnOOp/GmS6ktzvYxx5ZyzMgwzH3qC8WTy3ZyCM4O07ifqayBO8LErgA9Aa5ZUrijC56LaFQwwAW4/r/8AXp9nH5UEUYzlVA4qlZzAS7BIDIjbGXPtkYHf/wDXWimGB2nBB6V6aBlpCevINSYYkDrnvmoI5AoOc+9Nu0e7tJLeNzH5w8svjkKThse+M4qiTy/XtQj1LVby6iB2O4CE/wB0DaPzxn8ax15PzZq5qLKdQu/LACGd8BegG7jH4CqYHzckCuCTuztirI1PDkfneIdNjPQ3MZ/I5/pXsRbJ+8MdcZryfwhFv8UWHHCszfkjV6k6lQccV00F7pz1viGSSFCeCc9APSqkj5zxjPYdqlcAk7uuORUJU7SPlBzzurcyKM8gC42bjnjJNVo5XFwRxnYOg9zVuRH3Zxxmq5Xy7hj/ALA/maQD9mCTggelYSNOobSLQL5sbODKRlYk3HB9zg8Ct0FmHUjvVC3tYtPthGkm9yxaRj1Y+p/wqZJspOxSuI1tri4uShknk2BpAfmBICnGex64/wBr2rOkmJd1UBfNVw2Wy2duB2xjPpWzcHzAcqrE4BU8grnpx/nr61izW8UrH96Y5DlghiJ2ZOOCD7dDg1jNW2NYO+5biSeMqpml3HGDHLsA/Sr6b/sE0ZH7xGV1JAU8jB3Dp/d5HWq6NBDb7pXllVB8zJEBj65P9KlstR1GE+fp8QExYkfJvCqRwOevr04/lV0thasr3VvHDq8W26R4Zm/1joSM49PqeD6Z61QuThCElUp5wzt5CHBHUduR7VpXHiO+nJtdYt7eXzfkMoi2Ffdto5A646/Ssu7M9pKVISTJ5OTkc4+91PUVm3oWizGxfUriYOyBo8bl6qzEZxj0wxp8k888TecVZ8/MSuOnGeO/86qWksIhQSO0efmYsCy8ZUZxz3681oLbSTQyTxL5sIJ3PH8wH19Pxqo2ZE7orM0kywxzyO8UGRCpJOATyo9jUzRqlgsSnaZHJI54x+HFIjKN245PG3AyOtPllR3JBKqE2jI6dKexPMin5MyP5okYk5DjkmrFrpyXkyfaEXDEoc/oQR3qIJI7mTcg6LkDn6Gr2lJ5d2MLtGffnHpUWNEynbvLAGhnj86NTtPqKkkhjllRoCxUrznqDVqJFVy455J+YfjxVi2iaW5BCEbugAyT+VadNSG+pTitTnkVditgp6Vbl8q0WYyxlygwOcLn0/8ArVnwTgl1LMV6b0HAPPtxU866Ec1zQihQAyOkjRqwVii559KdNHG+/bL5IdQPLDD92PTnnJHPHrVOW6ngvFR1gUxJtSJDvC4HfBxkZz165qkbueOYON4ccDzMc4Pp0qJNsl3YXcSQrsERCEqCAcgcdvesprFp8tBggHByMCtuSJ5kkkdQgPRgcc+3oKpxELhZlVyoxnHPX/8AXWWpUW0M0p1utSN5HIJWjJcD7p3YwB9cHP4Gtew1mWVonOqWhUFiYVj3AqP4mYgDnPAHSuRimZrSUNE4Bc70U4BBz07nB7VNd6lLJeCGCKVkhQQoVHksVA4yo4Dcnrn3rVy2Rs43Z0kmv6lcPNbQ2CFi6rHJgguM8kA9eB6VGvixbe7jlgjRXf8A1hYkEN67ccA88Y9K5M30nnRyQxuskeFXdLuC8cceuKQ3nnuzyABzxgIOnbkdxT5w9mivcESOzeshPX1Jpka7yRjpzTTlupyM49M06M4J9TWJsdP4GjVvEkZP8EUj/pj+temOwNee+A4y2q3MvACW2D/wJh/ga9AOSvcY54rso/CctV+8QsFJ4ABppjGfw5zUuN3zDketNbpkitLmZVmAJ+UdTiqUqhpWJGMIP5mtKQdMVRlQsSAcAUAU33Hp+VVZCw4kU8nI4q5IjB/mAb8ailV2PIGe3HAFIZDGqsemB6Vl3saRatKWVcPCCB79M1sKPLwCOOuar3UZlvbcxKGdzsBP+6wP6H9KmSuiouzKJ2pFDN13sqsg4yc4zill124adI7eyg8rzMKfmLZJyckfKD6jn60ptRPZwTFCZYARjAJ2d/0HSq6an9hufN4lMSArFkqSR1PsB+tZN6GltSO61S11OOSKSzWEynKSKxLBsAc+oP6fSonJn0uF5ARI0eVJOQwxjP5riqP2g3Uvl7T/AKvCICTtYHIA9fT8a0UtGg0dd+I2GWVW5LPnB47Ac8VEW3cpq1iEW0k1tbxxgLJK52bT0XJyT7g9/fFXzBdxOs0JWF04MqOwcewIA/WotKWLfIjqBJIFKt3B9v51sxxCTfvH3sZx04xWkY3VzOUmnYoJc333rlLC83cnzoSH5/2lwaUysolCWWnRswIyEZyAf7u5quGBmbcqr5Z7ZwcetQPDKZt6F8DqVNU4kXZHDFCsflsikuCePbrn1qWARws5jJC4+6T+lSggIo24IAxuycE9R7ChlC5AHHX6jtU2LT0IIEeVwDj72M1cu5oIJ9sMkkibVzGPlEmOmSOTyc4FMTZGpDDIIwMHHJqs7hJsCHa/y/Nkbgfb0GP6VnUlrYzk7leZ7g3bibd5g+VwMAqDxwPT8qDCUjQRMhkGGYDkEf3cewxVi4lWFmNtuWV18twyjKjrz6mqtutt5m+aUEDOQAPlHYn/AD3rFz7CuOZl86dZiQHIcKowWyOB/n1pm2SV1mKKybAOFJHP8jxUNyyRMksbAZO0r/snof0qY3Rhs1JAKsTvRDjd9f0qmupQ9jFcEyMG3qQAqqCox257cdDWexkkJckZJ6g8EetWLmQwbI2McoC4dQPuj0HvUwQyKoYrIu0EbVApq+w0UbORo/NaaZYXRVZQJOBzx8w6euPas5nmF3OiSO3mOWLnksfcn1zViG7SxtWh8kbyT1O4HuCf/rU+6la4VpoVEikclnKtuIxk+vfn61b1NVozOldCEgeMI+TvYDaOvahTEsLAnhTyvVvr/Or8saM0KyOcxjadgB2k9OPQVHqMCW1soS4aTL7TxgHAyePX/GlbqVfoZQGeSMgf4UqDvnHpTVPzYz/9epF4YVmWdz4FUrHfS9yY06fU/wBa7NWxXH+EN0WkSyBQfMmPQ+gArpo7gcBgwJ/ECuym1ypHHNrmZZ3AKeMc0hYEc5OelRlx1zSM2SOMmtBDpFIU4IAxVM5HQ1dkP7oZbn1qJI1PLGgZQdSZOcEHrTWRFU8jpx71dni+b5QcdPpUCQBsFzx3pAVUt9xI4Oe9ZU6st6pDFQgLLzgnH+OCK6kz21tE8gQghTyAMD0rndTuXe2+wQIpmcCebAA2KoO0bu2APx/Gs5MuKGWjgRksjrboSC2MEDJIxxzhecfUVyt6iWuo3EZw4RiAykc+hBrs7SxlbTg1kwnmMkbtG3OMbWyPxOK5mewW/u5LmGfduY5EsTqZQepHHJz6dD7VlJNo1T1G+HtNS9vHneVEjhA5fgK7Zx7HGCRz1xW9cRpHpt18qKysyNnkkgjv1NJolpa6WdivHN9pISdJdoKkZ7enXkdeOtVvEFpa2QVI5GYzSFkixgpzzwO2P1FOPuxHJNszUlQC2dRgghGDAHHzYGfzrfEc0YMbHegPJHB+nPWufgD/ANnXDiIld64ZiNuRwQf8jqK6QTkxsXCFQfvxkkfj6VdMzmWEVWjCshU9R3xTvs/yMdoIOPxpiSoygBvm6Ae/+eKfH0yCQhBOQe1aGZVaFUYSFyzngAHgVFMQjFccAYz2rRZFIyzkk4GB+tVJ42KEcbcd6h7jvoVOJLdivUZIPsBz+NVXmZrdIW2MRyeMHHv71bispY97SSrChAIy3PPQ4qk/3jGAWYn7wHJ59P8AGuapZsh7kUcStFKWZQ2BknIOAahjkMDKvmcZ5Y8gg/8A66t2nmtKQjfuN3zBx29vrT5LdY2DxRAyZOVLDB570KHUaGXiFrZYgIxkZG0+nOR/nvUsEiNF58kUKI2MMeM+mR9aUxxxZZgu91CnHIQd8e1N05TFbsCqhi+AccsecfXj24p2UWBDJAFiLOiqzyZ+U4GcE859qHdSoIJX6VZZ1kLADJweoqmyMp3BWI6YINKVlqijC8xpo4wGO8427eF+hFMLwxIVbcxMeNo4XPr71DbzLFKDIu9MYK+3f6VOLqIyRymMbwQoGOMCnc6LWJBsB2SRDeyk7sEHPsR+HWqsxkWCOEuWReV6cZ6/yrQeAX/MJk8xRhkAH+OarapGsc4GWJK5cFcDP4cH60NaAtygWKuMYJGfpU7jbIQR04qvj5ucg1ICWYE8tUFtHbeHln/sy327liLO+4ttDc9P0rroXIUGSIKSMn5s4471yOiSvbWdsjq4AUNyuVGeQcfjXSR3SyRkpuc+hBG73+la048l2up57upvQtR3CuxCMpOemKmVkJOAVbue1YheWOTD4y3RgMfrUsd5PG3AWTiuhM0saZbGdwzn9aarYDEYzVaO4ST5CCh9m4qTayk8E8Zp3Ae7555yetJMEARAcDvn1qPeM4zzngGoLmVlj/d5LlvuA84wfyGabYWFlureBlaZiAGGyMDcxOfvEfyFctqut+SgjRI1Mr/vWVcFhnjOOuBxjpWtqXlxwqEjLSySbjLx94A/0zj0xXPJpTa1qV1CWI8i3Z944Af+EHPbP8qxm30NI26l2O9itrUShxvfmP5BgkjjOckgemBVGW+gEaxTNI6RqVQcKevXnp9QMnp0qhPJ5EMWYw+Ixy3HAOCR75GM+mKzZck+ZuLBj1PUH0NZSmaRibkepxoySu5Zd65xwUweMEcj1/GqtxcM1y8qytwuwHOSc+/qf61lhjyQT0xj+lb9lplkdHS6lmd7l5crGjDbHGoBLP7nkAVKbkU0kLEc2MtsMKbgnKk9GJyPoQR+tdhsQwq7KuWUZGOmQO9cJaFr6WGJiMuwU13qxMF2vztAGfX/ADzW9MxqFZkZ2iUO7EDowHH49as+WoGCeF4Ip6RqucZ/DmpTC3UDg9q0MyucMCQMAdOecVJaqpkw647dOM+/+FPK4kyeg71n3FyDEs6SRKnJQk88d/8AA1E3ZCZNfvaRTST3V5bzeWOIQO54xjufrWTc6nbw7EiEBiLCV40QtknjDMSCf/r1HLdvHGrwqiwsSp3KBu6ZLHqR05+tZsiH7iyQOmS3ytz0564Nc6j3Gorqaj6hH5O9bVQrspUn5ADnoAOo+vaq7XUkwnMYUgngRIGzjqR3xWO8cjYkSORYiSFd8gH8emaiMxjh8thn5s5zyPb0o5Ui1BGiL0zZSKIGQ8qwckjjvVe8mZpo3hlkyFyWdsknHJwBwKjNzJOwbzW3hCoC8EAfT8KcZCTJukGAgJ2qSDznHNNIajqSf2pN5JjdnVQQXJGWJ/zir9trB811hl2wooUM52s5/vdD1447VkrbzXCedtAWR9gY/KATzj2qKSUWgMUDq5z80qE4bHp045qWh8iZlliM4PXrTA545qd0RAUUqxxkkc4qAqQcHrSZui1bTRJMvmbzHnko2Gx7E9DU2pGI3R8iYSwgALIM/MMeh5B9R65qpBD5jjccAnr1q5cwxI5XcEAONy/MtUr2JbSZSYHJNOTGeOTTmhlQ5ZGw3QgcH8ajAIOKkq6a0OxWFI4Y8lwAi5IBOOPTFamnhig/fxhOgJzkn06Vzk9xchdrDenUNggcfUVH9rlm5VWGM7QhO0epFdFzm5TtYpZXk2q0ZbGSpAGaaz4kHmou8DGB3rFtrpy0MEyYkcYHzj8yPSr8QkBwN6nsPWmrE8qRdfcEyV2qOmOKejlFDK5VvQEjNUyzQ5A2nseeR+tWLaSRyF+QYz1Hp61Qy005KKsy5XtmmBEZSFXBYclsYPHeq1zdW9qmbhwqbuFJ61Ql1yxe1cRzTJKwzE8YXgjvg8Ecd6TkkHK2Wb2Oae4hEcLPsRmJB4GcAc+v0rI04AwahA7NDJd3kVsxAyUXaST+tZE+s6xDdSWjXMcxU4V2UDHIbgDj+ECr1qf9Hj2yiW7eZ5puMYJQqFUe3FZc6k7o05GtGZFysiWcBJ3SxM8TAjgcKcfmGqkyojnazDd0VlyMehPf61txQsokgeWJtzgsRk4Y9eD/AJzxWlb+HzLbAiUgvGGIMY2oScDI/wDr1PLfYu9jklSMiRzuwi5Kd/z9Peu08Pacl/cBpbbyoY4BC6YIDMVHJ9fvE47YHtWBNpM0cg8lAXZSwZeQcdRg/Q+tdh4f1ENpUdtGuL6NsujZ/iOdx9uent6c1UI2epM3pocdoYW2uoTclkHnCENnGGB5PPbOAfqa9IEL7i2MDNcn5M0dzeSxtaC1VmBkbDFupICjPUkntU8c5YmSTXL1FUAkRoka/kd1XDRWInqzpViO7J7mpJNqL1/Csv8At62tIV/0g3IPzZZwT+BCj9asLfw38C3FuxZDwQeCp9CK0uZtFbU7yO1spAzDfICAp9OhJ/Cud+0pHF5iwI+75RPIGIBx0A6e9O8VjbfQShwd8eGQH7pH+OaxhMzRhGYlVB2gngZrObu7FpaGhCYiImltw0ZcksHAZsZ4xyAv4VRuCWDMoILkliOmD9KZGyPIu9yq/wATAZIH071DKwL4DHaem7/Cs2NLUVXdgELsVDcLu4z/ACqZ0jRcLMm9jjBXIA9Sen5VXadz8vy4xgcDpTS3IB60rsuxMEBjO2dU2kYXb94+vH8qVJY4tzW6zyzpyJBwqcdSOfwzioFKrLmQPsORhTtJ+ntTWuZFEqQs0UMn3o1ckH6+tSx2Jvth2l/NnNzk/vA4wAevbvzVUjf8xNRjLHHt3oHB+bj0JyM/pSuO1hqwSszA9uvzYzTzG0QXcq4YA89QPSnbELxq5JIb+727fWkdCsJzl0z0znH41VirjlZ2lUY2pkBgOn1/SrLeatyZJWDx7G2uo+/7H86gtt8c3nKFZF6s3Iwe1RJK4glK/IOOF4Gf8+lVGStYTjdXTCSaS4BQsW53cnvTEwrKN46gUJghi5x9B1psSMZEyCMkdfr71DfVhFW0Ogjbav2eSUGNWycsdp9D7Uy/L2SBCwPmDPHYdquXFrL/AKuYtldwYSArtHUA59c1hYMglZ2PyKSAx9O1XfQzik9S/plwYrqK5nkCJn/WOck464z144x71buPE06Rjy7NFMjYWR5A34cdDXLmVztJCggdQoBP1PerFiZZJ1iSMSBnXOexzgHP6VCm9kaOC3OhfVLyS23PNHE23JCR5JHOTzyPqKQ+Ipbl1+zxSQQoMsxbAP8AvNkAjjOPbvVSHT53vJ7eWdrezBbzJ3TgAA8bc/N9Bkmq90lvdWcK2u8iPfnbnYDuOCQckEjB68UPntuEeVPUk1DWpru8kkc2qgDASNd6cEdsAEcZ5ps92sscgjuY5gwAEbkoEweMA9RyxAGAM81l/ZpT9xS4/vL0/OpVs7xWWRImVvvKdwz9ahtLVl6JFuW2hYrL9qt43fOI3LZBH97AOKc5uLR4zIYnjc7FZMEZ4J5wG78Zqg8d1JcZaOQyuf4U6k/SrX+kXEzSxwowbBUBfXgYHXHHaklskVzPcsTJNuiVLeSUysf9pTjrx1HTk1opfXJjNu9vLE0arjfJgFT0JK9evTvVWK1WeFdy7cgjAZgOvTGenXOarmSwt97NBJcnbhglyYynrgBdpHTjtgc1ta2ple+hqSQajcLEpiABOE6qGGQB1IJ9+O5pwvdRtIZleLyk/wBZLIxGVGeehyfT2rCuLwXCkRrP5IYeWkku5l9eQB7VfubqeaJoJkRUdBwzfMR2PXqPSmmu4n6DI7uYLLFFMh/dlSQeGYjnH4U2KwvrhWbEZ2AYLSgbieAuemTzj6VWhtpFmjWMKMk4cHnGByR+Hr3q0xkhdULoueqY3M3p1/wFZJNXZTkV5YzsjhDKbp5doAOD6YPPGD6ir+l3d7pK38LgBmQbfnB2tuAzxnnBP5VBPc2kl0sdrbQyzyscsyg8n+vvU1y4NtFZSRJwSqtFAAzeztnJA7fSphV93nenqQ5JrVFaa6ind2mulYjoZMn2xmpIS8Tll2LuGNzjIxVVrVEysUkckg+UhF6Hvktxj6UFnFuZ5Zi+ThQCfm/z6/lWvPpqJpDC54znFTRbZEdHeQxJ8xCNwTnGeR6GqzSGfcVXaCeAT19akjngtcfNvYrkhlB/Ln+dJ6otLuBRG3Oj7FHKox3H6cCn+QQwDsiA/eJOSv4Dmq5njkIWNGGOSSasBX2RHy4yoBOWzg+xxQ2kTJ23GtI0S7FZWDDGSg4Ht6Uph3WyGM/NISNueuKiaVJSP3apjgojHJ9xkYqaF7ZJFcOwBBADDO0++B+uKnnXUXNboVG6qApDKuDj19ajeRpFCnoOg9PzrRitZ4zt+Ut1Lf8A6/rSJbxmLqG55A4Gfxqb32D2iKKzxxhiq7iem4YP50qtDcyKhmEBc4ZpMhB9cZ/PFUicnAOT7U9HK9gc+oq0zXlsXdphLRLMjMjHBVxg/Q+ntTXXFsCQNzPx9KaqK0TzBQi7guM5J9ePQUjzq/lIOicc/WrVkri1EVQRISwBXDDPU/SkUgyghioJ9eRTVco4K4zjHIzT48b04Xg5qbJj1VzUsZ/MimhIbe2BHIrEBfqO/H40zU0e2txEr+YZHxu/vcDP602OfyCI84BAkIHGfoenSqt1eG4kwx3ZwFLH7oBznio5m/QhLW5TJy+CSQDjIqd5wwQRxhAgGPUEd6jM25Au1QQOD3qMHHUUGpOXdwpE0rNjGGPQegOabMvzB1IxIOQCeD3H9ajViDkYPsRmpF3MuwYyfXjH40CLli4ZhHLNEjE8NJJjaPTngD9amE0ZvNkDRmIkK85ZsfUA8n8uayD8pweKtwSwwoufMZmOAVHT6etY1ItkSj1NCCeS3nla3uJomkIVJYEP3geccZ5x1GDUM4uGuw5mmcqqsHOMg4HQ/WkkeRIi075DfcUcAjtgdquPLZOIZYWlEKrukJ/gb+7z1P54p05u6iloJSdtClJNcIdkkjNjgq3GR70yRZLlS7GNUVTtVFPbr25+pqZ7x5JmuI18tWbCkKM5HU04zia1mlnk86feMCU4IXuc9TzWkpteYJszYJFhkyw3D64H1rSguUMcjpA27I8pTyMdTx3+o6VQ8wKPNgWMHgEMAdvuM9qkmuJHiVZLmNiuMLGhH/j2BUtt7FNXHlrkk7JCqgYJRgAc+9JGQjGURSl1zvlictgHjnr+dLaJHjI3tk8lSMD+tElxNa3e6C5KyDkMMZA9M1PMpvlYt3YltLmKNTGshSI8Fl4LH/a79KlustEA9mXLfJHJvyMcAEe9U0leeUZDyTsflcAbifT3rVR1EC+c8swb5ds6fKnqQB37A0pU0nzGNV8jTRTjea3Z4pIDJEp2PtHIOPXvV7T9LF8zyrayxwRfMQz+WCenTnmrBWHyFCqPL3byjc598dfzqKeeAxxranHUAxliHUn+IMcfpWbnHmuv+Ac7rOStFWfcsXEFggIuLXz+DmRQPkJz0I60kWnxmFo3ijmg3NJGNoVlZuuT1bHpmi41C3eT52RV2jcqkBcdvf8ASrsd3HEquwRUJ5baAR7/ANO9ZTnUtoc0p1VHr/X9dDFTSYY7rc4CoRyAx2jk856/hUlpbybppYWiiZW2BA/X3HcVrpeC5k2B2JOWAEeePUZPFQrFbh2ljfcWO5o2PJ57/j71MalRpxluN15tNTMaTSpWVSzBFLEESHCj6YzULaPfRNlUDAd1OcVuXUjxRh7SKPIz5gJKkj175qkurCcbHMasfuMDuB+hFap1krqzNoVa7V42sZbSr5QiZ32hSu1kK/hnpSKkzL8hjAHAL4xj2rSvYRchnkjaO5AGDyoY/wC1z+tY0ltco37y2bnpsHA9uOK1jVbOinNSXZksM14zgmaIxqCNrHYp+hwMmmX8crS+a0LohUDPUfmKvX9xZ3U2yKNo+BvRzuXzOh246DNUo7trSLZBLIkqnv2B6j0Iqk1zm63vYaym3tQS+WLjaoP8OM5H1qCCFri6CbgmeSz8bR60+9uxcLH+5EUqkkmNiFP/AAHsc+lRpcSFTubOFxnHOPrWrb2LSdjRt9tte5jbYgw3mMPmH096LrZO6GN1QyMS2ZMlR796qssg5MciREHBaPHt/PioXQwhQW+Zs5I61Ld3uTYtagDHmOblwwwUII244GfyqmEnuFJSN3AzkgUroWY+WS+Blix4UUrREIkbzoozkJyQM9TmqS0sWtELJayj7kLkBcsw+bnv06U2KF5mIUdBkljgAe57Vq6bbQvZSP5DzSKPlRY8+Yfr2A7n8qZJbjmS7mVI05FuhBOfTPQH8zVcugubUoSJbRp8rvM/97G1M+3c/pTFhlILsuFHJzxT4A3mtKIpGVAW+6Tt/GrMpKiNZCRlQ7N+dZSbvZD1Kpt2ZHf5VCdRzmmQxCSVUVmyT1A6e9Ak8tZI1JKsevrVpIHtZhGSPP25dTjCDGcE+v8A+qn72om2T+RBDOs1z+8jRckE4Dnn5ePwJNOWfzZEaRI2djtitx8qoCOCcfnVGaeWSNd/TOeR/n1p0bsqtKWJmlyFJ647n8en5046RSJs7ak8jDYqqCVG7YP9kYGfxOTUklossBMExeXjMDrhxnrj1/Cq8oIuxHuz5aKpIPUnr+prSMunQacyCN5blxkErt2n1z3H0qklcl6bGTGEt28372MqN3H5CoZpJJHy5ycVLeAIPLQEIr5BPuoNRSLgqCCDt5/M1Cir3NEurHJlBuiPOMsMdKe3Co5KnzMnHBI5xz9ahAPAOTnoAOTW1baBK7q8/wC5VuQB8x+gx3pvlWrJqVIQ1kyhHviYPCFaRQH37eB+Herd7m7XzZPkkC5x179OOnP1q9c6N5CBkjcpt6SEKw9O9VorKC5uSAhjBUgIBjJ7DP1pKUZLmRh7SnP95F7FCbVJpmwAIxjHBOaW2nkUl1kYS7uMdPcelEdlIWbzYWGw7ZMnAHOOc+9WotPhVjuaQquCwztHPT3P4UnCKVi5ezjGyKtzdqczAFZnHP1z/OrumW73bqLiG4dNwChG2r3JPPHX9aebi3t5GW3gJk+8GK4xj+6Ov45roxcxFLfalyCcbsYOSR3OPfPpxWdWXJFcq3OavVcIJRjuBESrgTg8YwRjPpg0nlROEVpSBkZI/izx14x0pLtlKgxsW2KQ29RjAPf3qi0kXlSBrghm6FSMqT7VzRh7vmcEYNouSG1iXAdtqjGN23cfxwDWXOml3MzRTRKHGcvGR83tnPBrOntZTOzrepPLECRG2dwH48H8Cafb2cKSeZcMHAOQidx75rpUFFXbOyFFQXNzamnDbxrblbCcSID0805U/Qn9KgYTROCwBYjnCmpftpK7kWM7jnbjnOOcY71F9pG3MiE+hLlcVnLVaoj3uq/zMKKOMRmYs5wTjbHlR+Jp32SO5LSLeQIvH+sJBqG5unlckIeVwC5yQM/1quDuznHAroin1PSipbstyaVdhv3apMMZHlOCSPp1qoUeJ8OrIw/hcEH8q0Le+SW3Nteyz+WBiFkVTs9eoz6Y5qzqCXMunDy75b60jwQCoEkffnueuKuytdFczTsyh58wUMxMkfRgzH1qKRy6hnHXGB04qLIZh8opQQ7orNgE8n0FTbqVYnLNL5akqiOc4PQe59qswWsbXCMiGSIHJlb7oI657VLFYj7Uskik2wbKxr87Ovb2x71a+1lufNAOeI4z8wHXn0z6DFaKN9zOTb2I57tJFYGdYY+0QJ598Dr+NZUyv53EXlpgE5BUY9efWt1L66n1a4hQMQo2AgYVDt+8T9alkhRlVLxHZy2913t5YI6Ek8ng9KJPQj2nJ0MIXUJRVuVaUZOUVtgHbqOpx6Y/GrlvNbXsxRNOREXlpTIwCLjHOP8A6+TTY1E11IpxGrKwg2AZfHbnpkZ/Gq8twv2MxwL5cZbAGeSfUn1xR5mt7ima3guCbVWZw2VkcDK/QdB+ppI5GUIrKx3HngcrnoM9+TzVZI22gLznritCBYlQsEO/bsJJOPr7f1o1B6ETW/nGWZvliTrk9f8AIFJDGkzhm+UcLj29B6AAUsrho/s0JJ53tyfmPZR9OtSfZLmDTbm6MErRgiPzViYopPXLYwDj+dIW5VyZZWkH3pHzj8c8VMEL7uDk+pxj/PNV8MbdZPlKMSAevTGa1Qrx2Y3Pk4BVn7d8H1H8qdgk7FVLM3YWMP8AvW5BJ4AGariEM/kSNskjY8le3fj9fzqZ4Nm3YWEr8NG0fMfH+Bq3Na/Y41LwJlj/AKwltxAHOckjpUrfUhSs7N7jLK2hVyRMVaM4O3kkHGfw/wA96t3GpSeZJLGSqBwqAEj5ccAH6/zpq2limteVA7vbGLdlxjkjP4gHjP8AhRqxMZiRBt2ZTI5Y45PP/wBaokk52OeXLOqk9bou2uoI8xzb+WjKS53gjIPp36mpJ1jkufNWU7nYAQ+UPLHIyeec4rl5pnaYB9wwDtFTRX5gkV0ZmKjgMM4PaplRXM5RdglhLPmg7G80NvHJIl44hWR94kGAQccDBByKVLdJpAI440Riwidyx3AHPtyB3z3rMh1C2mnSZo2R8t5mcsHyfTqOP6VcignkR1MiKu7KI7kYHHbp6U78q1E7wilLclgsRHJ5r2xcL8+Ey3I5Ge5+nXirKvIWLBIoi2c7wR1HPHBz1602MBI2kCNwckbt4b1POCP5Uw3Al82FopCmDnkAr35z07cVzOUpO7OZuU3diC7ikRR8sTA85frjscj/APXVGRrTzxHHHG7N1ARsN/wJjnH8qe1kHxNbzOVGQN/Jx3/D86zpYEZxmJg5wcSEZI+lXFJm1OEb6MsRtFbxl/nG5iSscob/AOvjjr1qzLJbzZG19xUZUn5hx2P/AOr9apvcwrBH8kkckfOA+APTjnOOtUDI4lY+d8pGSSuNwrXk8zf2XN1NRmSIDb85HOEAOB0HfP8Ak0x2mI3QyRsQcHoTVckyW5aI8ryVJ3fkarBt6Kkhcheh4yM9vpScLbgqdivHFLM21FycdzgD3JqNeJF9D39quPdmCKWCI71Y4MmOM98f5zxVR5XlO52yQMDitTqi2/QcRtOBmlimkgl8yNyjDgEU4xMoJ42+pPtUJOWFAxxIeXcQBk54qW3iTeN5O08E9MelVxyeBmp1IC5zmqQMuzwR28UPl30M5OSyxFjsOO/HP4e9FvEUm3So8mQGRUIKvns3ccVXDhQVb8qlW5dVVAMoD0K5/nVE6k9zcSykKjvACo2oMfgCe3H/ANeqttdXMcx8ouzngocnP1qxvR1O7lT1GKtY+0WTQwsEk3blUdXGOVz607C0RXKyyXMa3FiY2b7ssQIKnsfTA9Kq36ol4XjZSrgONo4yTyR+ING24iiZFkdIyM7ckA0+4h2WlsRksFdTgdMN/wDXpPVDQluXeRFSIvKwwAqlmJ68Ad637Hwh4i1eVI7LRb+XI5zCUj/NsDNc3bzSw3ULxzyQPE4cTR53IQc7hjnI/pXt1/o/jjUIkurrx1psOnMAwlhZoEkUjgnAGcjn71S3pYGjnIfg74gFrJcX0+nWEcKNI4lkZ2UKCTnYMDgetXPhDPeWviaazSaVbZrAtNFuIR5Ww6kr0yFVh9K29d1DQNC8BS6B/wAJGtzNJCXlFsDNJc7mLEE7sIGJAJJ+6DWT8Lgw1iwuHPz3731xuI/hjjSMfhueT8qSWgoo5P4iSRn4iarHsVYoZhGERAigFV7D8KohImu7YyhTEZBuB6EDkj6V6h4l8NeAotZ1TX9e1eW6u5bhmNhBdIjbyABGEX5yRgckgDqa8rvlLWvzAZx6/wAqaZFRWaL+j33hrUdYtF8RG7t7Z41U3UDALG4x1BDEx9vUfTOPQBpPgqa9it9Mttd1h7hmRGhJjgcjqfMZVGB6g4FeXeFfC2q+Mb+bTNLjjDKvmSXE2RHCB/eIBxnsO+PTJHrVrqLafp174a+H8kGsa1bIst5cXMwUSHoTDGSFfHTqAMjliaiS5nqEqak0mibxDJ4L8I2Fvpk2i2kk6neluNrMM8lnkb5sE9O59MV4/rs1jPqZlhRbaN5Nwh2s20HoASBniu/0h/C/j7V5NM8U6O2keKHJUT2xaJpZAORhs4cY+64IOOD2rzjxNpUuieIL3RZJA72k3l/KOH54bHYkEHHPU0JWdxez99SuUbiSBXEWCy92YYwfXg/hTrO0kuZfKaOOLOTvKjAUdSSTUtxbxQSzRXCuNqcMCCQc/wCelXLU28UJtwFePO5WZeZcfXkY/wAafNfVCdT3fdFistLRGke583y8ZUDIx6/L+FIus2yRmJoztViVVW3DkcsBgDJ4H04pkyyo6tYsWbJLCM4J45zVAXwmyLiBJN38SfI/5jr+NS6SluyI0lU1k2zZj1GzlUIqyR7jjdnB+p5qW4jikV1YhmQfdOR6c54PvmsJba2nB+z3G1jz5c/BP0boamhuJLWJobuLbyGRnXqR796iVCy0Ilh+XWDLPkiSVcNG6HB3ux3buB8x4/DjvVOdZbeQ7yhz91mIORVnz49kgQZJG4ruI/Q8Z/z1qq0kMgUoN79MYAJPr7iiKfUqCd9SFCsyeXLKkadSxGccUy4QphN8bkdSrblP0P5GkMblmKZCHGSe1QvgKE4yB1/SqOiK10Eid4ZdyMUcc1cytwvyMkT5yysOD7g/0qnvYDDHcD0J5xUsfl7fmHH+yeR+dWVLuRvOzQiAKAobcfUmo4lDP8zBVHXPenwx+YxBBY4J60112y7VAx7HNBSstEPfLjOeTz1/z2qMZyuOMUsuPMIHQHFIozKFAJycYHemMtW1nJcbimFiQZklbhV+p9+wqJQ7vhVO70FOWRoWKyKSM4aMnGCOn0NTIhjALMOud2PzpojUjCH5yFyw6HGfwp8Sn5zJKIggOX2k89gPrUokC5I2nOcAdSeoql5jzsozyD8qim9BrUmDorcJgZ4JPP408zHI2t82c5/lVNmYj2qeH7pIGVXrjrj/ABoTBo01naRi544IbB7nAz+OfwqhLK66fBGjMuHkyAfQjr9KmVy0ROCgYhR78E/0FT3cazWlsT8rncASMc8ZBqnqStDKZmK7v4scfWva7y10LxBoOiabH4p0+CHThHv/AHiuZcIF5XcMdDj6146tnIHCujKzfdyMZ/8ArVoyQvp9nGBgs/zMB1JGMfhU2YSatY9BWx+HGi6ZJ51vrGvI0jF7uGGQRBl6gMuxRjJ9aveBNV03VfHGmLpds1lptlpVxDDFcTCRkj3F2ldhwMs+Pw61PpeqaZ4B8DWWttbXNzPrjh3hWUIACMgYORwp9OcntUPw81eb4jLrGieI7WzmtltlEU0FusEkKs23aCuOOAf+A85pMo4HxJf6frHjbVL/AEoO9vcXBkiaXGW6AsPQE8gehGalht7U3Vs2pXtzb2jN++lgj8xo1xjIXvzj9eD0rm7ULDdSKDuWPcuQcbsHr7ZxmtVdWTyPLZF+Xg571aStqZyWp6tB4W02HwiltD8U1tvD75X9zHbwrJu6h2DBmY993NZnhWy8CeEvFEWoWHi+61i7SOSMW9rYNKCGGD/q1PtivMHXTcSS/ZF39fvHn8q9t17U4vhF4Y0nSdDs7dL6+VpLm4lBYsVC7mIyCfmbAycACocTRSW5xerReDbnxZczSP43l1W8uGnWGKxRXyWyNob58DjHpgVu6n4U8I2tjLeand3VrqtwrTBtTvo/tUchzy6qcEnuDk89quaX4suPHPgbxIuoRRCayi3+fDI0YxsZgAAcg/JyAcEHkV5I0WlXWCYvJcj+BiP501G5MrPdDNRR9L1G6tLvyZJoGwrwyB42zghgcnIIOcj1rJeVpHDbwpByuGPHOeO9adzpCRxNLDIXVQTjbz046dqylVTkD5snrS5WtAgorVGpDevLBGsmN/O0rxuI6jHY88fyqtIIY58+WsiljkEnJ56g8cVVnADpgjIUcjinsRcsHEoWYfeDHAY+o/woBQS1Q+5jSOTfHhozkj5uR9aktr+ZIzE22aMjBjcZyPp/hTGjnEZTyfNLcl1wSPoR2qrIcMeCpHVTwRRdoa1Wppq9tOv7t/KfJIjkYmMn+n45qncRz2w2MhjUvvTByM+zVAZTIPnPzf3/AF+v+NW4bt4F2E70bgoQGH5HijRi5bCQsu5op1cY5ABx+VQzQOJQVUMjfdbsf8DVmVYrpP3TmM9lLZXPt3H0psRmtGAnjJjc/UH6H1oUe4LTUpjvuOMfoaaOScnp2q1cwvC+8tvjb7r46+x96ryLlzgjPtzUPR2KTHh0gi2j5nY/N7D0qNHbDMSSe5qPqetP2sQFUdeoFFh8qGjOc1JC7xzb04K9/SnQxMZNr/KPUnvUgjMXmIzDa5xhWBNNbg30IY1MrF+2e/AqUsd3HC9eOeKTAAEajcR1PpTZ8Im0fePX/D/PrVbAMWQiZGGDtPyg9KmtV/eg4xtGD9eg/nUKIWVzjsAPqTirkdswDpGGdhIqtjkcck56elCBlBgVJX0ODVsoyxooUn+99atGzht5mlnfcSxZVU8DnPNQyXCyk4x5XQAjBPena24r3IYnLK8TDhjnJ7EZrRnYS6dE4wpEu0KOc9c8/rWeikMZOOASf8/jWnFGBZ8kMqzK43emMEmnETKv2+7hJiR9oPQEZ2+4zU80gubdZMjehKncckggfMfxrNMtxLLJKGZVdixboB+NPgiNxcDLs3dmb0FF76A0ehaf4r0C/wDC+l+HPFFrM0Vsmbe5h3ZHJA5X5gQOOhBGKsReNfDXhTRbyw8G21yb28XD3k+75eMZy3JIBbAAAyc81wK2EksplVdgzlBj/PNWIrdIyszLwP72M5H9KOS4OWliFrU29p8/EkiHAx90f/qrOiRxE75JUe/er17cNc3cZ6gtgD9KqRxNtCFSnAJb1/zilLfQm9kQl5SCvUEYINenXXjzQvGPhq1tfGGl6jJfWClEvNMlVXkBUAkh+BnC56jIyMdK8zZWVtpGP7uR1qxEhjjAJIZiRtA5A9c0im9D07xx8Q9P1bwlFZ+H3Fqb0hdQikhKzbQoAG4Da2cYJznGMcE48oLsD0ateLTjG3nSlTG6/uXQ5jc9Pz9v0pZ4VnhRFjSM9CSQoBJx9TU83QzdZXsVrXUpIcDPFWmubSdiJoI9x/iX5W/MVBc6MtvC0n2gMwOPLIwT+I4rPMcqNxznpjv9KtTuOMoS1iy7faczqJ7dmdOBhuv596zQpAPX39q1LC8H+pl6dAM9O9W7mxinj3leT0YdadlLVDU7OzMSMuysqAbgN3A5I9c06STlUmzIhAKv/EP8+lIYSgyGBCnBB4oJVo+edvzDJ7d/yqSxjwtt3R4kT+8vUfUdqdEvmwOvG5fmX39RUattfdG345watRyksN6xEtwTgg/pRYHexXVHLEqRjrnParNvO7t5AJ+Y/Mc5Uj1waZIyGRg4dTuzkYYZp8aCPIU5d8Y4xxQkxepb3hY9rsGBH3T/APWqJ5guBEq4ppjhjb9/KWb+6pwKY5tAchMfUk/1qxWIobWR1ZooJJQDgsF4BqV4nICNIq9STtwvsB/nvVmTVWFu6Qo0ULNgndljn07D/CqbB5lY7DwN5X720evt3/OsrXFFylq9BYlZFlJUsFXg5wMnv/8AWpCJrliQrMVAyacYSxJiEhyxwu3GB29u9T29tPBLHM0gQckEck1SVir2IPss8aDam7PdOR9ahWzuJGHybRjqxxWlcXkag/OGOe+B/LoapNeFiFijGTxyeKenUIuTWxPDZRxR/vfnycnnAJqUXsIRkjVdq9Npx/k1RdWbJkYNhuCOh+g71GzKzgAcAHp6DnFO9tgtfcJ9zfvC7HJI+Y847VHH24B96cIzICWOMeg4AxViOOONRvbB9AM1NrlXsSIo8okgt6YGMVNNk2EsS9Qqsee27mqlxc/KEiO1R+Z/wqW2UiynUAEmFR9BvOaq/Qm3UqpaOXwRkjsOSfpXWaLoKvp8t2t3aGZFLG1Jcy4DBQMBSuSSMDPJrItj5EKx4Cykck8kD+ntW7bXsVno72YjQebMssk245YKCAmOmMsT9celVGNiZSuO8Qafc6TFcPNMjeUYUARCMmTzMHJ6EeXn3DA8dKwLiyvvsOm3PmRzjUDKsMcbHduRgG3ZAA5bOckYyc0X2uyX2mCw+zokYKESK5y+wsFJGOuGI646cVJB4mng02xsYIYo1tBMryEhzKkrKzKQykDlRyO2R3qXJspJE6eGr1m1LzpbdTp1y9vOyMX+ZCA2OAMc8ZI3YIHQ0y60mS3v7iyh3M0MkiGaONnBVHKl9o7cD6ZFT3vima51m81oxQ+ZfqTsVtojO8MNpA7EEcg5Dc5PNYs2oSz6o975hWRpWmXEm4oWJJwfxPapsS43dy6PDN5JNbxQSwytNA1x8wZMbQSyng/MAAf+BAdTim3GlXVvrMWmj97LKIzbPKvliXeoK4Df72Op5qRNZkhks764jjkeKFIVhJI83buw7+rDdkHnBANVvtdzqOuy3lqI7aWcklkJxGNuCQeoOO4554xmjYttWuzoY9Bmh+wW/wBstWkvXk2owfyi6xq6/OOuQ4XI4J9eDTE063/tie2lt4pJo443Cxbo40O0Ejk8kFip9welZB1y7gnsoptpjsztVCzJ5qbYwFcA9MRqfr2qve6vLdX0lwHaMONqIsjNsBGGwT0yeTgDJPQUrIwlSi1oSTXEYlkjKyiMudqSjDkHJGeOeuetCXLyrI0zbhgKpAzt9F61XhiknZ2FyWlUZJYktjpjNRiOdGG7aMLnGeg98dalJbi5Ykk0blUliAXH8a9CR15qzZ3wkAWQ89CajjG6NZZMuXJBIPfpVSSP7PKrLu2MOpFVGVmVFrZl68tVEUkvJyAH2n7vP3sd6r20TGdVim8piNudu8N/n3qxaXIPOcjoQf5VO1pFNKHDsqj7oBO5B3AHQ/j/APXrRpPVF36Miumu7GPMiQPuJAxEmDjseKpBBcLuSNUlPUR/Ki+5z39hVqWDcfLeWUIhJUsyjqevHc/WnyOvleXGyrjGCe9Fu4LQheFYkjd+ZwABs4GfXHrVd5CmTnD4/L2p0k6RZIYu/cseKqSNI53HPNS2ug0MZsk88+tKqFuhU+tORQF3Fhz60jFNowB9cc1Fy7k8bQQhXkRpHJzjB2ipv7QKHENsqHvgfzqBlYLGGyqhc8+5pvytIqDAQH7wqhWTJmv7tkJ3DB6j2/wqCSeWVQrSMcdAOlRjJySSQegPU/WnbeMAqfcdKAskOgkkUFFRG78oCfwzTgmCSWySeFyP17UvKwtFsJLsDn2HalaP7NGVx8+TnHQe2aLAIxbLBTlT3NNkVVLbSc9APUd/5ULKBE8arkMBkntikdiEx1UKDj0P+NACxOSxKHLcMD/ntzVtYFzlSjv1CKew5/HiqdtGjSofMwCeQw7UjSM1w5z8xbOR/SmmDQBPNn2+rVrWsccMAd35K4IPs27/ABqumJEU7ZfmILGMAjd2JHr+lNklg8iUCQsSgRRtxTWmonqXNM17VNHaafT7x4GuAPN27SGAOQCCD0rSfxh4iaCyA1WceSD5RwmU424ztzwOK5+JgSoGdx9QMAf4VPFmdYQUKOCQrIOCo74poTNifxZqpsjbXN07wEKSu1RyMYxgcHKryPQdaonxj4kmuZJm1m4aSTbvLBTuAbcAflxwTmsu5V4y0cjZYY+brketQqAD7VLKWx0sPjDX1jaJ9RcRABSVSMYHGBjbg/dH5VJZ6/q815ZxDVHaK0O6EbV+TYpAONvoSOc1gMxBEefu4B9yBk5/Fv0p9pctBcLIo2qAdw9Qf/109CXe2hqy+NPEck0+dVmVTIGJ2pksrIQc46gxJ+XuafbeNPFKTRyW+oSO0YwB5UYVQeOyjuaxLiIB5c5x5p6HqCNw/rVZGMLq8TMrA5DA4IPapHujdXxT4is/Jtl1SaM2yGJFCJhBjbgcY6YGa3rDxB4jutNFxDeu14zlHaTYoZB0H3ecfhXHpeyXEgmuHyV3MuRwWJ5P51o3l5PZxRxWaCJGXPmBssPxzxUy0Whz1eeyUdzPub2ea4lkk2rLJIzyZAUls5Ocd85p0QLL5jYzn5Dnnjv+HpTzaxXkTXUVwS27c5mGBuxnGfU444piSLIgkJO9AAhYYGSf096Um2tCnK60E+SFCXGWUkhC2cep+hqnOZPUmPHAPar04aTaz5HyDAHoOv6mqUwCyFcDj2z2poqG5EjbJPkJII6f0q/HdbGKSdRwfb61WeKPcNhKZAIz3pZjloyuWfGcgdf61Sdi73L322NRk/N3BPrVGeWSd+FK9wMVW+bPOfxFO8wp6hscn603JtD5bbD28lcZLdDkgZJNSqBJAoXJ5yd3Sq6sD1/KnBgoBDDIyMEdP/11nYTQ1gcD5gQBgYppUnrTuAGOfoP6UnmLvPy8fWgpXJIySvJz/wDrphOWGeaKKsYp5HPenHhuOwGKKKAHLwjEdduc/hTZGYNGQxyQOc0UUCQpADvgY5/xqOXgHH90fzoooY0Ptu//AFz/AMaZCAW59aKKANBABZuwHIXAPtWevp2oopsSJ7f7/wCBq3aE/an56W/HtwKKKaEypdklpMnPOOfoKbH91aKKXUfQlf8A4/Zv+ujfzqMfeX6GiigRPISbBCTk/Lz+dV8/KKKKbAE+63+8R+laMwDo4Ybh5WeeecDmiis5bGdUisP+Pu193IP0xUcfKwZ55zRRURIXxf15jbcZMhPJ2qcmlB/eTj/PeiiqW5XVlRifLK54DHA9OlMwNh+tFFNGyJG6j6CmS/w/T+tFFUxoYfuilXnOfSiipADxmm0UUDP/2Q=="

/***/ }),
/* 25 */
/*!*************************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/videos/酷爱电影的庞波小姐.mp4 ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/static/videos/酷爱电影的庞波小姐.mp4";

/***/ }),
/* 26 */
/*!***************************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/posters/酷爱电影的庞波小姐.jpeg ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAF/AQ4DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD32SRIoy8jKiKMlmOAKojXdJKlhqlkVBwT9oTAPp1qLxJbJeeHb63eTyw8ZAkxnY2eD+Bwa8CTR08zU7Ka3WKO4uxhNo+XMTKR/wABYMPwFb0KUars3Yxq1fZpu3S59Cf21pe5l/tKz3L94eemR9eab/buk7S39qWW0HBP2hMZxn19K+b4w8v2y88pTcLaRSyKFHMsU+JB/wACwT/wMU1CloL+GONJYZS+zKjnECsp+u0mt/qa7kqvfofSw1fTTMsIv7UytjannLk55GBmlk1TT4pBHJfWyOcEK0ygnPTjNeWaLq9nbpY6a2itLO6Wyi9AiwhaKPHX5uPatZ76C11EW7aabiR40YTBIzjkgD5ueOtYyo27lqpdPY76bUbG3YLNeW8ZPZ5VX+ZqR7q3iCmSeNN3A3OBmvPNV1O20nU4/tWnPe+dAdu1Yzt2uTzv9c9q1daAkj0wlRjzhwR0ytZyp2Vx8/vWOsF/Zk4F1B/38H+NBvrQDJuoAP8AroP8a86aJBcHCKMKnbvlqSeJfsbfKOU9Ky5tbDcmj0B9a0uNtsmpWatjOGnQH+dN/t3SP+gpZf8AgQn+NfOviiNP7b+4v/HvF/CP7tY3lp/cT/vkV6cMApRUubc5p4pxk1Y+o/7d0j/oKWX/AIEJ/jR/bukf9BSy/wDAhP8AGvlzy0/uJ/3yKPLT+4n/AHyKr+zl/N+BP1x9j6j/ALd0j/oKWX/gQn+NH9u6R/0FLL/wIT/Gvlzy0/uJ/wB8ijy0/uJ/3yKP7OX834B9cfY+o/7d0j/oKWX/AIEJ/jR/bukf9BSy/wDAhP8AGvnrTPBmqakqyC1jt4G5EtwNgI9hjJ/AV09n8OdLjw17eyzt3W3hVF/Nsn9BXnYmtgcO+WdbXstX+H+Z1UViKyvCnoevf27pH/QUsv8AwIT/ABo/t3SP+gpZf+BCf415vD4L8KxH5tNkl/66Sk/yIq0PCvhQjH9jQL9d/wD8VXnvNcDeycvuX+Z1LB4rrFff/wAA77+3dI/6Cll/4EJ/jR/bukY/5Clj/wCBCf415lqHgnwptG2GaGR87FtmLk/8BIP9BXM3Pw/nNyv2bZJa9WE4VJCB/DgEgk9OoruoVsLXjzRk16r9b2OKvOdB8s0vv/pnrdv4vS/ug1nCj6eshRrh3IZ8d0UD7vuSM9hjBrafWtLjcpJqNojrwVadQR+Ga820Nhb6RJJOfLVZHZtwxgDHb8K4zxdHHfTQ6ssKL5xMUg2j7y/dJ9ypH5GqoQVSqoSdr/mckMZO7uj3r+3dI/6Cll/4EJ/jR/bukf8AQUsv/AhP8a+YPKjx9xP++RR5Uf8AcT/vkV6X9mx/m/A0+uPsfUH9uaT/ANBSy/8AAhP8aT+3NJ/6Cll/4EJ/jXzCIo8fcT/vkUeUn/PNP++RR/Zq/m/APrj7H0//AG5pP/QUsv8AwIT/ABqWPUrGX/V3lu/+7Kp/rXznouim/uFxEm3PXaK9Y8P6HHCipDEAR3wOfeoqYGFNXcjow85Vna1kd2ssb/cdW+hzSs6oMswUepOKhgt47SHjA4+Y1zGvap5mUVsIK5KVF1Z8sdjrhTU5Oz0Oo+22u8J9ph3E4A8wZJ9Knrxy01EzeLNKiXBH2uPJx717GOlXi8N9Xkle90ZySTaRmeIbhbTw/fXDqWWOItgd/avIZ5l1GZb1cRSfaAZYevO3G4H3GCfdTXq3i8Z8J6l/1xP8xXkljEwvAoGRIMN7EdP54+hNclOapV1P5Gdek6tCUVv/AMD+vuM62jCapqUSAESQrOfY79jfnhCfpWVLZG2trqIBmls5TwB1RVG0j1zGcfhV7UTJp2p3sxDF7WGKQjoXTzvmH4qTVqK5trfWboXeHspEwXyTyIAVYY55XdXuS6nl01JWfl+TNnSUH9qWe3DqkdqNxPby0wcYro5Y9+qhgoJESck9PmrJ0S50OJbOC4cjV2jt9gCSEH90mzkDb610CXGnxXBW6LeeUUrhHPGTjoMda46s9F5HZGnpLXdozfE+P7Rs8JuJgkz82O59q3tWz9n0/H/PQH/x0VnavdaPa3ludVk+cxsI8RucfNz90e/etHVsCCx9Nx/9BFY1HemvmVy2qXMUtm6b8P5tTZ3BtceqZ/So2bE0jf7Z/m9VvNLQoD/zxGPyrkcbO5rujgvFP/IcP/XvF/6DWREgklRCcbmAz9TWv4o/5DZP/TCL/wBBrHUlWDDqORX0dD+FH0R5lb43Y7fxB4T0uyihW0a4hlbcN7yGRTjH3lP17Y+hrip4ZLadoZV2uOeDkEeoPcV6V4hlFxa6dcLysqFwfqFNYB0oaxIlqEJfOVcHHl+rZ9PbvxXkU8dKhd1XeKve/Qzo81VqMVds5/TdHv8AVpGSyt2k2YLtnCqPcmvRtA8MWmjQo8scc991aYrkL/ug9Pr1+lamnWEWmafDZQD93GOTjBc92OO5q1XzGaZ7WxbdOl7sPxfr/l99z6zBZVTormqq8vwX9dwJLEknJPUmiiivAPXCimP8rK/b7p+h/wDr0+mIr3Ecu9Z7fBlQEFGOBIp6qT2PGQex9iadb3EdyjGPcCpw6MMMh9GHb+R7ZqYsBjJAycCsbUHI1ISxOySxKE3r3HUg+o9jXqYDHOl+7lrH8jysdlCxcuem7T/B+v8AmUtV1m4a+VbG4wkY2nIDJK2edwPUdvzxWFqNve6vEYNJtC0DBbmSEEFk4wAM8nkuOOoArXuo4pAWkhhgyw3zw7gUBPLbOVOBz29ap28y6LqlzHaGSWJFWJcODgKTjJ4BByTn1r3oYp8vtcOuZroeCsBKjXVLFPkTvruvv9TjJIpIJWiljZHXhlZSCPqDTa9Wt0s/F8RtpbPzbhRzvG1ov9rfzgfmD2zXQeHfA+j+Hyk0cRur1Rzcz8kf7q9F/Dn3r1cPnSqU2503GS6dPv8A+B95dTLZRklGSafVHiDWlykfmNbTLH/fMbAfnjFT6dYSX1yscYznv6V9Gljg7iSO4NYd7o1lJcC4gtoork9TGgG4e+O/vXRQzB1HaUbCeXy6O5iaBoa28aQxp8xxuNd9ZWiWcAQAZxyah02wW0hBx8560l7fiOQRIQT/ABGuevVlWlyx2O6FNJezh8w1GYMhjXnHXBrzDxPqBiYpE2Qxx9K7fW9TghsmVGUORyR2rynV5xcTKwcNuOWwe9ejl1K0XJhUqOEeWBJ4cJfxVpRPJ+2R5J+te9jpXhvhm1kXxBpcpik2/ao+dhx1r3IdK5c0knUj6fqYQ2MXxd/yKmpf9cT/ADFcFp+kia3aaVTjGB/L/Gu/8Up5nhq9T+8gH5sKoJFDZ2LhlPAIBHY/414843lc2jK2nc8t8U2+Y21IFT5kUds4/wBpZc5/KsCweJNEuxIzebC6ohK5HyKDH+afKfpXaXtmt7bz2bkbLhyiOeiyHmNv++gR+NcVp8ZYTwXIZY5ZfLkXoUlEWV/VWX/gVetg6nPRUmedXhyylBdNV+f+aOstLUtrOlSxsfLiS2yCOdpjjwetdDdyrFqUandkxQnCj3PvVfRLWyextLyW9RLvyLYLAZUBb9zHj5Tyc1buoLGS8jlub3yZVjjCx+ai7gCccHk0pyTsn0v+pcYS5ZeZQ8XhZJ7ZFLqxjYscZHDn3GK6TWGxbWZ9/wD2UVl6jbWV7dRNeXa25ELBQ0qJu+Y5PzVo60N1la+gOQQf9kVjUa9ml2LUX7RswG4kPvIw/VqqocSIvbyox+easNkbM9dxP14aqvS4iXvsiz+tYyV4jd00ji/FA/4nJP8A0wi/9BrEre8VDGrDH/PvF/KsGvbwzvRj6HFiFao0dZa67a3WiWWnyuyXVsSFLD5XTngH1Hp+Xeux0Kx+y2QkdcTTgM2eqr2H9fqa4Lw3o1xLq1sbqzmjgnj3xySJgMgOWI78gAZ9/evUQckn3wa+K4ixEVV9jTej1Z7WR4SKvWfov1/yEkO2J29FJpwGAB6cVHP/AKlvfA/MipD1NfNH0I1jgqPU4/Q06o5uAjeki/rx/WpKBiOu9GX+8MUkbeZGrdyOfr3p1Up7xLMlWGSZDhc88jI/DORQlfRAld6D76VY7Vl/jcYQe/Y/gcGs/TNPvdadmtkXZ1knlJCBvQYHzH2HTuRWjoumjWbt7q+w1rG3l+WWx5j4BC4/ugHJHf6ZrrJ5jZrERF/oqja+wf6odjtH8PY46demce1g8CnFTn16HNXxfsW4Q36s5hPCM/nrG+oxj5CzGK3JC8gAfM3fJPTtWX/wr6TTXQx3Bu7MKBIsMeyfgYGASQffHPoDXaXt3BalLpH3Oyj5E581O2Ppk4P4d6hl16AqDar5ysAQ54B/DrXpQVKhfl0PNr0quMSVS77FTRY4bKwKaPiQcyPFMBvl9SHGOR0wRx0rajvrWSBJvPjRXGQHcKR9QTXFT61PYeKbdoYIgt46yMATgPuCOQP9oMpPuM96wn8Om91ua9baFS8ncOsf7xfm4Xd3T27V2UqarWa6nHShVpudN/Zt/X3HpralaOB5VxHJn7uxt2fyq9p8G4ee/U/dBrH0TTPO2yyhdq9cDGa2r28FuBFHjeeOO1dkoKH7qG/U6He3It3uLe3fl/uozmQ+nase4t41G6Tz2YjgoQvPYZ9c1cgUJuuJT0XJJNYup+JbCAsF8tJ8bfMfLbM+opc6paIEm4uFPXuPi0jThLJ/aVhGUVDIGlkd8pnnjOMjiuF1vU5DOi2CRWlq6BlSCNUOepyQM9Tj8K0nFzFJNDd3LyR8ybwxOAw2EH9D+VZ0FmLm8glkdFjkYqvOdoHPIoVWc5WcvxOObXLaO7LHhqCc63YS3LyyATpjcSdpJ4ye2cdP8n2IdK8506RF1axiQFIhcLsTPJ5+83qa9G7VNZaopRsrGdrqGTQ7xR18sn8ua5++uwdD3ZyZlA/pXVXmDZyg9CvNecXk+JX04nJt5Dxn6kVhJ2iOKvU+RXiiSa2vI3PUZU/3cHjH5CuX1qxLtfTwuoN04kbtslRQG/UBvxrq7bYBdFlzgKDn0I7fma55ZY3lutMuHEcsrYhfHBkVW6/7y4/EUYKs6bcH3/yKxmHc4e3j039P+B/mS2lqs/iPSLgW8I2RWzbsDK/uUwPXHpWvqosRrcDXYg8xookjMigknc3A4pdBtP8ARob9gGWS3tQgwcqUhTJ9Oc0/VdGm1HVLa5S4SNbfyyVZWJOCT2r1FJOUU+l/1PPlCXLJehS8ZWTX09gscKSyLG5AYDIG856/hXWayALOzH+fuiqslsbq+gmV1XFtImCrEks/sPb9ata18tpaZ7dfyFc9WV6aibRi+ds5+ReI8j+Ef+zVVVSbuPj+GL+VWLiZUK5PGxcfX5v8KitpQGEhI4Ef6LXLUuoM3ik5q5XvPC2m6uUvLq7vIpGQJiJQVAGcHlTWLbeFLKDW7yaaV5tIsSpHmAbpnKg7D04BPPrkD1rro7qKK2gha2aV3TO4TFNg3AdO/WsjxLrENpeXenXCssTTnZIvOxgEOCO4OTXXSlWdLkg+n/D2ODGWTbS6ljRpZb/VbrUJgQBGETjhcnJA+gUfnW7H/q19SMn8eaxvD8sU/hsTxElZGfqMH+7W0CNxUfw18Pmf+9TXay/BH0+VRthIfP8ANjLj/U/8DT/0IVLkZxnmqd880cLssPmoAGIRsMMHPQ8Hp6g/WnC8t2lY+aEIX5lk+Qrg9w2PWuLldj0Lk8sbyoY4o3kkb7qoMkkc/wBKuHTbwIZJfs1unrJLk/oMfrVe1TUJ5Y59Pt2UqTtmnGyMg8EYPzMD7D8a0r+y1G7ERItSyDnbKyqT6gFTj869TC4KMqfNUi79tjjq137TlUrL7zLliAUhb9N3rHbM3/oTCst9JV5GdruZiSDkxqDwc+9bT6TqESNJKbKKJRku9wwA+vyU+y0e4ugzzTeTCR8hSMh29wH6D0yMn0HfqjhEvhgl/XqWsTTh9pv+vQZoSRW2kX8dxK6x/bP3bkc5EcZyMejfyqxd6+PKRYy0EhJ3Oy4HttPQZ/MU290G7jgZtMuvMbdvNveEbHOAOHVcocD0YcdB1rHSa68wxvZNE6krJmZcKR245Pr06V0Tc4RUVsY0o06k3J73CVlACwybZZXBBTBPXk4OR0zzU0caxRqiZCqMDJzTYYhGXYkF3OWYLj6D6CpOvFc53JGU8f2zxXac5Szj3yAc4JYMB9TtUD611Omae0mFKBWZ2ZvqTk1U0bSYoBcTMC0s1w8jM3rnA/JcCuigf7NB8owenSvdwj5IcsdzxpXjUnO+sn+WiJrm5j0+3EMWN+O1Z3mJEPtMxGOp55qO7kjhDyXEqIoBJLHpiuK13Vbu7s3e3eI2kcu1SrDdIDkZ9h6Z71vzRjot3uc9WooRtHdnSeM9YNj4cha3O5rvLIV9AM/zIrkLPw2z6e00yyT386eY8yyARRLn5lPqcfr+q6WLrxr4eWJbiE3Wm/6tCdu6JiBz2yCpro5NSsdHgfTMobpo/Lfy13ImevXk9Sfc1hK97LdhCpGEIST1RAYYn0q2jhmjmkEQDbSM56YPp2HNU4LZbSG4iniAlZF2Nxkf4dKsaNE0VtNA7BeQyZGCcd/yqwqxwkvt3tnIz0HvXRRp+zViPZNzclqN0eCQaxaO8bg+cnJU+tekjpXm9hLP/bNkpZirXCHBOe9ekDpU4jdCaa0ZXvmCWUrN0Aya8i1G5dfGKXA/1V0Ah9Nw4/wr1HxHIYvD184yCIj0rzSe0/tHT5YVA85Ruib0kBJH59Pxrklq0jSCtFs1biKODdCATI8Yyw6cZJH1/wDrV574raZFtbqNtk/miRXH95RwfzFenW0kep6dbSj5JDBuVSOPmANef+PIY4YraAZEkckrY/2c8fzrBxlGodsKkHRa/rf/ACujQ0Zl1PWbDULdmS3kSE+T55CoQNrKEzjgqR06Yq74qjc6/ZhZ7iMBIsiOVlVvmPBANN+GNpN/wj084gLRtcbRIMcENz79xXS6pFJa6ne3coUWxtoow5I+/liQB16EV7EJq8b22/T/AIJ4U6coucI3K2rW0FxqVnHcSSxoLSZgI52iBbzeMlSM96TxZqtra2VpEJd8qEbgpzjgdT+Fct4j8T3F3dImmQ7BHE0YkfG7BbOfasjXbr/RolJ5JJ/QVFrqzNH7smyW98SBVULFk5xlj3y3+NZ0GuXVzGRvK8/w8cYxXO3lwTIi5zhs/qa2dC0+a+Vdg2xj78hHA9h6muTF1I04XeiRph4VKtTlirs7Lw5faculWRvbi5N1iTakSbyyiTPP4gdxRqNrBqupTXd1HuR5jKkJOQOMckdeO3T60WlnDYwCGBSFHUk5J+tT14mJzWrUXJTfKvx/4H9an0mFymlSfPVXNL8P+D6v7jXtlVNKiVVCqMAADAHzCrSf62Ue4/lVJJo4tFMsjqiR5LMTwMEGn/Z3vyxnR1t3GPIGQzj/AG8cj/dH4+g8eur1G3/WiLjomvN/mxb65dNGv7+KMNaWtvJLLO5xHhVJIXux7cce9Y8z674e0KHX/EsOj3NvLsIhJK3FsXAIWNXzG7AdRwTg/Nxmt/XHm1D4aeILE5a7g0+WI5GC6hMo2PdR+YNR+JdYsTofhDxLe2hvNNEomlhVVf8A1lq4U7TwcMcc+tfQYLCUIQ546+f9bHk4nEVHLleiOk0PVLTWtLiv7K4E8MgxuViQCOoweVI7qeR+p0q8e+HOpX9r4uNo4b7LfW4YxCIIkagHyXUDGeBsJ75Gelew1upKSujKnJSWhG1vC8yytEjSL91iuSPp6VmeI/Eum+F9OF5qUrDe2yGFBmSZ/RR/M9BWvWXDo0K6v/ak6C5vADHFNK2Tbx+iDGAWPLEYJPHQAVS8xvyOe07xX4m1ZhNB4Pube0J+VrhwrkevzsgH4ZqF3vtR1OW41GGC1dMRSWkD+YAy/d3SfxkBucAAcDnaTXX3t99lMaqoeRz/ABdAM9TXIWExnsI7hzueXdLIR/eZiW/XI/Csa8/dsjrwlF83O9ieSaOBQ0siopOAWOM+w9fwqWw33jq0UTLHn/WSjGfovU/jisy6st11YufmuJ7NJXb+7vZiFHoAOPfqa6i0jWBFRRgKMCuPmcJtdjtnO9NSXUuWqiO3Ck8rkEnqTk80Xl7Da2oZ2GeQB61H8wY8hTv6n0IrI1K1F7DEy3Dp5cxJYDG8Dqv4/wAs16tBykro872PO7s4nxhJe391bx+a8UFwzqAVIYHGfm5x0qfTLHxDaRwG0nhMZjaMPLnCgjAHr34rW1i0Tdb9R5LGR93OAM4z/hVjwnBK0E+qX8hh0y2cPGu7/WOMsT6HlvzFaxk2tScTQpUoKcupX8NeH73w/bT6lPdINSvUCJgY8oOwJLDjJ4H+TSW2jwm5Z7u885pJOJAnLkruyc9O9UtS1+bVdUkljBSINhEBOAATj8eKsW5cjLHDf1xiuijTd+ds5o0U9DZt4LWHYQzHfGC2CM54/wAasJcWamPzslTwcfxHB/qKx1YIgAOGAwTu61VvLoCPG8cHgZ6d661G5r7sVqbNjg61p7gZBmUD2NehjpXl2g3i3t1YIqqDb3Uakr/Fk5yffrXqI6VyVr81mc85qdpIyPFH/ItX/Gf3eP1FcTYWzxWxnO7O7Kjtxg/5+ld5rsaS6NcRyNtRgASf94VgajGltZJEBj58f+Ot/wDWrDl1uVGXu2OWtbpraW7s0BB06clSP4oWbIH1G4fga5jxxI11e2aPgFNy7gc7kZuD+RFdYIy2v3QYgBreKQAD74ZChB/EA5rlPFGG1OO13fNBCBux6kn9KprmhfqiZS5E13GeHfE11oVulhDdThXmMogCR+XyTksSpY5wOARVHWdZ1HVNZk338qrsRmwFOfzHH4VQUzNdwO6kAD5imducn/61Dbm1CUgMV8tM4zjPFdkIqy9F+SOWrNuTZHqNzPHdosUzR7kJOAD3PqKsa2xEVvknkkE/hUNxD5+qQg5CCMliAcdT1xU+tIZI4QO25vyFO1iWzM0nSJNa1ErkrBEcyyegycAe5/8Ar16JbwRWsCQQRiOJBhVHasXwghXRGYrjdcOc+vStuRiqZHUkKPqTj+tfIY+vOpWcXsj6/LcPClQU1u9/8iTp1orcjsbWIYWCM+rMoJb6k019OtH/AOWKofWMlf5V5ntUdXt12Mq0gN9exxvlra1IlZT0Mh+6Pc4Gfbj1rant4bqMpPEsinswzUKwrp9lOYiWOWkJfnn3x2AA/AVr3ejXNrGZLedrsJy0TRhXI77CvBP+yRz61rGjVxCc6f2TjrV4RlaWlznbfQ7U69b2ttd6nZfaYpA5tb5wPlGVyrblI+/xjHJra03wm1v4Xu/Ct/ObjS1bNjcYHmxLu3qrrjBKOMgjgjggYrI0w/2n4vgMUxVFgmkjkXuFUICP+BSk/hXUR6TcLIG+1befvLnNezgZzVBc2u5wYmjTc2m7bFTSNJntJ4I57GEeQob7Yk3Vgx+RU6hMYIz06c4yehV0YsFdSVOGAPQ+9GKZDBHB5nlrgySNIxzkkk81vTpRpq0FZHHCCgrIlJA5zxVS31Oyu5vKguFd8ZGAQGHqD3/CrEkYmieJujqVP0IxXO6To84vFkmK+VbsNskbZErDpt9vX8ueas1iotO7Oglt4Z8GWJXx03DpWPdaHDA81xbXEVrE7mR1lHyI55JU5GMnkj1J9a1L65NpZSTqgd1wFUnAJJAGfbmufCyXU4mupDNIOhYYVf8AdXov8/UmuTFYmnRVpK7ZrQjUbvF2SFNvF9vt2ty00ENvHEJMYB27vX69qvKMuoz3pu4KvNVjckzADoOp9K8eeJlObl3OqMXy27Es95suZN6sqHAD4yvHuOn41VVo/JAjk35mcn07VX1C6PllIWDZ+8wPb2qPw3fWlxd/2et1bG8Dv+5IYvg4ycAYxwfSvey6rPlcZLYJyjQgpSdkaenaW+rabcx3UbI0+4HdksgJ4HPT5R+Ga5XxlrF60UWlWltJb28eFSPyyucZ5569K7TXfE+leEo7S3uC6xliGSCBnONp7D3xXPX/AI18O6/CfN0rUZ1iIYO0AjaPJA+Vi2Rk+lenGMpzTtoeDiKzqO6foc3oOiz38TyC6iVowWkEkmCAO+B/nmugOjCHEcl5CJQm4pgk55OPyxVC2n81mitoZA0pVd8m1SADkDgnn3oiumhffGEBJxy2c5ruswpwaWpqXukx2unJcm4BBGFIThzn1z6fyrAubSa4mEMCvMznEYjXO739hx3xUl7dva+WNyrvQNtwSOcjp69ao2ut3umvfQW9ylrKIWkYi3DOxQZCkt069K0ipWuYYqaSsdLptn/Yl/o1i2DNPdiSViQcf3QMfj+VenjoK+ddG1/UdU8b6I89/cXCnUIgf3aKOT3A6CvoodBXDXvzamlGyppLoUdbUto10obaSnB9+1cprVxuhskUZViuW/A11etnbo903y/KmfmGR1FeY3F81xfWrktsz8o9sqBWXNZWNYxbd+ws8hg8SxADEUlkqHA/iGGH9a4zWJmk1qdzxsYRH8FArrtQkP8AbKxgnPlR4/WuPvXW41G7X+JnPPrjgVaj18jGrO115/pcrn7S8sXlCEQbRu3bt3GQcY47VXluLmO6ZIRAVO3Jk3Z/DFaWnoJ7OWUqQY1PT+9uOaz3/wCP2VcL0Q5Iz6V28qSSXZfkYyk23/XQbc3c9vPiERHcgzvz6n0q1qjeX5JI5BbOPpSQrE+oYkZVBhOCwB7n1qPWCTHGe+5qnzFLW6NPwvfpG0mnyNtWZ/MgJ6B+6/jx+IrfuRti+bja6E+3zCuD4a3jYdQf14rorDXPtVhJb3xJcRkLOBknjjcO/bnr9a+axWGdaXND4vz/AOD+Z7+AxyoLkqfD37f8D8vQ7+2fzLaN/VefrUtZ+kziW1IBzzuH0bmtCvm5K0mj15K0mhCAQQQCDwQe9bGh3LyWrW8jEyWxCq56shHyk+4wV/4DnvWRWjoI3zXjjO1dkXsW5Y/kGX869DLJyVblWzWpxYuKcLs5u73aL4rkubdQfJldhGTgPHKAzJ7cnIPYqO1dVZeI9KvSEW7WGY/8sbn90/4Z4b/gJNZfiXRria7S+s4nmLgRzRIMsMfdcfhwfwPrWC+k6iUKvpd2ynqpgLD8q9B1K1Gbio3i9fvGqdCvTjJy5ZJW+49I2sRkKSD3A4pkkYkQo24Z7jgivN4tN1C2P7jS9RiJ/wCeMDp/LFWYx4hD4EWswoOsjrKw/BVJJ/HFbRxEn/y7ZhLCwX/LyJ3gtvM4lkklH904Cn6gAZ/Gq9xq2n2k32d7mMzhc/Z4jvkA/wB0dB9cCvOvEE90lvE8moXxSMETJcvJETnGCFbAOOePQ1maNbRT3eqEKoeKzLwunBRgAdyn15PNdUFzxutGeXXr+xrKm1dd1+h3+pancajbSW8KLbRuOGc7nyDkdOF5A9ap2upp5f79CjjhsDOD3qno93LeabDPOVMpBDMowCQSDx2OQc//AF6h1pkt4ftYzuLKhUfx54/MD9BXk4qlKs7dUe9S9jGnzbR3uXrnWoQCIw7n6YFZrX0twHBYKp/hX0rPSeOcbo3DY6juPqKMncNvXtXDCmoPVHdCELXjqaNtLIZFhVS5Y4VR1zXWTy2Pg7RX1GdQLx4/LyF3EnJIGB2BNZHha2eFrjWb0hLSIbYFxgueMt/QVn6g7eKtTD3cKyWUbZEb9G56V9LgsLOOre6PmMfj4YjSC0i/ve1/69Tz7WNRm14293d3s8heaY5EGCv3ONpbp+P4Vr6Tb2qQ3m4S5YLuICrn5/qa7+08J6GjJGdItWJBIXb09+vFWZtB0u0tJimmwLJt3BSuA2O3WvXTjBWPMoP2krpaI5LT5YIbu1ePzgyOChYoApz7VFNeQs8ksiSNIxy589QGOeuNtatnbRXV4oS1iEanLbRjA+taU+maVBFumtUIHUt3quZJndJ2RxGqahBGID9nVg9uuMzldvJyMjrXOSX0Uk0yJBDGfJlO5ZHZunTk112oRWM0mVtYkQcKuDwKyJNPsmYkW0YJGMjIqqlVQjY8pp1ZtvYwfCE6N400IiC1UnUIQCoYkZPbJr6jHQV4F4c0iCLxZpMkcUWEu4yGxg8H0r34dK8+UuZnfDYxPGDsnhLU2XORAen4V5lbustvE4AJRkOfQZH+Feo+KIXuPDGoxICWaBgAK841HT00tVMJGzy1jlUdpAuc/jj/ADmptc0U0rJ9Sa8t4kuZr0nMv2cIB2XBbn6nI/KvN1nP2qRiM5kY49s813d7dq9hdTqQRwpOeh4OP5V55E5kfcOu4g4/nW9Jb3OTFv4UvM1YHlUNGkLvHkuzhwAuc9s89KrPdGG4kiWB3LbcspA/PNaGlDNjdEAnCAk7Se7d+1Z7SBL+ROcuqd+MCuxq9vT9COayb9PyEe5MFztSBpGMeflYDjJ9aZqv+ri7HcaZMpa8XAyRGf5mn6xwkf8AvNWMtrml9Wg0yzuNQlW2tYjJKwz6BRnqT2Fd1pHg20so1a9f7VL3X7sY/Dqfx/KjwNbW8Xh8TxMHlmkbzjjkEHAX6Ac/jXTV8Xj8VVVWVOOiX4n0ODw9N01UerZlzQHTWWa0QCJQFMY6Aen09PQ1pI6yIrocqwBB9RSkBgQQCDwQaitofs6GIfcDEp7A9vzrzXLmWp6Ld9yVmVFLOcKoJb6DrW14eiMeg2bsMSTp9ok/3pPnP5AgfhXO3cTXAS2jdhJdOLZOflBfILEd8Dcce1dLochk0SzDcPHEIXHoyfKR+Yr2Mqp2jKffT/P9DzsZJNqPY0KQjg9PxpajKN54k81goXHl4GCfX616xxmbPe39isktzZrLbxjc0kHZfXGc/pUw1nTyoPnMpx0ML5H/AI7UOuXF1BahYFRElYRPMTkoGB6L6npk9zWGq4QBQdqjA9hWE6jpuy1OulRVVXenobOoeKNNsLUyyG4mQsEEaRsAxPQfNgVwq6xp0njAXS2q6fZXVt5Ei8bVOCNx2jA6j24q9ql/ov2drbUL63UMR8qy5dSDwRtyQRXE215DLNERMjO6FCM4YMpJ6deRk/hW9Kc3Tc3H87HLUw0J4unRc/dl6Xv0+/Y6jw7c+VNPYMytkmWJgQQSOHwfyb8TSeJ5/mtbcHgbpW/9BH82/KueLPZzLdWzqjxt5mwngkdTj3GQfUE9+as6jew6heNdTEwRCONdkgwQcE7ffknp14rOKUqvtF/TOmpgsRGj9SfdJS6ct932aSs16W3H2t6Y7GZFs4ZC7bhLLncuBgbcYI6k5z+FbXhy1kubdr27kBhjZVAKYZsqT1HfOO1ZumWYupQ4kDQY3bgcjA61o6zq0emeHY7W3ABMvH4IBn9T+VV7NV58so6mGZcuXxp0cNJpWlfXfbXte7expPrCarrp0a3GLeG2JCg9wU/xrYihWzh810C7egxxmvPPAkgfxXNIxLM1qwyTx/yzrsdW1KSa3a2hMWxiPvKcjB9e1e7Sp8qsfOSm9IrYtQ68ou5kTDStE2G9CBmpNNZnjaWTJ3HAy2QR34rjNKk/0+OXdtUMSTjOB0/rXZWxSOPam0LnPynIz3oqxsepSgoQsiRIobKFlQgICWLHv/npXN6rqouZQgcLGPugnG73q7rt0wgSNDwxO4euK46/uUEsUU8Urh2wphQsR7EdcH19qcYqK5mcuJqXfIi5O5DHHQ84P+NQ8MQf51ktcG2uQYZjLCTh4XyGX6A8j+VahODXFUlzO44Q5VoaegA/8JJppzx9qj7e9e2V4b4euIm8T6YomQsbqP5Q4J6+le5DpWZtAgvTizk+leWa7bXMOs6tJIp+zyQLOp/hO0qB+PUV6L4lvJNP8PXt3EFLxJuAboeRXmmr6vcT+GdNtFUN9sZoJJCeQVcDA+oNVHcKivGxy9zci2tzbD/lqTI2f7q5Arl7aQqcknJ4Ndh4tsTYrczYG1FTaf8AZIbP6muKhyWZehUZI9q3tq7HHOTkk/6/rU6TTbuSKxkhjE+JeJSkZZduTjccYHeqby2q3BMqlpTjadrH+VaGhfaG0a+aJA0IA81i2COW7d6oxwtLdswydihsAfhXS1e1uy/JFXSTIzNDDdbpc52YGFLdz6UatzHEfVm/lQWSO6V3+75ZX7uRk7qdqQyIR/tGs5xtFMq/vMtaBrlxok/yKJIJB+8iJxnryPQj1rvbXxLpV1D5n2jyTjJSUEEfiODXmCjp7GtCzO05z2rwKmEpYl2mte63/wCCehSxFSjrB6dun/APToL21ul3W9zDKP8AYkBqevMoIi17pc/khxGxOSucHzM12+m28k0IeUyeSR8iliN+epx6fzryswy2OEjGSne9+nY9XA4z6zz3VuW3zuU9X8WWGk6tp4DieS2ukmnEZ3eUgBVs46thjx7V1Jv00fUpLgOJdI1A/aIp0YFFLdefQnkHpyRxxXnfiTxHp84ews7C2uVX5TcOuAp/2MYPHrnH1rE0rxHqmixG3tLkG1Y5a1mQSRE9yFPKn/dI9819BluWzng1aLi99Wtb9fL0Z5GNxahiW4yT6W/R/wCa2Z9AQzRzoHidXU9warXdxHYyid/PbzPk2g/KMd/rXiL+L9YG37NLFZYzxbJgH8GLAfhiqkviHWp7uG6n1S6mlhbdH5j5VSRg/KMLyOOldSyvESWtl8yVmFFNOz81/wAH/hj1Lx2fENzp1k/h+OSW2BaWcwANLkY2YU8kcseATkCvIri9ur0Zubu4nB7SSMR+XSu5svive21osMui2kjoMK8dw0a/ipVv0NcFPM1xcTTuFV5pGkYIMKCzFiAOwya78Dh507qrBaddL/19xx4utGWtObs+gwYUYUAD24pedysDhkIZW7qR0NJUsNtLMwCIfqeBXptcys9bnApOL5k7NFyVF1a2LLBGJC4WRieUIAwV9uadKJnu4bmVB5LSG1ErEbDKmCw45AIK5z9O1MisruKby4pjGZRhioB4HOR7j+tdNp1ja3PhC8sol8y4sbg3BQ/eIKDJH1Xd+Ir5fE4V4etKC7XXp5n22HzH6xhYVnvdRb6N+X5vsXXnis7NoURUyhyFrjtWvTd3A2uCo5GDnkgVcijndlt1ulKSfIjznGzPAyw6jOOopPEKJ/bE6RRxxJEBGiRxhRhSVyQO/BP40Zd/Ed+pwZ/RSUKn2tb/AD/4YZok5s5ZJo8bgiDpxg7cg1sR6jPO+IyvJ7L/ACrAt0VbO6yTkeUOMY/hrct0GnWCvJ8s8w+Ud1X1r2Y6Ox4tGkpLmL9pcPYSv9mbAKmNiVBHPWug0yU/YkyxJclifx/wFcOLgocZP51uaJqaupt2blcsnuO4orLS52uSjGwazP8A6bIoztDFlz6HFZRlIYYJBAOfWnalP5l7KVJIBxgH2qmZkHXIHTOOn+FcNapfQ41G8m2THMjqZDu28ruJOPzqC7WSZkt4po4mYbyZIy4KgjIwCKkikBYx5G4dMHOR6im+XuuWlwWbAjQD/PrXObGh4YjZPFmlR5TH2pGJWERnHOBjJ+te8DpXimlQSWXiHQhwHl1BFkIPbaxx+gr2sdBQzSGxg+Nf+RO1T/rgf5ivLtKU3mnWkR5+y6nv+ilM/wAxXqXjNS/g/VFBwTAece4ryXw/dNaf2msmOIxMMd8Bhn+VXTtezIxF/Yvl3v8Anp+ozxTdNe6KykDExlRX91K7R+Wa8+YhZiPukZAI7gdRXWatcmHwvBDIqfJdpJ5nO7JXDe2MY/KuSmZdyjBDbQwPrnrXQtVqY16bpS5ejOn0S5MOkSRLdKn2hirRFlywBOOOvc1HFKtvcTMZVVmVV2swGRUmgix/sqQyxk3Tj90+zOMM3ftVOeM/a1KrESSoO8DOMDpWzTv8iFt9w50gebbPIEAQYy4XnLev1qTUQGCHOANxzUckKy3RyVGEGMpu7ntg1NfcgD3I/Ws5/CWlr8ikrYkK+/8AU1cjfY7D2B/nVDOZ885zkn3JzU7kicj/AGRXBCHLUZq5XibmiWrajqunQGV0gVHlkAPDBWzj8eldR4qunh0o28bFWuA4ZhwQirkgfXIH0JrD8FTsL5IONrwSEcc5Df4Zrpdd05r+0Ro0MkkLFhGGCmRSMMoJ4BI6e4FeXmU4Rx9H23wpL83/AJI9HLoyeGq8nxNv8jypY2IAwQPau10CXTdRjlsJdFtoYPlUPnc5zxksRnPHUVFu0sSGGx0uWaZcbxOHOw4+6R8uPqTV1UW3jnuBBFC20M0cWdq44AGSecn8zXfnGYYfEUeSm3z6W1a+ej+4WVYGrGo5TVoWd7r/ADOJNnIJmiUFmVygwMk4OKsS6LfwMFms7hGIyA0Z5/SungbXLyLyre0CRlcfIm0fXPc++c1I+k+IoiYlvpthHKxsxUfXB/nXpf2rNWXJ+P8AkebLCq7aOSbS7pA5e2nUJ94mMjb9eKhNowOMEfWu6tNN1y7lkWPWnjfO0qE5zjHTn8/zrTXw/cxxsNU1RLrYNzq9urMB7NnOPSnDOFe042+d/wBCXg5PY80Syd3VcHk4rXhtyknK8EYz6V38XhizljSS0iiWULnbKzEHjPbmrZ0WwEQa5hSLHGY2OG9/89K2WcYeO5lPA1H0OEgtpfOCwqGkYFVTG7d34H4VQ0+4vtF103rxNv3FZ4P7yHGRz3HBHv8AWvRU0qG2aaXTy/mBTEHkX5ASOcHGWqk2hW9xcrHeBQik7WWQq+MAYJI56Zz71nVx2EnNza3Wu97eh0UoV4UlSvondLS1zAbw1JdXDSWc0H9nS/vIpCSx2NyBtx745NZesQCLVLuJ5Gba5BdsFiCSc9ucGu4/s+LQ0PkzO1izZdZOTbsT97I/gJ6+h56E4xdd02O4u5HGVmZVU+5xgcfhXHhXSjU547anXj61avRUKnSxh6HZwtaT3l1xaW4jZvRmGCFqtPey6heNKVO5zhUHYdhVvVHS20mXT4HJhtmQE4+++Rlq6LRvCFzpzXc1+0I8hgivk7WG3dleOeK9FSSd2zmpSUabMSfSks9Ma6vpCsoGEj7/AErD0x7i71K2gtAWuJJAEHp7k+nXPtW14h02/wBTumuEvLeSBZRCAGbjJZc4IyeVIP1FY39n3WlW9y8d9bLPJAVAUsH2NxleO+CPWidRPS5im3LUv31zZi7e2tJRMIfklmXpJJ3I/wBnsKhaRVXe2cDuvUVy8cT2G6YXEPyhQ0fzAnIyB061tWGpWwi8ySRkf+EbT1+tcEk5O5okktC0JLWc8bGK85UEEV0GlWBgQTTD96R8q/3B/ia4/UdTUqEhfLMwJI9P8auaB4gv21aO1u7oS28pZmebqgCk8HsOO9Tyu1wbOyjB/wCEj8On/qJpn/v29exDoK8Js9Zju/iFoOnwcpBehpG7F9rAAewyefevdh0FKStY0hsYvi448Kakf+mJ/mK8TmkIvbuKPq1nsIHu4/pmvavGJx4R1P8A64n+Yr55uNUI1W/SInLKI9wPQA0QTb0KlJRV2M1u+N3p7RhR5aSAKf7x9fp1rDJMe6GUglD8pHr6Vp3ZH2DH+0f5VkAggk9a60cdWo5y5md54XmEPhu4jaIOJ8hW/uEFs/zqODYjTs6MxdVCbf4T6mo/CtjYXGi3FxcSH7VCMwL5xUZy38OcNVO6tvOn8zDkKUXIcqOxxx171o7307f5CsrW9PyJRDI0rlJCnyBcjPqf8ai1WdLdVeU4QybSfTJ61W1RPMm4aQERkjy2IzyfT8Kl12Az2/kqPmJ4Hrgjis5XtqaP9CEIfNcnGQAD+dTZEkqnHOApqlpM3n2zRPnzYsI2f4l6A/0/CroUhw5/yf8AOK427SkvQ0S0Rd0fUDYG0mjjDSKxwxONo3YP16130ev2rJl45UOAcAbh0/z1rzax8hlt0mBLAkpjPXP/AOqujt5BIAvB+6OePSjHYOliaa51quvUeDxE6FR8r0f3HZLGmqRo4sBOuPlaZVAH4nn8hU6WUdrqNhBcxKwuJxEoiwEjJBOeeWOARnHHpWfp1xdRQKz3CyP8pj+fCbfSrE8zvPa3WCfsMwmR5FO0Hkbf1rwqWDp05p7o9eriKkouPQ09WmGlWpnZHkRZRtWNsCSIKGcj8Mnip3ttPkltrdNsjXTB4ljkI3RYLA4BHUKeTXJXkx1K0tNOS7jWOGV5EXyvuq6FSijJynzN16dOwp8E7z3KF1DPDYtYQlbY/cI27/vcnHpx7V6EYULaaHFJyvrr8zoYLkw6+2jizktkOmm8IyCyfdG3jrhi3QmorSe3ax1eRoEf7GyIr53Fsru5OOOT+FUrK3uNLmspzPZqlramwBmiZQyMwYAAtyRjFbFjYXO/UvtCWH+mMjOghfYcIFB69ePfkVLdJzXK9Pnvc0p80d9yRprGw0+TVU2Q288KmPaf3hfByp7cYP8AkVYvNNkLs8ryxwguWlLqFUKMqx47+/HFY2q6PdDQYNO+22kUUTSPJKTlWViT0xxj1z2rN1Xxct1osltcXUEFpfafGkcskgGCfvNtzuOQensM1tUpUZN/8EJOXLc6JrqCTUBZteut39n+1eSY9yeX/veuKoxwwXGpQR/bYUkvY2mhtpIyZHTnB3dAOCcegrkpPGOi2tol2ZDLqB0trNWt5Q0RIG3lfvLxgjPfI963NFvYBb6DrF2J01C009YjapEWEmVYKS38IO7PPSo9jTSs3pbuY8xes0WW2X7GbZ0uvN8i3ckPMYzhl3fdwcd+oNcfe3W9mS1EqxovyI3LIuDwT3xzye1dHoUwtLPTUltrh7rTWuGRlKLA7OxP3yc4wf7tczr0MdpqKwwujhLdAzRnKlyDurpw0KcKnuO+nczqX5dTIv8ActhduOgdAT9WH+Fak3jZtZvLezW0ljE0pRQ1xuCl2bHGO28flWPf7ZLS5t1kj87CPtLgYUEEn24rGtVezvra6DW0hglWTYZwA2DnGa9KRyxmnG3mei3lzF5VrZaSiy4u3W9mfKlWZXYryPuqX/MCua1a9It4YUx50zojEMfkXG5T05OX6j0rLbVmt2BhMMSpOZZV+2mRpT8uVJYZ6oKzH1Oaa9W4kCug2gIZ+AFx0/KsWC0Fu4Q1tNK0xJlaN1Ow87Uxz6Zz3qhblmySeBxxVuW786Axuqlzzu8/vgDOO/Sqqrs4DJ/32KS0HfQkf7+c8VNYHN4M/wDPKX/0U9Qhd+MvEP8AtoKmtylvcB3ljA8uQZDg9UZR+pFNoSaOh8MMB8UNO4yTfjnJ/umvpIdBXzP4Xnhf4k6VOsqGN9QXaQwycggcfWvpcdBWFTc3p7HN/ECUweAtZkBwVticj6ivmS0YvdyMf4gSfzr6U+JnHw313/r1P8xXzRaki4bHoa0pbGVd6mndEf2e2QD8wAz71W03meYHoLaYj/vip7o/8S4/7wqvpv8Ax8XGeP8ARZv/AEGtkc63Or8KuU0Bx5cRWUMN7feBDHp+dLEiCad3jjYFVCFwOvHSsDSdSuLe18jzXVAC0QAXbnPIYEZOfYjFOGsmY72ujbhSA0Sru346kcHH403V1TRdt7ly4tZJ59wVSoiKncR1yanvxudf94/zqg+thg3kxGJTyJGAJx+uKdNqUCLH9onaSQZ3bVyf0qJy93U0TvoLPbfZbg30a/LIcOB645H4/wA6k8wN93BDD5T6+lXbG4tNRtZrYlssoyjrhgPXH4VkPHLZXT2k3DL8w9wecj88/nXMknK5bb5dCW1cIY2IclF/hHHOetbNncYgZj1IyPyrMsbyC2Fzv3fNEUTaucnBH8zVnTWik8iE3Eas+EYk4xnvz1raVSPKlcUacrt2Oj099Su4IpIo4igBJRedw9x2rQu7uYLELm4CN2JG7b9SKuaXYw6ZbNErs6sflyMc9z/n0q+73U8ht7ZoGCgGRnOQBnkha8l3TbUdDvlGpy6tlCzhSC7a7uIYJwT1/iA7Fecfkav3cjtYTPZXUywooXaq/wB08jceahksjFC8b3E1q/mfImzcmR0A+vpWzaNClslvL5yXEYG5pYgpIPfjjHUY5rKTT95BFfZKf2BLvT0Mck9v5vz/ACSZRj75OCKzTdz6feKXmuWaEbQsO5Y3UnJGDkEn/Jq8LmO1ZoLRUnfJYxg4VB3Zj/CKxNZZ5CMSReYTuTYdxX156ZpQi23ZBJpK5g+OfGk13HLo8OnvbxSY3XDyDLBTyAB2JGOevNcAxLNuYlmxjcTk1e1oSrrFwJslgRgnuMdf51c8OeHh4gN6v2vyGt412AJvLOxIGRkccc9+RXdCKpxM+aU9ytoOhXviPVl0+wVDLt3u8hIWNB1ZiPqB7k10ujWV94X8e2Wja/qv2eydTLaPFKWt5nB+TOSMc5GCOGx6g16toPhy38L6KbOxCPOA5M8icuxJYbsckDOMe3vVW4NvYR295atc6n5McsKtaxBgQ2x9rLGoCjK8ccdM80lPmfkOUNDH8Ra6mlySQROZpAfmyPlZh/Cfp3/KjQfC+n6np9teXskqtIC0jiUjcx5zjHfJrA0vRLzxHdTX2pCa1g8w4Ux7WcnngHoOeTXcW1v9ktY7W3dhDGPkUnnPue9TUzChh/c1cuthewlUfvaI5S98M6VYLLMEuArKYtpvWyykjPVcZ4FWbfwJp1xpsM89vdWkESsWJvSGOTnLZXrjHp2rfi022uLtJrqSbYP4Tg7T6mtDxVdpoXhaX7KVVmVlAXktlTlvw4NCx/t/4TsiVhYxdn+bPGfEmk2MeoSyW5kdJGdnYzEhmJJO3gcZ4rC+wQuFURPgDIHmnvXS6nZJbWix+erPKQdu3kk9W/z1rK1a2fSZPsyxOioBudlPQ52gn3xXZCpJpLqZVqaU+VFL+zolYkxOCwIz5p5zUcGmi4ufIjsp5D1PlyFjj8BWtpiTiB9WFi91plsyrctnC4bA45znJHT8a6K68TraWLRadarEhGf3ajp+FVKcou1rmLpO25ysljDbSKJrGeJ1wvlvMQQABjPFZNzPZpcOi20jcYY+ef8ACtLUNSe4i+0uSGYlssPYAVgxwSSOuVZ5JD8sY6t/9aqc3bU1o0YtXZ0ng8258Y6E3kOmb+LZmUnJ3emOlfUI6Cvn/wAE+FGt/EGl3+oZ8xbtNi8hY2GCAe5Pp2r38dBWLlzM2cFDRHLfElC/w61xQpbNseB1PIr5+0rT9OafzLiSSRSWwFfbsUYyW79M9x07mvoL4j/8k91vlh/ox5UZI5FfPum2y316FZJJjI+xFgB/fc/xAfdAHOfp0q18LOWt8SOnkt/DssUQj06Z4EkJZpGyAFGWB9T/ACz71h6hp1tZXs0lrE8MMtpLsjbvlffkduvX2rY1Ky8uYJcOE2xsILW3wfKGOAzZwDkEnGST3zWYwQGecmSdxGwdiD85K5+Xj7vBAOeo6dKmkveTuzOO5j6Ppt3qt5bWFrGsk05ZY8sABgEnce2MV3dp8JfNto5L7VWhujEQ0cMKuqt2yxPOO+APY0/4Xwxi/wBWkuI4hdQrGsZLKWjDbt4Hp0AP5V6Xis69V81kbQirXPG/EPw4v9DsDfWN01/FGrNOBCEaJQM7sbjkdc45FcZbCCV9004A54yRn05Hr+lfQ+rENZXEfnNEWgdQyMAykggEepFfPcs/nkI3kb14YLhWz6Y9eOlVQqc7al0HOPLZ9zRS/toY/KO9QCrBllIJxyMgjHXn8KtatqVvqk1i6xfvkgJZyMA87RgfXdWHBbSXLbraGedAcOVjYgcdyBiovN+zajlyAgG3/dUirqr3fdKoL3tWajMEUsegGa7zwr8Pm1eHWF1ItDNCTa24B+7NgNuPqBlRj3b0rG8G2yXtxqyJax3s502RYIWJwWZ0Bbjn5RzxzXsOgC/h0n7Tqd09zcvl5GKhQWUkFlAUYVsAgEZA6k1yPY73fc8s0K/1O1S3tblC58kywIxzuTcysM9flZSK3NOu7z7U4a23yXAwin5QOmefT1qnrdzPceIdFSZfKuodNeeQIgTyhKSQvB7EMM9+M8k1YgdHl8yciWY4xvOF/Pn+VacqcWmjCTkpaNmmHuLK/E+oQSLLF80KjLIfUDHHfP4U3UdehughuLeeKaFGERUkMGPT/gP1pr393cKDpNvPPeQYl2xqWIIBAzzjnnrWNJc3dz5f9oxPHcRsfPic87u49vXiud07vU0hCc7qOr+RoxavdX8LQNGUjzyyrgEY/iP601NIvLuHz3iWNQMYj5YDtkCtHSLMyaWrJOsYl7JCNyEEg4Y5J6f/AFq2YfMi6yb+wBUD+VTKU07Q0NVg5SV5/meV+KfDOr3gS8soXkMKBWjCEMwyTxnrwelcp4YvdTi8T2qWDolxO32eVZY9yMmcsHQ9cAZ7EEDpXuGseb9gurgTzLIkMjRqkjKAduc4HcY4rmdLSE6pdXUq7765jQtcPy8gUcgn16H3/CrjOSi+YiWElBcy0XY9J06/iv7cOuBKoAkT09/pVO51mWDW47KK0R7WON5rq5d8eWFGcBQOSSV69cn0riLq+1TBm0JDK9u+2UxjMnI42r3XPU/pjmn6Vruu6pHNDrm6N4ZkVo1tvKYgqTlvX27UKk+VSexPOlLlaOosoVt7S3iQYCxj88Z/rVjILBckZOOOtZWmXvm2cKlsBh+7LcnA45/KpLiP7b/oszzQhmVmMWPmAPKnP8Jxz9K8KNJuvy1HbXU6+V8vMu1y1LOluDvcKQQPm7k8YrnvEOoFghuJf9HiidQp7d/16V0FzaWkNtcTLHGvlxyFccCMbCMAenauA8R3Md1p2lXCBmguY2Zk3Y+ZcDnHcZx+Fd+Gw/JVil1uv1/IUpwUHfc56Gd7i9hZm3OHjHP+8ABXTPf2us+Otf0+7Mnlz3KrEEzhvJypBx2wCeeKwFKr5EsSnYJk4/ukMDg9qsa/pgtfEk2raS1wIpJ2mCq43QyEknJx9zOfzxXuWsve0PNqO07o6bWTZHwxqF3bW4uWe2FuHSXbHGMhdxXIBxwBxnpXK2OjazcWIlj02WRP+WZLKm9fUbiKrSeJZdSurWC/JNqlwjOkeFRuccgY7Z5r1OAia5jjxuUktkYOQP6fSvPxmJqUFGEI3b1Nadm3I8e1KwvkmS2ntGjnL7trnIA4AOemKtK9voMrJbf6RfNHiaWaLCqT1UKcEcd/59a9A8XRXrR29xZ2kLSRSFY2YAl1ZSzKewztwPf0rGHhWDVLtZLmS4t1kVShjYMVzztIb9MdPpXRSqupBOS+4qMbJ2Nrwley3Z0e6SRVkklSOZDzuAbGf0PNeujpXnOjeG103UbCOx2rbQyxkvI255cBuTgYHLH9K9GHSriRUd7HK/Enb/wrvW9zlB9mPzDtyK8I07W5tPgaztbWPzZ2AeeMZabnGD9ewFe5/E//AJJrr2TgfZjz6fMK8e0Oxj8PWbXOGm1G5UtHIBxDDjI2+jEcn6j3q3bkszjrbl5tMmbw7fPMoOo25a4EKv8AMsPAOevK8nA7MeeDUM1taQ6Ik8c7FruDai7gBhRuYjuMHIz6mtbR9ct7HxNaLcywrHdytbuZ+i5UZB9Oo61wUPlyXl4ttN5kESTpCyp1jxhW/Lb+NPD3u0wS0T6lvwxffZbq5u5VYWYQI20dCcEcdzx+tdxb+IbeaARW2pbkHzCONzkfh1riLVvs+my2qRkxmdnbA++oCqv5Yb86S28sNvhBQjjH92uTENObsdNLDSmrt2vseq6Bdreec4JkufM3MpHODxn8x29awZNct7e+uofNEbQSPvRYkUg5PBwPaqPhazuNb1iR5pJIbO2hYMsTbTJI+QBkHONp3exqXxL4d0a11m5v7q4nL3W1xAkiIEAXb2XJHHH45zSwsvZzs+qJqxbh72lilqFm2tIl3I88tnNITh2IDNjqQOMZ4HoBXI3VpHbSALsN/a3SeU0uSJlGGVWHfp/Ou/0vUrOKwTT7eFoNKtkbdLI+7ZzuJJPJ5NcVLPHrfieS9toPJiYk2yMOw4Vm9yCW/AV6FaqpK39aGeFpS5ly6/q2dDoniC31D4n2OsPHNaR3RVZo2kACSFWRsbTyuSnUDPpXrl1eXYnRYY7Q2GVMtxK+UCfx5OQAcZAz3xXh82nR2K2jWjOJRdLhyedx7+3Kg1Jc2qoPM8lmke5M0rBS5Yk5J45Pc1wwUKkkk7I9XEYeth4tzje3bbU6bWtQ07xB8QL2Wxk3CysI7eRyNqyt5hbjP8IBAz3rUs9KbUDFDA8sQkH31QYIHfPYfzrhdKSWO/uridPLe6d5NrdVXjaD+AzjtmvXfB+mvZ6StzMGE1yAwU9Uj/hHsT1P1FaVV7OOjOKD9o9djZsrKGxtVtoFIUcknlnbuzHuTXmWq3dvdaxfTwSxvG07YZXBBPA611+s+I4Rctp1uXJ+7NIo9f4V9fciqctvp2pK32ZYDMBtmeALkD0YYww6/wCNcyTWrOylLld46lHS9a0600yGH7QJJRuLRx/MRljWxaX0N8haDzCo6lkKj8+9cro8Fv8A2Y0d3HFtt3ZGLKMLjjr1/KtKwOoz20bWVwWtRKyiOY7WVAeAGxkjH4jpW9WjyJSvubYfFOq3FxtY3ZYvPIjb/VkHd75GMfrXG6fpd3dxM8LxxPbcB5CceYo5X+eT2BrrZbmCytGnuZFhhiXMkjsdq/Unk/zNeb694rutdLWmnK9rphb53I2vPzk59F9u/f0rOEXI0r1YU1ru+hr+G/Edtqevr9mt5Y42gWOUvjAYnCgY7AgDPuK669UyiLy1DusoGDnkd+fQdef61514Pb7J4xg2AiO7hkhcj+E8MD+YH4mvRIblZUVYpbWMY4UPub8uKc48r0McPapStMxprWfTA8t3PCYmmKw+ShAQdQD3BPPtUtzrV2YQtqmy5jypkLD5vUYII5wOtO1a8htXXZi7vcZVpSDHD77Rxn0H5msvSrG91G+aO0bMzZkmmk6cnJZsDuT0H4VwVMGnN1VLX8DZQUY2eqRPe6xc31hDEZVkWTDP8gTP1A9+3tXNa4gstM0yyDbniEr89fnYY/ka7CbQdZ01v3enQ3MbTF2e3fJRS2SMHBzycdareOtItFtobuOKNLjeEMrHbiNQev6D6V04RSUoRlsm3um22rfJLtucVS0uaz1/Q4BG2uvOSCrY/Gu6+xobq7tF2G5mh3OrsFAUseM+pBrh1MMNxt80NIBvZCMFR2yPpz6812ug21lquo6hf3Nx9oMSlnVDwqqPX1PH516tWMeXmlscST57HPSaFoukq91cyJcspIFpHNtcN0GevHOT9DXQaZqFvA0d/pt29xbqdk0Mo2PET2YA457MOKo6zaxXUL3V0RCyRfukRfmX2x3HTn61yMFxLb3PmxEpIABuX+IHsa4KzVXR9Db2V43T1Ot8TeLoLqK0XTLudvLZzMjqQGIxt56EZyfyrZsryK/tY5rORMDG4DJKH+7jsa8zd1UxJHHI29giqoyRgDJNdX4OsFbUXvZFuFgiARklAUSk84IB5UDnB7kVSpqnpHY6IuDp6vVXPTLfV7PT5dNhmlQ3N1OkaRhxkbjjJ/Wu0HQVyFpKv2+1jSOMIJVAAjXj6ccV146VvpsjkkrHN+P4o5vAmsRSoHRrchlPcZFeCXovpI5Utp/MQsSIpOGB3buG7+nPavffHZx4H1c+luf5ivCERriZYYhukkkCqBwckgD9TVK3UxlucyLa/wBS1F4TATcAF2VyFyM4zzweTjj1p1heXNrcFIZAqz4jmRxwy57+mOv1Feo6NM1hrd7pcEdjdW3+sSe9YKqKCFZ843Nkfw5964vx3ZQWvjW5ktERbK6AuINiFVIK4YgH/aVunHNZOpdv0H7O1r9x8ZUxqV6EZH0qIpsulYcK42nHr1qrp92Aogk4xypPp6VammQW8jKwJRd3HYjp+tcjVmeqpKUbjtB1rUNL1OeS2utrSjEisgZW2k4HP/1qh1K4utWupdSu7kkyXEitt4+VQu1V9Bzj8KjtJLa3QbpkaTuFOSPyoF9Ha2kkBG5nYvHng7jjgD9K3o2UmzkrQvFdyg810EltZZibWQo8kajG4joo+vX8Mmtm3P8AZk0JktmaWWEOxDAbSWwce3Cj8KpQSWcaxOWleZv9ZNJF+7QY+6gz1zxuPpWj4gYi802KNQzkAnB52hmY/oM/hW1aSkve3d/+B/wSMJV5K0ZR2T/4f/gGrJJEjxmfbG247N5HBxjg9M4P61e0+C4utTtLe0kaOeSTCSIxUqMHJyOcYz+FZGoy+Vc2bbC6l2yNucjA/oa2dLvY9Mu3nEef3bICoxjI615VTmUbx36H2dWTcZxirtHT6f4DiTVEuJL/AM6yxuEezDSEHkdT8n64OPeuj1S5lNwLXBSNlDEj/loD2+nYj+nWHTNXsxGtst3bu1sio5D8DKsVOTjIO0ficdRUFrqjGFAHAkyPMZl3MfXr05/lWeNryjBRk3fufK04atpGdqs8Op3y6dAissH+tkUZOV5CD0APX3+hp9tpMcQO9m5/gVjgf41p3t/LFErWtss+5yM7c8DHOBjJy2Kz59UitrQ3MquA7ERRkYZu+Pw5Gfat8NJeyUF0S/H+tjWglKT51qPj0mwi3BbSM7m3ncC2W9ec1Hqmt6bokBe+uki2j5YgQZG9lQc/0rkPEfiG6Ol3LG4NuGQpGkTFSWPv1J71xFtZJu82b5mbnb1J/wB6uuEHLVsqviFRfLBGzq2tXfiW5Wa6UxWUbZgtQcj/AHm/vN/ke9eomWWRsbvLT/Z+8f8ACrtjCJrtFIyi/M2fQf8A18VvpFHnLmqTs92XdKilt83n3ZBgxD2Bzn8SB+H1rtNS8MnW5lu472NYpERohKhYxjAwB7dfzrm++a6/R7r/AIlFqvdFKfkSP5YqKUI1pOM9mdWMi6NOLp9Gclq2hXvh+K3P2yOSOXcAkafKhH+8Perug6xPpzs9s3EwBaKTLKSOuO46g8etafi2TzdIQnkpJ/Mf4gVhxaeZVtIBP5BWQFpAM4z1/wA+1ebmEXQqKMXpbudGCqe3pOVTWzO5s/FdtcMEntZ4n45jHmAkkAYA+bqR2NS38/h7UNg1GS13ROGT7TmJlbjGNwHPI/Ss7TLHT9sV9b+Y5G4F5W/1Zxzx0B/oa4+/+y3Go6xcahJdpJcSI9p9nOPkUDYfYgqM/wD6q3wkJVNZP7jCvGF7JGuPhnpk+oz6hp2qLMs82+QuRL5YzllXacHPT5vSulv5LPSoJ7eSGFYGj3+XbxFTjOBuIwqrnFeT2WsataX7mO9kRFy29ECMe/LDrnnrmu3OtWd5Bfyq/mSmH5vMJLMDnn6Db26V1zpzclGTujl0gm0jG1e9jtYpis0PnDIU+Ru3H2bofrXDPKiNsJ5K5+lWp7ya5JMkh2sxcLngZ9KqRxne0rjDtyAf4R2rCyTZ0pWRLErPMAsZcBTKwPGFUck54GOOvf8AKrI8RazJ/o1gqadAGLlhgv2A5PTgdBycZqpt3BmYkBlKcehIP9BTHhyxZWzMRhQ+W5Pt6/nVKtFMylKWy2Om8N3OtS+KtG+063dzRtexFkkkbDc9MZxX0IOgr5p8JsF8Y6KpuBJJ9viBSUEkDd27V9LDoK6I3tdmMpqeysc/46IHgnVsnA8g9fqK8CN/Fpl7a3ki7447iN2QdWVWBYfkDXvvjnjwVq2Rn9wf5ivnHXbeVpTcJhlSJjgnkkHOMe+RVJXRjLc9H0WWGLTfEviOeGDyLeSWK2KkOrQxjJVSOzOwBx1wB2rzC91TUNZvTe6lctNORtHGFQf3VUcKB6CvVvEumf2F8IZtMHDQW8MchHdzKhf/AMeJrx+JgRjuM1lJWjZDg+ad2WooQQHYnB6Y60+7srhmCmVFUnAkbhfxA7/XirdjHlEfsq/rV0gMCCAQeoNc/M0z0oUk4mLPp62cIafU5cHgRxR4J/Nq1r7QPsvha8ltYw4idXmmcjewHXHHQZBx/Om/2Vb3ZEbAoFBbKuFPpjJzxz0rqDEt1o0+l7HPm5ZirYH3cheDnt+OK6IO0FJvVs4q6ftOVbIxbq4hTSZrgWyC1+z71bdlQzLjIAPXn/61VfCb/wBra1FJdsJHWFo1ycZwqrx+Bb8zVyXRYpikSJJg8lc7gCOnHfn+Vc/qVjPpt3JFsaOSCTzSUP3PlHII/wAis6CUuvUqo7fcdDDqMCokSszXG7yvLA+Y7SV+mOM1dIbnLf8AfP8AjWZotk0UKXd1n7TKmcN1XPJJ9znNah6Mp/CuasoqTUD7DByqyoqVbd/1r5vf5mjoNrcXXiO0ijupYbVYjJcIjECXY2FU/wDff5ZrprPT1F9PLLI0oZSI1Y5Ealug75yP5Vg+HdQisr9vML4dMAJ9QTnkdq6S0uo32MGyHZkyeoPXBrqhSg6N7e9+J89iq18e430Tt+H+ZbjRrcEJMwjzuw4DbT3IJ6cAevT1riNZ1RHae9kyIYQVRDwQo6D2JP8AP2ruXILxoed7Yx7Yyf5Vwvixo9U1S20xIYy64EkhHLMwyMkdQq8/U+1OhQ546GWLxcMI27f8P0OOLXV+63FzIg67F2BtoPpnpVkcDFauoaBcWZL24a4twOoHzr9QOv1H5Vkg5HHNdDi46M8ynXjWXNF3FrW0qHbA0xHMp+X/AHR0/M5rOtrc3c4j5CdXI7L/AInpXQAAABQAAMADsKxqS6HqYOlr7RhWrpd7HBbujsSwclEXljwOg9PesqtjR7FZLaacffdwp47AcfzNZqpKn70FdnRiVGUEp7XRW1Ga61BVjxHDErh8ElmYjpntj2piGQAeYVYnuowPyrRls2U9KoXhNpGshUnLbcAck4OP1FePiHVqT5qmrN6HsoR5YaIzjq0VpPqcX2mZZJI/LSKMnDMqEgntxz+QpX1W1nYvGBJbRxqC4zvdggLH6ZyPwNYl3aSvbSR5V7tpWySxAU/exzyOoBx608y/ZZilxcRxWssplPGBuwPlHoePxr04zVOkqadpI8ypNSbt1ZDrmqWdhfvapDI8gRT2UgMucH86jtr5b6xu9sckEv2cSCNwx3Bd2Spxzlcj8a5zUdZk1C5SCWKDi5Db0OWIycAk9VwentU2muPs21bnEksis0cUahSADnccZBHTj1NdalornKm3KxcMEUiNySHO7JPQ/wA/wqtFMsT+W5MQyQytlgPp9anmcQuH3Bd/BB6H/PrWs+lafF4eh1q7u7S4aciNLOGX98oOcsfRlI+6eMZ5rns2rHRUlyq63LOieHbvWL63jCSQ2sgLG4K5AUdSPc9B/hXpun6LpmjpG9naRxcHEhG6Q47kkZz71y3w/luWtbn7D5d9YWypFjGy7HGeU3EFV3bQRjP4V00ut6U0U6vqNsuAVkUsySJ7EAZBrknCUXZoyjJS2OPuDBH8V7K2WKPf/aEExUD7jMoLY988ntzXuY6CvARPaXvxS02+tWd4LjUIHjcoVYcKvGeqnb9a9+HQV6VP4EjPk5W33MHxspfwbqijqYcfqK8MtLdb3xLotlhWS41CNXB5+RTvb9ExXuXjguvgrVmjJDrbkjAHUY9a8Eh8WSwalpd7PYLJb6fO7GENjeHQpkE8Ajd3rVK6M57np3xCR5fAWsNg58tH/KVCa8Jh6ufwr0K98YxeINMl0DTNMjs31HEA86Rn5YjABGAvbnH4Vxeo6Zc6Lql3p155ZuIGAcxPuXlQ3BwPWsXL3Wh01qmaloNtrGPapqhtmBgUdwMGpq5D2I7ImtApuVDHC4JP4V19lAlvZQTTkKJm3Kf7rEfKT6cA1xX2tLL9+6swHy4XGeeO9d9cQxtZ29uXMsayxxllXC4+7x6nmtoRurnn4qTU+X0HwajaTXClbmNQY1JBG07jnIOeh6cVxvji3D67aXO+ORUjUSIr/Mqhzx6cg8g11unaSltMJZ5EkfcFjx93PY8964e+eO71LUZlUrG08u5j7E5Oa2oJyvc5ZOMZJx1Hf27b7gHikUt2BBqwmpWspXbKAcjhxtP59K5DzDcP57JsyAEQfwr/AI96sQOHDAHOODzWbw0bXPap53X5rSs/lb8jrrHUbdr+0khLuHSRJQI2+UhiD/Tp6VefxLaW109qkU07MysHhwFyRjIb1G39K841N5YY18h5I1cMshSRhuyQcEA9ODxXY+dYeLobQ2wC3dtEFkt0BQpGeCB2wrDIPTDdua2g+VpdDx6zlKUp9W7noVlqEt3ZxXksSRkIefMVlz65BwO3B9a5qzeCfxPqT5V5ECiNgcjAAViPU5GM+1Ym2xiR9O0uR5XdxJLHuDbmGSh4A4HOW6dKgtZzp8sFyh/1OAwH8SH7w/rW+HtqlscWZVJVIpPd/od3WdqWjW+oozDENz2mVc8/7Q/iH6+9XiXd2VCAqkguRnOPQf41AhnjvZFeQPAQgQEfMrYPP0OMfWuppPRng05yhLmi7MztO09YtPVZUMc+5vMKHqQxHf2FSPaSryhWQen3T/hVPWNeTRr2SOW3doyol3gjAB4P6g1mJ4+sHOEt5WP1x3xXHOFO9mfVYbF1vZxlF7mwQQSCCCOoPUVtaRq1jZqllcXCRTyhpUV+A4ztAz0HOeuK5ka9FdYaSxmiXkeazDjGM9/9oce9ZZvEv9WcrFJGbeHy3WQYOWbP5YxWMKS57XOrF4tTw6fW560ypzkZx39ff865jxle2+lWtkZLVrgSzMdgfYPlXg5/Hp7ViafrN5pyeVE4eA4/dSZIH+7/AHfw49qoax40nv50h+xwLaxSh3X77SY9CR8uOcY596t0Ndjz1ik42vYVoUMv2+JsxhCWIORyCev4fpVTX5ANIVpNqFnTPPTgnFX4Skum7E5SWctkdCuP/r1meJJT5VvH/eZnP4DH9TXley5cTGF7v/h2dlKXNG5xt+iw3sciTgl0WUlR90+n1GB+daWjxkhpWwSV4OAOv/6qpywieO7fkNA0agA8HeSDn8q17BQkBx03Y/AV3zeli6SvO4yZftN6Ix9xB839adbWN3qeuGysIvMuJ2WGJPRgo5PsBkn0AqeGIRKRnLMcsfU133wn0pW/tfXpVy3mva25PYDBcj/x0fgahStqGI0j6s6K68G2CeGIhoUQsdUtYvMtL2EbJWkAzh26sHPBByORxxXnt1rVzqdlbeIni3/aMW10IztMdyuSc+zoVYemGHavS/FPi7SfDCpFdTbruOMXEVogO6Ug7UGcYAyMnPZc815F4UR9R0rxFZ5G9YItQUDON8UmG/8AHZCPyqqW95bHn+15aiSept+G76OTxVoqGBlzeR7fnGAc19BDoK+e/Cojj8U6UWAz9siUHJ6k19CDoK6ZxUXZG9Obkrsw/GShvCGpqwBBhI5HHUV866xHIEvkBbYCXUZ4wMHP86+j/FQJ8L6gAu4mLp+IrwDVLQzXDWjIwbcVOeeScf1qU7MJ66HM2tzLDLFcwO0U0bCRHXqrA8EVLdXNzfXM95dSyTzykNLK/JY4A5/AAVStifLKngqcH/P51eg5tLweiIR/32KTjzKxhTk4yLUNwsYysie4z1q4tzCy7vMQexYZrLsAhvAsiqwZSAGGeav3CwQQ7lt4mdiFRdo5Y9P8fwrndJdz0KeLlskW9NuI5ta07DKI0uo2Z2IAABzz+VegX0rz2rMoUQJJGd27Jb5x0x0FeZG5jtZCGCKPL3GOMgbucZx+NN08KL2328ZYDjjrxTjaKjbVMyruUp++rNHq011amOWNw3lhGBzGwXAHrjivIbnMcQtmXax2tMnYMBwv4dT7n2qQB5YJQ8z4SJn+Zi2SBwME+tQ6dbNqmpWFs7Mq3c6xs3UnLAEjPU8mrpSum+hlyvZdR2mRQXl4Y5jlFQuVz97p19q2buzify1jCxbRj5V4x9K9CsdJtvDOmX7WSXNxHtaUQNtYgAfdBAyR3Oc1wjt5kjOAgDEthBhR9B2FXhpe3k3bREY+LwsYpP3n+B0eleBNH8ReCb1rf7Q+siN0UyTfLHMo3LhRgYbjrnqfSvOPDmqTWSX9vHCWNxCM9AYyMjJPXA3Hiu40HxFeeHZ5pbRIZBMFDpLnHykkEYPB5P51x+pc+NLi5hhe3tbxpJoo9+4IxXcwB9A2ew4I4rVU3Cd+hFPERq00r+8drbwDRdDnOFWaddvyjHJAAH4LmsMqGUqw+UjBHtU8jbLS0tll8xIow2Q2QC3OB7AYH51DW1GHLG/c8/E1Oadux0uh3Us+nIp2vIuVYlscjAOeO/B/GtMREhtz5dhjIGAPTA+tczoUky3lwY1DRRhDJhuSTkcD6d/XArpYTK2XdkKtyiqpBA9znnt6VrFp7Hn1oOMtVa+px/jmxlvYLaaJA2+MxsAcEFWzx64y35Vx0FjbwTxPHNOzBRuyAoLDk9O3HT2r07WwV0i7nRQ/kSCZQeh6Bx+pNce9pbainnwMY5P4sr3/ANof1/nXNVjeaa37HpYOu1TUXsiGRHvbx4kYBRl13DkEgZ4/AflV20thDc3kgJJkkUZPoqKKgj024nvWcOkZMgIG/nb3IPr1461pYUElRhSSQPqc1NOD9o35HTWmuSy6kN3K0Ns7oCXxhQBnn/63X8K5vtXQahIIbKZydsm1fJIOCCSyk/p+lYqwK1p5sRJMf+sQ/wAI7MPUdj6Gq9onPkIVJ8nMbWg3Ae3eBj80RyuT2b/64/WqfiV2+2W6gxgCIn5s+vt9Ky3ZhE6gna4AYeoBB/mAaZBB53mZJARC+B9QP61zPC/v/a3/AKsdFPENRUUtSe3mSDQJ7VQokluckY529c/mAKntBi1T3yf1rKUYY/pWxbf8esX+7WU4qLdurPQw7urkmSPugluwA5J7Cvb9Pih8O+EorSYCJbGzUSM/yqzlcnB9S5I9cmvG9MtprzVrO3gQPK8ybVY4BwcnJ7DANd98QdRa7n0zRVglUXM/2qSY4KhIyThD1Dbtv0B681lKcVuPERlJpI8l8QX+pahrVzf6zFJb3k7ZaOWNo9gHAQBuwHFdN8J083xdcRbd6vps6lfXlOK3zqfihIzHHry3kPeHUrVJQfbOM1a0nxGulSiW48L2MGoTI8f2zTIlVRyMBx6EgEke3FVHFU5rli/6+Z5zwVSnPnd/69DG8N2jr4s0EbX2CeOUHI6ZA/Q8HHcGvoAdBXj+k2sUV/4fnXCwrdiKNZF3MWZgx2854OeenNewDoK7OfnimXQvrdFXU1D6ZcKQCChBB7ivEvFFlJY3izQkZLDtuPykEH9Bmvb78ZsZv92vL/GWm/adMnfdtCISAOM1cY3QVHaovQ8XuQYtVuA38bs3THU5/rUsbhUlUk/Om0Y9dwP9KrXhHmqynkDGRToNzYLNnIqE+xEk1Im+ZSCpwwOQfetITNOouFXLH5YkPQHoT+efwqssYdewJq7FDMJImijUW4BG5vTPb3rCactDqw04xvL7vUyrjS7iHF010GuSd3C9+Oh/H0pIdXltJY2uLNCyEMrIfLzg+nI/LFbF4Mog27iSR+n/ANaqIRGj2yR5jzna3cf5zVTSSTsJyb3F0cXOtXosdPtZHuGVmA3AAKOpJOMDoPxFb3hrTr238b6RbajE8UsMbzeU6gFcK2OnB5xyPWpfBl9pfhnV9TiuVlQTsqRzldwjRWbg45weDn2Femxm3u0huozFMqg+VMpDAZGDtPv3rlrVXByglozuw2HhNKaeqZFqWppoul3OpOcfZ4yyjONzdFX8SQPxryu91iaQ2bzQ28kku1pHt4PKLhweSAccEdgM10/i3V1udQGkIrNDCN8zgghn6bP+Ag5+p9q4G+a4hSwUsJEUogbGArLu4568Hr7Vpg4OFpPcwx84VpuFr2NxHWRdyNkfy+vpUdzGkqKrDPJKn0OMfrkisyzu7O38T2S3lxLLbSKY5HhwpG44UnrkA84rpdV0q40fUpLK8jAlibIPZh2Yexr1Y1FJ8r3PAq0XRaknoUhgjjpUV3cC1tnlK7mCkqvqQM/lS3E8dpatNJkKgJIHJPXpWDfXlzK8rgK6GIYAlUbVI7A8k9ziqnNLQmhR9pLXY2tF1kabqFnNNk21ztguOOdr4yw9w2G/CvQTCFd42k37GKMFfKZB5xjt3xnvXjBlnkSBHt8Ro6nd5qnd7YBzzXfeDVvF05p1mtRa3MpeKNnyR1DEYx3GMd8ZrKlG0211NswpxdNS6r+rHT3Nqt7ZT2Z+7PE0X5ggfrivHrK4l0+53m4DMMqY9xOQOufpivaFyrqe4INeK6laeVd6xdhsfZ74w+UqnkuT39OtKvH3oyOfLbyco9NDrYporpJAjA7WMbgHlWHalnuY7YRmRgvmSLGvuSa4+fUptP165lgOd8zKUP3WGehqK71ibULiKeWML5DgrGGyuAQfzPrS9r7vmdioPmv0Nu/vDDpElsUBK3jeUxOfly+VJz2yD75qhp91dyajbwWsUDyyP5arITtfPGDz0NV765keV4SE8kuZ1cYyc9P0/Wl00/Ztcs5ZmEcazJKJAeNvUH8+K53FXv1OhuSehoXNu9vKYpVUEgMNrbhg9ME9f/rVJbWch0bULzYNkZWIMT/FuHT8xS3szzSwiQAPFbxRMF6BguWx/wACJrrtM0qO5+HekhvMEmoa/HbgbsAp5uSfyQ1vKTUU2ZU4J1Gl0OB1a3azlvLZhgxOUwfY1seJ1bSI7OKKKKCZ97MUJO9eMEg8DnPT3pnjuJYfFmuxLu2pdFRuOSQAtL8SXjHjvULW3LvFaCO3QZ3EkKC2P+BMRWcmnZM1p3inZ9R3g651afXI7uDYUsz5jtsHJIIC/jz+Ga7l79tT16B57HHlxMd+7Lxq5BbB6YA2gD1rU0vwtYeDfA8c2oCT7aIxLcKrkBp26J9BwPopNZWlOxTbCr3OoXA3Oo/5ZoM43E8Ko5/E8VmqcJ628jPFV5RtBy13+S/zNW80lTC11ZMJLRQWJHVMdmHY1z17dRWIEshyysn7sEbm5BwAfXip7m9d7iOBme02kyXTH5SEXnafr6fSscWVv4k1K4u4ndLtlad7cR/ewP4efpxXGsDCFZSbsjdZhVnQlyq9up1HhOxd9astSvNpnknUxxr92JSei/49+texjpXjvhPUkn1awgZxuEybTnrzXsQ6V6DVtDLBVPaQcvMrX5xYzf7tcrc263IMLYAkymSM4yDXVah/x4Tf7tcbBclrMvIxLRyupPrtc4/TFaQWgsS/fR4f4t8PXWgSr9omjlUyvGroTzjnoQO1ZixND+7cFXQ7WB7EV1XxFivLnWxLLn7KE3RKqAbc4BJPqcCuVRGO2ONSzngD1rnTa0l0NZtN3iXbPBBJikc5wCBwB+Nb1g/mP9m2jaFzjuK7ix0DTbext7U2VvO0UYQyCMbnOOST15OTXL63pw0jxDm3j2W9zbq6LknDKdrDn/gJ/GqcedWZbTSXJuZWq2EoCmNtoIPBHXp/n8ay47dn+1Gd/migaRVVcZ2jpXe6RpcOtW7yTTGPY2xf3QcZxk5yfpVC68MzTxyeRZ3C3GWQOxVY3QjByOp+lY1JShHkmvmVCcZNKWjOQuA76zL5i4iMigsp/hOMkfnn8KswanLodxqEVpdyQMzeXGA5XeQ2D04J96n1Pw3eW9+srafctuCLiO3JLfIAQD0zwaz7vSg9wTNFexEHcgvMeac8knacHnNa+5OV1rcbjKK/yJYtTUyTyStGmyMsoLck59T1qndXFzqENrCBAUgfflHJycY5z+ND6ZbO6mcFtuRhWKk+nrUq20MSARRICBgEjnjpzWyjrexhsTaBpLar448P20qRiOS7XzAnIKpmQg/UKRXsvxIezj8PxXN1amWc3CwwSI4Uxlgx5OOV+XkV5DpOqT6FqdpqcMXmTWrl9kjAIcqUOcdsM3Tvit3xH49uvE1lFZT6ZBDGkomBimYtuCkD7wxj5jxWU1JzTRrHldKSaOaupJF/eXE6sOm1YzgA8cVW86B4b4s8bMCwXI6gLgY/wq4rEAkREeu3BP6GnZXYXZQFzzuwP0rZp7tnMly6Ih03TJdVa1t4RGC6p8xx8oABJI69q7208NWVqm0l5ec9lH5CqXhCCIefcbVErfu0AHJHUn6dOa6rZ6kY9TW9KNld7nm4yrOUuRbIytT1C00HTDcSskUanbGD0Lf55rzhbuyljaF1jZ7xt7gjJkZSTk471reIr8anqTs0TTQR/LCNoxjueQev9axnILf8ea5H3dwJI/Ssqrcn5HVhKKpQu92UTbxvNqsrsDJHIVQZPUnrTb3TraHVILePyxHLjeoB55wc896vuqPvP2AbnYMxBZcn8qSW1glZZpEcFCCVdcgj3OemazUdLHVt1IYrKyFzcQySKGjVVjXdghiemM9FAJx70JFZtEsVzOWdt/kx7iPmLDp9cd6vpHFNemZ7fO9QPkiJ+bP3ifpVtLK2wGW1GVO4fKBz+NHs3qHMZc1q1oyIxYhlDKWOSR7n14IpsUe4WvytlnYghiOBnpzWzdWfm6fvJOYHzzz8rduPfH51lC1ZruPO/YIyQwTIznGP0rSafKrGcLqbsUNQR0aYOHJ4JLncfzqrFuuLia6YZKnzTj1J/l/hTr7UmkgZI/8Aj3dwScYYgdM446/0rRtLd49Ek3wMfNUuCVPIxx+grFu5ttud1of9nX7P50e9TjIbOQfwNdfawQWYMduEhV/m2hss/uxPP0FcPolxBp1gLh4h5gUNtz1OOKQ6zcB2lkcl25Y/0H8q0nWhRgk92edPDTr1pSWlu5v65Nb3EIaaBJGL4ViP4R29xUOn+HXtrKW+to2lin4GDl41HbHU855Fc3ea1LLcQrIVKhcKB255rvdE1i0/scS29w6mJShjZT94DqP/AK1TVjCS5m7NFUlVox9nFaPU5nw1aCDxTZTM4jxewxBCvLnd19R1/GveR0FeFWl3K3jvSYGPAvYW6epHevdh0FQpRlFcruejQoypOXMrNv8A4BkeKA//AAjN+YzhxFlSDjkEGuU0PzJNNLSn5nkZyfUk5rt9URJNMuVkOEMZ3cZ4rh49T0vTbUJJeREk7tqHe35CtaexjilaSZn+K9EttY0oJPCxlgOUmRjuRe+B0P0Nc34R8GWc6XL3aw3kom8tTHKygJtBGQCOTnPOa6SfxDBfzG0tN29kON4wc9uPSuW07wj4qsLiaW1htm35/wCXqIqx7HDEGhxjza7DpT1TPVIdM2LwiKe+K4n4gwS219pZZka3kSQIoGGSQY3E+oIK/Qj3rq4PC6TQp5d5suNgZ4yuQpxyMg+tZGueEdee5097RY7jyXclvNHyAgdn6j8+laylG2jO32j2sUvBoE2lXgQRM0MwYAtz8yjqPTjj8a3keRTgFPw7Vzd7ofjJL5bhbaTzoxsE1u0Shl9DgjcPqK2NEi1p4JhrNusUyyYjK7QWXHcKSOtY899Gi6kU4pl/yiw/1ij8TWfc6BZ3km6S0tpmPXK4J+uMVpGKVSCeB3yaesjLn5RgjGAvzfnWcqcXqjJKS2Zylz4M0h2O62e3bHWKUkA/Q5rHuPBS7j9lvQfQTRkZ/EZr0oF5BhkUD0x/Wo2tAWJC4+goikgbXVHlk3gjVYySIo5PaOVSfyrAXTGguWhnjZZY8qyNwQRXtrWQY429+uOtcNeTQaf8Q5TIwEBKJKx6LviXk+wODTXqOMU7pI41tLjJyqAe2BV7SPC0+oy5Plx26n5pCmT9AMnJr02fw3p1w26W2AJ/jj+Un8utZaj+ytSbTI42eDcGQn7wDAHPvzmrpu71MKsPdvEms9PgsLdYbdMKByT1b61ieJ9ZS1gaxgLGeUYkKqTsU9uO5/lWzqEWozutvYDllJJ6Hj3/AKVzFzoF/DlpbObryxXP4mtalWysjlpYRS99nM7nY/u4pGz0AXH86jJZiVPmA55z838jitxrSTnrx2qNrdjkEO3/AAI/41iqlzpdIyDa9CdioBktt6n356U5VjQAmaHA6fKP8a1otNiPJhj/AOBLn+dXE09UOUGM9digU+Yr2Zg+azE/6QT9Ij/hUqeY/IuCuPVVGfzHNbhsDydppfsbA5IPPqKLidIxy8vkzrl2UxkN8qgEfz9+KisBIwcZUxqeFxzk1vtEvkvlRtCkn24rItIrW1t/PN8swO0MIxgr/wABPJ/Kn7RJWkNYeUk3HoYGvaasW24t7YgEsZygJBzzux+efrTtH1aLyY7NkYMi/Kyt94D+VdfEbSVgsV3EzN90Bxk/hUzWJweufWp0vdEWaVmc6ZxK2EST5fmY9cDp/P8AlUL3CM3XIHTB6+9Pu9G1u31Ga/tTG5bgKjfMVxjGDwfzosotaG2C50+RQq8SMg5x2J9ay5E6vOxptKyMzVGR44/naIgn5icfhWl4Rv0t3lsjOGSX94hJx83T9eKutZ3h+/ag+xjU1FIdRspLa6is8+TMrkCLdwPYdqWJoe1g0aU5uEkzU04LJ8RtKdVJxc253o/HXGD+VfQQ6V8/aRf6jqvjnR7y5tnQm7hU7IRGoUN3BOe/XrX0COlTRTjTjF9EaOXNJy7le+/48ZuM/L0NeEeJ4bi18SNaWpSG3lZXGxCNobqAc8DOele8XkTzWkscZAdlwCa43V/h+Nb2C6vBGFOcxplvpzW6fuuzszmqQcqibV0cDYx2ccpRX3SgZKQKzke525P4mtS0aW6uhHYw3t7L0xChwPq7YA/OvQtA8JWPh6ze2tHm2yNvkLNkscY/DjsK3UjWNQijCio5Fu3djUJc21kYnh/Sby0iWW+dUk6iCJiwX6sfvH9PrW/SUZpt3OgR1BU5FZbwksSB361qmoyntQNMyjbHuOPpR9kXnAA/CtUxj0pPLHoKkLmSbUjv+lZupeH4tSYO808UqjAaNyB+VdP5Q9KPJFF2B5dqOl32jzADUgcjKhZyrY/3SaxDaxX13K1zBcz3UknzOuTv4AHOcdOPwr2K80iz1BQt1bpJjoSMEfQ1hXPga0cE29xJH7OAwqakedWu16FU6jpu6V/U5OK2vtPs3eGLUYbeNCzbNwCqBnueOKo6XrRjnkl1ATMZSD5iNuKgDABB5PHfNbureCtVjspBaJDcsQAFQhSRn3wP1rA/4R/XQwVtFuwfZQR+ea5lTdGXutsdWv7TSUV9xozeJYdPvC0ai7i2hojHJtxnghuM9e3FdjYyi9s4bmMo4kUE+VJvUHuM8Z/SvPpfD+tQth9GvPqiB/5GpbGx8RaZI02nadqUEzfexCdrf7yng1bqTv7yMfdWiVjvZdMtpnZpYIn3AZ3IKpT+GLCfIEKR+6Lgj9a3tLW5udItJb6Hybx4lMqYxhu/Hb6VaFv7VsrlJs4weDFUZjvBn0ZKQ+GZYR8xDL6gZFdsIAO3NOEWBjFP3h3ODOhejDPsKpzeGHckrcyoT2zkV6O1urdVB+oqJrBD90AVSlJCaTPMZvDeoCN1W48wFSMZI6isFfA19tGSQ2B95QAP1Jr2dtO9ApqM6d6oaHZ7gk47M8ih8DXDOpnlCjjOzFdK+nkEkjI9hzXaHT19MfWmHTgf4c01ZbA03ucU2nn/AJ5n9KYdO9UP5V2p03H8NMOmj+7VXFZHEnTv9k0xtM3Doa7VtM/2aYdN/wBkUrjOT0jTPL17T5Bn5bhD+ter1zNrpwS9gfb92QGumoYBWbrmtWnh/TGv70S+SrKh8tNxyxwOK0q4v4pHHge4P/TeH/0MVLHFXaQkfxS8NO4DPeRr/ea2bA/LNdTaanY32nrf211FLaMpbzlcbQB1ye2O+eleNWUfhk/DK5lvTZjWd0vkEMPtBbd8nA52/XjFULS7u7f4a6oiswtbnU4ofY/JucfjtUGlc0cE9j0q6+Kfhy3nMcZvLpQceZBD8p+hYjP4VuaF4q0nxGjnT7gtJGMyQyKUdB6kHt7jIrlPh14Y0e78JRaheWFtd3F00m5p4w+0ByoUZ6dM/jXKW8KeHPi/FZ6eSsK3qwhc5xHIgJX3A3f+Oii7Fyxd0j16y12w1DVL/TYJT9rsXCzRsMHBAIYeo5xn1rR4614D4g1a70n4janqFlLsuIbskHsRtUFWHcHvXUeIvigl54ft4NHEkN/dJickc23YhT3Y9iO3PXFFwdN6WPQrLxDp+oXepW1rI0r6eQs5VflBIJwD3IwQfeubX4reHHCkfbsEZz9mP+Nc78JGBtPEKjtHEf0krA+HS6BLPejxB9h+zi1jMZu2UAHJztz3xjpzRcfIle57LoviLSvEELyabdrN5eBIhBV0z0yp5H1rK1r4gaDol29pNNLcXEZxJHbR79h9CcgA+2c15f4Ouns/FOpXOllzbwWV5IpOeYl5j3fjsrQ+Fmi2Gt6jf3OpQx3ZtkjKpMNylnLEuQep+Xv6mi9wcErnoGi/EDQdcu0tIZpYLmQ4jjuU2bz6A5IJ9s5rqa8W+Kei2OialY3GmwpaG5jkZkhG1VdCuGAHQ/N29BXd61qHieXwjpt54ftopr6dYpJlYKSFZMnAJA+9gHngGhEuK0a6nR6lqMOl2jXVwk7RqQD5ELSsPfCgnHqe1R6Tq1rrVkLyzWfyCRtaaBo94IBDLuAyMHqOKzfEery6V4fgku7YSPcyw2s6Qscr5pCsUwCWIycDHPFUdC1lY/CWqJZwSRxaIr2kHn58x1ihUqXUqCDz0xTJtoT614+0DQ7trSaaWe5Q4kjto95T2J4APtnNP0HxzoniG6+y2sksV0QSsM8e1nA67TyD9M5rzv4W6NY65qd9c6nEl21vHGyxzDcGdyxLkHqfl7+pr0SPwFoEOvJq8Nq0U0bK6RRSFIkcfxBVxz6jp7daSuy5KK0H2vjXSbvxI2gxC5+2rJJEcxYTKAk8/gataf4n0vUtZvNJhmZb21co8Ui7S2OpX+8B7V5hoTf8XsmH/T7d/wDoD1ga+bv/AITvV3sfN+0xXksqGH767eSR9Bk0XK9mr28j2/xB4lsPDVtBPfibZNJ5a+VHvOcE/wAhVXUfGmlaXo+n6pcC4+z34Bh2RZbld3I7cV5P4i8a/wDCT+GLC3u0C6hbXO52QfJKmxhvHockZH4j21/GbY+HHg8/9M0/9E0XEqe1zq/+Fq+HePlvuT/z7H/GtHXPHej+H9TOn3ouvPCLJ+7h3DBzjnPsa4jwdqng3UjpWhz+H4pdUdBHJPJBEVZ1UsSTu3H7p7VR+IhjPxItxNs8opa+Zv8Au7d5zn2xmi7sHLHmseh6X8QfDuq3UdrFdvDPIQqJcRGPcfQE8Z9s1Nr/AI10nw3fR2d+LnzZIxIvlRbhgkjr9RXk/wAQ18PQ6pEvh422PJYzizYGMNkbMY43dentVz4nNMNa0r7Tnz/7MiMnru3tn9c0XBQTaPYdX1O20bSrjULsOYIF3PsXc2MgcD8ayLTxfpF94fvtaiW4+yWbFZcxYbIAPA7/AHhXG+KfiVout+Gr/TbWG+WeePahkjULnIPJDH0ql4dbPwf8UH/ps/8A6LjpOzYlCy1O+svF+kX+hX+rwC6FrY5E26LDcKG4GeeCKs+H/EOn+JoJ5rDztsEgjfzY9nOM8fnXm3hds/CrxcfQv/6KSt34PHOlar/19r/6LWmmEopJnTeIfFGl+GZbePUBOTcBmTyot/C4zn06isu1+Ivh68vILWJb3zJ5FiTNuQMsQBnn1Nc38ZSftekAHBMU+PzSrvg/VfBmtana2Fp4fiTUIYRN57wRD5k25IIYnOTnOKLu4cq5bnpAgUMCOxqaiiqMwrkfiRY3mo+Dp7axtpbmczQkRxLliA4JP5V11BGaBp2dzybwl8MbPUNIju9dh1G2vPMdWty4jG0H5TwM8j3rtNb8HWV/4Qk0CwijtI1Aa3wDiOQHIJ7nJ6nqcmulAxRSsNybdzxLSrnx94Nim0yDQ5biEsXXNs06KT1KMh6Hrg/kOa1/A/gzWZ/EZ8S+IY2hkDtMkcmPMklYEbmUcKACcDr04AHPquKXFFhud+h5JpvhzUZ/ivf3N9pNwdMmlug0kkf7t0ZNo57g1v6D8NLHQtXu9QaZrrbuFjG4/wBQCOp/vNyQD6e5ru8DPQUtFhObPKPhZour6WmtLqGm3VqJbWJY/NTG5h5mQPzFYfgv4d3Oq3M0XiCx1KwhihRomAVNzZwRyD2xXuWAOwowPSiw+d6mJo3hLR9AspraxtMCdds7yMWeUYIwSe3J4GBXly6B4u+H+uyz6NZSX9qw8tWSIyrJHnKh0U7gw9R7884r22kwKGhKTR4r/wAI94t+IGuxXOt2b2NogEbM8flBI85KohJYsfU/nwBXoni+bxBpuiQDwtYRXE6yqjIyhtsYBxgEjuFHXgV02KXGetFgcrnJ+MX1X/hGLSS2tmN8Lm2aRIIROY/mBYqCj9OcMFJGM1P4Via50e8F4k7STzv5wuIDGXyoGeYYi2R32+2eOOlop2FfSx4f/wAI74v8Ba69xo1pLe25BjSSKLzlkjzkK6A7gRxyPwNdR4f1zx7rPiGyN7pAstMRz9pBgMO5dpA++Sx5IOAK9HxRilYpzvujyTRdC1iH4vy6hLpl0liby6cXDJhCrI4U59yRT9H0PV4vjDPqEumXSWJurlhcMmEIKEA59zXrGB6CjABziiwc7PLfHXw3aVpNW8PwjzTlp7JMDf6tH2Deq9D256xeLdE1e68A+FLS20y6mubeNBNEiZaMiHHzDtzxXrBGaTA9qLC53oeLaXf+OtJtLW2tvCcX+josaSvYEyEDjJYOOcd6s/EDw5q2q+PY5bfTLyayeO3jeaJMgDeQ3PsDmvYNo9qMD0FFh8+t7HH6P8NPD2j3kd2sdxdTRNujN1JuCMOhCgAZ+orlPinoWsan4ltJtO0y6uols1RniTcA3mMcH8DXrlJgelFhKTvc43xl4XsH8I6kNK0K0N8Yx5P2e1QSZ3D7pA9M1zWg6Jq0Hwr8R2E2mXUd5PK5hgZMPICkY4HfkH8q9XIzRgegosCk0rHk3h3Q9Yt/hn4psptMuo7u4L+TC6YaTMajgd+QayPDsvjrwvBcQaf4dlZJ5BI/n2pY5xjjDjsK9wwPQUYHoKLD599Dx7x1p3iPxFpfhy5fR7l7w2sn2qOCLHlOxTjBPHQ9z0qfQ9V8bW+o6fA3hWCC38yKGWcWBVljyAxLb/TmvWsD0owPQUWDn0tYWiiimQf/2Q=="

/***/ }),
/* 27 */
/*!********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/videos/失控玩家.mp4 ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/static/videos/失控玩家.mp4";

/***/ }),
/* 28 */
/*!**********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/posters/失控玩家.jpeg ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAGQAQ4DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDxUUoPvRR3rsOYcKnjxtxmiKyupkV47eR0boyjg1ZTTL8kD7HN/wB81STM5Sj3IApGOadsJ4zU72N0kixvbyhmyVBXkgdalXT7wKxNrMMeqVdieZdykVzxkU3y8NStkNyOM0pZQ2cjH1pFDGTaTRgAe9OPOeetX4/7MFugeNmmzli8rAY7ABRSsJysUlxUgFaC/wBlgAtahgOoWdwT+JFDppgYbGum3DJCsp2+2SOfrVcpHP5Mzihxk9KTjpjNaYOloMSR3jHtiRRj9KiRLF2xG1wvrvKf/Wp2Dm8ikqg96YVOelaJt7dSCsh565dP8aiKxKcKnmerbiM/QelJoakUcDNNxV35G4EA/wC+zTGWMNhoiB6hzn9aVirkSRNnJqVkIFTRxoFA89MZ68f1NOkW3VRm5Yt6KgI/9CosTzamfISCAOtQlGY1cK25yWnk9sRD/wCKqx5ukrNu+zzvGIwu3zNpLd279fSp5S+a3QyWiYen4Um1+wrVmuNMdcW9k6PkHLzFxj0xxTxPpuwB9PYuByVlIH5UuXzDnfYyFSTPQmpPJlJI8tuOvFWbme2ZlNtA0OM5y+c1Cr5BOR+dTYq91ca0ZC/dJ9qNj4wsZx9KDJk9afHHJK+1EZmxnCjNINtxhDg5ZSPwpCGboDVtrS5VSWt5AB1+X/PpUsVrdFFkSGUgjI47U7MXMu5m+U/PynHc0hjbHStE2F6Ofs0uPXFQzRyQkJIjIxGcGpaZSkn1KRGBg0ozjgfjT2jyQO/epQqRjB5NANlUCjAqURZpPLPNaWFcaiA5ZvuL1x39hUkcoDA+WgI+v+NNkHCDtsBx7nrSKMEUBudA+vJPapFcWpaRcESRybSCO49DTo9SgeJNskkRBG7czOQPw71gOcYx1qe2QtKoB5NaKTMXSjYv3ElgXdYg7js/lhc/rmpY7y3YY8k8f7A/xrPxtO3HSnhCoz0qrsOVFyW5h8twIjgjH3R/jWftj5O5v++f/r1YVd4AUGonU5IGOO4NJ67jirAjoPl5bPYjH9animcPh5H255INMgiCnLc1MsW45HJNERMmuFLQrIkkuSSNpNVYvODkl3x9TXqmj/DNpo4Lhr6Q+bZxzhJLJlC+YpIG4Fs4I56dQawx4Sh1DxDNpNtcFGSWZN/2d3IEe7nYvPO3oPWo9orj5Wlqcvpz2kd2G1GO8uLXacpbShGzkYOT26103gnw5pPiKzum1OSTzYrmGJNl2UISQhRkbTyWICnnJznAGau618OJtDsJ7uK8uJ/IjhZ0k0+SMMXIB2v93Az0PI6HmuYt49SEL+VJcwwW86znYzBI5eitxwG4wD19Kd1JaMPgfvI0dN0HSbvxFeQGYx2cdtJLAN74kYBQPnZVIX5t+SBkADvS+MPD+keH9X01oPMubSVC09ublS4KHYwLL93Lq35H0IGn4V0ObxXe3/2zWXgmaMCSRz5ssytwVwSCVAUZ9PlFJ488Ovo9xZiTV21HMZiBkGHjVTwMZPy8kg9zmpv71rjv7t7HCzhZrqVrKOSC3JzHE0pYoMdM9+c/nULxzocMZP1r0nRvhpJf20c897eWpe2iuCi6c0gwzYOGViG45GOeeRwRWdpukyXviYaBLc+UfPaIzTQMpCjknZyQcAnBwBjkiq510E01utziMOYx87ZHuarSLIpx5j8e9d1rnhD+xdGTUBqKXHmXUkQjFrNGQoOATvUYbIbg8cHBbBrlJYSRyDyetCs9g1T1MouwBDEsp9T0+lOjhZoTsLZJ/ujn9asyW4AOKu6aVjXZJwnrUtFXMuCHy5N7cgdiB/jV1XjU/PGQP90f41qx2lvcHAZB9eKlOiEggjIx1qOflE/eOelkt5CMo21Tk4Uf41Ygms/JJikaKboA0fUe5FPvNMaBtoHB5piWmIguwlvpVKV9QcVsSSXlsxSIRSSj+75hCn2Oafc3LX0YBjSKIE5GScmqJUJIdqkc8cVHI8mNuTjrihtiUFclGGh+YKEXPAJ/Tmqbrsfg57g+opecHqacEJiOexBFSaIrODnNNIzjqPxqy8agZ7+lMW3eToKQ7ltbGdp44FXMshwo9amvdFvLIp5iK+4ZPlHdt9j6V1r2scKvMkqBFPqORVaRvtTgiSQsM4J4xXL9bbd1ser/AGakmnucZLE25AQV+QDkelM8k10V7ZtLIp2M0jOIyMfe9D7Y6VntblS6EZKsQSPaumNWMlc4KlGVN2ZllCzAAVZSEphmyD2wamERTPy8dean+/EBtAAOOnerTRk0yELzuBzVpImmjUE4X6dasJpqmMTvKIIh94Nyx+g9ajModVRMgKTz3qo1FJ2iKVNxV5FeRWQ+Wv3R+tX4dNiOzdfW3zBTgbvlyOh47d6jW3ferIuQTnmrrIGXCJjsRV6bGMmxv2S1iBEd1EzA9fmx/wCg1GkEIk5vbfPsGyP/AB2tO0sxgSMwBXqDT10XMylxiPBY9qTmkSk31Ov0fxmmjaSIzYWJdY44UMDFHaMAj5iytk5JPYe1Y0E9s3igas91HGkt3JcCGCdozHkllG/ZxgkdsHBqvcRWzfZo8ny0GGZeuPSpBpqXD74EEMRHyh33ZqU4rVrcUud6J7G/rWs3etW0oeCwheOQywzWt06rhjl96Hh2OB8xAxXPtcyeQ0IniRZdvmBZDiQjoSOnGT9KQWMuCFjYY6nNTvaQ/ZFKQDzO7EmleC6B+8e7LnhfUE8O6rPeyR21w0kPlKPPC4+dWznB/u4/Go/Fmpf8JFfw3pjtrXy4/KKC4D5y5bsoPf3rNEBOCfl7ZA/WqUiyvOQFLRg8ZqvdvzC9+3LfQ7DS/Ej6Xo9vYvZ2E1vHBHFtEyhnKyFyWZkYY+ZgFxxuPPpkaNcPpXiSTW4JbXIlldLcXTICGJIB8scqMj5cAHHSqUcEuwoinJHI9Ksf2XPEBI6naGGT6e9Lmgr+Y7VHaz28jT8SeIv7T0aPT3t4ISkonQreyysjMfmzvHTk8Dp2ri49Oe4J2TwsB/CGOfyxW3dWwdlWLd13bmHf1qe204xxvO4Vd42ghQefXFSqkYovllJ7nNjSPMbAmgA9PMP+FQTaU0cpRZ4CRxhd3P6V2keuXumWVrD/AGXbSRW7KUZlIdgHLnPfliD/AMBqmvje8gubqZrO3cyyb9txlio2BSAfThT+GK0unsYc1VPY5e5sHslB8+J337dqZyvHU5Arc025SKEeasgP95QTx75pND8VS6TLOZrSC6eedbhpJs5Drkgj8STXaaNe3moaPP8A2dYPOJIVgYxI3y4UjkD/AHifxrKra2ppGdRPRfijlLq2sbn5jKyvjJyO3tVGTTouqXJkGPmQDsa7DUrrWpbu2D6MAI1Eaxurtu2Orke3+rUH2qqPEOtW8s93caI5AjjDmVXdlUFmUlj0+Z859gOlQqdlo39w3iG3rFfejh79A8roQqEddwxWI8G5wqZZu+FPFbd3KFmYFfkIwVBzn6mqlsYUnL4ZhtyAvY1Ub8pr1KJtPJIMivk9AyEA0/ZmJ/3anOOR+NTtdPeTkSStHGoJABNR+Yh5LOWHGF6AdqpAVDGGIwM1MCLdAGcAnnC84/GlfdgHnJ6KKrvGxPz9fShgdVMAoxj3HFPt5f8ASAsWFGOT796ydYupLad4jyU9Kg0G9WS/KO3JHc8D1NfNty9k5n2MZp1lS2uegafpNuAokTkgHBPJrn59EkkvLgLC3yyNwF4HJq3Frki+YJEd4mkCebgYHso7j3rsViluH2tNI3Pyq3YV5312vhr1Kj0Z01sJSqpRfQ89k0eQKsZUB2P8Qqb+yLfTVWab/SZd2UijbAGMdTXcXkF1An7uNNgHzbv8KxY44rqXdMBGOBhR2rehmsq6u9F5bnFPL6dP4dX5nHzi4ufnnLZJJAx3/wA96rfZ3aQRqCzdtozXa3ulQyxhhKIVXgjnOKyGj8x1jSNVwMYUbcj3xya9vCY2NRWieRi8HKm7yG21mxtVYSKxA5jyMj3qWxtTJdjZCjBeDuPFdBpumKbdlZUjJ6YqeLSBFfbF+ZsZweAK7PbLXU850XdWRjvbNJdmQptJxlcdx61qQq/llQFOR6VIgaS52xqGB4GFz0rprHR1lCSzJhiACFGBU1KqS1CFFt6HInT1CbcHkfUA0sVgUAJI46DOK7uTSIs4dVVfWqF9YQxKBHtwT+NQsRfQp4e2pzILrIQhIPGeSQKmSEQlyyiYkZBxwPwNaa2K8/0705ba1tS1zeyGO0j2h1AJaQlgAiAclmJwBTlVilqEaTb0OcFu1zOzygpH6opPNRpDEH5G4A844/OotU8L+Jtc1SSQ6tDp6qzJHbwZbyU7IdpAyO+Cee9QSjxF4YZTrqrqFhwhurZRujXP3mXAb69axhjoSdmdE8vnFcyNlNyY8kDnoW7VZntrloVZ3WUAgYOcHP8AhUkGnrMiyxzB4nXcrqcqQehHtSrZzswCuWQdMkgGuhyi9Uzl5WtGVI9Emm+Ybg3U4PQdutR6ajQ6lGt+GWIPjO0ke/0rS8y4tbjzMBol6pu3AnucUtrrEk90gnRzFu4G/G0dsACrg76y1JmrfDodDcabA8PnJe2JiD7Xk85tqttzjgeg7VyfiXQbWSRLXZG07IpiaE/eVsEHkD174x+FdpFrdta28dv5F0QhLKJZo2MRwR8oI5wT3rltZ/faj9phnuHl4P8ApDB2fpkccEdseldb5ktbW+R58ORyXK238/1OVufCk8cHmRWcsrRylGXaWwR1BxXSeE/teiWD2kujXUyPIJiYw3y4zgEFTnrVSax81wEsrOSWaYnDxd2PCgE4AGeMmm6jYX0LrFLbRrvGQvlr6kZ4z3BpVOSMVyS1Z2RjSneNWD081/kdO2uRrKksmk3YaOSR0wzYG/2281m3N/BqekR6W5lREcAs6sN4zwOnbnqe9Yll4R1PUALlbG3kiLFA3yD7oyeCQf0reg0SPQFRtStI4fMPyOhTPHJ4XJ7j0rNa6NkTpYeLU6cWrd3/AMA4TxDpK2t8yhlZMblRV5Arm76R4YvJjwobkoBjj3rtfFMslzqStaM3lqm1vLz0z71yeqBPNJKbAecu4LH60knH3WXzKXvIxVyxYkgDac/lU0YVuAw55JPFEdqzuTtDLtbp9KfFCC3TaufqaoCGaVlGAAv+7yfzqrucn5V/OrzRNJcLDFG8jscLGg3MfwHWtODQEjcrqN2IZsZFrbxmeVR6uFOF+hOfaplKK3LhCUtkZN/extLLI65Z+pqDQbeS5uWQSCNZAY95IBPfGapzP5hCEDjrWro+pCymGbdWWMhgCvII7jNeJyclNqK1Po1U56sXN6I77RfssUVqWmhYDejNjjIPp3NdjbXMSTsxIyclTjrXBWLLqmoRXM5WFVYlSCoyfoK6+8VZ3vJIG3SISVOcZPp19a+XzKlFyV9+vzPdov2u+xLd30csvmgsx3EKBwBzgZrP1JXiX7Q1uqx7cEgdD2NM0TSxqcQluvMWQk/KD931roNRs4TZNaspWJkIyTyPeuZOlRq8i3X5FScNII56O2e9t1ljzsUkPv4yR6eoqOSzja5WMQLG+Nze3pzWzpVgthoruZJJI42IJYDIUn07jpVyaCJo4wZfMwoUkLtyQf513LExpVGottXt/X3nNOCnCzWpz0QmR3T7gHAI5/OtmCOO5gWGRCs4OS/Z6HtUJOM7h61MiSAqBxg9hX0NCqpRVlY8DEU2nuWF05IAPKQb053seBXUaashg3yAEhcqQMAism3R5I1Dtlh0Ga0YpZEUIM7V42ntWspNmMYqJUmV55yWK5znAFQtY/NuYAL6ngVqJEGJbAznqaJTGnyk7jjPA61PPbYfs09WY/2eQMpidZF6EcYqjqbi0n0hpIgv/ExUndyFAikO4/7vLf8AAa3d0DTAk+WV/hC1n6zFGZ4muATbLb3G4KOWJj249uCaipP3dS6VP31Y5jQfEVlcFreGC9Zo1ZllMahZMDJxyecdj61HceIU1dZFFnPE6eYEilKliVHQgHvS3Wo/6Qtrp9hEyRWxRS1wIQjN3x3Facmqwy6XDlALmMgEghh6HBrlslG9j0HduzZmeA4zc+EoSOiyyBR6KTkD9a3XtZok3BSO4PpV/wAM6PHpuki2gjCoHYk/3ySSWx2zkflWvJpzOm042+uMGuulW91HmVaPvM5JLOA4PHmHqGH9aa1mqHdbrhiOoHQ11H2CKHPyAH1NYms63p+iFhJFLLMMfIqnH4nB/lW/tbvQx9joMH29V3NdyKuc43VXuPMmIDyE4HDuK5ZvGGqancPJYzWaRKeLeW3OD7FjhvxrS0DxM2tak+n3GnvFcIm5/LXKxj1Y55U9iPpWqct2S6dti2ttskyv7w+uOKsQ6fcXMnKMqj7x6VsR2UgUmMeWD371agjNtC6jlz365NKVV20JVLXUxbto9OsWgEce48j5QT/9auSu7lmfcshVSeyjiuq1GKe4P7wH2UViy6RM2SV+XoM1vCpp7zMJU1qoowfswuWd/NfC4GeOa5vULTddFeWJbABPNd7/AGPdNaNHDGAN2TIeAv41nx+EZVk82O2mvpTn5nykK/U8E/mKtV4xerBYWclojntP0i4v0e1s7SWScpyUHCD1bsB71PZeE4HmEcnmajdcf6NYnCr/AL0h4Hviuq8u1tYHiu7sTyFGzaWj+VDnGPmIwW+g/Ot/RpbWXSHtjFIGI/d21sNisf8AaxyfxrGtiJpXWx14bCU22nq/66f5s5q18NJ/qW/d/wB6w0o4/wC/tw3+fate00WOFWt7WK4l28tb6SfJhi/3pT80j+5461oxaTJAyrfswLH9zp1p99/cnsPU/rWdrmt6TpgS21a8Z3Vjs0/TZdsUHuzjln/+vXMpSm7LX+v6/wAzoqKFPU8U+x/N8pTG7O7PStqx06OeLdg5U4OCBn1rBicK7Bm3KeMA1vaQ0xjaRW47iufEc0VdM9TCuM9LG3EIbgwxtB5fkMSMEg/nXW2ctpd3MzSTOPmIwpyMZ45P865ZJkDA87yACxrqNJhjjBmVgScnGK+ezB/u73dz1aTa0RuWUtnalRbIspxxubPJ9aq6gLqBZJmwI9pLshyFAGfyqdJbZj5rwBXxyNvHFYviXVpr6bSvD+nhUuL6ZfMcDICA+n+elcGBp+1q7O/V+RhXrexi52+86DSGefQrOeQSKtzGJW3nkKeg/LmodUtILeaGSNnKEkqM5HHt261v3N1bwbrPy1KqoWMqo7DAGe3FZc4WTGIxgcAVWHaliG13+/8A4bZGNOpLl5p/8ONiVJSCCDkDkGrkE1jEX8y7twY+XBlX5frzxWZNb6tDaY0iC2knIwGuJNoTpyBj5j14JFcjpXhzXbbWLhpINLubxnZpA+1nDnnJweOueBX2WHpx5dXY8TE1Hz+6rndXPi7QbLkTtcPgkC2iL/rwP1qt/wAJr5u9rbRbhwpC5uJljwT06Z9azz4V8QzLulNlF1JwnTJ5pV8N3ay+VPq2xjjPlwn+dbSdGK0uzmSryetkXX8Q6zcg7LTTYUyAC8jOeSQT1HTH5U+z1DVJ71rdraCcg8bY2RXHqGBI/MVe0zwfZo48+aa4kA5Ltj8QorrILWG2jCRIFUDsK53NP4Ymyg4/FK5Qi00hVLqFOOg6ipbnS4bu1e3k+44K89sjH9avs4RckgY9TgVmajrlpZwuWck4OMdM1Kpuem5XPy67HnVrbPHeXBicxXS/u5HhkMfmBTwSR14qtH5Uuu2+nwKnLiWdhzuJI5Y+vFYmuwi6kMu+URtx5sRIK+xrf8IWcFvPEMFIchmZz8z/AFqHRqcvs7O/b9fQ6PbU3791buesx26pwq/hVKa5nu7h7ex2gLw8xG7B9u348/Sr15NGLH5JEDOv3mPAHcn8K5aTxdotrZvb2OpRrKDhpJiEbPsHAz7YGKtxd7LpvbU54u++/wBwzXtQufD9skcEL6jdyFmeR5FUJ6f/AKsVw1vo3ibxBeCWSxtrOzbk3Ms5cnnkKq8k+/Su8tfDr38kd3qNxKVyHEK4Xf3+fAzj2roWhHAHAAwABjFVFxS1Sf3/AOf6Fu93Zv8AD+vxOKt/Aem7VSVriUqch/M2YPqMD+tb+keGLXSYHithIwc7meV97t6ZPoOwrSkkEKE+YQR261VTWJAGDJnHQgdau8mrGTsnqWVsCCc5IppsFBJz1qu+vNGgZrcsCcccVFJ4gjRQTEwJ6AHpRyyYc0VuLdWjEMgGOODWJKRpNyjvbrMfvYJyDj1rTfXAyElMjH5Vzeqahu5kwpPALN0FaQjJ6MznOK1RDf3d1q0plKFlXlUQ7QvsBVe4u9SubBbJosQRkkKW6GtDREt7yCeWS6itwgLbh82fpVYwapqZeG0aSysyMyXUw+dl747AY9PzNVzRT5bbFqnJx577mHBp0rStGzB7mWMqkUA3OxPb2Hua6GwP9i2vy3CCZkLFlIKRgHlhn7+P7zYT0yetObVtK8O6W500MiTZV9RuQHkufaJD1HH3jhR71w2pa5/aoaNVlitc7mUvvaZv70jHlj6dh2ArdU5Vd9jCVWNHbc3db8XhopLTS7qaRJiftF6ch5/QZPIXHsPoK4adUZ/mO38KkYMXJUHPQc9KrOcHlwT+dddOlGCtE4atadR3kYCrPJOzghRnAzXTWJWCJDlskjNT/wBiPbSfPD17MMYq/DZCQBWwmBnk141aSlofTYWDprV6mhbrHx/Ex5+ldHpv7uIpvyOq81zdrY3BjUbNrA5yTV+KK5glAJwB0ry62C9orM7XjVB7HVoTGhkkYBQMsTyMVzHgxm1rxrf64QTBaRsIfYAYH8xVPxJrk1loclurnzLj92o7gdz/AErqPAGnrpXhF5JEPmXUqp78fM39BW9HBRo05NbvQ87EYt15pdFqbKK5xxVuKMgAYz71NHCG5BGD1q9DCFFTh8FClsgqYhzWolvBxk1figRWLqiB26ttGT9TUajA4FTx5HbFd1rHNe5KF4xjk0wWu9i5K5HooqyhJHSpBGO3Wpeoc1iKK3SPBBwf51SvtTW23ID8w7+tX7yYWto8rdQMADqT2rgL+/3XB3tg4JPtWtGlzMynUsrst3utO5O6Rh75rCu2juyS7sT2JGaWWVGH+NQl+eMV6VOko7I4p1HLczdT+2IIXto1+xqSbqM7st8p24GOQG5OPb0rMsNeuLKygjgv7HUpoyyuGkMc7DPARSCuR05647V0bSklQGKk9COoqSKU+WrBzz39a0lG5EZJEFjqOqaomYobu2UnP+lgcfXn/wBBrs9GNtbQx/aylxcr/wAvBQAg+wOcVyh1GC3dEmnijdwdokkC7semTzVv7Y2AykFT3B4rkqYVPZHVTxTSs2ehx7Jk3o+5c9acVwOK5Lwrqxk1FrZiTHMp2/7y8/yzXWTS7OmB9RXFVp+zdmdNOp7RXRVuIPMXmsyS3MXODx3Nc34o8bSafI32S8jfcWjWKNRkcZDZPPPtSeFrebT/ADbvUr0XF7cr+8U8iPnIwSfTAIx1704ppXFJpysLrV49tNlyxAX5VB4HvXJ3vi2+83ZbRxqqnG4puJ/Oun1oJOSW6ngVz1tpTXJ8uOP5j1OK7KbhGN5I5JwnOVoAmv6nelVFwYRwDtAH8q0NL8P6l4haUvLiNOGlcf5zVy38LC2iEs7CNB9526D6eprp9LaLS7B5nPkWijPzjBbPc9gPb86yqV4pfuzaGFlf94UNK8MWXh9DPe3BmdfnyyfKueBgdTk9O5PSsLxZ4pjj323lBnB4sycqD2aYjqR/zzHA7nPFReI/GS3cn2fTTtQEkzt97JGCV9Djjd1xwMCuFlt3kPypV0qDk+epuRVxCgvZ0jNvbia6umnuJTLIx6sc/wCR7UxFyOCcVrR6LPMNwhbHdjwB+NVpoo7WRU8wM+cAJljmu32kV1OJUpy2VypIgVcgZHvxVCUktxj8K15rV8ZfIJ7MefyqGLTzMxEbgY65FXzIz5Wes29paakquAsyqPuMcMKjuPCodS1ugwR91iP59DWCivbsXsLssuf9W3DL7j1rpdN8SkIkd4i4J2+apwT9R3r5ySnTeqPsozhVj7hi/wBnXltLu2PwNpBGMj3qzbB3YxzRbgcZ7dPU12IjsNQimkkjiuLVFG0o/f8ADpXm3iLQZzdTHS3aWGXIMMTtvUdxg9f1rOnjlKbhy7dTP6oql7Xujmdblh1Pxd5MZUWltxnORheSfzr1OC6T7DpcemTRXNqkG5pIWyN7HkZ9gBXnOm3drpNpNp+q6R59vMdrOV8qWLOASHHIA645HtXcaBqOn6Zo8FhoVo+p2sZLH/SVW4LE/MfLIAPQY2kg47V6HxJPf8jzZwdOVnodPaXgl4dNp781qIuV4Irhb/x/osEZCaVqDXQbDRyxiLZ9Tkn8MVoeGPGtnq979i8o28p/1bE7o5D6BsAg/Uc1LjJK9iVKLdrnYpG/UZxXJ+IvEl7BqU2n2U62q2ygzzlAxGQDgZ471ua3eyabot5fNbhzBHuGTwOQMnHYZyfpXmGo3F1q93c3Cfv5IwrzoiBQyKOCAPQEf5FVTTd3p8/l+hNTt9/oeleFNZm1K3xdS/MTiIyY8yTuSccY/CusRT61xvg6wfTdJin275Lld7yMvLA9PoPQV1SM7YJGB7Vg5JyfKU4yau7L0MvxTe+RZiFWAY8se4FeV6zf+RdqzkYYooz6kmu08QzvJcy7gfm7HtXknibUbTUQ9rb3ETmJxHIxfaAecjJ9K9jB04pXkediZNvlR1cd4kzFVmRmXqFYHFSeZzXF6Fq9s8zwW0rsivs2vjIP9fqK6YTHBDAj8K63BdDl5mtGXJJ9oBA3EdBmpYJcwp2+WqC5Zc+nNSxPtVfYUnFApMp6/oDa3NazLcxRmAEbZId6tk56ZrI0/Trrw6+oSySQeVKqhDCCoOM5+Xt1rrIpN8eByetcl4seVraNYHA3vsZeS2MZJz0A6U1U5VaWxXI5bHWeD9ZAvLS4lP7tJAq464ORXpU2q2V1BLbTkrFIhjfDYIBGD/8Arrw3Q7gxwpH0VBgH8Otet6ffW15ZwTyReW8qAlgMqT0P65rzcxhaSmjvwLunBlu20PQIUlNlYW6mXG99u5jjpyTkfhWXfabBG+UYnHvzXRReVFDxgg+gFUruOJ0+QhD715kZy5rnfKEeWxzU1qbgj5DkVpabZm0IbZz9K2LdVeGPci8DtWmi2sMBm2gyL0BNOrUclymtLlpRva7Zn3OyQG4vsJtXKRnq2PbtXnOvy6hr17t3O8CH5IU6L747n3rsb8Pd3J3N948k/wAq0LTTbaGIMIkEmPvgc0Up+y97dkYinzpQv/XY80i8OneqvHgk4AKnJPoK3ovDsFq21gGZBmQ9Qp9B6n3rq002OzaS9Y5dQQg7Anv9apXCiKyDnuC7f5+lbOvKpJRv6mMcPGlFztrsjz3xLe7CbWIhVUfNj+VcvaoY42vXA3tkR57L6/jWlq7tOxP8c74/M1m6vKIohFGpG0bR/KtcH+9k5vZbCzH/AGenGkt3v/XqZ91dMQzn6Cq2iu0t9PIzEIqbfxJ/+tUOoSiOIJuyQMU/Tx5FgrEfNKxc/ToP8+9dFP8AeVeboc1b9zQUFv19T18+FLmOQM7W2D0Kylf0Yf1qa88JwTQAztLHMinZIrj5T1B9/pTo5YwmIfELYHSK6i/xBrVh1aGCDEzQ3IA6RYB/AVxzlJ76nRTai7rQ5RZNV0SGVvOT5RgywsPmU/3l5707StWhku1F8GiZmGJU5Gc85Gcj8K2prnwtdeZGVlhd1KsCGjPIwec+9YEvhGBIEGk6ubt4n3LHJMgd17DOeSBwcgZrCrhadRNNcrfU76OPqQ3d0aHju7QW7RXEatEGAjORu477uvIriPCenT6te3dtYKJCp3rGxGSO+B7VT1a2uLj1gP3SHUgg+hHY1d8H3WnaTPvv57iC6RsxuiqeO/Vgc/5NaYLCTwVHli7v8/xObE4iliOWKVkjpbnwtqVu4luVZQT8wlyf51cTQpPD2o6ZcuM2N3iORcA+W5weD6ZxXpelaoNSs1aGeK+QqDh02uB7jkH8KSDUdPudQk0kwBLmHJ8psYGRkevqD07/AIVlUqyqSWtvl/w6JjFQTVr/ANfeaEkcUiPHIisjgqyuOGB6gjvXO6B4OsdF1K/kiZzBMFWFHx+7XnK5zkgcdcdutaUOoyNGG+0WsygZPmBkJH5VoW97HMACAhPQK2Qaal0IcWtSZLZY0VUJAUAAdeB0qnrN/wD2Zpzy8b8HacdPetGuQ8fw3B0qKeEO0UZYSBRnGcYNFR2i7Cp+9NJnlGp6hq2lsk1hM91aSyYa2uDuERPO5W6qPXsPSmWGiraWSyi1thIS5lMTCXqcgl8ZPXH4UzUDcS2RWL7GFkOwi7Y7G6/Ke2ags9K1NLgySeG9HUpgl7W6CZGPvYVu2O4rKrJ1KPs5S0O+jFQq80VqyC2toX1CfegBLAbwo568E9RWzFCUwvmMw7Atu/nWfb2qm8uJGDLLkLzkcda1YvlYZr3cvl/slP0PDzKP+11PX9EWVjUEq3cUsUeEXvx3qTglWHOB0zjNOQr5a/SupyONRB0eZDGvlRju5X5h+OcCr934MN34ZTUVIaVC7thNu5DgZAH0696gg27g2A57Ken416jpqs2k26zgMWiAYY4Oe1edi6kopcp6GGit2fOMt3DpMuJBIwzwFQkmvTPhxqUWtaRcRbHimjlMiRSMD+7OBkY9wcjnqPWue8feEza6swiJWFxvjb19vrWRoOpXGj3sd1asBPCcqG+647qfY9DXn1MZUrQ5ZdDuhhoU5c8ep7esLIoWRM+hWopVhUDKcjsTmrtlqNvqWmW19bozQ3MQkT2z2PuDkfhVe6t1lP3sE9M9BWSdupafNuis19DApZ1REHUk4AqH+37O5jbyGV9jbSVHQ0240+SSJk+VlPqOtZM9vBpUQe4SQLJyq28RYv8Aj0H4mrTixtuOpPcX6OWymM+lWbC7TAxEi9yW61h2N/HqV3NDDZ3cKouVaUBgfxHAPtWzBZzK4+TOfXinKyVmQryfMjT1STOlIynhmzx9Kxdek8vSpDubiDH3vatXUUcaGqsMNG+D+IrC8QyFtAZwetuD+gqafxS9P8zaa9yH+L/I8ymKvqEeCwC5bO4kcD3rD1B916MszYOckelascn+lzuedsZAz7kVz99J88jHjsMV6GF9zDtnDj37THKPRW/zMq7Z7q7WPPLsF4rQuJUDAIflAwPoKpWa7rh5z0jGB9T/APWpJJDLIfb1rqoJQhzM4sQ3UqcqPoibTNeXgS2t0voyjP8AKs2XTtQyWksGhf8AvRYxXYJMm8Zb9asREOWIIbHYgV4kcQ+x68qC7nHW1tOE/eyRk9hcwBh/L+taNvDBcgpLZ6PKT1KEjkfyro3jt35e2jYj1WqMmkabIxb7Iqse6sRROumTGk0chr/hS5up3vLK0VZIwFW2VtySr1J3H7r5OB2wAD1yOFtNG8p3a933DxuVkSeIxvHnrkf3hkH0Nezrpax/6i6mQehO4VV1Lw8mqwpFezecqNujbLI6N0yrA8cVdPGOKtLVETw6butGcdpV/feF9Pe+0uX7XZFSZYeWVD3BI+aM4pLa+0rXoYHF7NZ6pGuFluH5lfOVzL3PoTit6x8KXukatFeWWqMYt371XiUu6f3S3AYH3H45pq+DtPgvZ7kRS4mUq0J+4O/A/wAgVM60JGkKUkaGi60zXh0bVR5GoocJIVwkw9vQ/oe1dTFvUYVF/wCANj+leOrrM9iw07U7V4EQny4LsF1Uf7Dj5l+qkiuz0bxXaXKpZNe7biMAbZpAxcdsP0b9DWbTWqKS5tDtoxcA42pj8RVkIzqVkVSrDBHXIrFivplflj9CKnuLs3Ki3JVUI3SFTnIHRfxP6A04TTdjKrTlEpXXgPRrvzQYWRZBhl4Kn6qRg1V/4Vh4faOOORZ2jjUqqbwowTkjIGew71tQNCij91Du77VK1ox3KthVyB06A1cHQvqTKVZLc8O1LRIdIuZILWPYgkJIUk4PTqTk1AiHPzHJrpvEt5YXWpXe6aaJklYZaA7Wx3BXP9K5STULWAb/ALSuz1YFR/48BXt4ealBWZ52IhJSbaLmSpXAJJBFODbQB0rEm8R2JlCi7gOBg/OP8akXXIJYx5bGZzziIbv5V0NdznV+huwzkOCOvavVfDziXQbRyxPyEZPsTXiNrqUszsI7aWRwxRgCvyn0PPWvX9GvHj8OafaoghuHhLEyEfu+T1xnmvNxrjZWZ3YeM3urDfFenwatpDPD88kQ3KVUkEdxmvEdSie1uHJhlQn73yHAPrXv0enWn2QQzzNO+3DSSE859PavMvG3gpYLaS9trlbkbgGiePY+D3DE/NjuPSvMhC8rvqegqiUeVPY0vhZr5n0i90hyTLZS+ZGD/wA8pOf0YN+YruifOY4xmvDvBl1Jonii3dSFW5P2SZC2SAx+U59mC/ma9tspSYF+0IwfsccmpqpwlysqFnFyQLbOr7mkb8OKlGwLtDHjtmmT3vljEUMu71LYFZsuo3p4Uhf956z5i1GUtWbUewDov4GpN0YGSQB9a49ItSmjVRfRsehYkc1bj8PXEw/0i/LD+7Hz/hVpruRKFtzZ1FEuLSeONlYtHkYbPK81yt2gufDMinqsTp+XT9MV0Vlo1vpzmWKSVnxghsAEfSsj7OI/7Rs8DCkleOzA/wD1quDXNp5opO9K3Zp/ieTGzLG8KjoF/rXI6iDGGB45JNehpEBbXp2g5Kj07GuLvYBNdbR93cSfTArvpSvRjFdWcleP+1VJvojEOYbVV7t8zfjVNptnA696u6m4SUjjPtVGKIH5nGc9BXXL3vcRw03y3qPqfT6RuXGyWT8TmtiFTFa7uC3fmsiK9SGRflLZOMGtWRm2KUICt1FfO+8t0e67PYrG8csfk/JqqXWqahDcQR22lS3IlbbuWRRtPbIpsl0jXDRqx44Bx1qU6h9hs38rm8lIii5xszxn29fwqE7TSktCmm4aMpw+LbeS/ubMwu8kN0LYGD51Y+pPAHIb8BW6twp/vD864s+E2spoZJ5IWvWRSkiBjsk5O4+59O2cgV1SO5Ubsbsc46Z9qWKdOLTpipQk17/3l1mjYZ37fqcU0OVP+sU+mOarwyRTyvBHcxPMn3o1cFh9R1qdbUA5fA+tYKZbSjoyC9httQtzb3trBcRH+GRc49x3B9xXF6h4FK/Po9yibW3LHcjdt9QH9PYg13Re0W7itXnWOWXOwEE7sfT6Hmo2IBxgVcMQ4axZPJGXQ88i1/X/AA1cLaXloJEUB2jL7gU6ZQjJHQ+3tXS6f4007UtSsLO3imJumKM5dQIm7A+ua0NRsnuxDNbuIbyBt0UuPzU+xp15oGnaikge1ijd8ZlhQK/Bz1xWssTSkk5LXyI5JR0TNlMA4y4I6g9RV6N0EZJO4gdCKzbO3ljt4Y5ZTPJGgVpSMF8DGT71emUW9jNKx2hVPJPTtWMJc07R1MqlmeU69pqzX8oe9CBzxujO3PoSDXPXen30ETRpGkyjKMVuAvOScAMBxgiuxvSj3UjIwZSecjrWfeOgj2h8L0weRXvQirKxzzk22ecENa+db3Gl3RLMzKwgDgj03A4zSaK2prEbQae6KJGO+ZwgCk8cAEk117tAro2xOG6qMdjUc0isT83XsOB+PrWlkZXZHo2m+TdXMyyyA3MnmumF+Un37fqa9j8I7TZyRufMK4wWwflySK8iiuEgCqrAYPJ966fTvFF3plm81pHFNIuMxykgMvfkdD0rKoluaxu1Y9N1aG4mtBFafu2LZLIdpwATwR05xSGMyWUcN0PNwoDB/m3cYOfXgmvL5PiXrSMsjNCuzOYvKwp+vf8AUVzeqfFzxKFZYrq3hHGGjt1z+ua4qWH9rVdSEt9PLQufNCCi15lzxt9tuPEN5o8hcxWNt5kExG1VU4KKAOc9MkeldV4R1q41HwtY3cqMbsbo5CjE5dDtJz74z+NeM3virxDqmoXFzPMks10m1wkQG1RjB46H0Hua9P8ACsU9n4ds4Ydeto9indGIhLhiST3Bzzz71pjI8sUt2a4aV22dNPc6hK2WkcD0XIrkr6/1U6odPkck7gyZO3Kn3q/qutS2CIbjWFZXOFJtCmcDJ/izwPaufbVrCa5g1nVL6dodjrbwQIokYY4Ziclc9uvB7VjRpzkr8pdapCOlzvIr59L04PcQQRRKuSwkx+tZ58SXd8QunwPGh/5bEED8M9aw9C1fw/q+oFViupbxFLxjUpN5Cjrtx8ox7AV05vTjafJVO7HOBWU4uDs46msGqkbxehp6dcXQtQ1xe+ZJnqSAKi1C+S3uVuhH5u6HY4VhgkHjn6Uy3mtGRCuxgRuB29R61a22LxP9oji8sDPPTnjtShJ81luU4xim2tDza5uUtbe5Ro2yzlgR04FcZNLHHFLKzgMB0zzivQPEqaczSLFHHwSpETlWJ7jFeV6pIhmdEJPPIb+VevQhOKTetjzMRWo1OZRum7X+RlvunlaV+5pBvlJEfQd6vadpd9rFytraW7zN3CjAH+8egH1r0nRfh5pttBu1aVrqYj/VQsUjj/Hqx9+B7VrOvCC1/wCCc8KE5vRf5Hpn2RFlG7kE5OT39vSripFIyx+coKDpu6VyEfj3Rry4WOOf5XGMlSCrehHp71zmt6g0GuXU8G1lZgQexG0VxUcLOrLleh018ZTpR5lqenTaZFEDI3OORzjn1rKXyEjMpuovMLEjMg5+br19K1bS8TUrPDOAwGGHXjA4FeQX0JMsbNH5e2VQiBe284rOhhXWbTdrF1sX7FJpXueqHTlNy15JI7nZgbmJC9yR7njn2qTTbq2urpUgJdcbixyOfoayNVvrl7K3jt5WiO5dxjPJXbioNGc2N/GykgEHIY9OKzjhG6TnLfp8hyxq9qqcdu/qbTQ+Rd3dguni4hhkE0JRVLoJCWB55AB3cimyX8cQYzTyYTjhenbq3JP061JOllqckb3cTPIF2ZWRkLLnO1tpGRnnFF1LbNNb2aIsMMY3BoosneDhVBIIGBk55PFeTiMM6stW0n06HqxalbS76/1rv5WE06IW9yTeecb1oCYmlwf3YwWwf7xJBPTsO1Rz6hbB2RmkBPGVHIz6VmX15FZ3Y+zqQ0nDylizNwerHk1kyTNNIjuzbS3O0jNeph8vvHXY4cZjIUpWTu2dZBpksEYT7dduy8KzOMge/GD+NTLLLA6i52srcLMvAPsR2P8AOsq38RC7aTCuoXjbkbi3cVPLfpJEIirBCPXkcZBH41hOhUb95alxr0uVa6G/ayMY1fG0kcgHOPxpmsTLNpEqM/ykjkH0NZNnqmLYLliwzyzZNRX14x00gttLOen0p0sNKM7tWMZTpy+F3OZunhRjhXJH901iX9xJtJS3A/2pGx+lX76Y7WJlKr65xXL3d1b7jzJL9Dx+Zr2IbHJPcqXN7cF1RSjOTwOgq2mdg3EZ789aoP8Aa5dt5b26/YLdlF06kMV3Hav1O7sK0lYMv7t0b/ZxtI/CquJIAVBxjNdj4Fjjk1QllB2xsAMdcjHNcUXIbkEH611ng66e3uppEAZ/JYqCcAms6sHODiupcZKLvLZHa3nhbRbiWa4ktAJJeWYN3PcZ6GuE174Vzy3Ut1ouqRxknKW95HuUevzDPHpla7LVtQu5tIjNuB9oZxvRG6Dn1/Cls9Qm/s62M/M/lrvDEZ3d64lh8VTjzWe9io4vCTlyua2vujmdJ+Htrp/kX2uajNfzW7eaYo4wluG7HYBlsHnJPbOK6Ox0WwtdQe8it/MsrkGTfH86bvUbT1J7GrUl20e6QnCKOgHNLZ6r5UMkcarGufMxGAN3TnjoeRWSlVd2ztfIrJHL+I9agTWf7O0rTzfxRhlvbdYmO8kcKWweB6Ywc1z2m6NqdyALrQIkZmb97MO27IABXgAHAzx/KvR5meZ5rm1KOoQeZGi4kBXjJHf61lLqbuCA4YNg8j+9nArp9rU5OVRXrqcc6NGo71JP0HeHdKtNL0wY06KOeWIK+8Kzpx90sBz7+9SX9nezGNbOaOFQQSx6/ljB+lVXv7mDIdVAV8deo74qUakgnlgMo81CA6A5Kk//AF65nSq83MtfxOqFShCPItC9ZW0kNskc7q7qMblGM/UdK1bcBJEI457VhfbpFlCFgCexxwO5q3JqEtnbSXM6Zt41BLqMkk9AOep6AVKpVL3NJV6duVHFa9p0urR6hqHkSPcS3Qhtt67isaHb8uORk89PWucsfhtfXd8sl86W1jnJ2NmR/YDHGfU9PSum0e81E+IL2GTUrd4JmZ54YFBaJhtPlhhwQA2M98HpW9Jf74ZJN5QRj7oHX2FdFBYinfXc5q8sPOya2I7Wws9LtFtbKBIYV/hXqx9SepPuacWFULy+lis5Lja7BB8yrgEfnVKDUpriJZEZArDOGcZH61vHDza5mYyxcIvlseZ6eM2zMm1GI65OcfXtXZSWsptLdpQWYxrluTn86870++USGMHEeeh9K9Ctdet7v7Ok6/Kiqgdegx0yO9epCfLI8erT5lZmpb6xcaTE8yuG+XKKe7dqgeVbm3jku3VZDhtoPU7if6VNqWlm4s8RFWXhlZenBrHubC7e0QBC+Mfd+rf41fuN80TNe0iuWR2S3yLatdxSoyCPIB61i2GqSXN6GkcBiCEIUAA+9Z8IltrJPMJCOnl7Mng45p2jIWvEUKzNkkBRmspQUYtlwk5zUXodLZajeqhebBwwUHHAqpeaxJG00Uc7YQttUN055rUhltY0k8w4yfkAAI6Y5yawNQEb3E2WG09cHP6Vxxgpy1jY9Ks5YaPuVL3K9nNJeX586c4AJG5uM9P61oFhC5DrtKuCWPVQOtUrIWsbM7FzgenvW5qNlbvY+dbyFwwIzx/St/axjJQZjDA1K1J10zNVxHLMpR3uJ5WCLG+1lU84yOv41uQ6hFFnzP8AWQx5bsMcAfjyK5S0sbmXUB5RCPEdwLng/j0rQu1H2oqHUk4XhsDpitamFTSOOGIcbvqdFbXCfY0kifhhuPAOTuIpNVdhYQBiA+5icD3x/Sqlg9sFNqgmDKh3yEZXIIyevTNP8QSGOK3RmJIiznGO5rgbvd2a16/1selRVmtenT+tzkNVmdiR5rD3GKwMR+YC7NI3oTmrepyFpDyTVbT7I3t5FCvLO2OegHc1qtEU9Wd1ommmbwpPauixi/RmX5s5x9zj2Iz+NcrcAglZkAYHqRyDXWuFtr1Zkl3QqQihVPyjAAGOgzisfxLD5d40oXCzKJV4xjcOR+BzSa5bPuRTnzNo55nIIGc+x5roPDkxF2FPRkZSM4zkVyzyZbGehrUs5dltK3/TJhVrdDlszubmeSZY1PzBHVgu4EHAOR27n8MUyyvL20014ntnxsiIZVDscHDZ+b0B6AdRya4Z1K3k8O75owvGBznk9/Xjitj99b2oSZZoWitQCMlCdrOa67wsk11PJ5a127rbs/8ANm1/a01oJp5n2Z+Ty2XcquWAGAcZHX296zZdYlnJsZT5IRCNxk+8fTH/ANeqGoapdSLcQttmVrx0Cum4qA/GPTFZ2tT7bwxj5SGJyDg5qZxjOakXT5qcJQ2V+nX/ACN61vHtdUtZ0nMLRRkM7zKPmGeByc9RxXQxazpmvh4Jn/s/UD8yysAscxHr2B+leXmY+Xbu5JIkY7jye3enyzRGZ4Dh2VgBtO4YzzROip6vcunVdPRbHcX632ns0eoJK7+ViMhuM56gng1lX4vvPluY2Ms0qks0S5/pV+08T2sxjsLqK3utILFMTAgR5A24J/GmXnh7+0JZbrRJ4bm3VcfZcgOmBnAA6/zrKEuR2krfl/wDSpB1FeDv+f8AwRP+Ekk0jT7aK4Aup5od0iSArtySAD7isjxDrFtq2hyxSu0DSuCS2QqlR8uG6d/rWLemd2G+0eBkBBQknGD7/Wt7wtfKLG5t2ZcO4O04+bI9O/SipRSi3Hf/AIJdKs3Nc23/AADP8BLdwx38trOPKgjACnHzO3P5YUfnXZwXUl0hiu3DRgYMfHBYkH8KzoLGGzsZpopIoQk75iijChwUzntzTvPtha2txECC+2OV+5YfXtnNZJJlyet3p5XL8esvfXUttElqlw7AIkg+VwFPUde1Q+GdMgv7CVJo4VeGUh+DkEknH5Yqne3tvplva3QgjkliJ2MGC5YhupAyeD0qho/ie5sjNsit5PNClsnAGMgYIxng9+abpzcH7JW/4AKrBTj7V3/4J5BGZFG8BsVraVdyPKVyTgbunpzWS0zFSM/nT7K5NtMWHQqVIFbX0Itc9a0bUBNo5ilmAZGJGWwOcd/epXurZ7dUij34ODgA9/1rg7W+uobUmDds+8zgevar2lXH2vUIYmdiHYBvWud3TvcvR6HocGkma0ikTcC/RAMA1Jl7S1Kf2ehiiAZmeYZY5+n8q3msLO2t0gj3vFEAWUPyWGDyR06flXPSpBeazZIEdYp51il5G1lJ7c8HHH607uaV9iGo07tbly3l1i8iFzYQw2dq5zuuHUKzjKgruHpxwMVX1S01i3i/0lVkinUBJIdrq3PHK+9aNpcfadWv5ryFwbcRQxEwmRI1dCWXYDxkYGQDgKQMGkF+39ofZt6yRSq24r8qE+aYztHbjdk5Odoas/rkqa0Wlr/hf8iXh41HdvX/AIJzECwRSut80yc7QI0yQfX09OO9WraSWSIRrdsmCygiAsSo6kehrnje3MwLZfJ6EnvjAPT2rchi1hrSJ1mYiP50jKsxBIPbbjn61vOSi7ytcw+sezXKm7G1aXSK/wBkWVTMyqA00R3fLknv371BeXgMarHLGIrglt2wqx29jz06flVXSre9ubtJ5rZ2cytmTyzxxyowMdayrlgmmacCCWDScdM/N3rWNfm0iyE4yVzcgljkuHVkhUgY3Enk569adqku6wseR/x7D7vTqaxVkb7TgnORuABq1cc6LYgPnZCRk8cb2xxWFRXXzPQw+kvkc1eNmQj+dXtA/dNNOvDBditjoT1/QfrWPeS5lO3pXVaJaIfDzOx2uWMmPVeB9c0Lpc0qX5XYaskqMDIHEbEMeMZwetX9fuE1HSYrqNJFSORowXHVTyv6hqoX91JJcMWkOR8v0qZXa68OagGJZo5In5Hb5h/UVVSPu3Oeg7VLI46UYlzW3pDDzGUjP7pv5GsK4JWbDda29Gj3vcSZwsUOfrnA/rUnRJ2TNO1sLvUIvOhlRpC4BRiAXLenbt+laOvn7NpWnSSW2JLlzBKHJ242DJwD1Prmr/g2WJYHaSZFKoDtPIIyAOB3ycZ6DvXU6jp8VzYLHLArquACMOoOBtbpx8tJ3ck+x4n11wlKL6nlkFwGvhKCBGZJCxIBCuyng/XmifRbjWr24u7QwyJblTLGh3MBgHcQBnb7/nipNVhtNK1SQG2HCpOp3Y8zJAGOOevQjtVC0166dIpVkmW4Q4Wa2+V0x1PA7+9dEeaSvBanXSkppOQ3U7W0b7OlukY3SlV2vgfN79APejVb/Tpha2kttfWjQxgFlSNi/GA3BAYEdx1ret30zXtLtP8AhIN1vdTgvHe28YUkq2MMOjH34PvVS+8LX9tNEdQeO40yGTZFcJlgyhCeo5UdOGwTyBnFJS2UtGr/ANLudMqUley0dv68jP03QJNQtS9lfWksedzJISrp6b05I/Ue9Nimm0iQT24lhvFtS7MxIIKuBgDHQgD9as6RGbPXk066sZLG9bJhltn3QyDGeFfrkc8HPtW3qtlbyrOmoIlmXBAv4YsxfMQSZAfmTkdjt+lTKq+az2/r7yFT0utH/X3G/a61Ya5NPa6mi29yhjZLokDdgjaffkDg1ynivw82jRJcyRxyxSdbmNQoDEnkgDqeOv51pXvhK5mmMkOp2oRlTJ2MwIByCGUkU9ptR8O2EVtqccWq6c6rCYozuYsSTkKef89qxpuMWnB/I3qXmmpr5nKWWpQ2+myWBd3eSVpVbZgHKbcdfaqe0zjHmNtUE4PRcc8fr2rob7wZaanZ/wBpeGrhpEBJe1lHzofQd/wNc0I0gtZDNEkdzAu9R5ZD5yOpPA4PpXXCUdXE5Jwkmk9ixEls9pMbgw4G0xuZjnJz1HQfpUKyaXHIWeVT2woIP1zk/wCTQ1462cfmxxSNMxkJkiXcMDgdsDknoOtSLd27N/qYowAMoI1Tn696pNiaVv8AgHmTAfwtnPrSAEHBBFLxn/Cr9tEkrRYySjA49qyOi52XhbR5dTsVgaOWO3dR5mTjrg5HHtXcaN4T0mxvo7zcpkhmyVl5GDwMfiRUPhmSzNlEr3eSwzhzjHtx0HHvW9FawfZnlSNmWQEO04GWznjb2HH5VxybbNktB+pnULyQpDA8sakF/LjJ7j5cj2B6+1crfRXtrqEU0tnd2sIYYMiFVyOnOMZrqYI1mt0iuIkdPtqbkYZVsROR9RnFZOmq11FFLLbwwJNI8E6W6lI5YvJZ2LJkjKkAgj1FQq8oXVtEKVFTXmyBfEM+sSyiDSLue7MRSWewkZS6Hj51AI6k/mcYqhJqN5eP9ns7ZjcSIV2xqS6KAQVAA7DI7ADOAOtWfDukyXelTxAsklxM0oKnBxBHuUfi8iip4rEy+I9RliBAubSeYHHC+ZDvxn/gR/KqlUhzyVlp/wAD+vkZqlJxTvv/AF/XqZmmx6hpmsRBdNeS5VSRbTQklgVIzt+mT+FdZba7qXkGzi0yZ3twgdQjFkI5GR+FR6nosdqsi27Hy4LS7thuYZCtCZF5zz99h+FS2Sr/AGreP5gJN/ZHAOf4TXNUqRrLmt08yXgVezYln4gu5tOtE060a4uk3SSxbXcKdxwxIOOSTxXJ39tqEscCpZSuyyyEoseed3THbnsa66zji0aFMuBJHISAG4a4xj8RGpx/vN7VVufPa3DwTeXGp3S/MBlT1/H9a3w6UG+RaC+qRjZt6nJNbX8CNLLbXMY6MSpArXvgLbSdPRyu7yOe4OWY9/qKr30Za5fzHdbYsSoixn2wTnimajE39nwS7A8Yj2oTjzAo4JyOvOfyrqm3JI2oxUZOxzd1as0xkMqcnpXX2wlGlIkMEzReVsMu3CZzk8/hXJIxaRY8gAn93J/Q10UUNzeW7JI07lZJMIirhduMnnpyf1oW+o6j92yKV1LmaQju2a0NHbNreQdTc27BRkdVIb+hqUW9pa2MzXVgkiyKJYpWkP8AEoO3IA5HJ7Vn6fNCmp2qPlYWcoSPmZcqR+PUVblzRaRhFOE4tmVf2kiSMWUEDrgjI/CtLRCotb0ActFkH2DKKqalLCs2XiJY/wAIYAD8e5qXSriXbOAI1jMZBQAkjoQefpWaOqa0Zt+Htah0idGuFkaMjG1e545z1x2I6EGuv07xhpskLgzr5iII0VGIZztUHHpyv45rgNIsY9W1O2s5HdVYMxZeowvvS3M1sokRLKBVLHIQAcjjPA696UtJcq3PIngXP95F2LHxAuIjr7OsrFfJDhY8EA4wf59K5CzeaSVS43H5VwFABHv68VbvN091gkkBCFB568/zra03w9eTxxSQwqVcBgSwHG7bn16/yNejhaLauz08DgpTjax0OgeGn121hjiJCwo3DH5Vyxx26n+laWmPc6JBLBLIptgWRhIu4EZ5XByMH0rS8OWF/ptuMyRxxsE3ME3Hhsr19zVTWrA6jb7ba4DN5wjcv8q72IxgAf7QrSpTjNuD2PZWHWzEe10fVLmy1KxuVVrScyG3PzKxx2B5UjPT2p010iKUljxvBG1lyG/xFc0vh29tQ8sd75TxrvYxB/UcA45Hv065qe48QxT2D21/F9pLqUbKjcoIBJU447flXBXwcoNWd0ceIoyo6vS5z2o+Il8MambfRYGFtjdJDcHdD5hHJjA5X861orTxt4p02yukgNjYXBIkuYl3EL6hM5Xpjn+VYmpWdpNFHHBdSXNs6jcsi/vI269B046HBFdd4R8eXWiwxWEkKzWUShGkMmSOOB0A6VnONo3irv8AE8+Du+WWn5Edj4fTQtSt4dO1dYrh0PmJM5MrnnBI6HOR8vB9DWj4h0SPXdPZpGWO/CEGRIyQx47Dr29xWlrllo3ie3+1W4RJXAbcuAT9a46TUdd0SRk3i4jHygypv/Uc1x068qktH7y7ndUw6pQu17r/AK2/VGD4g0+KwukRI5FjZB5IDErnJDYyfoePWqVpGYG3o0QYrja7Yx+Fdp4ktYJLWGSRSZIAChz0yT+fQflXMvqC2cEK2jOlwQfPbAweTtHT0r0KNRzgjza9NQqM8sjwGBI4rV0+4hgcY3eufSspSOhq1EURsn8MUMpndaZq1tbOC6DgYKkcn6V6Nok1lf6cJ45ZZQxMYVznYOhA9Pf1/KvF7CcSzq7OdwIxx6V3Gl6mbS4SSGQo7nDLuwB+Fc1VdjanZo9GSyMYgj2zKDdRktGdrABWGc/X0qKOOS+tSJUlBm/0aUO5kZd2cFWIBAJXkdCK5G48Yy6LJYPF5UsTuY3gkyBzj589eMnP1Fdvb6rYuflLKokypVi2/ghfw5zXHOm9zoi1sZ2n2UtvBCkVuTLFFE5U8Y3y+Y3f0RRTptN2sogcjZb3NuFzwcbtv/jrAfhW1KiiMSkRZeRUQ/eB4AUZI/z0p7wW6gRKiRs4baqrjkrhjx+FTyu9ytLWM28iE0N1kc/Znznsyqwwf+AyfpWK9pJJc37RTNE0t1aAMB90rn5h71uzgkyhZBg71OcnPGxv5/pVFo/ssjs8o3M6SSKGGW29B9O/vThTcUOUkyFbaJrdjbKwjz5saM25lz8rgnucgN+NS29u5tpFYFQeRxwaTRreKO6eSJpFVpGaRJOmT1PPI6fSukltoVQsFLA4wAc9q1T5dBcqkrnBXmnySylCQF3ZBA6Vka6rWkcKdWEQwnTaMn/69dxJ5TzTRpNEXh4dFILJnpkDpXC+L/3c0fzZ/djB9smuiDuzFxS2OdSbfJ/sk8rnvW/ealbxfuEhcbPlkcKQCcDJznnkDPtXJWr5ulDH+LpXXXHh+9v3MofbGcOu7pW1le7M5SfLZI17O8tW0FXEUYmNqIy23kgAEAg9cYrMsFiluLB3bBhl3AcDPz5P6CjTdFvVuEjuZMxmTlVJ+YYxitOPw9DE0cmXUq3XzCABkE8Y9qzsoXs73FKUp2dtjl9UWM3srhVUFjtAOQPbmorbesbMzbEUH5vb0p+qYGozdsSMCD0HJotQ8+IIUDyOdqjGcn0q4mkuppeFJjH4nthyB5cmOMZ+X/JrPvJttzMuc4Zh+prfKi1gCPE8IRGIbeQA4wBjHp7/AErmruDN5Mu/y5CclXBbk88ED8elVFc03NnPJ8sFFdyqbn/SlyOR1966aw8Q3cHlW0cixxhic4BJz61xcoK3HOeD1xwRV6zNw8kezy92SAenOOK76dXkVjowmMdLQ9Cg1LU9UR4opuLdV2r64PHNJBqk9pc7NTkmAiYZiRFIbpglvXIBB68VkeEPFE9ncGzvAslq5VweFCODjJODkdqb4ivZtQ1Caexm3WW8CVFfaC3XIXsOB9Kw+tz9q6bWnc9J4xOjzxev9dDT1fUpIXkityZbbdxI6gAcA4Pqw45rkRdRi6cSZZyDslLEMpxx061csdX8tyt35UsU025nlUtHnB6r+NOuNNs3P2hlRy67DDBE0QQ/3wfmHHHB61dSu2uWR5uIxUq63KUMUsk8CLcxSSSuqLklDkkDnAxjnmtfU/DhsBCHvUHLAyy5Cg4zjrng5Gcc1Db6UJLkSWqTfLlol3gtuUZ2/KSQcjr/ACq5qOoSNbmx1CFIJ3P32kEmC2SGI7DBHOa5JufMuV6dTngo8r5lr0J9Jiv9KuFR7iBbaYt5b7sq5QdBj5u46ir93dS/b/scnlvLJIrrsmZVIOCMHjscc1npA6w6TatcI9xG1xNIBklSUBC84yfl5A/+vTL7VLi+iIMcIcHcpSEB1ODxuz6DH4VzyipS8/6szojUcFbp/V0aniFlksyVxjC/hyawrfw4moWkV3HdRKzbhIjyhSCDgcfQVCdYkvLErJneMcjoetV/tEDWyI0UPmKTnKZJ56n+X4VpCE4R5UyJzp1J8zVzyhTz6VaQDINOGl3g5MJA/wB4VIthcgcJnHowrYyZZhIX5gcVtBbuS0W4jVzEnJfHA+prGt7SeaZInaK3RjzLK3yr9cZP5CtvRb/+z22XMMU9vJjzYpCcOAc445rGrzW903w6g5WqaL9TdtNHvLuyCzKM5V0EiYVhkZzkZxjiu2h08xwxRrky45YcKOe/4Yrmj4ytUjXyo42dv9YXBA98cVVufFMs8nlW9yZAVGFYFeTzj/aI6dhXlcuLqSvJWR7jlgaatF3Z6bb36QxxQNmeTHyxxYLNg9Qei/7x6VoiVhbfbZkhHlqWO05UY67cjnPT34PtXlWkTwxzgRzSWs8zZSO4P3iB1bBAI5I9vTrXfRXlvORYOyLEQrZU5UbTnacDGMgcZFaQqNT9nJfP+v8AM5K2HSp+1g/lvb+vQnZLi18PP58zLeshYjaPkkYk7eR2zxn0qpDAbrT4ZLuLypZ48MHQK2Rxn1wQAce9T2fiOzubmWw1Fkt9Wt33yW8nIZf+ekZP34yOh6jnPSrNwvnsWKRSOudp3c8+/wBK6bHENtjbxmWNAN20s2Vzk981ce4QyGEMRsjDAgcLnoD798elYsl+IbeQpFMSkojIlXy/MOOqjryeM8dM9KyI73UbR1V5IHtWWQTEKTI84kYfKTyRgDHsPpS5bmnNZFkOlrvitYYxLLIXmk6tI5OSxPf+lcf41LRPCrkFtnJFdjoe2dG/0cqjFmZ2bjPoPU5znBwPXPFcx44ija7LsTIIxtSJAc//AF61p6SsZy1Rw1mAZDIxwAPl+te+x6dG0EEezGIlGSOD8orwK2tru7vow6NDEDyCMYr2Gy+IHh6CBRLqisQoXIjc9v8AdrSqm7WJg0tzVexRXKqo3DnOKjitPN82PzGjIxv6kNjpWYnj/wAMrK5OoR7XcZbynGexz8vbApR408ONjytfgijDYbdZyMWHtgDB6D+lZWkWnE4TxNEYNevI+TiQnPrnmjw5L5eu2Ekh/drMu7PoTg/oad4suLOfW7i8tS8lnM26ObBXIxydp565qlp6nekkZDoGDZ7YBzW8dYkS0kd/r8VrEXjRdmB8nGAT0rjbu1eGMzxhldDwVz1NdnfeJfBV9EBJq5Eo/iNtKM8/7lZNzqfhsI6Lqu9M5B+zSj8xtqqc7KzMakOZ3OKdJUAckgg7uFxj3q1LaN5zNHNFGhAxyeuOT0+tX7u50ZwwivtxPQeTIP8A2Ws28u7eWQGOVHPQ5Vx/StuZM5nCSJrhUlkdVUDKpgqNvAGAMfUZ+tSQRbE2CTCHBIHQkZ5+tUWmjaYMJAMnAB3ZPU+lXbcwkRlp4gCcHO7/AAq1y2M5c97EBheB/mUEqcHoc/SpFaUWxj5a3yODzjrgZ/GrV08SzEs0aknOFjYDt0GKW3ntlYK04QMMZ8piDj1GKXMhcsicyvHpESQyyIkkjK8Yc7WC7SBjp1JqK8vtloiJ5Qc/PjygRnpkA8DirZvdPSKCFpFklVpH+4wBzjpkc8AflWZdiyNw+6by1znLI3HHTGOfTNZpRube90Jft6l7eeNgJopFAIyMA5yAM9MGnDU7fzmPm8Et/wCzf41nCXT2mKi8iAbgYVhj9PpTW/s8KSL0M+c7tjAY/Kk4wY7zEtiwQ4kKjuM4zwetKsrLI3lsAMdzTydIyAb+VmOeFT0+op4l0aNv+Pm6Bx0aMVbkiVGRQQJI+H2gjnGQfwqcCAsA0RX5uAQCDx6dqg2yzJ5e1lDDPynGKnWOGNACJC3fa4XNFzWw4S+TMUMJYnlQF55+nWo7lp4E3eXIGbjc20Z+gApiMto5McjKWySi/N+PsaaITcFpJTJM3XDKSB7elJMYINsh88TcjLBVDcYxz26Dge1MuLp0R9qn93gNHgZkz/EevPfNTpb4DKIGbJ+XKkD8Cf6VBcWn2mGSKS3WLcDwqNx7j3p3EZ2oXvnp5aKJI5GwgA5P+yT+prqovGN9Bp1pYmH7RdW2RJKGwrqOgwO+Op71zVvpkcNx52JHCLhCByCeuf8A9VWpIZJ0KMJCmCG4Kg+3Hasp0oztzdDSFWcE1F77nUL40jvbmCTUDaWyxo3lXEqtuQE8KAMbgDyGBBwTn36CPxOVlt2SZb+PYWmuIG27AeELJ0PfoQSATivNXs7kxOskYwWDLldxCjgDn8akjR7RpWgaa2MiqH467enHQGpdFdAVR9Tq9R1mWSeSSMq8yv5RILIiZJ+cDuSMc57jpmrenCK4RpZWYx4wfmO6bJLdeyn+6MZ+nFcRK4S3ilZ5sxgAtnggsM5PoehrZvb77OiXDSuJI/uxx4Bb2yelJ07Kw1PU7aO62oiRhkAUBfTFcR4pihm1J5GthJJJGH3+Zgjtzk47dqfceIr9zGLSBVUL828gkHrwQeB6YFUNduW3W0V9PE9yiYkC9nOSVx7DH51FuVpmilzGRAbuJwqRt5BODhycfn0rS+1f6RiXyYpBjy/MQ5I9QSetULVYTclYZSkgGCqEAN7nNXMBWHzLKfunLgn6dOK6IMxmhrzCSYBnjYkjIRgfpVkyiNVlVVwvVsD9faqMzst2NyouEOFPBznFI92ihUk+XJxycZ+mTWqMzYbV4o9MO6KNyrBUby9+0kZPfnp0rFaVBvk+13cbZ+ZFQICfw+UfjU1neSqrxW6JJJKu1WYA7T2bPPI5/Ws+9u5FJT7RIQpIPzZ9vwrCejZvB3SNWZ7eS2Z/KX5AHAwqk7cdRnr61cCWsoQbzbS4HzKMCT69QP8AJrAimiMYBCYYZUg4J/x71LBdeXCn71SSoBDHoOO/9K2uYG3IZbZQkzyMGBw0bhsnt7j60zfbYUpI5Zvl/fMcqM9M9wcfr2qh5qPGFkC8ZIJYjA9Of6VPG8SQ5eKNyRkKWOD+fFIZNOisjLHIqvw6jLD5uMY+n+PrWvp0sRt/Mj2bguSj/wAB6H6n/wCtXPI8buNhRF7fvOT+VbdpDHbqTnEc67DznY45zj3HH5VpB6Mxq6NEzy2cSsCZmEb/AC7RgH8Og+hNMM0O6QxMzxscr84GR7ikumDbo0ZkVmJcb8huT29aopZHkliRtxndtwexqG2ilqXpWilQ7o2QAblJY5Hoc+tVZbxZojazMVdG7jh89wD2/l0pHgbeUWQHI4ZWwP8AAmqd3bTlV23PKk7Cf4fXr60rspFVyI5xubHoCORx6VIk0G1ThmIHBVidv+RVKSGaaWRXiCSRkbhu+76Hnt3FMjs7hMFBjvxmhNgyaWNcIVwygZDbeR75oaQpkSbRg8OiE5+oHQ/oaYpnik5YNx93k80skPmuGWEocdQ+KbdmC2LXnOA581+nCnFMd2xhnBbsD0HpRJICpC7ox3YPjH+NVjGgVjkgZ4HXvUoob5ixv+8Mmwjlhg/1qRkOcbWxjpnJ+v0qMQFkLMGIHy4x0qSOW4hj2hVZTwu8Hj8e9O4D0jjLjJY9zxinHYm1mwD2G7qKiZ7kp94oncjip4Lcja5BG8524LY9zzQA+KBncHBUjDcYOB7+/tU7SqqnDtgAg88fnmjcVABZxjqN3B98etVtheXaA3HXJzmo3K2Bn8wklm29QAM9K7ZLOGTwcPDLXqJrbXH9qiMyJsKbAgiL7sBynz4z7VyCWjO4ViyjOeD29OlPkeKBPIRSU3fdIBz+VMR6J4CMj+F72BtP320sN0ySyQxTG4ZlVQP3kqnOAVCYCkdWFZ/gsS2XxB1iA26Wyxws7RARxKsYIBUIBIMlZM4DDvkmuCKxMzSsqNzhtyDn2qCQxBfL2gKvONo5PvQO561quoQW/wAS9CluWG1QrQIZo1VIyWUM2UXbsWMkglvaq/j29hk+Gl/cWalrN5EaBoynllGuWIYBWHy7AvGwYLDknNeG39rLJK8sACo3VABiskq+47uD34pNa6lJ6G1oV4o8TWjTW5uIBMnmQbyvmJkZXI5Gele++KdR0240PxQLeTzbi1imim3QoFilaYKiBimd20MBgngdc4r5+0CL/Td+MgDFb1yIxsAXLKPTimhNnrfh+00u38L2VzHZpKFljnvd1xC4jbyVzcSBiwjKgybEAyxK7gOg5bwjLY6XpWuzNc3ENqtxCsN8sVsZXA3gII5d3JDBiAONp57Hz7OCSFHLZ6f596s2jOoJ3AMT16ZHvTJueoW7WT/Fm6l8mBFmsJPs0xeIxq3kqBNhCEbcu/jj73QYqn8X7jTfs+l3mmQWUjpeTzPJFKjk5IkAPlseGbd+MZ555811ck2h5D87sHkZFYDak7SFyihic5UVMi4n0l41v7BfB/iCKK9tbiW2NtbPGWChHIVOADnO35sZIytTfD6+tbbwPpQnltbcq7486dc5klYqcAfxknaDyB64r5z065a4u0G0BEydxxxx0FasWzdKFVNzqHIxzkcenPWmthPRnqXwvvdNtbvUzfx21pHLPBaRvO+ZCWdiYdw6jCZPABxye1Pmj0y3+MtmVtbdo5rVbht2NiXJiZ1dRkDduC4UkckYwcGvLRKrhlZI2zxnbj8OnSmuIVXEYCr3Hb8RVEXPT/iAltcxadqMemSWk5c28guUWKSRQgYMVDtuO7eS/uAfU5tpBDe20ajauCMjI9v8AfwrhrJI/tCBQkZ9VXaD+Vei+F0h37JVYptbJA/EA10UY3VjlxE0ncgOiz3DvH5JaUE5ZehOc/kc/rR/whuoPtcW77u4wK7i5dVLx3MspthGuAgGeT056D6VnmWxM3J1CRP7puNv8q8+eIquVoRXzv8A8A9mll9HkvUqO77I5n/hEdRHW32EHozKPzyeasL4LdkOb2zQA8hpRwSOldA15ZRLiCwce7TsTVOe8EigmyiPpvJbH51lVrYyStTtH5X/ADZpSy/Bp3nOT+5fkc5eeB5pcyWdzZT3UQ3BYp1yV9weOuO+c1hzWkQJUxtFMp2urEAg+hrspdQlWLaltapjPIiGa5zWo3kuBcBgZdg3KT98DoPr6VtgJYmUnGvZ+aVv+AcmY4ehQgp0ZN3drM5qZGicEAhvbpU8N4PLw67T6Dmm3LZ/eQtuHJyeCD/nqKqDbyWIBPY9K6ZrVnLB+6i0wkDsIy+RyMgY/Wp4EuEUM4JJ6Btox+dLFeCMiQhGY5JyuM8e9RS6t52PuqBx97r7HFXaL6k3Y92nVipCovRsKDnvzmoFMqtudVPcEqM062D3UnnyP5FrHwZSOp/uj36fSrMltevfKptIo4nAKM0e8lfXAohRbInXUSoJRuG4DnCqBj/OPepIroRsSBu+bqoU59hnnFa50icwRssu4uCEEUCrux2Azn86rJZ32nX0UQYtPJkkSf6teOBux8x+nHvWyw1zBYyBTN3Nwv70ZOMBQMfkBU0UkqYCeU29uDwSfxrR1DS9Qa5eC6WySYKCWjfOcj1A9/rWHqCXel3AgurMLhfkYncjL22kYzWNSlKKv0OinWjJ8vUtm5lDLGgGSCSMkn9TXVfD3wYPFF9cXF9DNFpcMZXdFJtLynGArc9BknHqK8+a6EhzGW5IwA3AweleuaVf+O7n4d6Xp3hnQVhha1CDU3vIg5Uk5MaEjaevzNz7Z5rFWNZXNm0+EthD4lmmuoo59EEO2CB5ZfOEvy5dmGB/eGM96x/EHwXnvdYln0KaystPKoI4ZPNdgwHzEnnqferXw28KeJtD8SSXGsWs8dqbJ4xJJeibMhZCBgO3YHnFZ/xB8G+MNY8Z3V7pNrcSWLxQqjJqAiGQmG+XeO/tTtpsSn725xPjrwVqHgaytZ7y6tpxdOyIIFcEFRk53CvSNK+CWhx+FjbatZW1zrzJKBeJNOqBmz5Z25HTK547d68k8X+E/FOhadHd69bSx2zS+VG0t6s3zlScAb2I4U8+1e0fDuPULz4LJFmd76aC8SIyOQ5ZmcJ8zHjqMGhDk9E0zi9D+B+zTF1C88TpbbWdZlNp+7Qo7IfnZxxlepA611usfD3wbq0MVtomo2Om3rTD97Hc/aGdcEbAhk7kg8elcx4M0fTNA0oaj448YJJEizRPoVzc+ZGjrIyMGj3t5vIJAC4zzzTfC3jDwHceI9SXUdF0nS7W5lD6bcC3jR7dQoTBdBmNjjeGB4JPPANAO5geM/A8XgzUdOs5dSa6W6RpGk8gRbAHUH+I561OvhzQ5NPe9h1GV7RQzebuXZgcH+Ht9KqeNtGXTdV0+ODxQdehu1byZnuDMYlDqME72HO7ORjOOlbUelyQeGpdJM8ZleORPMAO0FiT9a7sDSVTmvG+n49F8zyc1xDo+z5aji29fTq9nscolh4ZvYZVv9cW3ZZnVVik4ZBjDDcueefSo/8AhGvA3/QySf8Af1f/AIip4PB2l6hNLDLq8hvoWYTQxKvy4bGQDzg8dfWnR+A9FlWZo9ZndYWKylQh2EckH0xWsKE3Ffu4v5/8Exq4ukpv9/NekdNdunXoZqaZpUWuQ2OmaiJrGUoHnbBKE9cnABroZPCul6bLFLPr4tyykR+b5abunTJ57VzstpZWWpWsGj6h9uLgEMpBYPk8DHHT19a7WG51S9ZBPoNoir903FzvKj2AQn+VLD0ac3OMo6303a+9DxuJrU40506j5ba35VJ+dpL9CG3g8KQQKpv9NkmVCvmyXCHccfeK7sfhVW28OaZeqyW2upcsoywhWJsfgDwK3bvyILKaaLT4Ll41JESog3EdRkjj/wCtWZFqmrRHdH4XjjB6lJgMj8FrrnQpJqNSK+Sl+n6nnUsViJRlKjOV/wC9KH6/ocrf2J07XJLSNy6JIArkAE8A9unWu10MszDJI3EHGc5+v51xt55smpubnclz526RN5faTjjceuBiuy8PyL5cUjKf3S9eDnHrXBQS5pW01PaxDl7KPM7u36Hd6nHGkUcSEti3jJ3eu41j+WA341du7rz5Q+TtaBdufTP/ANeqmRuH1rhUbXue5GonFNbWRZ0rT01PU47NpTEHDEsACRgZ6VJq+kx6bfyWsTySqqKxZl55Ge3FdLoWp6b5draxRqLvysMwhA5A5y1Sa3ryRR3WmCGVnkhxvDAKNw/OjlI9tLmsea3CBVPFYOruBMCRxtx1robpSGI9zXNavn7ScDkCunDR95nJmFT93Fef6HM3biG7M0eDkDeg6n3HuP1pFdGUSQnKtzw2ag1Bx9qGTggDGPWqZLRsXgUlW+9GGxtb1HsayqStNjpK8Eao1GFeAAOeMfNj3zRLrCMC+T5o+6QMYrNCMG3NxnoMVFIAedxU9yxzio5miuVM6fQZrbUZ5I7viOO3YxiVztaXgDJHI6k/UCpLS5RL2eBfOtvI5llkRJYEx2O4I49MZJrkobo2rFkyVddrKRkMPetM699pgFtNFHcQ5GFugJCuB65VgP8AgRxXbSqxcdXqcVWhJyemh6Ani7Q7s2rRX0UV9EQF2xvt2+mCDt5/PvTJtSsBemxuri2XzHIl81n2gn+7hevvmuMtr+FIcWtnaIuP+WVwy5+o8zP51Yl8STWLBE0bTyAB/rN0nJHXBYiumM4Jas43g/e91aeqRtFTNB51jcRrYr/y1lDnH1YnJ9uM1g6/NHJuCmfZhVRJWySPXHbOM49qH8U32oK63j7o1QmOGJhHGGBHUKRxjPesK8uGmkeYspd/7vAUY6DFY160ORqJ14bDShK8hIAMAZwpJOM4+n9K6/RvFvi+C3t9L0fVr4xxqIra1t4Y3P0GUJPeuKiclFIJHRc+9ew+GPiP4S8G+DrKC2sHutbNuPtQtrcR7pfR5W/DpurzT0GegeGDrPh/wtdan431oySgedL5uzbaxgfdyijcxzzjPOAM94PGw8S6loFpq/gjV5MhfMMEAjYXUTDIZCyn5h6cZGR1FeHeL/HmteMpB9ukSCwibdHYwE7FPZmJ5dvc8DsBVnwV8RdX8GoYkVb3TGYs1lK5GwnkmNudhPpgg+mead+hPJ16mF4s8X+I9ctf7O1nVLm5igl3+TNGiFJACvICgggEjB9a+j9L8Wv4i+HVz4ks4Xs3+zXTQq7iRlMQYKx4xnK5xjH1rxz4q+MvCfjHw3aXOmWxh15btfOE1ttm8ry3BBkGVZd23vn2rsbDxt8PtF+HM2haZrp5sZxHFMkjP5kiMSpYIB95sULQpq6WhxPhT4uNo2ki2uvDlpqV480k017NKFeVpHLEkeWe59a9j8f+IbbwTocGopo1pemW7W28t9sYGVZs5Cn+70x3ryHQ9H+ETaLp0uo+Ir6LUWto2uY1eTCS7RuAxEeAc9zXoHirxh8NPGGmx6dqfiN1hjnE6/Z0mRtwVl6+WeMMaFsKS97Y8h8W+JU8Va2uojTbfTVFusJigcMrbWZtxO1eu707Vo2sbS+AJYoC+7y5cLFzv+Y/LjuDTfEdv4GsL/R38L6ncX0Pmsb77RvcIo27eGRc5+bpnpVe78aQwL5dhZFgOjS/u1H0Uc/yrtwbpxUpVJWureep5uYxrVHThShezUr9NOjJPh/pN2l/qk8sJgEQSJhKQmCTuPX2A/OpPCt0txpGtXI27ZbueTLjjBTPPtWRqHjNW0TyLVAL66Qm5kRCiox4OO5bAAz2pvhLV9MsdDurS+u1haWV/l2sTtKBcjAPvXTRq04SjTjLRKWu2rOHEYevUhUrTjZtxVlrpF6v/IztCyb+zWK6SKQSAJLjIU/3sHt9a7FpNO3iK91u71OY8fZoZCQx/wCucX9TisS+uNFebS4rTZNZ264uAkJDOBgANwMkgGppfEcVmpGlaRbwMAVDyYB+mF/q1ZUJU6HNGUk9fN9OiWj+Z0YuFXFOM4Qa0f8AKra9W1dd7JHYWchWJQ9slpCCAkRK5Vf9oDgfQE1SSaW80RRYTmKZSYw6qG27GwQQc5yB2B7VTtvEOlRszPqckjuVLJIjMEIH3VAXAFYVprbafdXD27maB5WbY4Kh1ySCMjKn/OK7quLpw5bu6d07PVdnoeXh8vqz52o2kmmrp2e91r+u/wAyCKWSbUfNneSZ3lyzuuGJ9x2+nau38PANBIpHy7CP61xur38V9qK3MMXl5Rckj5mbvkg4I6c1o6Fra20kqAF0dcFe6nFefh3GM5RvfXfue3iIynTjLltpt2PTbzEVja425Ee0kfRaoedlxz3qmNftHjGdk6EDCMxTadopTrcIX5LK3Hvgk/qa2nhlKV0zKljpU4KLi3Y04LyW2mWWCQpIMgEds8VfX7XdkTyiaWR1+8VJ47VyNx4gvScxSeSBjIiULxUdz4iuDbxgXs28AhsykccY71H1WK3Zo8wqS2ijd1C2ljYtKFjXJ+aRgo/U1yeo3EElxId2NucMDkGsu+1eSaQhpXc/UmsC81RlPLbmHZTwKf7ukroTdbENKY/U8vc5C5QqOQO9UkcAZ5Y9zTYrss53OTu5GG6H0xTXzNIQXb5eOTXmTlzSbPThHlioliSZ5SecD0ximPKAibMhsDLHkk+o9KaxycDP1PemtwTkHI6n0pDGEFyxAY464pjRsqFTHjJBzt5FPSbywwxncMU1pBI+5I3yo69cfpTFqKkYGGKFueAelWFcK24pjjncobj2zUHnsFIZX75z0x9KUzkjlH5GBTC7J2MTIMAgA5JZcUiKkq4LMR22jPP48VA0oDFTGQOuCafHKRHxGdpORg0rBdkYRlmcBSQR2HpUjRSNglcD2H+fehSzklY2GPkzwMe1S+Y4j2eU3YDHtRYLshILZGOvUDqTTyrCMjYfTIHH0pVdmb5UZmPvTZpHWIDymK53ZyOKVh3ZnSRu0hHlt69KYEkK/JG5z04qfzmiZ38mQADnpxmkErBBmFiqgtkEdD60WHdjY3kQgEMCRkD1qWQybsEMD05qMXbOfkjfAXa23065qZJXeRwY3DYyV60A2yRhLj5g3Hc9BUMsMjLnYxGcZq47sN7NEQQQSdw4qD7WuQioW/4F1Jp2JTZnGGUkkRsccnjpQEZWAYY74q55jIPmhJ8z5ByOxpHDynHkNuJxub88ZpWK5ieDeqZUMNvBOOlTGP5SSv1wahWdpcxhMs3BB7YqyJmI2lM7/RqZN2RQoXlk2hiMA8VJsZlK7CGTnn0pEd45+IT8w28EcYpzTMjOSmA4wBnp7+9Fguxp3DG8EIBzgdKi/eht6q2T91lByKe86sjKE2lhSpI2wfKTkZB4PbFAm3YuQahdAbXUSH3Ug1ehvy4IFu5I6gP0/SsZZ2deIyw5CjjmmiZo92xXwwyAfQd61Vaa6nPKjF9Dbk1ALlfs0mcdGf8A+tVBtQlWZ40tlGBkdTxUQvGkBLIcEZwcZAH9KilkZj5qIxJU4x3odab6ijRiugXEl1M21g3PO1RjIqk8T7icHpVtrn5hIYW27Puk9Peq5ucKQqnBGD+v+NZyd9WzeCa2RBnDk4wc8mlaQljnG71HcUzcM/Nk+h9KRhnr37ioNS/uAOM9uKaW9Bx1x60E4PfpxSB2GcMfmGCAccUxDM5GB3p0cgjDdfmGM0scTyyLGiksxwAOpqX7Bd7SPs0nAyeB0p6ibWzGNOGVlOeeAO1Sm4HUo+cYGR71T/AUu/oM8UXDlRYDpIXcxkjjk4GOKfHkMMR4H8PNQJJsUjBIY9v1qyrOrMQHXdjkJzxTuKxIisu9WXa2/eajdysgO0srchuxp5bczERuA3BBX/PtSscImdwIyOM96BWGR4wGEbE5JzjrUMkh+4quzEYxt5qwG2xjIYEcH5ars5WVGKMccYxzSQyo7P5bjym+cf3cDj+dQufK3I6MrFduCOfr1qyZd5EW19+CMKhzx7Zqxa6VqesrJcWGj315EvyF7e2eQK3XBKjrgihlq/YpRN8uEt3yyEZVc596WN3aTzEhba3yjCjr3FOJMKMkkEieU5R1aP7rcjBz0PseeKks4biZorWK1uZp2YkRxwsxYAdlx+NAWfYdJdny2Ekb4YYwRjOKgjb5spCeucj0/wA/zq1fWdzZGKO9sb22Zs7FmgaMt06Z69vzqwNF1q3ty82i6mkcUe93a0kAVD0YnHAPrQK2mxnNcPMsf7s53bsgZz+tEVxiRAIySDnAUZ6c1Zgtb0KhFjdELCJ/khY/uyfv/wC771LBoupzCK5ttH1KWJxuV0tZGVgehBAwaLhbpYqRSlP+WTeYXJDbeT7VYDtlP3chw2fu96dDZ3001si2N08skrRIiwMS8i43Ko/vDIyO2RSrOGOfx5BH9aBNPsIJSzqyxyMAx6Lj1pjSb1SMqd3QHHQ1Jv2oAyMcEkbV7c1Cspfy2+cnB6jt7UxEhKk7UgJ6/X3oSQqgyjhcYB9aSOQgrlGOc4BHY9+tNV9pj+V88jOzr9OaQCxyHag2EkEnhcg0NLxtCtjHHy8/lQsjRxrlCMHGSMY605JjxtVs4xjFAiJbghdpzgrt+lC3ACIrg5UHJ2+tRndz8h44NCRyuxjEbM3pRqVZAZgMpzgjjioiOeKfNBNEoLoyjtmkVgyjtSY1boROMDNN6cc/hUzYxzimrgZB5pDLCbmbCI7N6Kuf5UvkS9fJm9/3bf4Ve0VZ3viLVrvzAoJFrIqMRvXjJP8Ak4roLhdYEcZthr6kO24y3UZBTDbgMd8fh1FMLHL27Pa3STPDJiJssCpHt3HFXm1iMKwED5ZSvLDuMelad/a3lxuYW+stbFm84TzxuSNx29DyQw5/+vWNLDY2t+Y7uC9SMBCYw6FwCuT83TuCKpSa2IlTUndlP7ZLgDI4AH3RSrLNIx2ZYgfwpnA/KrYk0EMhNvqeMfMPOjznj2/3v0osbq3t7uZk/tCO3aMri2dfMYZH3sjGPX8KVx8qKsFxOksjBtrbTGcr+Yq0NRuTkF1Iz3X3z/Oopg3nSyKk/lliQ0y/McnuRxmrOi2llfaxbWmoXq2dpIW824dwoTCkjkggZIA6HrRdhyp9CP7fcfMB5fOTnbyMnP8AM0hmaZi8h3SdSfyrotT0Hwzb6dczWXiBZ5khZow11C26UOqrHsUbiCCW3ggADkDpVnTNE8JTxym88QLBtnaONvtcYMqh9uShjJQEDcGJIwR9KVx8ljlpWAixgcEYFRGZeSem7OADit29sfD0eoQRpqxaFxarJsmWXyGeQ+b+8CAOqIucgDlx1wasy6L4OkhdLfxOyTMipGZwuwSfKWdmC/6va+AB825G5xTuHIQ+ANVs9K1/ULme8S2aW18uHfcvahz50bMPOUExnapPT5uV74PS6XruhQ3FtZi8t717zW7vUo5VEMSRRGRVUv5hURMRCzBeuCvHIFc4dB8GS3ty0XidhZtGjWfmMEkaQhwY5cx/IMqp8zGAGAw2ciDw5oPhXUdKS41jWhYTNJjy/tkeSN+MFTGSvy87skd+MYqWaLTQ2rrWof7M8byReJ7WVb68uRaabJcOE2GbzDOqhSGkO1QnTG4nPGKm0/xZHfeJ9Ihu9dNzb2nh6dHuL+9ZQbqaFmkUyn5l+YqgxyAgxzXI6ppPh61ucQawDH9lhZkSZZykzS4ZQ6oocLGC5wByQKhfTdESTZJflGaXywI7lJRENuRIWCYYdsDHXrxQBv8Ajy8tLnRvD0Fve2ckkd1ds4tNTkvPLDCAKS0hBXO1sDgcZzya7XWvEHh+68N3+lQaqz3QtJF+zfbxvVdgEYM5n8uUoybmQFsiTAGQc+SyWuhiaJRc3BUxSOzrKhyyuwVOVGNyqCDk43Dg1VMFiv2dVuN7G3V5fmwokJJ2g7TjC7c++aHoiopydke96D4t0n7LasPFFo1uunwRtHd3SLKkiptBIZgFznlQT8wyT3HHeHfE2kabpOgabKd1x9kv7iUx6w4WItbFVViOFdzGcKD8m5SOTivN1t9PKIWuyDuwRnOBz/s1f0/T/DU4Ivtangbz9uFgyuzaTuzj1wCe3oalSTdjSdGUFzNr70z0zwV4ns4bHw2H1CGwRJ7uWQXGpQZgjaRNoILxsQcN94u3yklSCpXzLXrkSeJ9QmkdJQ8gYtHKJAxKjncJJAT6/O3PetT+x/By3caL4jma3JxJL5XK8HkDZkjheeevtmq8Fj4cOuwwPqkp02TURC1wBtK2oA3SHK8Elhjj+E8VaMHroYQudq429ePWo4piIgpBAUY4Hvn1+ldfc6V4MVITZ69NOCsplaf90yYl2qQmwljsw23Pzc8r0EMWhaA8KRQ+JITcF38wlMqFAyDjr0DZOSO3BouTynMNc/vI2UH5FIIxinx3KAxjJwh7jk+9W3ttMdEIvhGGVDlvmbJxu3IPu7TnucgcUwafppmVBq6cnljHhRz65645p3DlRDJMjhQASN2TxTknjAACkfMDVQE5xweTS7sc4ouTyosCZVdjjq2c4p0VwiXTyOrFHUjA61V39uaTeMcnmi4cqLl3cRTRxpGjKFJJ3Yyas+GIrVtfRb2PfbeVIzDYGGQvGQQeM47E1lBlxjcPzrU8PapDo+tw30pJjRJFO0ZPzKV9R69R0pPUcUloeirbeH01CWGXSLYgRSSGUW42EjAUBWUcEnsBg47V534ps4rLxNe20CoscZVQExt+6ORjjk12n/Cb+G9sot5L2EBsJ/o4YlRx1J3AEdt3YZB5rhdcvLa+1ie4tCfIfaQSmznaM/KScDOe9JGkrdCGzmitZhJLZwXS8fJLnHUHt9MfjVv+0rQqB/YliCN2SC/OQQO/bIP1FZhLd+g9qMuACOM0yDWj1K2RQDomnswP3mDZPJPr74/AVXlvbd4nRdNtULLtDqTlenI9+P1NUstxzjFIck9vyoAdjJB9+Kmtbm4t5me2mkhZlKFkOCVPUfTioCWxzgeuKcAyg7WBH0piL0+oXt1HsuLqaVM9HbI6k/zJP41VPHSgFhHnPPfionZ+5/DFICwrHoT+tOMm0HGfzql5rYxu/SmGRv71A7Fp5M9+PeoS5zkHoKgZ29aQMfWgLDixPc03PIGTikOeaQZx1oGKcluTxTtxA61GM+tLg+tADu9OGPWo+c0oJxQApzmnLgDPpTOc0oLd6AJkIYY3Y+pqQ5XBVuh/KoVJHINKXIzz19qBDy7Y68GoGc+Znr9aQu3rTMk4PvQMk3E+9PC9CaYAc9uKlySP1oELjBI5P40jKPX86XtnPPpSElvT8qYCBcjGR7Vbt9Ru7a3EMUyiIFjtKKeuM9v9kflVUdOD19qTBB60gNRPEOrrtxeN8ucfu14z1xx7mm/23qIjCC5wuQcBFHIXaO3pWdgikxnoaANCfXtUkmEkt0HYnOTEnX8qqXFxNdy+dPIZJNqqWPoBgfoKhPIIahCSPegD/9k="

/***/ }),
/* 29 */
/*!*********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/mock/top250.json ***!
  \*********************************************************************/
/*! exports provided: count, start, total, subjects, title, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"count\":50,\"start\":1,\"total\":250,\"subjects\":[{\"rating\":{\"max\":10,\"average\":9.6,\"details\":{\"1\":1210,\"2\":1362,\"3\":19321,\"4\":140177,\"5\":713825},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"爱情\",\"同性\"],\"title\":\"霸王别姬\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p67.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p67.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p67.jpg\"},\"name_en\":\"Leslie Cheung\",\"name\":\"张国荣\",\"alt\":\"https://movie.douban.com/celebrity/1003494/\",\"id\":\"1003494\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p46345.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p46345.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p46345.jpg\"},\"name_en\":\"Fengyi Zhang\",\"name\":\"张丰毅\",\"alt\":\"https://movie.douban.com/celebrity/1050265/\",\"id\":\"1050265\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1399268395.47.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1399268395.47.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1399268395.47.jpg\"},\"name_en\":\"Li Gong\",\"name\":\"巩俐\",\"alt\":\"https://movie.douban.com/celebrity/1035641/\",\"id\":\"1035641\"}],\"durations\":[\"171 分钟\"],\"collect_count\":1549067,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"霸王别姬\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1451727734.81.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1451727734.81.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1451727734.81.jpg\"},\"name_en\":\"Kaige Chen\",\"name\":\"陈凯歌\",\"alt\":\"https://movie.douban.com/celebrity/1023040/\",\"id\":\"1023040\"}],\"pubdates\":[\"1993-01-01(香港)\"],\"year\":\"1993\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910813120.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910813120.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910813120.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291546/\",\"id\":\"1291546\"},{\"rating\":{\"max\":10,\"average\":9.4,\"details\":{\"1\":1048,\"2\":2033,\"3\":34505,\"4\":238966,\"5\":793136},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"动作\",\"犯罪\"],\"title\":\"这个杀手不太冷\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p8833.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p8833.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p8833.jpg\"},\"name_en\":\"Jean Reno\",\"name\":\"让·雷诺\",\"alt\":\"https://movie.douban.com/celebrity/1025182/\",\"id\":\"1025182\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2274.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2274.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2274.jpg\"},\"name_en\":\"Natalie Portman\",\"name\":\"娜塔莉·波特曼\",\"alt\":\"https://movie.douban.com/celebrity/1054454/\",\"id\":\"1054454\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33896.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33896.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33896.jpg\"},\"name_en\":\"Gary Oldman\",\"name\":\"加里·奥德曼\",\"alt\":\"https://movie.douban.com/celebrity/1010507/\",\"id\":\"1010507\"}],\"durations\":[\"110分钟(剧场版)\",\"133分钟(国际版)\"],\"collect_count\":1991251,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Léon\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33301.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33301.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33301.jpg\"},\"name_en\":\"Luc Besson\",\"name\":\"吕克·贝松\",\"alt\":\"https://movie.douban.com/celebrity/1031876/\",\"id\":\"1031876\"}],\"pubdates\":[\"1994-09-14(法国)\"],\"year\":\"1994\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p511118051.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p511118051.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p511118051.jpg\"},\"alt\":\"https://movie.douban.com/subject/1295644/\",\"id\":\"1295644\"},{\"rating\":{\"max\":10,\"average\":9.4,\"details\":{\"1\":928,\"2\":1730,\"3\":27448,\"4\":193737,\"5\":705781},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"爱情\"],\"title\":\"阿甘正传\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p28603.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p28603.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p28603.jpg\"},\"name_en\":\"Tom Hanks\",\"name\":\"汤姆·汉克斯\",\"alt\":\"https://movie.douban.com/celebrity/1054450/\",\"id\":\"1054450\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1537890386.77.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1537890386.77.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1537890386.77.jpg\"},\"name_en\":\"Robin Wright\",\"name\":\"罗宾·怀特\",\"alt\":\"https://movie.douban.com/celebrity/1002676/\",\"id\":\"1002676\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1345.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1345.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1345.jpg\"},\"name_en\":\"Gary Sinise\",\"name\":\"加里·西尼斯\",\"alt\":\"https://movie.douban.com/celebrity/1031848/\",\"id\":\"1031848\"}],\"durations\":[\"142分钟\"],\"collect_count\":1694595,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Forrest Gump\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p505.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p505.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p505.jpg\"},\"name_en\":\"Robert Zemeckis\",\"name\":\"罗伯特·泽米吉斯\",\"alt\":\"https://movie.douban.com/celebrity/1053564/\",\"id\":\"1053564\"}],\"pubdates\":[\"1994-06-23(洛杉矶首映)\",\"1994-07-06(美国)\"],\"year\":\"1994\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2559011361.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2559011361.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2559011361.jpg\"},\"alt\":\"https://movie.douban.com/subject/1292720/\",\"id\":\"1292720\"},{\"rating\":{\"max\":10,\"average\":9.5,\"details\":{\"1\":595,\"2\":1094,\"3\":13258,\"4\":97334,\"5\":438694},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"喜剧\",\"爱情\"],\"title\":\"美丽人生\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26764.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26764.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26764.jpg\"},\"name_en\":\"Roberto Benigni\",\"name\":\"罗伯托·贝尼尼\",\"alt\":\"https://movie.douban.com/celebrity/1041004/\",\"id\":\"1041004\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p9548.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p9548.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p9548.jpg\"},\"name_en\":\"Nicoletta Braschi\",\"name\":\"尼可莱塔·布拉斯基\",\"alt\":\"https://movie.douban.com/celebrity/1000375/\",\"id\":\"1000375\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45590.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45590.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45590.jpg\"},\"name_en\":\"Giorgio Cantarini\",\"name\":\"乔治·坎塔里尼\",\"alt\":\"https://movie.douban.com/celebrity/1000368/\",\"id\":\"1000368\"}],\"durations\":[\"116分钟\",\"125分钟(加长版)\"],\"collect_count\":891246,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"La vita è bella\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26764.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26764.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26764.jpg\"},\"name_en\":\"Roberto Benigni\",\"name\":\"罗伯托·贝尼尼\",\"alt\":\"https://movie.douban.com/celebrity/1041004/\",\"id\":\"1041004\"}],\"pubdates\":[\"1997-12-20(意大利)\"],\"year\":\"1997\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p510861873.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p510861873.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p510861873.jpg\"},\"alt\":\"https://movie.douban.com/subject/1292063/\",\"id\":\"1292063\"},{\"rating\":{\"max\":10,\"average\":9.4,\"details\":{\"1\":864,\"2\":2137,\"3\":36656,\"4\":205417,\"5\":648609},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"爱情\",\"灾难\"],\"title\":\"泰坦尼克号\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p814.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p814.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p814.jpg\"},\"name_en\":\"Leonardo DiCaprio\",\"name\":\"莱昂纳多·迪卡普里奥\",\"alt\":\"https://movie.douban.com/celebrity/1041029/\",\"id\":\"1041029\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p53358.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p53358.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p53358.jpg\"},\"name_en\":\"Kate Winslet\",\"name\":\"凯特·温丝莱特\",\"alt\":\"https://movie.douban.com/celebrity/1054446/\",\"id\":\"1054446\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45186.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45186.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45186.jpg\"},\"name_en\":\"Billy Zane\",\"name\":\"比利·赞恩\",\"alt\":\"https://movie.douban.com/celebrity/1031864/\",\"id\":\"1031864\"}],\"durations\":[\"194分钟\",\"227分钟(白星版)\"],\"collect_count\":1587886,\"mainland_pubdate\":\"1998-04-03\",\"has_video\":true,\"original_title\":\"Titanic\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33715.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33715.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33715.jpg\"},\"name_en\":\"James Cameron\",\"name\":\"詹姆斯·卡梅隆\",\"alt\":\"https://movie.douban.com/celebrity/1022571/\",\"id\":\"1022571\"}],\"pubdates\":[\"1997-11-01(东京电影节)\",\"1997-12-19(美国)\",\"1998-04-03(中国大陆)\"],\"year\":\"1997\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p457760035.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p457760035.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p457760035.jpg\"},\"alt\":\"https://movie.douban.com/subject/1292722/\",\"id\":\"1292722\"},{\"rating\":{\"max\":10,\"average\":9.3,\"details\":{\"1\":1027,\"2\":2042,\"3\":37247,\"4\":232693,\"5\":677880},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"动画\",\"奇幻\"],\"title\":\"千与千寻\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1463193210.13.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1463193210.13.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1463193210.13.jpg\"},\"name_en\":\"Rumi Hiiragi\",\"name\":\"柊瑠美\",\"alt\":\"https://movie.douban.com/celebrity/1023337/\",\"id\":\"1023337\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44986.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44986.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44986.jpg\"},\"name_en\":\"Miyu Irino\",\"name\":\"入野自由\",\"alt\":\"https://movie.douban.com/celebrity/1005438/\",\"id\":\"1005438\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1376151005.51.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1376151005.51.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1376151005.51.jpg\"},\"name_en\":\"Mari Natsuki\",\"name\":\"夏木真理\",\"alt\":\"https://movie.douban.com/celebrity/1045797/\",\"id\":\"1045797\"}],\"durations\":[\"125分钟\"],\"collect_count\":1566353,\"mainland_pubdate\":\"2019-06-21\",\"has_video\":false,\"original_title\":\"千と千尋の神隠し\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\"},\"name_en\":\"Hayao Miyazaki\",\"name\":\"宫崎骏\",\"alt\":\"https://movie.douban.com/celebrity/1054439/\",\"id\":\"1054439\"}],\"pubdates\":[\"2001-07-20(日本)\",\"2019-06-21(中国大陆)\"],\"year\":\"2001\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557573348.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557573348.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557573348.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291561/\",\"id\":\"1291561\"},{\"rating\":{\"max\":10,\"average\":9.5,\"details\":{\"1\":402,\"2\":665,\"3\":12533,\"4\":95863,\"5\":378431},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"历史\",\"战争\"],\"title\":\"辛德勒的名单\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44906.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44906.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44906.jpg\"},\"name_en\":\"Liam Neeson\",\"name\":\"连姆·尼森\",\"alt\":\"https://movie.douban.com/celebrity/1031220/\",\"id\":\"1031220\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1374649659.58.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1374649659.58.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1374649659.58.jpg\"},\"name_en\":\"Ben Kingsley\",\"name\":\"本·金斯利\",\"alt\":\"https://movie.douban.com/celebrity/1054393/\",\"id\":\"1054393\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p28941.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p28941.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p28941.jpg\"},\"name_en\":\"Ralph Fiennes\",\"name\":\"拉尔夫·费因斯\",\"alt\":\"https://movie.douban.com/celebrity/1006956/\",\"id\":\"1006956\"}],\"durations\":[\"195分钟\"],\"collect_count\":828723,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Schindler's List\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p34602.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p34602.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p34602.jpg\"},\"name_en\":\"Steven Spielberg\",\"name\":\"史蒂文·斯皮尔伯格\",\"alt\":\"https://movie.douban.com/celebrity/1054440/\",\"id\":\"1054440\"}],\"pubdates\":[\"1993-11-30(华盛顿首映)\",\"1994-02-04(美国)\"],\"year\":\"1993\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p492406163.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p492406163.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p492406163.jpg\"},\"alt\":\"https://movie.douban.com/subject/1295124/\",\"id\":\"1295124\"},{\"rating\":{\"max\":10,\"average\":9.3,\"details\":{\"1\":1412,\"2\":2362,\"3\":38120,\"4\":237809,\"5\":655006},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"科幻\",\"悬疑\"],\"title\":\"盗梦空间\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p814.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p814.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p814.jpg\"},\"name_en\":\"Leonardo DiCaprio\",\"name\":\"莱昂纳多·迪卡普里奥\",\"alt\":\"https://movie.douban.com/celebrity/1041029/\",\"id\":\"1041029\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3517.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3517.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3517.jpg\"},\"name_en\":\"Joseph Gordon-Levitt\",\"name\":\"约瑟夫·高登-莱维特\",\"alt\":\"https://movie.douban.com/celebrity/1101703/\",\"id\":\"1101703\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p118.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p118.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p118.jpg\"},\"name_en\":\"Ellen Page\",\"name\":\"艾伦·佩吉\",\"alt\":\"https://movie.douban.com/celebrity/1012520/\",\"id\":\"1012520\"}],\"durations\":[\"148分钟\"],\"collect_count\":1673207,\"mainland_pubdate\":\"2010-09-01\",\"has_video\":true,\"original_title\":\"Inception\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p673.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p673.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p673.jpg\"},\"name_en\":\"Christopher Nolan\",\"name\":\"克里斯托弗·诺兰\",\"alt\":\"https://movie.douban.com/celebrity/1054524/\",\"id\":\"1054524\"}],\"pubdates\":[\"2010-07-16(美国)\",\"2010-09-01(中国大陆)\"],\"year\":\"2010\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p513344864.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p513344864.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p513344864.jpg\"},\"alt\":\"https://movie.douban.com/subject/3541415/\",\"id\":\"3541415\"},{\"rating\":{\"max\":10,\"average\":9.3,\"details\":{\"1\":722,\"2\":1919,\"3\":29462,\"4\":148939,\"5\":457392},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"忠犬八公的故事\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33013.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33013.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33013.jpg\"},\"name_en\":\"Richard Gere\",\"name\":\"理查·基尔\",\"alt\":\"https://movie.douban.com/celebrity/1040997/\",\"id\":\"1040997\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5502.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5502.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5502.jpg\"},\"name_en\":\"Sarah Roemer\",\"name\":\"萨拉·罗默尔\",\"alt\":\"https://movie.douban.com/celebrity/1049499/\",\"id\":\"1049499\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p17520.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p17520.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p17520.jpg\"},\"name_en\":\"Joan Allen\",\"name\":\"琼·艾伦\",\"alt\":\"https://movie.douban.com/celebrity/1025215/\",\"id\":\"1025215\"}],\"durations\":[\"93分钟\"],\"collect_count\":1232018,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Hachi: A Dog's Tale\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4333.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4333.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4333.jpg\"},\"name_en\":\"Lasse Hallström\",\"name\":\"拉斯·霍尔斯道姆\",\"alt\":\"https://movie.douban.com/celebrity/1018014/\",\"id\":\"1018014\"}],\"pubdates\":[\"2009-06-13(西雅图电影节)\",\"2010-03-12(英国)\"],\"year\":\"2009\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p524964016.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p524964016.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p524964016.jpg\"},\"alt\":\"https://movie.douban.com/subject/3011091/\",\"id\":\"3011091\"},{\"rating\":{\"max\":10,\"average\":9.3,\"details\":{\"1\":549,\"2\":1442,\"3\":26081,\"4\":148542,\"5\":435561},\"stars\":\"50\",\"min\":0},\"genres\":[\"爱情\",\"科幻\",\"动画\"],\"title\":\"机器人总动员\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13028.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13028.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13028.jpg\"},\"name_en\":\"Ben Burtt\",\"name\":\"本·贝尔特\",\"alt\":\"https://movie.douban.com/celebrity/1009535/\",\"id\":\"1009535\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1519794715.93.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1519794715.93.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1519794715.93.jpg\"},\"name_en\":\"Elissa Knight\",\"name\":\"艾丽莎·奈特\",\"alt\":\"https://movie.douban.com/celebrity/1000389/\",\"id\":\"1000389\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p31068.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p31068.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p31068.jpg\"},\"name_en\":\"Jeff Garlin\",\"name\":\"杰夫·格尔林\",\"alt\":\"https://movie.douban.com/celebrity/1018022/\",\"id\":\"1018022\"}],\"durations\":[\"98分钟\"],\"collect_count\":1076179,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"WALL·E\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467359656.96.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467359656.96.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467359656.96.jpg\"},\"name_en\":\"Andrew Stanton\",\"name\":\"安德鲁·斯坦顿\",\"alt\":\"https://movie.douban.com/celebrity/1036450/\",\"id\":\"1036450\"}],\"pubdates\":[\"2008-06-27(美国)\"],\"year\":\"2008\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1461851991.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1461851991.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1461851991.jpg\"},\"alt\":\"https://movie.douban.com/subject/2131459/\",\"id\":\"2131459\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":2505,\"2\":5050,\"3\":47984,\"4\":218884,\"5\":573094},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"喜剧\",\"爱情\"],\"title\":\"三傻大闹宝莱坞\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13628.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13628.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13628.jpg\"},\"name_en\":\"Aamir Khan\",\"name\":\"阿米尔·汗\",\"alt\":\"https://movie.douban.com/celebrity/1031931/\",\"id\":\"1031931\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5568.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5568.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5568.jpg\"},\"name_en\":\"Kareena Kapoor\",\"name\":\"卡琳娜·卡普尔\",\"alt\":\"https://movie.douban.com/celebrity/1049635/\",\"id\":\"1049635\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5651.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5651.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5651.jpg\"},\"name_en\":\"R. Madhavan\",\"name\":\"马达范\",\"alt\":\"https://movie.douban.com/celebrity/1018290/\",\"id\":\"1018290\"}],\"durations\":[\"171分钟(印度)\"],\"collect_count\":1434595,\"mainland_pubdate\":\"2011-12-08\",\"has_video\":false,\"original_title\":\"3 Idiots\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p16549.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p16549.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p16549.jpg\"},\"name_en\":\"Rajkumar Hirani\",\"name\":\"拉吉库马尔·希拉尼\",\"alt\":\"https://movie.douban.com/celebrity/1286677/\",\"id\":\"1286677\"}],\"pubdates\":[\"2009-12-25(印度)\",\"2011-12-08(中国大陆)\"],\"year\":\"2009\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p579729551.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p579729551.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p579729551.jpg\"},\"alt\":\"https://movie.douban.com/subject/3793023/\",\"id\":\"3793023\"},{\"rating\":{\"max\":10,\"average\":9.3,\"details\":{\"1\":348,\"2\":1068,\"3\":24006,\"4\":161967,\"5\":404616},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"音乐\"],\"title\":\"放牛班的春天\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3363.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3363.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3363.jpg\"},\"name_en\":\"Gérard Jugnot\",\"name\":\"热拉尔·朱尼奥\",\"alt\":\"https://movie.douban.com/celebrity/1048281/\",\"id\":\"1048281\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p9329.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p9329.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p9329.jpg\"},\"name_en\":\"François Berléand\",\"name\":\"弗朗索瓦·贝莱昂\",\"alt\":\"https://movie.douban.com/celebrity/1054351/\",\"id\":\"1054351\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44424.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44424.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44424.jpg\"},\"name_en\":\"Kad Merad\",\"name\":\"凯德·麦拉德\",\"alt\":\"https://movie.douban.com/celebrity/1000491/\",\"id\":\"1000491\"}],\"durations\":[\"97分钟\"],\"collect_count\":1084072,\"mainland_pubdate\":\"2004-10-16\",\"has_video\":true,\"original_title\":\"Les choristes\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p24744.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p24744.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p24744.jpg\"},\"name_en\":\"Christophe Barratier\",\"name\":\"克里斯托夫·巴拉蒂\",\"alt\":\"https://movie.douban.com/celebrity/1277959/\",\"id\":\"1277959\"}],\"pubdates\":[\"2004-03-17(法国)\",\"2004-10-16(中国大陆)\"],\"year\":\"2004\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910824951.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910824951.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910824951.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291549/\",\"id\":\"1291549\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":1207,\"2\":3371,\"3\":35420,\"4\":174407,\"5\":453953},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"音乐\"],\"title\":\"海上钢琴师\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6281.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6281.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6281.jpg\"},\"name_en\":\"Tim Roth\",\"name\":\"蒂姆·罗斯\",\"alt\":\"https://movie.douban.com/celebrity/1025176/\",\"id\":\"1025176\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1355152571.6.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1355152571.6.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1355152571.6.jpg\"},\"name_en\":\"Pruitt Taylor Vince\",\"name\":\"普路特·泰勒·文斯\",\"alt\":\"https://movie.douban.com/celebrity/1010659/\",\"id\":\"1010659\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12333.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12333.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12333.jpg\"},\"name_en\":\"Bill Nunn\",\"name\":\"比尔·努恩\",\"alt\":\"https://movie.douban.com/celebrity/1027407/\",\"id\":\"1027407\"}],\"durations\":[\"165分钟\",\"120分钟(法国版)\",\"169分钟(加长版)\"],\"collect_count\":1189627,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"La leggenda del pianista sull'oceano\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p195.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p195.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p195.jpg\"},\"name_en\":\"Giuseppe Tornatore\",\"name\":\"朱塞佩·托纳多雷\",\"alt\":\"https://movie.douban.com/celebrity/1018983/\",\"id\":\"1018983\"}],\"pubdates\":[\"1998-10-28(意大利)\"],\"year\":\"1998\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p511146807.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p511146807.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p511146807.jpg\"},\"alt\":\"https://movie.douban.com/subject/1292001/\",\"id\":\"1292001\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":572,\"2\":1679,\"3\":30415,\"4\":190743,\"5\":447069},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"科幻\"],\"title\":\"楚门的世界\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p615.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p615.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p615.jpg\"},\"name_en\":\"Jim Carrey\",\"name\":\"金·凯瑞\",\"alt\":\"https://movie.douban.com/celebrity/1054438/\",\"id\":\"1054438\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p38385.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p38385.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p38385.jpg\"},\"name_en\":\"Laura Linney\",\"name\":\"劳拉·琳妮\",\"alt\":\"https://movie.douban.com/celebrity/1053572/\",\"id\":\"1053572\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1485163747.76.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1485163747.76.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1485163747.76.jpg\"},\"name_en\":\"Ed Harris\",\"name\":\"艾德·哈里斯\",\"alt\":\"https://movie.douban.com/celebrity/1048024/\",\"id\":\"1048024\"}],\"durations\":[\"103分钟\"],\"collect_count\":1133820,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"The Truman Show\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4360.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4360.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4360.jpg\"},\"name_en\":\"Peter Weir\",\"name\":\"彼得·威尔\",\"alt\":\"https://movie.douban.com/celebrity/1022721/\",\"id\":\"1022721\"}],\"pubdates\":[\"1998-06-05(美国)\"],\"year\":\"1998\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p479682972.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p479682972.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p479682972.jpg\"},\"alt\":\"https://movie.douban.com/subject/1292064/\",\"id\":\"1292064\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":1744,\"2\":3092,\"3\":39968,\"4\":170942,\"5\":441161},\"stars\":\"45\",\"min\":0},\"genres\":[\"喜剧\",\"爱情\",\"奇幻\"],\"title\":\"大话西游之大圣娶亲\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p47421.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p47421.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p47421.jpg\"},\"name_en\":\"Stephen Chow\",\"name\":\"周星驰\",\"alt\":\"https://movie.douban.com/celebrity/1048026/\",\"id\":\"1048026\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45481.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45481.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45481.jpg\"},\"name_en\":\"Man Tat Ng\",\"name\":\"吴孟达\",\"alt\":\"https://movie.douban.com/celebrity/1016771/\",\"id\":\"1016771\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p49237.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p49237.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p49237.jpg\"},\"name_en\":\"Athena Chu\",\"name\":\"朱茵\",\"alt\":\"https://movie.douban.com/celebrity/1041734/\",\"id\":\"1041734\"}],\"durations\":[\"95分钟\",\"110分钟(重映版)\"],\"collect_count\":1153456,\"mainland_pubdate\":\"2014-10-24\",\"has_video\":true,\"original_title\":\"西遊記大結局之仙履奇緣\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45374.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45374.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45374.jpg\"},\"name_en\":\"Jeffrey Lau\",\"name\":\"刘镇伟\",\"alt\":\"https://movie.douban.com/celebrity/1274431/\",\"id\":\"1274431\"}],\"pubdates\":[\"1995-02-04(香港)\",\"2014-10-24(中国大陆)\",\"2017-04-13(中国大陆重映)\"],\"year\":\"1995\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2455050536.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2455050536.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2455050536.jpg\"},\"alt\":\"https://movie.douban.com/subject/1292213/\",\"id\":\"1292213\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":2428,\"2\":3526,\"3\":35196,\"4\":169676,\"5\":482070},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"科幻\",\"冒险\"],\"title\":\"星际穿越\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1392653727.04.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1392653727.04.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1392653727.04.jpg\"},\"name_en\":\"Matthew McConaughey\",\"name\":\"马修·麦康纳\",\"alt\":\"https://movie.douban.com/celebrity/1040511/\",\"id\":\"1040511\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p10467.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p10467.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p10467.jpg\"},\"name_en\":\"Anne Hathaway\",\"name\":\"安妮·海瑟薇\",\"alt\":\"https://movie.douban.com/celebrity/1048027/\",\"id\":\"1048027\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p54076.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p54076.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p54076.jpg\"},\"name_en\":\"Jessica Chastain\",\"name\":\"杰西卡·查斯坦\",\"alt\":\"https://movie.douban.com/celebrity/1000225/\",\"id\":\"1000225\"}],\"durations\":[\"169分钟\"],\"collect_count\":1132627,\"mainland_pubdate\":\"2014-11-12\",\"has_video\":true,\"original_title\":\"Interstellar\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p673.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p673.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p673.jpg\"},\"name_en\":\"Christopher Nolan\",\"name\":\"克里斯托弗·诺兰\",\"alt\":\"https://movie.douban.com/celebrity/1054524/\",\"id\":\"1054524\"}],\"pubdates\":[\"2014-11-07(美国)\",\"2014-11-12(中国大陆)\"],\"year\":\"2014\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2206088801.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2206088801.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2206088801.jpg\"},\"alt\":\"https://movie.douban.com/subject/1889243/\",\"id\":\"1889243\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":132,\"2\":488,\"3\":10840,\"4\":54178,\"5\":119052},\"stars\":\"45\",\"min\":0},\"genres\":[\"动画\",\"奇幻\",\"冒险\"],\"title\":\"龙猫\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1455201170.02.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1455201170.02.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1455201170.02.jpg\"},\"name_en\":\"Noriko Hidaka\",\"name\":\"日高法子\",\"alt\":\"https://movie.douban.com/celebrity/1019382/\",\"id\":\"1019382\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p29537.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p29537.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p29537.jpg\"},\"name_en\":\"Chika Sakamoto\",\"name\":\"坂本千夏\",\"alt\":\"https://movie.douban.com/celebrity/1025582/\",\"id\":\"1025582\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1503457262.72.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1503457262.72.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1503457262.72.jpg\"},\"name_en\":\"Shigesato Itoi\",\"name\":\"糸井重里\",\"alt\":\"https://movie.douban.com/celebrity/1379738/\",\"id\":\"1379738\"}],\"durations\":[\"86分钟\"],\"collect_count\":1045766,\"mainland_pubdate\":\"2018-12-14\",\"has_video\":true,\"original_title\":\"となりのトトロ\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\"},\"name_en\":\"Hayao Miyazaki\",\"name\":\"宫崎骏\",\"alt\":\"https://movie.douban.com/celebrity/1054439/\",\"id\":\"1054439\"}],\"pubdates\":[\"1988-04-16(日本)\",\"2018-12-14(中国大陆)\"],\"year\":\"1988\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2540924496.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2540924496.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2540924496.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291560/\",\"id\":\"1291560\"},{\"rating\":{\"max\":10,\"average\":9.3,\"details\":{\"1\":826,\"2\":1942,\"3\":22055,\"4\":102055,\"5\":291347},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"犯罪\"],\"title\":\"教父\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45035.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45035.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45035.jpg\"},\"name_en\":\"Marlon Brando\",\"name\":\"马龙·白兰度\",\"alt\":\"https://movie.douban.com/celebrity/1041025/\",\"id\":\"1041025\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p645.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p645.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p645.jpg\"},\"name_en\":\"Al Pacino\",\"name\":\"阿尔·帕西诺\",\"alt\":\"https://movie.douban.com/celebrity/1054451/\",\"id\":\"1054451\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p53524.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p53524.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p53524.jpg\"},\"name_en\":\"James Caan\",\"name\":\"詹姆斯·肯恩\",\"alt\":\"https://movie.douban.com/celebrity/1000050/\",\"id\":\"1000050\"}],\"durations\":[\"175 分钟\"],\"collect_count\":723562,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"The Godfather\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p592.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p592.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p592.jpg\"},\"name_en\":\"Francis Ford Coppola\",\"name\":\"弗朗西斯·福特·科波拉\",\"alt\":\"https://movie.douban.com/celebrity/1054419/\",\"id\":\"1054419\"}],\"pubdates\":[\"1972-03-15(纽约首映)\",\"1972-03-24(美国)\"],\"year\":\"1972\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p616779645.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p616779645.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p616779645.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291841/\",\"id\":\"1291841\"},{\"rating\":{\"max\":10,\"average\":9.3,\"details\":{\"1\":482,\"2\":1015,\"3\":16000,\"4\":107145,\"5\":280496},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"熔炉\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p55195.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p55195.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p55195.jpg\"},\"name_en\":\"Yoo Gong\",\"name\":\"孔侑\",\"alt\":\"https://movie.douban.com/celebrity/1011009/\",\"id\":\"1011009\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1409765749.47.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1409765749.47.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1409765749.47.jpg\"},\"name_en\":\"Yu-mi Jung\",\"name\":\"郑有美\",\"alt\":\"https://movie.douban.com/celebrity/1276062/\",\"id\":\"1276062\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1393488191.45.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1393488191.45.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1393488191.45.jpg\"},\"name_en\":\"Jee-young Kim\",\"name\":\"金志映\",\"alt\":\"https://movie.douban.com/celebrity/1331104/\",\"id\":\"1331104\"}],\"durations\":[\"125分钟\"],\"collect_count\":626055,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"도가니\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p52558.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p52558.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p52558.jpg\"},\"name_en\":\"Dong-hyuk Hwang\",\"name\":\"黄东赫\",\"alt\":\"https://movie.douban.com/celebrity/1317274/\",\"id\":\"1317274\"}],\"pubdates\":[\"2011-09-22(韩国)\"],\"year\":\"2011\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1363250216.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1363250216.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1363250216.jpg\"},\"alt\":\"https://movie.douban.com/subject/5912992/\",\"id\":\"5912992\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":357,\"2\":1170,\"3\":29467,\"4\":168066,\"5\":352415},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"犯罪\",\"悬疑\"],\"title\":\"无间道\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1378956633.91.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1378956633.91.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1378956633.91.jpg\"},\"name_en\":\"Andy Lau\",\"name\":\"刘德华\",\"alt\":\"https://movie.douban.com/celebrity/1054424/\",\"id\":\"1054424\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33525.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33525.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33525.jpg\"},\"name_en\":\"Tony Leung Chiu Wai\",\"name\":\"梁朝伟\",\"alt\":\"https://movie.douban.com/celebrity/1115918/\",\"id\":\"1115918\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p24841.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p24841.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p24841.jpg\"},\"name_en\":\"Anthony Wong Chau-Sang\",\"name\":\"黄秋生\",\"alt\":\"https://movie.douban.com/celebrity/1050076/\",\"id\":\"1050076\"}],\"durations\":[\"101分钟\",\"97分钟(导演剪辑版)\"],\"collect_count\":987508,\"mainland_pubdate\":\"2003-09-05\",\"has_video\":true,\"original_title\":\"無間道\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1403267018.07.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1403267018.07.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1403267018.07.jpg\"},\"name_en\":\"Andrew Lau\",\"name\":\"刘伟强\",\"alt\":\"https://movie.douban.com/celebrity/1106979/\",\"id\":\"1106979\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3547.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3547.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3547.jpg\"},\"name_en\":\"Alan Mak\",\"name\":\"麦兆辉\",\"alt\":\"https://movie.douban.com/celebrity/1126158/\",\"id\":\"1126158\"}],\"pubdates\":[\"2002-12-12(香港)\",\"2003-09-05(中国大陆)\"],\"year\":\"2002\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2233971046.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2233971046.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2233971046.jpg\"},\"alt\":\"https://movie.douban.com/subject/1307914/\",\"id\":\"1307914\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":804,\"2\":1984,\"3\":41806,\"4\":239640,\"5\":508671},\"stars\":\"45\",\"min\":0},\"genres\":[\"喜剧\",\"动画\",\"冒险\"],\"title\":\"疯狂动物城\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4815.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4815.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4815.jpg\"},\"name_en\":\"Ginnifer Goodwin\",\"name\":\"金妮弗·古德温\",\"alt\":\"https://movie.douban.com/celebrity/1017930/\",\"id\":\"1017930\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p18772.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p18772.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p18772.jpg\"},\"name_en\":\"Jason Bateman\",\"name\":\"杰森·贝特曼\",\"alt\":\"https://movie.douban.com/celebrity/1013760/\",\"id\":\"1013760\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1410696282.74.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1410696282.74.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1410696282.74.jpg\"},\"name_en\":\"Idris Elba\",\"name\":\"伊德里斯·艾尔巴\",\"alt\":\"https://movie.douban.com/celebrity/1049501/\",\"id\":\"1049501\"}],\"durations\":[\"109分钟(中国大陆)\",\"108分钟\"],\"collect_count\":1223330,\"mainland_pubdate\":\"2016-03-04\",\"has_video\":true,\"original_title\":\"Zootopia\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1457505519.94.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1457505519.94.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1457505519.94.jpg\"},\"name_en\":\"Byron Howard\",\"name\":\"拜伦·霍华德\",\"alt\":\"https://movie.douban.com/celebrity/1286985/\",\"id\":\"1286985\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1457505501.8.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1457505501.8.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1457505501.8.jpg\"},\"name_en\":\"Rich Moore\",\"name\":\"瑞奇·摩尔\",\"alt\":\"https://movie.douban.com/celebrity/1324037/\",\"id\":\"1324037\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1456810614.66.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1456810614.66.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1456810614.66.jpg\"},\"name_en\":\"Jared Bush\",\"name\":\"杰拉德·布什\",\"alt\":\"https://movie.douban.com/celebrity/1304069/\",\"id\":\"1304069\"}],\"pubdates\":[\"2016-03-04(中国大陆)\",\"2016-03-04(美国)\"],\"year\":\"2016\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2315672647.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2315672647.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2315672647.jpg\"},\"alt\":\"https://movie.douban.com/subject/25662329/\",\"id\":\"25662329\"},{\"rating\":{\"max\":10,\"average\":9.1,\"details\":{\"1\":771,\"2\":2744,\"3\":45899,\"4\":221081,\"5\":412457},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"传记\",\"家庭\"],\"title\":\"当幸福来敲门\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41483.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41483.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41483.jpg\"},\"name_en\":\"Will Smith\",\"name\":\"威尔·史密斯\",\"alt\":\"https://movie.douban.com/celebrity/1027138/\",\"id\":\"1027138\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1519305434.22.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1519305434.22.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1519305434.22.jpg\"},\"name_en\":\"Jaden Smith\",\"name\":\"贾登·史密斯\",\"alt\":\"https://movie.douban.com/celebrity/1010532/\",\"id\":\"1010532\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1378018910.89.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1378018910.89.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1378018910.89.jpg\"},\"name_en\":\"Thandie Newton\",\"name\":\"坦迪·牛顿\",\"alt\":\"https://movie.douban.com/celebrity/1040513/\",\"id\":\"1040513\"}],\"durations\":[\"117分钟\"],\"collect_count\":1200299,\"mainland_pubdate\":\"2008-01-17\",\"has_video\":true,\"original_title\":\"The Pursuit of Happyness\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p20409.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p20409.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p20409.jpg\"},\"name_en\":\"Gabriele Muccino\",\"name\":\"加布里埃莱·穆奇诺\",\"alt\":\"https://movie.douban.com/celebrity/1045093/\",\"id\":\"1045093\"}],\"pubdates\":[\"2006-12-15(美国)\",\"2008-01-17(中国大陆)\"],\"year\":\"2006\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1312700744.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1312700744.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1312700744.jpg\"},\"alt\":\"https://movie.douban.com/subject/1849031/\",\"id\":\"1849031\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":996,\"2\":3758,\"3\":57226,\"4\":253603,\"5\":465633},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"喜剧\",\"爱情\"],\"title\":\"怦然心动\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p16442.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p16442.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p16442.jpg\"},\"name_en\":\"Madeline Carroll\",\"name\":\"玛德琳·卡罗尔\",\"alt\":\"https://movie.douban.com/celebrity/1031867/\",\"id\":\"1031867\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p22277.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p22277.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p22277.jpg\"},\"name_en\":\"Callan McAuliffe\",\"name\":\"卡兰·麦克奥利菲\",\"alt\":\"https://movie.douban.com/celebrity/1004751/\",\"id\":\"1004751\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12355.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12355.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12355.jpg\"},\"name_en\":\"Rebecca De Mornay\",\"name\":\"瑞贝卡·德·莫妮\",\"alt\":\"https://movie.douban.com/celebrity/1049546/\",\"id\":\"1049546\"}],\"durations\":[\"90分钟\"],\"collect_count\":1315867,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Flipped\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1379484184.83.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1379484184.83.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1379484184.83.jpg\"},\"name_en\":\"Rob Reiner\",\"name\":\"罗伯·莱纳\",\"alt\":\"https://movie.douban.com/celebrity/1031903/\",\"id\":\"1031903\"}],\"pubdates\":[\"2010-07-26(好莱坞首映)\",\"2010-09-10(美国)\"],\"year\":\"2010\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p663036666.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p663036666.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p663036666.jpg\"},\"alt\":\"https://movie.douban.com/subject/3319755/\",\"id\":\"3319755\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":439,\"2\":1340,\"3\":21282,\"4\":130223,\"5\":297838},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"喜剧\"],\"title\":\"触不可及\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1375092314.14.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1375092314.14.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1375092314.14.jpg\"},\"name_en\":\"François Cluzet\",\"name\":\"弗朗索瓦·克鲁塞\",\"alt\":\"https://movie.douban.com/celebrity/1050210/\",\"id\":\"1050210\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41401.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41401.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41401.jpg\"},\"name_en\":\"Omar Sy\",\"name\":\"奥玛·希\",\"alt\":\"https://movie.douban.com/celebrity/1220507/\",\"id\":\"1220507\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42048.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42048.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42048.jpg\"},\"name_en\":\"Anne Le Ny\",\"name\":\"安娜·勒尼\",\"alt\":\"https://movie.douban.com/celebrity/1289597/\",\"id\":\"1289597\"}],\"durations\":[\"112分钟\"],\"collect_count\":749762,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"Intouchables\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41640.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41640.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41640.jpg\"},\"name_en\":\"Olivier Nakache\",\"name\":\"奥利维埃·纳卡什\",\"alt\":\"https://movie.douban.com/celebrity/1001404/\",\"id\":\"1001404\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p50463.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p50463.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p50463.jpg\"},\"name_en\":\"Eric Toledano\",\"name\":\"埃里克·托莱达诺\",\"alt\":\"https://movie.douban.com/celebrity/1010884/\",\"id\":\"1010884\"}],\"pubdates\":[\"2011-11-02(法国)\"],\"year\":\"2011\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1454261925.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1454261925.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1454261925.jpg\"},\"alt\":\"https://movie.douban.com/subject/6786002/\",\"id\":\"6786002\"},{\"rating\":{\"max\":10,\"average\":9.1,\"details\":{\"1\":1062,\"2\":2364,\"3\":30469,\"4\":123814,\"5\":296285},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"动作\",\"科幻\"],\"title\":\"蝙蝠侠：黑暗骑士\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1004.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1004.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1004.jpg\"},\"name_en\":\"Christian Bale\",\"name\":\"克里斯蒂安·贝尔\",\"alt\":\"https://movie.douban.com/celebrity/1005773/\",\"id\":\"1005773\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13801.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13801.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13801.jpg\"},\"name_en\":\"Heath Ledger\",\"name\":\"希斯·莱杰\",\"alt\":\"https://movie.douban.com/celebrity/1006957/\",\"id\":\"1006957\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p522.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p522.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p522.jpg\"},\"name_en\":\"Aaron Eckhart\",\"name\":\"艾伦·艾克哈特\",\"alt\":\"https://movie.douban.com/celebrity/1053577/\",\"id\":\"1053577\"}],\"durations\":[\"152分钟\"],\"collect_count\":770721,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"The Dark Knight\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p673.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p673.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p673.jpg\"},\"name_en\":\"Christopher Nolan\",\"name\":\"克里斯托弗·诺兰\",\"alt\":\"https://movie.douban.com/celebrity/1054524/\",\"id\":\"1054524\"}],\"pubdates\":[\"2008-07-14(纽约首映)\",\"2008-07-18(美国)\"],\"year\":\"2008\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p462657443.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p462657443.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p462657443.jpg\"},\"alt\":\"https://movie.douban.com/subject/1851857/\",\"id\":\"1851857\"},{\"rating\":{\"max\":10,\"average\":9.3,\"details\":{\"1\":370,\"2\":967,\"3\":14740,\"4\":78437,\"5\":205382},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"历史\",\"爱情\"],\"title\":\"乱世佳人\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3151.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3151.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p3151.jpg\"},\"name_en\":\"Vivien Leigh\",\"name\":\"费雯·丽\",\"alt\":\"https://movie.douban.com/celebrity/1010506/\",\"id\":\"1010506\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5289.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5289.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5289.jpg\"},\"name_en\":\"Clark Gable\",\"name\":\"克拉克·盖博\",\"alt\":\"https://movie.douban.com/celebrity/1006997/\",\"id\":\"1006997\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1397551638.55.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1397551638.55.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1397551638.55.jpg\"},\"name_en\":\"Olivia de Havilland\",\"name\":\"奥利维娅·德哈维兰\",\"alt\":\"https://movie.douban.com/celebrity/1010604/\",\"id\":\"1010604\"}],\"durations\":[\"238分钟\",\"234分钟\"],\"collect_count\":536325,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Gone with the Wind\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p11303.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p11303.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p11303.jpg\"},\"name_en\":\"Victor Fleming\",\"name\":\"维克多·弗莱明\",\"alt\":\"https://movie.douban.com/celebrity/1032275/\",\"id\":\"1032275\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p19067.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p19067.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p19067.jpg\"},\"name_en\":\"George Cukor\",\"name\":\"乔治·库克\",\"alt\":\"https://movie.douban.com/celebrity/1010711/\",\"id\":\"1010711\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p54831.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p54831.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p54831.jpg\"},\"name_en\":\"Sam Wood\",\"name\":\"山姆·伍德\",\"alt\":\"https://movie.douban.com/celebrity/1012588/\",\"id\":\"1012588\"}],\"pubdates\":[\"1939-12-15(亚特兰大首映)\",\"1940-01-17(美国)\"],\"year\":\"1939\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1963126880.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1963126880.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1963126880.jpg\"},\"alt\":\"https://movie.douban.com/subject/1300267/\",\"id\":\"1300267\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":419,\"2\":1259,\"3\":17954,\"4\":103180,\"5\":233970},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"历史\",\"家庭\"],\"title\":\"活着\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p46.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p46.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p46.jpg\"},\"name_en\":\"You Ge\",\"name\":\"葛优\",\"alt\":\"https://movie.douban.com/celebrity/1000905/\",\"id\":\"1000905\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1399268395.47.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1399268395.47.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1399268395.47.jpg\"},\"name_en\":\"Li Gong\",\"name\":\"巩俐\",\"alt\":\"https://movie.douban.com/celebrity/1035641/\",\"id\":\"1035641\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p27203.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p27203.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p27203.jpg\"},\"name_en\":\"Wu Jiang\",\"name\":\"姜武\",\"alt\":\"https://movie.douban.com/celebrity/1274290/\",\"id\":\"1274290\"}],\"durations\":[\"132分钟\"],\"collect_count\":595831,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"活着\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p568.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p568.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p568.jpg\"},\"name_en\":\"Yimou Zhang\",\"name\":\"张艺谋\",\"alt\":\"https://movie.douban.com/celebrity/1054398/\",\"id\":\"1054398\"}],\"pubdates\":[\"1994-05-17(戛纳电影节)\",\"1994-06-30(香港)\"],\"year\":\"1994\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2513253791.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2513253791.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2513253791.jpg\"},\"alt\":\"https://movie.douban.com/subject/1292365/\",\"id\":\"1292365\"},{\"rating\":{\"max\":10,\"average\":9.6,\"details\":{\"1\":121,\"2\":142,\"3\":2763,\"4\":27458,\"5\":133591},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"犯罪\",\"悬疑\"],\"title\":\"控方证人\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1425263540.96.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1425263540.96.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1425263540.96.jpg\"},\"name_en\":\"Tyrone Power\",\"name\":\"泰隆·鲍华\",\"alt\":\"https://movie.douban.com/celebrity/1048197/\",\"id\":\"1048197\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1134.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1134.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1134.jpg\"},\"name_en\":\"Marlene Dietrich\",\"name\":\"玛琳·黛德丽\",\"alt\":\"https://movie.douban.com/celebrity/1013957/\",\"id\":\"1013957\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1456671389.86.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1456671389.86.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1456671389.86.jpg\"},\"name_en\":\"Charles Laughton\",\"name\":\"查尔斯·劳顿\",\"alt\":\"https://movie.douban.com/celebrity/1010665/\",\"id\":\"1010665\"}],\"durations\":[\"116分钟\"],\"collect_count\":226288,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"Witness for the Prosecution\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p924.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p924.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p924.jpg\"},\"name_en\":\"Billy Wilder\",\"name\":\"比利·怀德\",\"alt\":\"https://movie.douban.com/celebrity/1054385/\",\"id\":\"1054385\"}],\"pubdates\":[\"1957-12-17(美国)\"],\"year\":\"1957\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1505392928.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1505392928.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1505392928.jpg\"},\"alt\":\"https://movie.douban.com/subject/1296141/\",\"id\":\"1296141\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":1346,\"2\":3398,\"3\":46443,\"4\":216994,\"5\":414566},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"奇幻\",\"冒险\"],\"title\":\"少年派的奇幻漂流\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1354193464.0.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1354193464.0.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1354193464.0.jpg\"},\"name_en\":\"Suraj Sharma\",\"name\":\"苏拉·沙玛\",\"alt\":\"https://movie.douban.com/celebrity/1322230/\",\"id\":\"1322230\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p48861.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p48861.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p48861.jpg\"},\"name_en\":\"Irrfan Khan\",\"name\":\"伊尔凡·可汗\",\"alt\":\"https://movie.douban.com/celebrity/1108861/\",\"id\":\"1108861\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1528099178.04.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1528099178.04.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1528099178.04.jpg\"},\"name_en\":\"Rafe Spall\",\"name\":\"拉菲·斯波\",\"alt\":\"https://movie.douban.com/celebrity/1032169/\",\"id\":\"1032169\"}],\"durations\":[\"127分钟\"],\"collect_count\":1135311,\"mainland_pubdate\":\"2012-11-22\",\"has_video\":true,\"original_title\":\"Life of Pi\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p595.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p595.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p595.jpg\"},\"name_en\":\"Ang Lee\",\"name\":\"李安\",\"alt\":\"https://movie.douban.com/celebrity/1054421/\",\"id\":\"1054421\"}],\"pubdates\":[\"2012-09-28(纽约电影节)\",\"2012-11-21(美国)\",\"2012-11-22(中国大陆)\"],\"year\":\"2012\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1784592701.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1784592701.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1784592701.jpg\"},\"alt\":\"https://movie.douban.com/subject/1929463/\",\"id\":\"1929463\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":570,\"2\":1605,\"3\":22180,\"4\":91868,\"5\":223873},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"动作\",\"奇幻\"],\"title\":\"指环王3：王者无敌\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p29922.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p29922.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p29922.jpg\"},\"name_en\":\"Viggo Mortensen\",\"name\":\"维果·莫腾森\",\"alt\":\"https://movie.douban.com/celebrity/1054520/\",\"id\":\"1054520\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51597.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51597.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51597.jpg\"},\"name_en\":\"Elijah Wood\",\"name\":\"伊利亚·伍德\",\"alt\":\"https://movie.douban.com/celebrity/1054395/\",\"id\":\"1054395\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p11727.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p11727.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p11727.jpg\"},\"name_en\":\"Sean Astin\",\"name\":\"西恩·奥斯汀\",\"alt\":\"https://movie.douban.com/celebrity/1031818/\",\"id\":\"1031818\"}],\"durations\":[\"201分钟\",\"254分钟(加长版)\",\"263分钟(蓝光加长版)\"],\"collect_count\":597197,\"mainland_pubdate\":\"2004-03-12\",\"has_video\":true,\"original_title\":\"The Lord of the Rings: The Return of the King\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p40835.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p40835.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p40835.jpg\"},\"name_en\":\"Peter Jackson\",\"name\":\"彼得·杰克逊\",\"alt\":\"https://movie.douban.com/celebrity/1040524/\",\"id\":\"1040524\"}],\"pubdates\":[\"2003-12-17(美国)\",\"2004-03-12(中国大陆)\"],\"year\":\"2003\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910825503.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910825503.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910825503.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291552/\",\"id\":\"1291552\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":380,\"2\":1186,\"3\":18170,\"4\":92213,\"5\":207164},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"爱情\"],\"title\":\"天堂电影院\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p43502.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p43502.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p43502.jpg\"},\"name_en\":\"Antonella Attili\",\"name\":\"安东内拉·阿蒂利\",\"alt\":\"https://movie.douban.com/celebrity/1277558/\",\"id\":\"1277558\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44286.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44286.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44286.jpg\"},\"name_en\":\"Enzo Cannavale\",\"name\":\"恩佐·卡拉瓦勒\",\"alt\":\"https://movie.douban.com/celebrity/1078332/\",\"id\":\"1078332\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1371022856.11.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1371022856.11.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1371022856.11.jpg\"},\"name_en\":\"Isa Danieli\",\"name\":\"艾萨·丹尼埃利\",\"alt\":\"https://movie.douban.com/celebrity/1074920/\",\"id\":\"1074920\"}],\"durations\":[\"155分钟\",\"173分钟(导演剪辑版)\",\"124分钟(剧场版)\"],\"collect_count\":557063,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"Nuovo Cinema Paradiso\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p195.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p195.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p195.jpg\"},\"name_en\":\"Giuseppe Tornatore\",\"name\":\"朱塞佩·托纳多雷\",\"alt\":\"https://movie.douban.com/celebrity/1018983/\",\"id\":\"1018983\"}],\"pubdates\":[\"1988-11-17(意大利)\"],\"year\":\"1988\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2559577569.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2559577569.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2559577569.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291828/\",\"id\":\"1291828\"},{\"rating\":{\"max\":10,\"average\":9.1,\"details\":{\"1\":327,\"2\":1114,\"3\":24835,\"4\":127374,\"5\":244468},\"stars\":\"45\",\"min\":0},\"genres\":[\"动画\",\"奇幻\",\"冒险\"],\"title\":\"天空之城\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4978.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4978.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4978.jpg\"},\"name_en\":\"Mayumi Tanaka\",\"name\":\"田中真弓\",\"alt\":\"https://movie.douban.com/celebrity/1016801/\",\"id\":\"1016801\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1496312006.95.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1496312006.95.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1496312006.95.jpg\"},\"name_en\":\"Keiko Yokozawa\",\"name\":\"横泽启子\",\"alt\":\"https://movie.douban.com/celebrity/1051412/\",\"id\":\"1051412\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1376305807.47.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1376305807.47.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1376305807.47.jpg\"},\"name_en\":\"Kotoe Hatsui\",\"name\":\"初井言荣\",\"alt\":\"https://movie.douban.com/celebrity/1015339/\",\"id\":\"1015339\"}],\"durations\":[\"125分钟\"],\"collect_count\":709888,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"天空の城ラピュタ\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\"},\"name_en\":\"Hayao Miyazaki\",\"name\":\"宫崎骏\",\"alt\":\"https://movie.douban.com/celebrity/1054439/\",\"id\":\"1054439\"}],\"pubdates\":[\"1986-08-02(日本)\",\"1992(中国大陆)\"],\"year\":\"1986\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1446261379.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1446261379.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1446261379.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291583/\",\"id\":\"1291583\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":589,\"2\":1185,\"3\":14910,\"4\":74228,\"5\":194046},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"历史\",\"战争\"],\"title\":\"鬼子来了\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1517818343.94.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1517818343.94.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1517818343.94.jpg\"},\"name_en\":\"Wen Jiang\",\"name\":\"姜文\",\"alt\":\"https://movie.douban.com/celebrity/1021999/\",\"id\":\"1021999\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1379678916.04.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1379678916.04.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1379678916.04.jpg\"},\"name_en\":\"Teruyuki Kagawa\",\"name\":\"香川照之\",\"alt\":\"https://movie.douban.com/celebrity/1037221/\",\"id\":\"1037221\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1499449986.84.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1499449986.84.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1499449986.84.jpg\"},\"name_en\":\"Ding Yuan\",\"name\":\"袁丁\",\"alt\":\"https://movie.douban.com/celebrity/1331421/\",\"id\":\"1331421\"}],\"durations\":[\"139分钟\",\"162分钟(戛纳电影节)\"],\"collect_count\":500739,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"鬼子来了\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1517818343.94.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1517818343.94.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1517818343.94.jpg\"},\"name_en\":\"Wen Jiang\",\"name\":\"姜文\",\"alt\":\"https://movie.douban.com/celebrity/1021999/\",\"id\":\"1021999\"}],\"pubdates\":[\"2000-05-12(戛纳电影节)\",\"2002-04-27(日本)\"],\"year\":\"2000\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2553104888.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2553104888.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2553104888.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291858/\",\"id\":\"1291858\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":231,\"2\":598,\"3\":9794,\"4\":49780,\"5\":90987},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"传记\",\"运动\"],\"title\":\"摔跤吧！爸爸\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13628.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13628.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13628.jpg\"},\"name_en\":\"Aamir Khan\",\"name\":\"阿米尔·汗\",\"alt\":\"https://movie.douban.com/celebrity/1031931/\",\"id\":\"1031931\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1493121366.9.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1493121366.9.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1493121366.9.jpg\"},\"name_en\":\"Fatima Sana Shaikh\",\"name\":\"法缇玛·萨那·纱卡\",\"alt\":\"https://movie.douban.com/celebrity/1372457/\",\"id\":\"1372457\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1493121856.81.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1493121856.81.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1493121856.81.jpg\"},\"name_en\":\"Sanya Malhotra\",\"name\":\"桑亚·玛荷塔\",\"alt\":\"https://movie.douban.com/celebrity/1372458/\",\"id\":\"1372458\"}],\"durations\":[\"161分钟(印度)\",\"140分钟(中国大陆)\"],\"collect_count\":1259143,\"mainland_pubdate\":\"2017-05-05\",\"has_video\":true,\"original_title\":\"Dangal\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1484120321.24.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1484120321.24.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1484120321.24.jpg\"},\"name_en\":\"Nitesh Tiwari\",\"name\":\"涅提·蒂瓦里\",\"alt\":\"https://movie.douban.com/celebrity/1366907/\",\"id\":\"1366907\"}],\"pubdates\":[\"2016-12-23(印度)\",\"2017-05-05(中国大陆)\"],\"year\":\"2016\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2457983084.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2457983084.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2457983084.jpg\"},\"alt\":\"https://movie.douban.com/subject/26387939/\",\"id\":\"26387939\"},{\"rating\":{\"max\":10,\"average\":9.4,\"details\":{\"1\":251,\"2\":468,\"3\":7147,\"4\":44008,\"5\":149140},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"十二怒汉\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1537.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1537.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1537.jpg\"},\"name_en\":\"Henry Fonda\",\"name\":\"亨利·方达\",\"alt\":\"https://movie.douban.com/celebrity/1048150/\",\"id\":\"1048150\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1427201867.36.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1427201867.36.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1427201867.36.jpg\"},\"name_en\":\"Martin Balsam\",\"name\":\"马丁·鲍尔萨姆\",\"alt\":\"https://movie.douban.com/celebrity/1041188/\",\"id\":\"1041188\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p40857.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p40857.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p40857.jpg\"},\"name_en\":\"John Fiedler\",\"name\":\"约翰·菲德勒\",\"alt\":\"https://movie.douban.com/celebrity/1007076/\",\"id\":\"1007076\"}],\"durations\":[\"96 分钟\"],\"collect_count\":316710,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"12 Angry Men\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1309.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1309.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1309.jpg\"},\"name_en\":\"Sidney Lumet\",\"name\":\"西德尼·吕美特\",\"alt\":\"https://movie.douban.com/celebrity/1010627/\",\"id\":\"1010627\"}],\"pubdates\":[\"1957-04-13(美国)\"],\"year\":\"1957\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2173577632.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2173577632.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2173577632.jpg\"},\"alt\":\"https://movie.douban.com/subject/1293182/\",\"id\":\"1293182\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":455,\"2\":2178,\"3\":46511,\"4\":213577,\"5\":343877},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"喜剧\",\"动画\"],\"title\":\"飞屋环游记\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6202.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6202.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6202.jpg\"},\"name_en\":\"Edward Asner\",\"name\":\"爱德华·阿斯纳\",\"alt\":\"https://movie.douban.com/celebrity/1054334/\",\"id\":\"1054334\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42033.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42033.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42033.jpg\"},\"name_en\":\"Christopher Plummer\",\"name\":\"克里斯托弗·普卢默\",\"alt\":\"https://movie.douban.com/celebrity/1036321/\",\"id\":\"1036321\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p58161.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p58161.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p58161.jpg\"},\"name_en\":\"Jordan Nagai\",\"name\":\"乔丹·长井\",\"alt\":\"https://movie.douban.com/celebrity/1004683/\",\"id\":\"1004683\"}],\"durations\":[\"96分钟\"],\"collect_count\":1072517,\"mainland_pubdate\":\"2009-08-04\",\"has_video\":true,\"original_title\":\"Up\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1288.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1288.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1288.jpg\"},\"name_en\":\"Pete Docter\",\"name\":\"彼特·道格特\",\"alt\":\"https://movie.douban.com/celebrity/1022803/\",\"id\":\"1022803\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1375330728.5.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1375330728.5.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1375330728.5.jpg\"},\"name_en\":\"Bob Peterson\",\"name\":\"鲍勃·彼德森\",\"alt\":\"https://movie.douban.com/celebrity/1294383/\",\"id\":\"1294383\"}],\"pubdates\":[\"2009-05-13(戛纳电影节)\",\"2009-05-29(美国)\",\"2009-08-04(中国大陆)\"],\"year\":\"2009\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p485887754.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p485887754.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p485887754.jpg\"},\"alt\":\"https://movie.douban.com/subject/2129039/\",\"id\":\"2129039\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":1221,\"2\":3130,\"3\":44747,\"4\":177594,\"5\":313242},\"stars\":\"45\",\"min\":0},\"genres\":[\"喜剧\",\"爱情\",\"奇幻\"],\"title\":\"大话西游之月光宝盒\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p47421.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p47421.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p47421.jpg\"},\"name_en\":\"Stephen Chow\",\"name\":\"周星驰\",\"alt\":\"https://movie.douban.com/celebrity/1048026/\",\"id\":\"1048026\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45481.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45481.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45481.jpg\"},\"name_en\":\"Man Tat Ng\",\"name\":\"吴孟达\",\"alt\":\"https://movie.douban.com/celebrity/1016771/\",\"id\":\"1016771\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1208.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1208.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1208.jpg\"},\"name_en\":\"Kar-Ying Law\",\"name\":\"罗家英\",\"alt\":\"https://movie.douban.com/celebrity/1108968/\",\"id\":\"1108968\"}],\"durations\":[\"87分钟\"],\"collect_count\":996535,\"mainland_pubdate\":\"2014-10-24\",\"has_video\":true,\"original_title\":\"西遊記第壹佰零壹回之月光寶盒\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45374.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45374.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p45374.jpg\"},\"name_en\":\"Jeffrey Lau\",\"name\":\"刘镇伟\",\"alt\":\"https://movie.douban.com/celebrity/1274431/\",\"id\":\"1274431\"}],\"pubdates\":[\"1995-01-21(香港)\",\"2014-10-24(中国大陆)\"],\"year\":\"1995\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1280323646.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1280323646.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1280323646.jpg\"},\"alt\":\"https://movie.douban.com/subject/1299398/\",\"id\":\"1299398\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":492,\"2\":2066,\"3\":34357,\"4\":148488,\"5\":267981},\"stars\":\"45\",\"min\":0},\"genres\":[\"动画\",\"奇幻\",\"冒险\"],\"title\":\"哈尔的移动城堡\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42411.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42411.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42411.jpg\"},\"name_en\":\"Chieko Baishô\",\"name\":\"倍赏千惠子\",\"alt\":\"https://movie.douban.com/celebrity/1056635/\",\"id\":\"1056635\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1365448692.55.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1365448692.55.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1365448692.55.jpg\"},\"name_en\":\"Takuya Kimura\",\"name\":\"木村拓哉\",\"alt\":\"https://movie.douban.com/celebrity/1041371/\",\"id\":\"1041371\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4422.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4422.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4422.jpg\"},\"name_en\":\"Akihiro Miwa\",\"name\":\"美轮明宏\",\"alt\":\"https://movie.douban.com/celebrity/1045925/\",\"id\":\"1045925\"}],\"durations\":[\"119分钟\"],\"collect_count\":773083,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"ハウルの動く城\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\"},\"name_en\":\"Hayao Miyazaki\",\"name\":\"宫崎骏\",\"alt\":\"https://movie.douban.com/celebrity/1054439/\",\"id\":\"1054439\"}],\"pubdates\":[\"2004-09-05(威尼斯电影节)\",\"2004-11-20(日本)\"],\"year\":\"2004\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2174346180.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2174346180.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2174346180.jpg\"},\"alt\":\"https://movie.douban.com/subject/1308807/\",\"id\":\"1308807\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":1370,\"2\":3291,\"3\":31656,\"4\":127704,\"5\":255138},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"动作\",\"悬疑\"],\"title\":\"搏击俱乐部\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p385.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p385.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p385.jpg\"},\"name_en\":\"Edward Norton\",\"name\":\"爱德华·诺顿\",\"alt\":\"https://movie.douban.com/celebrity/1035676/\",\"id\":\"1035676\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p39053.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p39053.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p39053.jpg\"},\"name_en\":\"Brad Pitt\",\"name\":\"布拉德·皮特\",\"alt\":\"https://movie.douban.com/celebrity/1054452/\",\"id\":\"1054452\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p19519.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p19519.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p19519.jpg\"},\"name_en\":\"Helena Bonham Carter\",\"name\":\"海伦娜·伯翰·卡特\",\"alt\":\"https://movie.douban.com/celebrity/1016680/\",\"id\":\"1016680\"}],\"durations\":[\"139 分钟\"],\"collect_count\":705077,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"Fight Club\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p22137.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p22137.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p22137.jpg\"},\"name_en\":\"David Fincher\",\"name\":\"大卫·芬奇\",\"alt\":\"https://movie.douban.com/celebrity/1012521/\",\"id\":\"1012521\"}],\"pubdates\":[\"1999-09-10(威尼斯电影节)\",\"1999-10-15(美国)\"],\"year\":\"1999\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1910926158.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1910926158.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1910926158.jpg\"},\"alt\":\"https://movie.douban.com/subject/1292000/\",\"id\":\"1292000\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":423,\"2\":1484,\"3\":33182,\"4\":153138,\"5\":256926},\"stars\":\"45\",\"min\":0},\"genres\":[\"喜剧\",\"剧情\",\"爱情\"],\"title\":\"罗马假日\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4157.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4157.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4157.jpg\"},\"name_en\":\"Audrey Hepburn\",\"name\":\"奥黛丽·赫本\",\"alt\":\"https://movie.douban.com/celebrity/1054449/\",\"id\":\"1054449\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p338.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p338.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p338.jpg\"},\"name_en\":\"Gregory Peck\",\"name\":\"格利高里·派克\",\"alt\":\"https://movie.douban.com/celebrity/1031218/\",\"id\":\"1031218\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12729.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12729.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12729.jpg\"},\"name_en\":\"Eddie Albert\",\"name\":\"埃迪·艾伯特\",\"alt\":\"https://movie.douban.com/celebrity/1048218/\",\"id\":\"1048218\"}],\"durations\":[\"118分钟\"],\"collect_count\":882973,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Roman Holiday\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1485245573.54.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1485245573.54.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1485245573.54.jpg\"},\"name_en\":\"William Wyler\",\"name\":\"威廉·惠勒\",\"alt\":\"https://movie.douban.com/celebrity/1010691/\",\"id\":\"1010691\"}],\"pubdates\":[\"1953-09-02(美国)\"],\"year\":\"1953\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2189265085.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2189265085.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2189265085.jpg\"},\"alt\":\"https://movie.douban.com/subject/1293839/\",\"id\":\"1293839\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":428,\"2\":1699,\"3\":27743,\"4\":130498,\"5\":231399},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"闻香识女人\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p645.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p645.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p645.jpg\"},\"name_en\":\"Al Pacino\",\"name\":\"阿尔·帕西诺\",\"alt\":\"https://movie.douban.com/celebrity/1054451/\",\"id\":\"1054451\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p20086.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p20086.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p20086.jpg\"},\"name_en\":\"Chris O'Donnell\",\"name\":\"克里斯·奥唐纳\",\"alt\":\"https://movie.douban.com/celebrity/1009272/\",\"id\":\"1009272\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1550134032.44.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1550134032.44.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1550134032.44.jpg\"},\"name_en\":\"James Rebhorn\",\"name\":\"詹姆斯·瑞布霍恩\",\"alt\":\"https://movie.douban.com/celebrity/1049801/\",\"id\":\"1049801\"}],\"durations\":[\"157 分钟\"],\"collect_count\":688813,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Scent of a Woman\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4831.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4831.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4831.jpg\"},\"name_en\":\"Martin Brest\",\"name\":\"马丁·布莱斯特\",\"alt\":\"https://movie.douban.com/celebrity/1055265/\",\"id\":\"1055265\"}],\"pubdates\":[\"1992-12-23(美国)\"],\"year\":\"1992\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2550757929.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2550757929.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2550757929.jpg\"},\"alt\":\"https://movie.douban.com/subject/1298624/\",\"id\":\"1298624\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":346,\"2\":983,\"3\":15776,\"4\":84398,\"5\":184779},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"传记\",\"历史\"],\"title\":\"末代皇帝\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p35177.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p35177.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p35177.jpg\"},\"name_en\":\"John Lone\",\"name\":\"尊龙\",\"alt\":\"https://movie.douban.com/celebrity/1027367/\",\"id\":\"1027367\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5526.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5526.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5526.jpg\"},\"name_en\":\"Joan Chen\",\"name\":\"陈冲\",\"alt\":\"https://movie.douban.com/celebrity/1044964/\",\"id\":\"1044964\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p31577.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p31577.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p31577.jpg\"},\"name_en\":\"Vivian Wu\",\"name\":\"邬君梅\",\"alt\":\"https://movie.douban.com/celebrity/1004773/\",\"id\":\"1004773\"}],\"durations\":[\"163分钟\",\"219分钟 (电视版)\"],\"collect_count\":495930,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"The Last Emperor\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p374.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p374.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p374.jpg\"},\"name_en\":\"Bernardo Bertolucci\",\"name\":\"贝纳尔多·贝托鲁奇\",\"alt\":\"https://movie.douban.com/celebrity/1035651/\",\"id\":\"1035651\"}],\"pubdates\":[\"1987-10-04(东京国际电影节)\",\"1987-10-23(意大利)\"],\"year\":\"1987\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p452089833.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p452089833.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p452089833.jpg\"},\"alt\":\"https://movie.douban.com/subject/1293172/\",\"id\":\"1293172\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":211,\"2\":693,\"3\":11204,\"4\":50160,\"5\":95687},\"stars\":\"45\",\"min\":0},\"genres\":[\"喜剧\",\"动画\",\"奇幻\"],\"title\":\"寻梦环游记\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1489594517.9.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1489594517.9.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1489594517.9.jpg\"},\"name_en\":\"Anthony Gonzalez\",\"name\":\"安东尼·冈萨雷斯\",\"alt\":\"https://movie.douban.com/celebrity/1370411/\",\"id\":\"1370411\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1510634028.69.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1510634028.69.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1510634028.69.jpg\"},\"name_en\":\"Gael García Bernal\",\"name\":\"盖尔·加西亚·贝纳尔\",\"alt\":\"https://movie.douban.com/celebrity/1041009/\",\"id\":\"1041009\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1416762292.89.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1416762292.89.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1416762292.89.jpg\"},\"name_en\":\"Benjamin Bratt\",\"name\":\"本杰明·布拉特\",\"alt\":\"https://movie.douban.com/celebrity/1036383/\",\"id\":\"1036383\"}],\"durations\":[\"105分钟\"],\"collect_count\":1014353,\"mainland_pubdate\":\"2017-11-24\",\"has_video\":true,\"original_title\":\"Coco\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13830.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13830.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p13830.jpg\"},\"name_en\":\"Lee Unkrich\",\"name\":\"李·昂克里奇\",\"alt\":\"https://movie.douban.com/celebrity/1022678/\",\"id\":\"1022678\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1497195148.21.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1497195148.21.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1497195148.21.jpg\"},\"name_en\":\"Adrian Molina\",\"name\":\"阿德里安·莫利纳\",\"alt\":\"https://movie.douban.com/celebrity/1370425/\",\"id\":\"1370425\"}],\"pubdates\":[\"2017-10-20(莫雷利亚电影节)\",\"2017-11-22(美国)\",\"2017-11-24(中国大陆)\"],\"year\":\"2017\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2503997609.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2503997609.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2503997609.jpg\"},\"alt\":\"https://movie.douban.com/subject/20495023/\",\"id\":\"20495023\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":295,\"2\":708,\"3\":13309,\"4\":81365,\"5\":179960},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"辩护人\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p345.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p345.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p345.jpg\"},\"name_en\":\"Kang-ho Song\",\"name\":\"宋康昊\",\"alt\":\"https://movie.douban.com/celebrity/1031238/\",\"id\":\"1031238\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4183.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4183.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4183.jpg\"},\"name_en\":\"Dal-su Oh\",\"name\":\"吴达洙\",\"alt\":\"https://movie.douban.com/celebrity/1203077/\",\"id\":\"1203077\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26917.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26917.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26917.jpg\"},\"name_en\":\"Yeong-ae Kim\",\"name\":\"金英爱\",\"alt\":\"https://movie.douban.com/celebrity/1314843/\",\"id\":\"1314843\"}],\"durations\":[\"127分钟\"],\"collect_count\":444858,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"변호인\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1394517493.21.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1394517493.21.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1394517493.21.jpg\"},\"name_en\":\"Woo-seok Yang\",\"name\":\"杨宇硕\",\"alt\":\"https://movie.douban.com/celebrity/1338840/\",\"id\":\"1338840\"}],\"pubdates\":[\"2013-12-18(韩国)\"],\"year\":\"2013\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2158166535.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2158166535.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2158166535.jpg\"},\"alt\":\"https://movie.douban.com/subject/21937445/\",\"id\":\"21937445\"},{\"rating\":{\"max\":10,\"average\":9.1,\"details\":{\"1\":382,\"2\":1139,\"3\":15355,\"4\":81501,\"5\":174632},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"悬疑\"],\"title\":\"窃听风暴\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p43345.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p43345.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p43345.jpg\"},\"name_en\":\"Ulrich Mühe\",\"name\":\"乌尔里希·穆埃\",\"alt\":\"https://movie.douban.com/celebrity/1053553/\",\"id\":\"1053553\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2462.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2462.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2462.jpg\"},\"name_en\":\"Martina Gedeck\",\"name\":\"马蒂娜·格德克\",\"alt\":\"https://movie.douban.com/celebrity/1049928/\",\"id\":\"1049928\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1362985206.01.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1362985206.01.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1362985206.01.jpg\"},\"name_en\":\"Sebastian Koch\",\"name\":\"塞巴斯蒂安·科赫\",\"alt\":\"https://movie.douban.com/celebrity/1019130/\",\"id\":\"1019130\"}],\"durations\":[\"137分钟\"],\"collect_count\":474018,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Das Leben der Anderen\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4763.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4763.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4763.jpg\"},\"name_en\":\"Florian Henckel von Donnersmarck\",\"name\":\"弗洛里安·亨克尔·冯·多纳斯马尔克\",\"alt\":\"https://movie.douban.com/celebrity/1044973/\",\"id\":\"1044973\"}],\"pubdates\":[\"2006-03-23(德国)\"],\"year\":\"2006\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1808872109.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1808872109.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p1808872109.jpg\"},\"alt\":\"https://movie.douban.com/subject/1900841/\",\"id\":\"1900841\"},{\"rating\":{\"max\":10,\"average\":9.2,\"details\":{\"1\":312,\"2\":783,\"3\":12702,\"4\":78583,\"5\":171975},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"素媛\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1403416347.83.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1403416347.83.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1403416347.83.jpg\"},\"name_en\":\"Kyung-gu Sol\",\"name\":\"薛耿求\",\"alt\":\"https://movie.douban.com/celebrity/1045576/\",\"id\":\"1045576\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p14020.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p14020.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p14020.jpg\"},\"name_en\":\"Ji-won Uhm\",\"name\":\"严志媛\",\"alt\":\"https://movie.douban.com/celebrity/1015178/\",\"id\":\"1015178\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1385020551.6.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1385020551.6.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1385020551.6.jpg\"},\"name_en\":\"Re Lee\",\"name\":\"李来\",\"alt\":\"https://movie.douban.com/celebrity/1336326/\",\"id\":\"1336326\"}],\"durations\":[\"123分钟\"],\"collect_count\":430846,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"소원\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1451911358.49.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1451911358.49.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1451911358.49.jpg\"},\"name_en\":\"Jun-ik Lee\",\"name\":\"李濬益\",\"alt\":\"https://movie.douban.com/celebrity/1028844/\",\"id\":\"1028844\"}],\"pubdates\":[\"2013-10-02(韩国)\"],\"year\":\"2013\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2118532944.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2118532944.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2118532944.jpg\"},\"alt\":\"https://movie.douban.com/subject/21937452/\",\"id\":\"21937452\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":539,\"2\":1989,\"3\":23518,\"4\":103951,\"5\":200016},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"死亡诗社\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p103.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p103.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p103.jpg\"},\"name_en\":\"Robin Williams\",\"name\":\"罗宾·威廉姆斯\",\"alt\":\"https://movie.douban.com/celebrity/1009241/\",\"id\":\"1009241\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p25694.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p25694.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p25694.jpg\"},\"name_en\":\"Robert Sean Leonard\",\"name\":\"罗伯特·肖恩·莱纳德\",\"alt\":\"https://movie.douban.com/celebrity/1031873/\",\"id\":\"1031873\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p25602.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p25602.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p25602.jpg\"},\"name_en\":\"Ethan Hawke\",\"name\":\"伊桑·霍克\",\"alt\":\"https://movie.douban.com/celebrity/1018984/\",\"id\":\"1018984\"}],\"durations\":[\"128 分钟\"],\"collect_count\":537044,\"mainland_pubdate\":\"\",\"has_video\":true,\"original_title\":\"Dead Poets Society\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4360.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4360.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p4360.jpg\"},\"name_en\":\"Peter Weir\",\"name\":\"彼得·威尔\",\"alt\":\"https://movie.douban.com/celebrity/1022721/\",\"id\":\"1022721\"}],\"pubdates\":[\"1989-06-02(多伦多首映)\",\"1989-06-09(美国)\"],\"year\":\"1989\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910824340.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910824340.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p1910824340.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291548/\",\"id\":\"1291548\"},{\"rating\":{\"max\":10,\"average\":9.1,\"details\":{\"1\":517,\"2\":1557,\"3\":18276,\"4\":87238,\"5\":177389},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"喜剧\",\"犯罪\"],\"title\":\"两杆大烟枪\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2028.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2028.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2028.jpg\"},\"name_en\":\"Jason Flemyng\",\"name\":\"杰森·弗莱明\",\"alt\":\"https://movie.douban.com/celebrity/1027179/\",\"id\":\"1027179\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p8696.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p8696.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p8696.jpg\"},\"name_en\":\"Dexter Fletcher\",\"name\":\"德克斯特·弗莱彻\",\"alt\":\"https://movie.douban.com/celebrity/1007028/\",\"id\":\"1007028\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6761.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6761.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6761.jpg\"},\"name_en\":\"Nick Moran\",\"name\":\"尼克·莫兰\",\"alt\":\"https://movie.douban.com/celebrity/1014074/\",\"id\":\"1014074\"}],\"durations\":[\"107分钟\",\"115分钟(导演剪辑版)\"],\"collect_count\":479302,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"Lock, Stock and Two Smoking Barrels\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p47189.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p47189.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p47189.jpg\"},\"name_en\":\"Guy Ritchie\",\"name\":\"盖·里奇\",\"alt\":\"https://movie.douban.com/celebrity/1025148/\",\"id\":\"1025148\"}],\"pubdates\":[\"1998-08-28(英国)\"],\"year\":\"1998\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p792443418.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p792443418.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p792443418.jpg\"},\"alt\":\"https://movie.douban.com/subject/1293350/\",\"id\":\"1293350\"},{\"rating\":{\"max\":10,\"average\":9.1,\"details\":{\"1\":501,\"2\":1522,\"3\":19012,\"4\":90373,\"5\":178680},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"飞越疯人院\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26019.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26019.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p26019.jpg\"},\"name_en\":\"Jack Nicholson\",\"name\":\"杰克·尼科尔森\",\"alt\":\"https://movie.douban.com/celebrity/1054528/\",\"id\":\"1054528\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1538030195.75.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1538030195.75.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1538030195.75.jpg\"},\"name_en\":\"Danny DeVito\",\"name\":\"丹尼·德维托\",\"alt\":\"https://movie.douban.com/celebrity/1040516/\",\"id\":\"1040516\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42525.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42525.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42525.jpg\"},\"name_en\":\"Christopher Lloyd\",\"name\":\"克里斯托弗·洛伊德\",\"alt\":\"https://movie.douban.com/celebrity/1027163/\",\"id\":\"1027163\"}],\"durations\":[\"133分钟\"],\"collect_count\":506267,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"One Flew Over the Cuckoo's Nest\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p37577.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p37577.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p37577.jpg\"},\"name_en\":\"Miloš Forman\",\"name\":\"米洛斯·福尔曼\",\"alt\":\"https://movie.douban.com/celebrity/1053561/\",\"id\":\"1053561\"}],\"pubdates\":[\"1975-11-19(美国)\"],\"year\":\"1975\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p792238287.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p792238287.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p792238287.jpg\"},\"alt\":\"https://movie.douban.com/subject/1292224/\",\"id\":\"1292224\"},{\"rating\":{\"max\":10,\"average\":9,\"details\":{\"1\":503,\"2\":1578,\"3\":23948,\"4\":99598,\"5\":194158},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"动作\",\"奇幻\"],\"title\":\"指环王2：双塔奇兵\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51597.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51597.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51597.jpg\"},\"name_en\":\"Elijah Wood\",\"name\":\"伊利亚·伍德\",\"alt\":\"https://movie.douban.com/celebrity/1054395/\",\"id\":\"1054395\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p11727.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p11727.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p11727.jpg\"},\"name_en\":\"Sean Astin\",\"name\":\"西恩·奥斯汀\",\"alt\":\"https://movie.douban.com/celebrity/1031818/\",\"id\":\"1031818\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1453792417.87.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1453792417.87.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1453792417.87.jpg\"},\"name_en\":\"Ian McKellen\",\"name\":\"伊恩·麦克莱恩\",\"alt\":\"https://movie.douban.com/celebrity/1054410/\",\"id\":\"1054410\"}],\"durations\":[\"179分钟\",\"223分钟(加长版)\",\"235分钟(蓝光加长版)\"],\"collect_count\":555617,\"mainland_pubdate\":\"2003-04-25\",\"has_video\":true,\"original_title\":\"The Lord of the Rings: The Two Towers\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p40835.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p40835.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p40835.jpg\"},\"name_en\":\"Peter Jackson\",\"name\":\"彼得·杰克逊\",\"alt\":\"https://movie.douban.com/celebrity/1040524/\",\"id\":\"1040524\"}],\"pubdates\":[\"2002-12-05(纽约首映)\",\"2002-12-18(美国)\",\"2003-04-25(中国大陆)\"],\"year\":\"2002\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p909265336.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p909265336.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p909265336.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291572/\",\"id\":\"1291572\"}],\"title\":\"豆瓣电影Top250\"}");

/***/ }),
/* 30 */
/*!*********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/mock/weekly.json ***!
  \*********************************************************************/
/*! exports provided: subjects, title, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"subjects\":[{\"subject\":{\"rating\":{\"max\":10,\"average\":9.3,\"details\":{\"1\":1020,\"2\":2037,\"3\":37177,\"4\":232199,\"5\":676211},\"stars\":\"50\",\"min\":0},\"genres\":[\"剧情\",\"动画\",\"奇幻\"],\"title\":\"千与千寻\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1463193210.13.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1463193210.13.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1463193210.13.jpg\"},\"name_en\":\"Rumi Hiiragi\",\"name\":\"柊瑠美\",\"alt\":\"https://movie.douban.com/celebrity/1023337/\",\"id\":\"1023337\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44986.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44986.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p44986.jpg\"},\"name_en\":\"Miyu Irino\",\"name\":\"入野自由\",\"alt\":\"https://movie.douban.com/celebrity/1005438/\",\"id\":\"1005438\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1376151005.51.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1376151005.51.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1376151005.51.jpg\"},\"name_en\":\"Mari Natsuki\",\"name\":\"夏木真理\",\"alt\":\"https://movie.douban.com/celebrity/1045797/\",\"id\":\"1045797\"}],\"durations\":[\"125分钟\"],\"collect_count\":1558263,\"mainland_pubdate\":\"2019-06-21\",\"has_video\":false,\"original_title\":\"千と千尋の神隠し\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p616.jpg\"},\"name_en\":\"Hayao Miyazaki\",\"name\":\"宫崎骏\",\"alt\":\"https://movie.douban.com/celebrity/1054439/\",\"id\":\"1054439\"}],\"pubdates\":[\"2001-07-20(日本)\",\"2019-06-21(中国大陆)\"],\"year\":\"2001\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557573348.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557573348.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557573348.jpg\"},\"alt\":\"https://movie.douban.com/subject/1291561/\",\"id\":\"1291561\"},\"rank\":1,\"delta\":2},{\"subject\":{\"rating\":{\"max\":10,\"average\":8.8,\"details\":{\"1\":27,\"2\":138,\"3\":2223,\"4\":9530,\"5\":11745},\"stars\":\"45\",\"min\":0},\"genres\":[\"喜剧\",\"动画\",\"奇幻\"],\"title\":\"玩具总动员4\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p28603.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p28603.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p28603.jpg\"},\"name_en\":\"Tom Hanks\",\"name\":\"汤姆·汉克斯\",\"alt\":\"https://movie.douban.com/celebrity/1054450/\",\"id\":\"1054450\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6409.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6409.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6409.jpg\"},\"name_en\":\"Tim Allen\",\"name\":\"蒂姆·艾伦\",\"alt\":\"https://movie.douban.com/celebrity/1017911/\",\"id\":\"1017911\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1397621158.92.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1397621158.92.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1397621158.92.jpg\"},\"name_en\":\"Annie Potts\",\"name\":\"安妮·波茨\",\"alt\":\"https://movie.douban.com/celebrity/1041111/\",\"id\":\"1041111\"}],\"durations\":[\"100分钟\"],\"collect_count\":109742,\"mainland_pubdate\":\"2019-06-21\",\"has_video\":false,\"original_title\":\"Toy Story 4\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1436104752.86.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1436104752.86.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1436104752.86.jpg\"},\"name_en\":\"Josh Cooley\",\"name\":\"乔什·库雷\",\"alt\":\"https://movie.douban.com/celebrity/1348615/\",\"id\":\"1348615\"}],\"pubdates\":[\"2019-06-21(美国)\",\"2019-06-21(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2557284230.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2557284230.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2557284230.jpg\"},\"alt\":\"https://movie.douban.com/subject/6850547/\",\"id\":\"6850547\"},\"rank\":2,\"delta\":-1},{\"subject\":{\"rating\":{\"max\":10,\"average\":8,\"details\":{\"1\":12,\"2\":48,\"3\":428,\"4\":892,\"5\":531},\"stars\":\"40\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"小委托人\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/pUilPzUlwdREcel_avatar_uploaded1450547560.2.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/pUilPzUlwdREcel_avatar_uploaded1450547560.2.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/pUilPzUlwdREcel_avatar_uploaded1450547560.2.jpg\"},\"name_en\":\"Dong-Hwi Lee\",\"name\":\"李东辉\",\"alt\":\"https://movie.douban.com/celebrity/1353574/\",\"id\":\"1353574\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1354444074.14.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1354444074.14.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1354444074.14.jpg\"},\"name_en\":\"Seon Yu\",\"name\":\"柳善\",\"alt\":\"https://movie.douban.com/celebrity/1006553/\",\"id\":\"1006553\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p_5X-73FQYd4cel_avatar_uploaded1527560639.35.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p_5X-73FQYd4cel_avatar_uploaded1527560639.35.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p_5X-73FQYd4cel_avatar_uploaded1527560639.35.jpg\"},\"name_en\":\"고수희\",\"name\":\"高素熙\",\"alt\":\"https://movie.douban.com/celebrity/1394427/\",\"id\":\"1394427\"}],\"durations\":[\"114分钟\"],\"collect_count\":2748,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"어린 의뢰인\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1523345261.35.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1523345261.35.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1523345261.35.jpg\"},\"name_en\":\"Kyu-sung Jang\",\"name\":\"张圭声\",\"alt\":\"https://movie.douban.com/celebrity/1332427/\",\"id\":\"1332427\"}],\"pubdates\":[\"2019-05-22(韩国)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img2.doubanio.com/view/photo/s_ratio_poster/public/p2561250762.webp\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557335459.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557335459.jpg\"},\"alt\":\"https://movie.douban.com/subject/33404726/\",\"id\":\"33404726\"},\"rank\":3,\"delta\":8},{\"subject\":{\"rating\":{\"max\":10,\"average\":8,\"details\":{\"1\":174,\"2\":869,\"3\":9457,\"4\":20556,\"5\":11056},\"stars\":\"40\",\"min\":0},\"genres\":[\"动作\",\"科幻\",\"冒险\"],\"title\":\"蜘蛛侠：英雄远征\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467942867.09.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467942867.09.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467942867.09.jpg\"},\"name_en\":\"Tom Holland\",\"name\":\"汤姆·赫兰德\",\"alt\":\"https://movie.douban.com/celebrity/1325351/\",\"id\":\"1325351\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1543479263.47.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1543479263.47.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1543479263.47.jpg\"},\"name_en\":\"Zendaya\",\"name\":\"赞达亚\",\"alt\":\"https://movie.douban.com/celebrity/1325576/\",\"id\":\"1325576\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12737.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12737.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12737.jpg\"},\"name_en\":\"Jake Gyllenhaal\",\"name\":\"杰克·吉伦哈尔\",\"alt\":\"https://movie.douban.com/celebrity/1048002/\",\"id\":\"1048002\"}],\"durations\":[\"127分钟\"],\"collect_count\":279397,\"mainland_pubdate\":\"2019-06-28\",\"has_video\":false,\"original_title\":\"Spider-Man: Far From Home\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1435142487.62.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1435142487.62.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1435142487.62.jpg\"},\"name_en\":\"Jon Watts\",\"name\":\"乔·沃茨\",\"alt\":\"https://movie.douban.com/celebrity/1350194/\",\"id\":\"1350194\"}],\"pubdates\":[\"2019-06-28(中国大陆)\",\"2019-07-02(美国)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2558293106.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2558293106.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2558293106.jpg\"},\"alt\":\"https://movie.douban.com/subject/26931786/\",\"id\":\"26931786\"},\"rank\":4,\"delta\":-2},{\"subject\":{\"rating\":{\"max\":10,\"average\":7.6,\"details\":{\"1\":0,\"2\":7,\"3\":88,\"4\":150,\"5\":49},\"stars\":\"40\",\"min\":0},\"genres\":[\"剧情\",\"历史\"],\"title\":\"印第安·豪斯\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/pA6xRpJ9q3-Qcel_avatar_uploaded1540441349.29.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/pA6xRpJ9q3-Qcel_avatar_uploaded1540441349.29.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/pA6xRpJ9q3-Qcel_avatar_uploaded1540441349.29.jpg\"},\"name_en\":\"Sladen Peltier\",\"name\":\"斯莱登·佩提尔\",\"alt\":\"https://movie.douban.com/celebrity/1403265/\",\"id\":\"1403265\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1453358205.4.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1453358205.4.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1453358205.4.jpg\"},\"name_en\":\"Forrest Goodluck\",\"name\":\"福勒斯特·古德勒克\",\"alt\":\"https://movie.douban.com/celebrity/1354169/\",\"id\":\"1354169\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/pA6xRpJ9q3-Qcel_avatar_uploaded1540441743.99.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/pA6xRpJ9q3-Qcel_avatar_uploaded1540441743.99.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/pA6xRpJ9q3-Qcel_avatar_uploaded1540441743.99.jpg\"},\"name_en\":\"Ajuawak Kapashesit\",\"name\":\"阿居阿瓦克·卡帕绪西特\",\"alt\":\"https://movie.douban.com/celebrity/1403267/\",\"id\":\"1403267\"}],\"durations\":[\"100分钟\"],\"collect_count\":427,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"Indian Horse\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562392521.65.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562392521.65.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562392521.65.jpg\"},\"name_en\":\"Stephen S. Campanelli\",\"name\":\"斯蒂芬·S·坎帕内利\",\"alt\":\"https://movie.douban.com/celebrity/1419285/\",\"id\":\"1419285\"}],\"pubdates\":[\"2017-09-15(多伦多电影节)\",\"2018-04-13(加拿大)\"],\"year\":\"2017\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2517787515.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2517787515.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2517787515.jpg\"},\"alt\":\"https://movie.douban.com/subject/27173633/\",\"id\":\"27173633\"},\"rank\":5,\"delta\":6},{\"subject\":{\"rating\":{\"max\":10,\"average\":7.5,\"details\":{\"1\":8,\"2\":35,\"3\":337,\"4\":469,\"5\":166},\"stars\":\"40\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"陪审员\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6570.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6570.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6570.jpg\"},\"name_en\":\"So-ri Moon\",\"name\":\"文素丽\",\"alt\":\"https://movie.douban.com/celebrity/1010047/\",\"id\":\"1010047\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1479642755.23.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1479642755.23.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1479642755.23.jpg\"},\"name_en\":\"Hyung-Shik Park\",\"name\":\"朴炯植\",\"alt\":\"https://movie.douban.com/celebrity/1328834/\",\"id\":\"1328834\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1484199905.18.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1484199905.18.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1484199905.18.jpg\"},\"name_en\":\"Soo-Jang Baek\",\"name\":\"白秀章\",\"alt\":\"https://movie.douban.com/celebrity/1366939/\",\"id\":\"1366939\"}],\"durations\":[\"114分钟\"],\"collect_count\":1378,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"배심원들\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562393318.89.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562393318.89.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562393318.89.jpg\"},\"name_en\":\"Seung-wan Hong\",\"name\":\"洪承完\",\"alt\":\"https://movie.douban.com/celebrity/1419290/\",\"id\":\"1419290\"}],\"pubdates\":[\"2019-05-15(韩国)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2554542206.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2554542206.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2554542206.jpg\"},\"alt\":\"https://movie.douban.com/subject/27115758/\",\"id\":\"27115758\"},\"rank\":6,\"delta\":5},{\"subject\":{\"rating\":{\"max\":10,\"average\":7.2,\"details\":{\"1\":27,\"2\":263,\"3\":1706,\"4\":1569,\"5\":561},\"stars\":\"35\",\"min\":0},\"genres\":[\"喜剧\",\"动画\",\"冒险\"],\"title\":\"爱宠大机密2\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p9414.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p9414.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p9414.jpg\"},\"name_en\":\"Patton Oswalt\",\"name\":\"帕顿·奥斯瓦尔特\",\"alt\":\"https://movie.douban.com/celebrity/1007015/\",\"id\":\"1007015\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p56350.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p56350.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p56350.jpg\"},\"name_en\":\"Kevin Hart\",\"name\":\"凯文·哈特\",\"alt\":\"https://movie.douban.com/celebrity/1286162/\",\"id\":\"1286162\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1452760482.06.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1452760482.06.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1452760482.06.jpg\"},\"name_en\":\"Harrison Ford\",\"name\":\"哈里森·福特\",\"alt\":\"https://movie.douban.com/celebrity/1009238/\",\"id\":\"1009238\"}],\"durations\":[\"86分钟\"],\"collect_count\":20903,\"mainland_pubdate\":\"2019-07-05\",\"has_video\":false,\"original_title\":\"The Secret Life of Pets 2\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p29587.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p29587.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p29587.jpg\"},\"name_en\":\"Chris Renaud\",\"name\":\"克里斯·雷纳德\",\"alt\":\"https://movie.douban.com/celebrity/1295449/\",\"id\":\"1295449\"}],\"pubdates\":[\"2019-06-07(美国)\",\"2019-07-05(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2555923582.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2555923582.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2555923582.jpg\"},\"alt\":\"https://movie.douban.com/subject/26848167/\",\"id\":\"26848167\"},\"rank\":7,\"delta\":4},{\"subject\":{\"rating\":{\"max\":10,\"average\":7.2,\"details\":{\"1\":10,\"2\":61,\"3\":450,\"4\":452,\"5\":136},\"stars\":\"35\",\"min\":0},\"genres\":[\"战争\",\"灾难\"],\"title\":\"狼之歌\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1556635630.02.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1556635630.02.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1556635630.02.jpg\"},\"name_en\":\"François Civil\",\"name\":\"弗朗索瓦·西维尔\",\"alt\":\"https://movie.douban.com/celebrity/1006321/\",\"id\":\"1006321\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41401.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41401.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41401.jpg\"},\"name_en\":\"Omar Sy\",\"name\":\"奥玛·希\",\"alt\":\"https://movie.douban.com/celebrity/1220507/\",\"id\":\"1220507\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p34948.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p34948.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p34948.jpg\"},\"name_en\":\"Mathieu Kassovitz\",\"name\":\"马修·卡索维茨\",\"alt\":\"https://movie.douban.com/celebrity/1022733/\",\"id\":\"1022733\"}],\"durations\":[\"116分钟\"],\"collect_count\":1559,\"mainland_pubdate\":\"\",\"has_video\":false,\"original_title\":\"Le chant du loup\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/f/movie/ca527386eb8c4e325611e22dfcb04cc116d6b423/pics/movie/celebrity-default-small.png\",\"large\":\"https://img3.doubanio.com/f/movie/63acc16ca6309ef191f0378faf793d1096a3e606/pics/movie/celebrity-default-large.png\",\"medium\":\"https://img1.doubanio.com/f/movie/8dd0c794499fe925ae2ae89ee30cd225750457b4/pics/movie/celebrity-default-medium.png\"},\"name_en\":\"Abel Lanzac\",\"name\":\"阿贝尔·安扎克\",\"alt\":\"https://movie.douban.com/celebrity/1338365/\",\"id\":\"1338365\"}],\"pubdates\":[\"2019-02-20(法国)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2542972538.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2542972538.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2542972538.jpg\"},\"alt\":\"https://movie.douban.com/subject/27150283/\",\"id\":\"27150283\"},\"rank\":8,\"delta\":3},{\"subject\":{\"rating\":{\"max\":10,\"average\":7.5,\"details\":{\"1\":2,\"2\":6,\"3\":48,\"4\":64,\"5\":29},\"stars\":\"40\",\"min\":0},\"genres\":[\"传记\",\"纪录片\"],\"title\":\"上海的女儿\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p7240.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p7240.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p7240.jpg\"},\"name_en\":\"Tsai Chin\",\"name\":\"周采芹\",\"alt\":\"https://movie.douban.com/celebrity/1036870/\",\"id\":\"1036870\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p37261.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p37261.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p37261.jpg\"},\"name_en\":\"Sean Connery\",\"name\":\"肖恩·康纳利\",\"alt\":\"https://movie.douban.com/celebrity/1018982/\",\"id\":\"1018982\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p10245.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p10245.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p10245.jpg\"},\"name_en\":\"Sandra Oh\",\"name\":\"吴珊卓\",\"alt\":\"https://movie.douban.com/celebrity/1010565/\",\"id\":\"1010565\"}],\"durations\":[\"90分钟\"],\"collect_count\":538,\"mainland_pubdate\":\"2019-07-02\",\"has_video\":false,\"original_title\":\"上海的女儿\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1559043716.69.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1559043716.69.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1559043716.69.jpg\"},\"name_en\":\"Michelle Chen\",\"name\":\"陈苗\",\"alt\":\"https://movie.douban.com/celebrity/1308349/\",\"id\":\"1308349\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562383475.24.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562383475.24.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562383475.24.jpg\"},\"name_en\":\"Hilla Medalia\",\"name\":\"希拉·梅达利亚\",\"alt\":\"https://movie.douban.com/celebrity/1309059/\",\"id\":\"1309059\"}],\"pubdates\":[\"2019-07-02(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561937291.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561937291.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561937291.jpg\"},\"alt\":\"https://movie.douban.com/subject/33381322/\",\"id\":\"33381322\"},\"rank\":9,\"delta\":2},{\"subject\":{\"rating\":{\"max\":10,\"average\":7.2,\"details\":{\"1\":3,\"2\":7,\"3\":39,\"4\":44,\"5\":16},\"stars\":\"35\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"学区房72小时\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1504773599.22.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1504773599.22.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1504773599.22.jpg\"},\"name_en\":\"Xuan Guan\",\"name\":\"管轩\",\"alt\":\"https://movie.douban.com/celebrity/1380712/\",\"id\":\"1380712\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6311.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6311.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p6311.jpg\"},\"name_en\":\"Xing Xu\",\"name\":\"徐幸\",\"alt\":\"https://movie.douban.com/celebrity/1274773/\",\"id\":\"1274773\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5975.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5975.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p5975.jpg\"},\"name_en\":\"Miao Fu\",\"name\":\"傅淼\",\"alt\":\"https://movie.douban.com/celebrity/1274661/\",\"id\":\"1274661\"}],\"durations\":[\"99分钟\"],\"collect_count\":568,\"mainland_pubdate\":\"2019-06-28\",\"has_video\":false,\"original_title\":\"学区房72小时\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1528872099.62.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1528872099.62.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1528872099.62.jpg\"},\"name_en\":\"Xiaoming Chen\",\"name\":\"陈晓鸣\",\"alt\":\"https://movie.douban.com/celebrity/1395363/\",\"id\":\"1395363\"}],\"pubdates\":[\"2019-06-16(上海电影节)\",\"2019-06-28(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2560837532.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2560837532.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2560837532.jpg\"},\"alt\":\"https://movie.douban.com/subject/30246061/\",\"id\":\"30246061\"},\"rank\":10,\"delta\":1}],\"title\":\"豆瓣电影本周口碑榜\"}");

/***/ }),
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */
/*!**************************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/mock/coming_soon.json ***!
  \**************************************************************************/
/*! exports provided: count, start, total, subjects, title, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"count\":20,\"start\":0,\"total\":89,\"subjects\":[{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"剧情\",\"动画\",\"冒险\"],\"title\":\"狮子王\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1466186775.21.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1466186775.21.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1466186775.21.jpg\"},\"name_en\":\"Donald Glover\",\"name\":\"唐纳德·格洛弗\",\"alt\":\"https://movie.douban.com/celebrity/1314061/\",\"id\":\"1314061\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1422444822.86.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1422444822.86.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1422444822.86.jpg\"},\"name_en\":\"Alfre Woodard\",\"name\":\"阿尔法·伍达德\",\"alt\":\"https://movie.douban.com/celebrity/1041159/\",\"id\":\"1041159\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1490091638.27.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1490091638.27.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1490091638.27.jpg\"},\"name_en\":\"James Earl Jones\",\"name\":\"詹姆斯·厄尔·琼斯\",\"alt\":\"https://movie.douban.com/celebrity/1013800/\",\"id\":\"1013800\"}],\"durations\":[\"118分钟\"],\"collect_count\":868,\"mainland_pubdate\":\"2019-07-12\",\"has_video\":false,\"original_title\":\"The Lion King\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1405546733.56.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1405546733.56.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1405546733.56.jpg\"},\"name_en\":\"Jon Favreau\",\"name\":\"乔恩·费儒\",\"alt\":\"https://movie.douban.com/celebrity/1027164/\",\"id\":\"1027164\"}],\"pubdates\":[\"2019-07-12(中国大陆)\",\"2019-07-19(美国)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2559742751.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2559742751.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2559742751.jpg\"},\"alt\":\"https://movie.douban.com/subject/26884354/\",\"id\":\"26884354\"},{\"rating\":{\"max\":10,\"average\":8.2,\"details\":{\"1\":7,\"2\":13,\"3\":94,\"4\":167,\"5\":195},\"stars\":\"45\",\"min\":0},\"genres\":[\"剧情\",\"动画\",\"奇幻\"],\"title\":\"命运之夜——天之杯II ：迷失之蝶\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1360495608.41.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1360495608.41.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1360495608.41.jpg\"},\"name_en\":\"Noriaki Sugiyama\",\"name\":\"杉山纪彰\",\"alt\":\"https://movie.douban.com/celebrity/1035537/\",\"id\":\"1035537\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1401447458.69.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1401447458.69.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1401447458.69.jpg\"},\"name_en\":\"Noriko Shitaya\",\"name\":\"下屋则子\",\"alt\":\"https://movie.douban.com/celebrity/1046546/\",\"id\":\"1046546\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p23870.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p23870.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p23870.jpg\"},\"name_en\":\"Hiroshi Kamiya\",\"name\":\"神谷浩史\",\"alt\":\"https://movie.douban.com/celebrity/1274377/\",\"id\":\"1274377\"}],\"durations\":[\"117分钟\",\"113分钟(中国大陆)\"],\"collect_count\":3315,\"mainland_pubdate\":\"2019-07-12\",\"has_video\":false,\"original_title\":\"劇場版Fate/stay night Heaven's Feel II.lost butterfly\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p-X6KppmI0EIcel_avatar_uploaded1525790121.19.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p-X6KppmI0EIcel_avatar_uploaded1525790121.19.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p-X6KppmI0EIcel_avatar_uploaded1525790121.19.jpg\"},\"name_en\":\"Tomonori Sudô\",\"name\":\"须藤友德\",\"alt\":\"https://movie.douban.com/celebrity/1392464/\",\"id\":\"1392464\"}],\"pubdates\":[\"2019-01-12(日本)\",\"2019-07-12(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561910374.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561910374.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561910374.jpg\"},\"alt\":\"https://movie.douban.com/subject/26759819/\",\"id\":\"26759819\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"喜剧\",\"动作\",\"冒险\"],\"title\":\"素人特工\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1546579487.08.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1546579487.08.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1546579487.08.jpg\"},\"name_en\":\"Talu Wang\",\"name\":\"王大陆\",\"alt\":\"https://movie.douban.com/celebrity/1341187/\",\"id\":\"1341187\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12665.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12665.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p12665.jpg\"},\"name_en\":\"Sandrine Pinna\",\"name\":\"张榕容\",\"alt\":\"https://movie.douban.com/celebrity/1226703/\",\"id\":\"1226703\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2431.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2431.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p2431.jpg\"},\"name_en\":\"Milla Jovovich\",\"name\":\"米拉·乔沃维奇\",\"alt\":\"https://movie.douban.com/celebrity/1025154/\",\"id\":\"1025154\"}],\"durations\":[\"113分钟\"],\"collect_count\":142,\"mainland_pubdate\":\"2019-07-12\",\"has_video\":false,\"original_title\":\"素人特工\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1453166767.25.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1453166767.25.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1453166767.25.jpg\"},\"name_en\":\"Alan Yuen\",\"name\":\"袁锦麟\",\"alt\":\"https://movie.douban.com/celebrity/1312751/\",\"id\":\"1312751\"}],\"pubdates\":[\"2019-07-12(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2560447448.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2560447448.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2560447448.jpg\"},\"alt\":\"https://movie.douban.com/subject/27155276/\",\"id\":\"27155276\"},{\"rating\":{\"max\":10,\"average\":5.9,\"details\":{\"1\":16,\"2\":44,\"3\":108,\"4\":45,\"5\":11},\"stars\":\"30\",\"min\":0},\"genres\":[\"战争\",\"科幻\",\"动画\"],\"title\":\"机动战士高达NT\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1502244105.4.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1502244105.4.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1502244105.4.jpg\"},\"name_en\":\"Junya Enoki\",\"name\":\"榎木淳弥\",\"alt\":\"https://movie.douban.com/celebrity/1378653/\",\"id\":\"1378653\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/pNZkDWpTYTsocel_avatar_uploaded1547469900.23.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/pNZkDWpTYTsocel_avatar_uploaded1547469900.23.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/pNZkDWpTYTsocel_avatar_uploaded1547469900.23.jpg\"},\"name_en\":\"Muranaka Tomo\",\"name\":\"村中知\",\"alt\":\"https://movie.douban.com/celebrity/1409158/\",\"id\":\"1409158\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1512268646.95.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1512268646.95.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1512268646.95.jpg\"},\"name_en\":\"Ayu Matsuura\",\"name\":\"松浦爱弓\",\"alt\":\"https://movie.douban.com/celebrity/1319428/\",\"id\":\"1319428\"}],\"durations\":[\"90分钟\"],\"collect_count\":1067,\"mainland_pubdate\":\"2019-07-12\",\"has_video\":false,\"original_title\":\"機動戦士ガンダム NT（ナラティブ）\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/f/movie/ca527386eb8c4e325611e22dfcb04cc116d6b423/pics/movie/celebrity-default-small.png\",\"large\":\"https://img3.doubanio.com/f/movie/63acc16ca6309ef191f0378faf793d1096a3e606/pics/movie/celebrity-default-large.png\",\"medium\":\"https://img1.doubanio.com/f/movie/8dd0c794499fe925ae2ae89ee30cd225750457b4/pics/movie/celebrity-default-medium.png\"},\"name_en\":\"Shun'ichi Yoshizawa\",\"name\":\"吉泽俊一\",\"alt\":\"https://movie.douban.com/celebrity/1419223/\",\"id\":\"1419223\"}],\"pubdates\":[\"2018-11-30(日本)\",\"2019-07-12(中国大陆)\"],\"year\":\"2018\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2558661806.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2558661806.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2558661806.jpg\"},\"alt\":\"https://movie.douban.com/subject/30201138/\",\"id\":\"30201138\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"剧情\",\"家庭\"],\"title\":\"舞动吧！少年\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/poROlorauh_scel_avatar_uploaded1517451746.06.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/poROlorauh_scel_avatar_uploaded1517451746.06.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/poROlorauh_scel_avatar_uploaded1517451746.06.jpg\"},\"name_en\":\"Jun Lei\",\"name\":\"雷俊\",\"alt\":\"https://movie.douban.com/celebrity/1388052/\",\"id\":\"1388052\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/f/movie/ca527386eb8c4e325611e22dfcb04cc116d6b423/pics/movie/celebrity-default-small.png\",\"large\":\"https://img3.doubanio.com/f/movie/63acc16ca6309ef191f0378faf793d1096a3e606/pics/movie/celebrity-default-large.png\",\"medium\":\"https://img1.doubanio.com/f/movie/8dd0c794499fe925ae2ae89ee30cd225750457b4/pics/movie/celebrity-default-medium.png\"},\"name_en\":\"Sicheng Zhang\",\"name\":\"张嗣诚  \",\"alt\":\"https://movie.douban.com/celebrity/1418858/\",\"id\":\"1418858\"}],\"durations\":[\"94分钟\"],\"collect_count\":2,\"mainland_pubdate\":\"2019-07-12\",\"has_video\":false,\"original_title\":\"舞动吧！少年\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/peGF4emMAelAcel_avatar_uploaded1551493977.08.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/peGF4emMAelAcel_avatar_uploaded1551493977.08.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/peGF4emMAelAcel_avatar_uploaded1551493977.08.jpg\"},\"name_en\":\"Wei Dou\",\"name\":\"窦微\",\"alt\":\"https://movie.douban.com/celebrity/1412048/\",\"id\":\"1412048\"}],\"pubdates\":[\"2019-07-12(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2555119986.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2555119986.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2555119986.jpg\"},\"alt\":\"https://movie.douban.com/subject/30276413/\",\"id\":\"30276413\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"嘿，蠢贼\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1399707920.17.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1399707920.17.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1399707920.17.jpg\"},\"name_en\":\"Baoyue Cui\",\"name\":\"崔宝月\",\"alt\":\"https://movie.douban.com/celebrity/1334949/\",\"id\":\"1334949\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/pL9vAARVRtLscel_avatar_uploaded1514355604.19.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/pL9vAARVRtLscel_avatar_uploaded1514355604.19.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/pL9vAARVRtLscel_avatar_uploaded1514355604.19.jpg\"},\"name_en\":\"Nuo Xu\",\"name\":\"许诺\",\"alt\":\"https://movie.douban.com/celebrity/1386365/\",\"id\":\"1386365\"},{\"avatars\":null,\"name_en\":\"\",\"name\":\"苏小淳\",\"alt\":null,\"id\":null}],\"durations\":[\"90分钟\"],\"collect_count\":8,\"mainland_pubdate\":\"2019-07-16\",\"has_video\":false,\"original_title\":\"嘿，蠢贼\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467125575.08.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467125575.08.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467125575.08.jpg\"},\"name_en\":\"Kaiyang Wang\",\"name\":\"王凯阳\",\"alt\":\"https://movie.douban.com/celebrity/1350019/\",\"id\":\"1350019\"}],\"pubdates\":[\"2019-07-16(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2560832388.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2560832388.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2560832388.jpg\"},\"alt\":\"https://movie.douban.com/subject/34445992/\",\"id\":\"34445992\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"银河补习班\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p805.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p805.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p805.jpg\"},\"name_en\":\"Chao Deng\",\"name\":\"邓超\",\"alt\":\"https://movie.douban.com/celebrity/1274235/\",\"id\":\"1274235\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1530362237.95.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1530362237.95.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1530362237.95.jpg\"},\"name_en\":\"Yu Bai\",\"name\":\"白宇\",\"alt\":\"https://movie.douban.com/celebrity/1337887/\",\"id\":\"1337887\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1478066140.77.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1478066140.77.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1478066140.77.jpg\"},\"name_en\":\"Suxi Ren\",\"name\":\"任素汐\",\"alt\":\"https://movie.douban.com/celebrity/1362973/\",\"id\":\"1362973\"}],\"durations\":[\"147分钟\"],\"collect_count\":584,\"mainland_pubdate\":\"2019-07-18\",\"has_video\":false,\"original_title\":\"银河补习班\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p805.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p805.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p805.jpg\"},\"name_en\":\"Chao Deng\",\"name\":\"邓超\",\"alt\":\"https://movie.douban.com/celebrity/1274235/\",\"id\":\"1274235\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p36973.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p36973.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p36973.jpg\"},\"name_en\":\"Baimei Yu\",\"name\":\"俞白眉\",\"alt\":\"https://movie.douban.com/celebrity/1316617/\",\"id\":\"1316617\"}],\"pubdates\":[\"2019-07-18(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2561542089.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2561542089.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2561542089.jpg\"},\"alt\":\"https://movie.douban.com/subject/30282387/\",\"id\":\"30282387\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"剧情\",\"喜剧\",\"犯罪\"],\"title\":\"灰猴\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1371633861.75.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1371633861.75.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1371633861.75.jpg\"},\"name_en\":\"Dazhi Wang\",\"name\":\"王大治\",\"alt\":\"https://movie.douban.com/celebrity/1314858/\",\"id\":\"1314858\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p53419.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p53419.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p53419.jpg\"},\"name_en\":\"Feng Gao\",\"name\":\"高峰\",\"alt\":\"https://movie.douban.com/celebrity/1275669/\",\"id\":\"1275669\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1374382499.95.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1374382499.95.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1374382499.95.jpg\"},\"name_en\":\"Jingyun Wang\",\"name\":\"王靖云\",\"alt\":\"https://movie.douban.com/celebrity/1328159/\",\"id\":\"1328159\"}],\"durations\":[\"98分钟\"],\"collect_count\":165,\"mainland_pubdate\":\"2019-07-18\",\"has_video\":false,\"original_title\":\"灰猴\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1534008742.7.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1534008742.7.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1534008742.7.jpg\"},\"name_en\":\"Pu Zhang\",\"name\":\"张璞\",\"alt\":\"https://movie.douban.com/celebrity/1398757/\",\"id\":\"1398757\"}],\"pubdates\":[\"2018-09-01(蒙特利尔国际电影节)\",\"2019-07-18(中国大陆)\"],\"year\":\"2018\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2561541477.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2561541477.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2561541477.jpg\"},\"alt\":\"https://movie.douban.com/subject/27199913/\",\"id\":\"27199913\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"匠心\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p38286.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p38286.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p38286.jpg\"},\"name_en\":\"Shaohua Ma\",\"name\":\"马少骅\",\"alt\":\"https://movie.douban.com/celebrity/1274966/\",\"id\":\"1274966\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33243.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33243.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33243.jpg\"},\"name_en\":\"Di Zhang\",\"name\":\"张迪\",\"alt\":\"https://movie.douban.com/celebrity/1315701/\",\"id\":\"1315701\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562154860.46.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562154860.46.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562154860.46.jpg\"},\"name_en\":\"Eleven Yao\",\"name\":\"姚以缇\",\"alt\":\"https://movie.douban.com/celebrity/1355368/\",\"id\":\"1355368\"}],\"durations\":[\"93分钟\"],\"collect_count\":6,\"mainland_pubdate\":\"2019-07-18\",\"has_video\":false,\"original_title\":\"匠心\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467125575.08.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467125575.08.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1467125575.08.jpg\"},\"name_en\":\"Kaiyang Wang\",\"name\":\"王凯阳\",\"alt\":\"https://movie.douban.com/celebrity/1350019/\",\"id\":\"1350019\"}],\"pubdates\":[\"2019-07-18(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2553935771.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2553935771.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2553935771.jpg\"},\"alt\":\"https://movie.douban.com/subject/30136664/\",\"id\":\"30136664\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"剧情\",\"动作\",\"武侠\"],\"title\":\"刀背藏身\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1485778268.65.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1485778268.65.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1485778268.65.jpg\"},\"name_en\":\"Qing Xu\",\"name\":\"许晴\",\"alt\":\"https://movie.douban.com/celebrity/1005268/\",\"id\":\"1005268\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1560423707.2.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1560423707.2.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1560423707.2.jpg\"},\"name_en\":\"Aoyue Zhang\",\"name\":\"张傲月\",\"alt\":\"https://movie.douban.com/celebrity/1353125/\",\"id\":\"1353125\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1526309480.04.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1526309480.04.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1526309480.04.jpg\"},\"name_en\":\"Jessie Li\",\"name\":\"春夏\",\"alt\":\"https://movie.douban.com/celebrity/1339442/\",\"id\":\"1339442\"}],\"durations\":[\"123分钟\",\"137分钟(蒙特利尔电影节)\"],\"collect_count\":708,\"mainland_pubdate\":\"2019-07-19\",\"has_video\":false,\"original_title\":\"刀背藏身\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41597.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41597.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p41597.jpg\"},\"name_en\":\"Haofeng Xu\",\"name\":\"徐浩峰\",\"alt\":\"https://movie.douban.com/celebrity/1316577/\",\"id\":\"1316577\"}],\"pubdates\":[\"2017-08-27(蒙特利尔电影节)\",\"2019-07-19(中国大陆)\"],\"year\":\"2017\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557644589.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557644589.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2557644589.jpg\"},\"alt\":\"https://movie.douban.com/subject/26757090/\",\"id\":\"26757090\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"喜剧\",\"奇幻\"],\"title\":\"猪八戒·传说\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/f/movie/ca527386eb8c4e325611e22dfcb04cc116d6b423/pics/movie/celebrity-default-small.png\",\"large\":\"https://img3.doubanio.com/f/movie/63acc16ca6309ef191f0378faf793d1096a3e606/pics/movie/celebrity-default-large.png\",\"medium\":\"https://img1.doubanio.com/f/movie/8dd0c794499fe925ae2ae89ee30cd225750457b4/pics/movie/celebrity-default-medium.png\"},\"name_en\":\"\",\"name\":\"郑冀峰\",\"alt\":\"https://movie.douban.com/celebrity/1414809/\",\"id\":\"1414809\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1161.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1161.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1161.jpg\"},\"name_en\":\"Eric Tsang\",\"name\":\"曾志伟\",\"alt\":\"https://movie.douban.com/celebrity/1002862/\",\"id\":\"1002862\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1545123115.59.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1545123115.59.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1545123115.59.jpg\"},\"name_en\":\"Ruoxi Yin\",\"name\":\"殷若曦\",\"alt\":\"https://movie.douban.com/celebrity/1406995/\",\"id\":\"1406995\"}],\"durations\":[],\"collect_count\":394,\"mainland_pubdate\":\"2019-07-19\",\"has_video\":false,\"original_title\":\"猪八戒·传说\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/f/movie/ca527386eb8c4e325611e22dfcb04cc116d6b423/pics/movie/celebrity-default-small.png\",\"large\":\"https://img3.doubanio.com/f/movie/63acc16ca6309ef191f0378faf793d1096a3e606/pics/movie/celebrity-default-large.png\",\"medium\":\"https://img1.doubanio.com/f/movie/8dd0c794499fe925ae2ae89ee30cd225750457b4/pics/movie/celebrity-default-medium.png\"},\"name_en\":\"\",\"name\":\"郑冀峰\",\"alt\":\"https://movie.douban.com/celebrity/1414809/\",\"id\":\"1414809\"}],\"pubdates\":[\"2019-07-19(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561910053.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561910053.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561910053.jpg\"},\"alt\":\"https://movie.douban.com/subject/30355305/\",\"id\":\"30355305\"},{\"rating\":{\"max\":10,\"average\":6.1,\"details\":{\"1\":29,\"2\":89,\"3\":210,\"4\":65,\"5\":50},\"stars\":\"30\",\"min\":0},\"genres\":[\"科幻\",\"动画\",\"冒险\"],\"title\":\"未来机器城\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1534766432.19.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1534766432.19.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1534766432.19.jpg\"},\"name_en\":\"Yingdi Han\",\"name\":\"韩莹棣\",\"alt\":\"https://movie.douban.com/celebrity/1399479/\",\"id\":\"1399479\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1534766550.03.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1534766550.03.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1534766550.03.jpg\"},\"name_en\":\"Haiyin Zheng\",\"name\":\"郑海音\",\"alt\":\"https://movie.douban.com/celebrity/1399480/\",\"id\":\"1399480\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1381744526.75.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1381744526.75.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1381744526.75.jpg\"},\"name_en\":\"Yuanzheng Feng\",\"name\":\"冯远征\",\"alt\":\"https://movie.douban.com/celebrity/1043136/\",\"id\":\"1043136\"}],\"durations\":[\"107分钟\",\"106分钟(美国)\"],\"collect_count\":4655,\"mainland_pubdate\":\"2019-07-19\",\"has_video\":false,\"original_title\":\"未来机器城\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1531724401.77.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1531724401.77.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1531724401.77.jpg\"},\"name_en\":\"Kevin R. Adams\",\"name\":\"安恪温\",\"alt\":\"https://movie.douban.com/celebrity/1392324/\",\"id\":\"1392324\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1531724416.93.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1531724416.93.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1531724416.93.jpg\"},\"name_en\":\"Joe Ksander\",\"name\":\"龙子乔\",\"alt\":\"https://movie.douban.com/celebrity/1308172/\",\"id\":\"1308172\"}],\"pubdates\":[\"2018-09-07(美国)\",\"2019-07-19(中国大陆)\"],\"year\":\"2018\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561782665.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561782665.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561782665.jpg\"},\"alt\":\"https://movie.douban.com/subject/27200988/\",\"id\":\"27200988\"},{\"rating\":{\"max\":10,\"average\":8.3,\"details\":{\"1\":17,\"2\":41,\"3\":240,\"4\":461,\"5\":546},\"stars\":\"45\",\"min\":0},\"genres\":[\"爱情\",\"动画\",\"奇幻\"],\"title\":\"游戏人生 零\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1497789604.25.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1497789604.25.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1497789604.25.jpg\"},\"name_en\":\"Yoshitsugu Matsuoka\",\"name\":\"松冈祯丞\",\"alt\":\"https://movie.douban.com/celebrity/1314691/\",\"id\":\"1314691\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1366715194.78.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1366715194.78.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1366715194.78.jpg\"},\"name_en\":\"Ai Kayano\",\"name\":\"茅野爱衣\",\"alt\":\"https://movie.douban.com/celebrity/1314532/\",\"id\":\"1314532\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p23363.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p23363.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p23363.jpg\"},\"name_en\":\"Yoko Hikasa\",\"name\":\"日笠阳子\",\"alt\":\"https://movie.douban.com/celebrity/1275199/\",\"id\":\"1275199\"}],\"durations\":[\"107分钟\"],\"collect_count\":11173,\"mainland_pubdate\":\"2019-07-19\",\"has_video\":false,\"original_title\":\"ノーゲーム・ノーライフ ゼロ\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p21643.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p21643.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p21643.jpg\"},\"name_en\":\"Atsuko Ishizuka\",\"name\":\"石塚敦子\",\"alt\":\"https://movie.douban.com/celebrity/1314066/\",\"id\":\"1314066\"}],\"pubdates\":[\"2017-07-15(日本)\",\"2019-07-19(中国大陆)\"],\"year\":\"2017\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561782374.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561782374.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561782374.jpg\"},\"alt\":\"https://movie.douban.com/subject/26837952/\",\"id\":\"26837952\"},{\"rating\":{\"max\":10,\"average\":7.2,\"details\":{\"1\":0,\"2\":15,\"3\":138,\"4\":166,\"5\":28},\"stars\":\"35\",\"min\":0},\"genres\":[\"剧情\",\"儿童\"],\"title\":\"旺扎的雨靴\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1538545061.03.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1538545061.03.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1538545061.03.jpg\"},\"name_en\":\"Druklha Dorje\",\"name\":\"周拉多杰\",\"alt\":\"https://movie.douban.com/celebrity/1396554/\",\"id\":\"1396554\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1539580440.17.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1539580440.17.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1539580440.17.jpg\"},\"name_en\":\"Jinpa\",\"name\":\"金巴\",\"alt\":\"https://movie.douban.com/celebrity/1362878/\",\"id\":\"1362878\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1539579380.69.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1539579380.69.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1539579380.69.jpg\"},\"name_en\":\"Tsemdo\",\"name\":\"才多\",\"alt\":\"https://movie.douban.com/celebrity/1402634/\",\"id\":\"1402634\"}],\"durations\":[\"90分钟\"],\"collect_count\":1688,\"mainland_pubdate\":\"2019-07-19\",\"has_video\":false,\"original_title\":\"旺扎的雨靴\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1525058811.52.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1525058811.52.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1525058811.52.jpg\"},\"name_en\":\"Lhapal Gyal\",\"name\":\"拉华加\",\"alt\":\"https://movie.douban.com/celebrity/1391840/\",\"id\":\"1391840\"}],\"pubdates\":[\"2018-02-21(柏林电影节)\",\"2019-07-19(中国大陆)\"],\"year\":\"2018\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561371801.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561371801.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561371801.jpg\"},\"alt\":\"https://movie.douban.com/subject/28510273/\",\"id\":\"28510273\"},{\"rating\":{\"max\":10,\"average\":6.4,\"details\":{\"1\":10,\"2\":64,\"3\":300,\"4\":146,\"5\":24},\"stars\":\"35\",\"min\":0},\"genres\":[\"剧情\",\"喜剧\",\"传记\"],\"title\":\"为家而战\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1500079626.83.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1500079626.83.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1500079626.83.jpg\"},\"name_en\":\"Dwayne Johnson\",\"name\":\"道恩·强森\",\"alt\":\"https://movie.douban.com/celebrity/1044707/\",\"id\":\"1044707\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1517034183.97.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1517034183.97.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1517034183.97.jpg\"},\"name_en\":\"Florence Pugh\",\"name\":\"弗洛伦斯·皮尤\",\"alt\":\"https://movie.douban.com/celebrity/1378921/\",\"id\":\"1378921\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1504372509.79.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1504372509.79.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1504372509.79.jpg\"},\"name_en\":\"Jack Lowden\",\"name\":\"杰克·劳登\",\"alt\":\"https://movie.douban.com/celebrity/1344553/\",\"id\":\"1344553\"}],\"durations\":[\"108分钟\"],\"collect_count\":2380,\"mainland_pubdate\":\"2019-07-19\",\"has_video\":false,\"original_title\":\"Fighting with My Family\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p7699.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p7699.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p7699.jpg\"},\"name_en\":\"Stephen Merchant\",\"name\":\"斯戴芬·莫昌特\",\"alt\":\"https://movie.douban.com/celebrity/1036473/\",\"id\":\"1036473\"}],\"pubdates\":[\"2019-01-28(圣丹斯电影节)\",\"2019-02-22(美国)\",\"2019-07-19(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2559337905.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2559337905.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2559337905.jpg\"},\"alt\":\"https://movie.douban.com/subject/26971054/\",\"id\":\"26971054\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"惊悚\",\"恐怖\"],\"title\":\"怨灵3：看不见的小孩\",\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562223888.57.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562223888.57.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562223888.57.jpg\"},\"name_en\":\"Petei Hokari\",\"name\":\"裴蒂·赫格莉\",\"alt\":\"https://movie.douban.com/celebrity/1400572/\",\"id\":\"1400572\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562151717.65.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562151717.65.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562151717.65.jpg\"},\"name_en\":\"Jinyun Chen\",\"name\":\"陈锦昀\",\"alt\":\"https://movie.douban.com/celebrity/1374406/\",\"id\":\"1374406\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561434947.6.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561434947.6.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561434947.6.jpg\"},\"name_en\":\"\",\"name\":\"王景晨\",\"alt\":\"https://movie.douban.com/celebrity/1416944/\",\"id\":\"1416944\"}],\"durations\":[\"90分钟\"],\"collect_count\":20,\"mainland_pubdate\":\"2019-07-19\",\"has_video\":false,\"original_title\":\"怨灵3：看不见的小孩\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562151601.28.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562151601.28.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1562151601.28.jpg\"},\"name_en\":\"Isara Nadee\",\"name\":\"伊萨拉·纳迪\",\"alt\":\"https://movie.douban.com/celebrity/1327530/\",\"id\":\"1327530\"}],\"pubdates\":[\"2019-07-19(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561451573.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561451573.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561451573.jpg\"},\"alt\":\"https://movie.douban.com/subject/30290872/\",\"id\":\"30290872\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"剧情\",\"动画\",\"冒险\"],\"title\":\"天池水怪\",\"casts\":[{\"avatars\":null,\"name_en\":\"\",\"name\":\"丁传惠\",\"alt\":null,\"id\":null},{\"avatars\":null,\"name_en\":\"\",\"name\":\"华兰香\",\"alt\":null,\"id\":null},{\"avatars\":null,\"name_en\":\"\",\"name\":\"黄设来\",\"alt\":null,\"id\":null}],\"durations\":[\"84分钟\"],\"collect_count\":8,\"mainland_pubdate\":\"2019-07-19\",\"has_video\":false,\"original_title\":\"天池水怪\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/f/movie/ca527386eb8c4e325611e22dfcb04cc116d6b423/pics/movie/celebrity-default-small.png\",\"large\":\"https://img3.doubanio.com/f/movie/63acc16ca6309ef191f0378faf793d1096a3e606/pics/movie/celebrity-default-large.png\",\"medium\":\"https://img1.doubanio.com/f/movie/8dd0c794499fe925ae2ae89ee30cd225750457b4/pics/movie/celebrity-default-medium.png\"},\"name_en\":\"Dongxian Zhu\",\"name\":\"朱东贤\",\"alt\":\"https://movie.douban.com/celebrity/1419564/\",\"id\":\"1419564\"}],\"pubdates\":[\"2019-07-19(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561900220.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561900220.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561900220.jpg\"},\"alt\":\"https://movie.douban.com/subject/30483696/\",\"id\":\"30483696\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"动画\"],\"title\":\"精灵小王子\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p34402.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p34402.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p34402.jpg\"},\"name_en\":\"Rasmus Hardiker\",\"name\":\"拉斯穆斯·哈迪克\",\"alt\":\"https://movie.douban.com/celebrity/1315925/\",\"id\":\"1315925\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/f/movie/ca527386eb8c4e325611e22dfcb04cc116d6b423/pics/movie/celebrity-default-small.png\",\"large\":\"https://img3.doubanio.com/f/movie/63acc16ca6309ef191f0378faf793d1096a3e606/pics/movie/celebrity-default-large.png\",\"medium\":\"https://img1.doubanio.com/f/movie/8dd0c794499fe925ae2ae89ee30cd225750457b4/pics/movie/celebrity-default-medium.png\"},\"name_en\":\"Amy Saville\",\"name\":\"艾米·萨维尔\",\"alt\":\"https://movie.douban.com/celebrity/1418968/\",\"id\":\"1418968\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p19586.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p19586.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p19586.jpg\"},\"name_en\":\"Jim Carter\",\"name\":\"吉姆·卡特\",\"alt\":\"https://movie.douban.com/celebrity/1077385/\",\"id\":\"1077385\"}],\"durations\":[\"83分钟\",\"82分钟(中国大陆)\"],\"collect_count\":24,\"mainland_pubdate\":\"2019-07-20\",\"has_video\":false,\"original_title\":\"The Little Vampire 3D\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/f/movie/ca527386eb8c4e325611e22dfcb04cc116d6b423/pics/movie/celebrity-default-small.png\",\"large\":\"https://img3.doubanio.com/f/movie/63acc16ca6309ef191f0378faf793d1096a3e606/pics/movie/celebrity-default-large.png\",\"medium\":\"https://img1.doubanio.com/f/movie/8dd0c794499fe925ae2ae89ee30cd225750457b4/pics/movie/celebrity-default-medium.png\"},\"name_en\":\"Richard Claus\",\"name\":\"理查德·克劳斯\",\"alt\":\"https://movie.douban.com/celebrity/1418967/\",\"id\":\"1418967\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/f/movie/ca527386eb8c4e325611e22dfcb04cc116d6b423/pics/movie/celebrity-default-small.png\",\"large\":\"https://img3.doubanio.com/f/movie/63acc16ca6309ef191f0378faf793d1096a3e606/pics/movie/celebrity-default-large.png\",\"medium\":\"https://img1.doubanio.com/f/movie/8dd0c794499fe925ae2ae89ee30cd225750457b4/pics/movie/celebrity-default-medium.png\"},\"name_en\":\"Karsten Kiilerich\",\"name\":\"卡斯滕·基里奇\",\"alt\":\"https://movie.douban.com/celebrity/1289177/\",\"id\":\"1289177\"}],\"pubdates\":[\"2017-10-01(丹麦)\",\"2017-10-04(荷兰)\",\"2019-07-20(中国大陆)\"],\"year\":\"2017\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561172763.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561172763.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561172763.jpg\"},\"alt\":\"https://movie.douban.com/subject/26754064/\",\"id\":\"26754064\"},{\"rating\":{\"max\":10,\"average\":0,\"details\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0},\"stars\":\"00\",\"min\":0},\"genres\":[\"剧情\"],\"title\":\"心之山\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623312.44.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623312.44.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623312.44.jpg\"},\"name_en\":\"\",\"name\":\"胡建群\",\"alt\":\"https://movie.douban.com/celebrity/1418869/\",\"id\":\"1418869\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623325.82.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623325.82.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623325.82.jpg\"},\"name_en\":\"\",\"name\":\"倪颂夏\",\"alt\":\"https://movie.douban.com/celebrity/1418870/\",\"id\":\"1418870\"},{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623342.87.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623342.87.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623342.87.jpg\"},\"name_en\":\"Wenqian Jiao\",\"name\":\"焦雯倩\",\"alt\":\"https://movie.douban.com/celebrity/1406076/\",\"id\":\"1406076\"}],\"durations\":[\"104分钟\"],\"collect_count\":3,\"mainland_pubdate\":\"2019-07-20\",\"has_video\":false,\"original_title\":\"心之山\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623286.5.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623286.5.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623286.5.jpg\"},\"name_en\":\"\",\"name\":\"张崇玹\",\"alt\":\"https://movie.douban.com/celebrity/1418867/\",\"id\":\"1418867\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623301.03.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623301.03.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1561623301.03.jpg\"},\"name_en\":\"\",\"name\":\"徐飞\",\"alt\":\"https://movie.douban.com/celebrity/1418868/\",\"id\":\"1418868\"}],\"pubdates\":[\"2019-07-20(中国大陆)\"],\"year\":\"2019\",\"images\":{\"small\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561899653.jpg\",\"large\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561899653.jpg\",\"medium\":\"https://img3.doubanio.com/view/photo/s_ratio_poster/public/p2561899653.jpg\"},\"alt\":\"https://movie.douban.com/subject/34445676/\",\"id\":\"34445676\"},{\"rating\":{\"max\":10,\"average\":7.4,\"details\":{\"1\":45,\"2\":263,\"3\":3061,\"4\":4190,\"5\":1118},\"stars\":\"40\",\"min\":0},\"genres\":[\"犯罪\",\"惊悚\"],\"title\":\"隧道尽头\",\"casts\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51251.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51251.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51251.jpg\"},\"name_en\":\"Leonardo Sbaraglia\",\"name\":\"莱昂纳多·斯巴拉格利亚\",\"alt\":\"https://movie.douban.com/celebrity/1010747/\",\"id\":\"1010747\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1386578407.24.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1386578407.24.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1386578407.24.jpg\"},\"name_en\":\"Clara Lago\",\"name\":\"克拉拉·拉戈\",\"alt\":\"https://movie.douban.com/celebrity/1032704/\",\"id\":\"1032704\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p16034.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p16034.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p16034.jpg\"},\"name_en\":\"Federico Luppi\",\"name\":\"费德里科·路皮\",\"alt\":\"https://movie.douban.com/celebrity/1032480/\",\"id\":\"1032480\"}],\"durations\":[\"120分钟\"],\"collect_count\":12626,\"mainland_pubdate\":\"2019-07-25\",\"has_video\":false,\"original_title\":\"Al final del túnel\",\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1532356263.59.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1532356263.59.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1532356263.59.jpg\"},\"name_en\":\"Rodrigo Grande\",\"name\":\"罗德里戈·格兰德\",\"alt\":\"https://movie.douban.com/celebrity/1006650/\",\"id\":\"1006650\"}],\"pubdates\":[\"2016-04-21(阿根廷)\",\"2016-08-12(西班牙)\",\"2019-07-25(中国大陆)\"],\"year\":\"2016\",\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2336413879.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2336413879.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p2336413879.jpg\"},\"alt\":\"https://movie.douban.com/subject/26661229/\",\"id\":\"26661229\"}],\"title\":\"即将上映的电影\"}");

/***/ }),
/* 40 */,
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */
/*!********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/videos/鱿鱼游戏.mp4 ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/static/videos/鱿鱼游戏.mp4";

/***/ }),
/* 55 */
/*!**********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/posters/鱿鱼游戏.webp ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/static/posters/鱿鱼游戏.webp";

/***/ }),
/* 56 */,
/* 57 */,
/* 58 */
/*!*********************************************************************!*\
  !*** /Users/mac/Desktop/super_hero_preview/static/mock/detail.json ***!
  \*********************************************************************/
/*! exports provided: rating, reviews_count, videos, wish_count, original_title, blooper_urls, collect_count, images, douban_site, year, popular_comments, alt, id, mobile_url, photos_count, pubdate, title, do_count, has_video, share_url, seasons_count, languages, schedule_url, writers, pubdates, website, tags, has_schedule, durations, genres, collection, trailers, episodes_count, trailer_urls, has_ticket, bloopers, clip_urls, current_season, casts, countries, mainland_pubdate, photos, summary, clips, subtype, directors, comments_count, popular_reviews, ratings_count, aka, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"rating\":{\"max\":10,\"average\":7.5,\"details\":{\"1\":207,\"2\":1597,\"3\":15940,\"4\":21732,\"5\":7641},\"stars\":\"40\",\"min\":0},\"reviews_count\":306,\"videos\":[{\"source\":{\"literal\":\"iqiyi\",\"pic\":\"https://img3.doubanio.com/f/movie/7c9e516e02c6fe445b6559c0dd2a705e8b17d1c9/pics/movie/video-iqiyi.png\",\"name\":\"爱奇艺视频\"},\"sample_link\":\"http://www.iqiyi.com/v_19rrmy33ic.html?vfm=m_331_dbdy&fv=4904d94982104144a1548dd9040df241\",\"video_id\":\"19rrmy33ic\",\"need_pay\":true},{\"source\":{\"literal\":\"qq\",\"pic\":\"https://img3.doubanio.com/f/movie/0a74f4379607fa731489d7f34daa545df9481fa0/pics/movie/video-qq.png\",\"name\":\"腾讯视频\"},\"sample_link\":\"http://v.qq.com/x/cover/uzuqdig87eggmiw.html?ptag=douban.movie\",\"video_id\":\"uzuqdig87eggmiw\",\"need_pay\":true}],\"wish_count\":17511,\"original_title\":\"9\",\"blooper_urls\":[],\"collect_count\":85691,\"images\":{\"small\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p494268647.jpg\",\"large\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p494268647.jpg\",\"medium\":\"https://img1.doubanio.com/view/photo/s_ratio_poster/public/p494268647.jpg\"},\"douban_site\":\"\",\"year\":\"2009\",\"popular_comments\":[{\"rating\":{\"max\":5,\"value\":2,\"min\":0},\"useful_count\":44,\"author\":{\"uid\":\"Lan.die\",\"avatar\":\"https://img1.doubanio.com/icon/u1275416-68.jpg\",\"signature\":\"人生如寄，要美且开心。\",\"alt\":\"https://www.douban.com/people/Lan.die/\",\"id\":\"1275416\",\"name\":\"Lan~die\"},\"subject_id\":\"1764796\",\"content\":\"实在没必要拍成长篇。\",\"created_at\":\"2009-10-10 15:38:50\",\"id\":\"166753449\"},{\"rating\":{\"max\":5,\"value\":3,\"min\":0},\"useful_count\":0,\"author\":{\"uid\":\"kindsos\",\"avatar\":\"https://img3.doubanio.com/icon/u1726503-52.jpg\",\"signature\":\"今生苦短，来生再见！\",\"alt\":\"https://www.douban.com/people/kindsos/\",\"id\":\"1726503\",\"name\":\"kindsos\"},\"subject_id\":\"1764796\",\"content\":\"期待很久的片子。。有些失望。。讨厌那种小人物拯救世界的题材，总觉得不够真诚。。\",\"created_at\":\"2010-04-05 20:49:08\",\"id\":\"237235918\"},{\"rating\":{\"max\":5,\"value\":3,\"min\":0},\"useful_count\":0,\"author\":{\"uid\":\"longbookstore\",\"avatar\":\"https://img3.doubanio.com/icon/u3006497-45.jpg\",\"signature\":\"因為看不清，只能悶頭向前……\",\"alt\":\"https://www.douban.com/people/longbookstore/\",\"id\":\"3006497\",\"name\":\"龍爺\"},\"subject_id\":\"1764796\",\"content\":\"整體的設定。。。太過模糊和陰暗了，不過優點的話就是雖說是老掉牙的機器人話題，但是角度有所區別，也算是推陳出新了的，可惜是留不下什麽印象的片子，看過也就看過了。\",\"created_at\":\"2012-09-03 15:34:38\",\"id\":\"576141936\"},{\"rating\":{\"max\":5,\"value\":3,\"min\":0},\"useful_count\":17,\"author\":{\"uid\":\"No.59355\",\"avatar\":\"https://img3.doubanio.com/icon/u1059355-6.jpg\",\"signature\":\"垃圾分类很重要\",\"alt\":\"https://www.douban.com/people/No.59355/\",\"id\":\"1059355\",\"name\":\"问津\"},\"subject_id\":\"1764796\",\"content\":\"2009-10-09 看过。寓意浅白。但故事讲得很不好。Story和Director是一人，如果能给其他人导就好了。嗯，比如Tim Burton。\",\"created_at\":\"2009-10-09 14:39:36\",\"id\":\"166407222\"}],\"alt\":\"https://movie.douban.com/subject/1764796/\",\"id\":\"1764796\",\"mobile_url\":\"https://movie.douban.com/subject/1764796/mobile\",\"photos_count\":136,\"pubdate\":\"\",\"title\":\"机器人9号\",\"do_count\":null,\"has_video\":true,\"share_url\":\"https://m.douban.com/movie/subject/1764796\",\"seasons_count\":null,\"languages\":[\"英语\"],\"schedule_url\":\"\",\"writers\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1503025530.8.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1503025530.8.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1503025530.8.jpg\"},\"name_en\":\"Pamela Pettler\",\"name\":\"帕米拉·帕特勒\",\"alt\":\"https://movie.douban.com/celebrity/1301900/\",\"id\":\"1301900\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1351678808.44.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1351678808.44.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1351678808.44.jpg\"},\"name_en\":\"Shane Acker\",\"name\":\"申·阿克\",\"alt\":\"https://movie.douban.com/celebrity/1276787/\",\"id\":\"1276787\"}],\"pubdates\":[\"2009-09-09(美国)\"],\"website\":\"\",\"tags\":[\"动画\",\"科幻\",\"美国\",\"人性\",\"9\",\"2009\",\"战争\",\"3D\",\"剧情\",\"CG\"],\"has_schedule\":false,\"durations\":[\"79 分钟\"],\"genres\":[\"科幻\",\"动画\"],\"collection\":null,\"trailers\":[],\"episodes_count\":null,\"trailer_urls\":[],\"has_ticket\":false,\"bloopers\":[],\"clip_urls\":[],\"current_season\":null,\"casts\":[{\"avatars\":{\"small\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51597.jpg\",\"large\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51597.jpg\",\"medium\":\"https://img1.doubanio.com/view/celebrity/s_ratio_celebrity/public/p51597.jpg\"},\"name_en\":\"Elijah Wood\",\"name\":\"伊利亚·伍德\",\"alt\":\"https://movie.douban.com/celebrity/1054395/\",\"id\":\"1054395\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33305.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33305.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p33305.jpg\"},\"name_en\":\"Jennifer Connelly\",\"name\":\"詹妮弗·康纳利\",\"alt\":\"https://movie.douban.com/celebrity/1016673/\",\"id\":\"1016673\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p55994.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p55994.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p55994.jpg\"},\"name_en\":\"John C. Reilly\",\"name\":\"约翰·C·赖利\",\"alt\":\"https://movie.douban.com/celebrity/1017907/\",\"id\":\"1017907\"},{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42033.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42033.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p42033.jpg\"},\"name_en\":\"Christopher Plummer\",\"name\":\"克里斯托弗·普卢默\",\"alt\":\"https://movie.douban.com/celebrity/1036321/\",\"id\":\"1036321\"}],\"countries\":[\"美国\"],\"mainland_pubdate\":\"\",\"photos\":[{\"thumb\":\"https://img3.doubanio.com/view/photo/m/public/p1279933684.jpg\",\"image\":\"https://img3.doubanio.com/view/photo/l/public/p1279933684.jpg\",\"cover\":\"https://img3.doubanio.com/view/photo/sqs/public/p1279933684.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/1279933684/\",\"id\":\"1279933684\",\"icon\":\"https://img3.doubanio.com/view/photo/s/public/p1279933684.jpg\"},{\"thumb\":\"https://img3.doubanio.com/view/photo/m/public/p494395796.jpg\",\"image\":\"https://img3.doubanio.com/view/photo/l/public/p494395796.jpg\",\"cover\":\"https://img3.doubanio.com/view/photo/sqs/public/p494395796.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/494395796/\",\"id\":\"494395796\",\"icon\":\"https://img3.doubanio.com/view/photo/s/public/p494395796.jpg\"},{\"thumb\":\"https://img1.doubanio.com/view/photo/m/public/p494387619.jpg\",\"image\":\"https://img1.doubanio.com/view/photo/l/public/p494387619.jpg\",\"cover\":\"https://img1.doubanio.com/view/photo/sqs/public/p494387619.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/494387619/\",\"id\":\"494387619\",\"icon\":\"https://img1.doubanio.com/view/photo/s/public/p494387619.jpg\"},{\"thumb\":\"https://img3.doubanio.com/view/photo/m/public/p494290044.jpg\",\"image\":\"https://img3.doubanio.com/view/photo/l/public/p494290044.jpg\",\"cover\":\"https://img3.doubanio.com/view/photo/sqs/public/p494290044.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/494290044/\",\"id\":\"494290044\",\"icon\":\"https://img3.doubanio.com/view/photo/s/public/p494290044.jpg\"},{\"thumb\":\"https://img1.doubanio.com/view/photo/m/public/p2453637517.jpg\",\"image\":\"https://img1.doubanio.com/view/photo/l/public/p2453637517.jpg\",\"cover\":\"https://img1.doubanio.com/view/photo/sqs/public/p2453637517.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/2453637517/\",\"id\":\"2453637517\",\"icon\":\"https://img1.doubanio.com/view/photo/s/public/p2453637517.jpg\"},{\"thumb\":\"https://img1.doubanio.com/view/photo/m/public/p1360133498.jpg\",\"image\":\"https://img1.doubanio.com/view/photo/l/public/p1360133498.jpg\",\"cover\":\"https://img1.doubanio.com/view/photo/sqs/public/p1360133498.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/1360133498/\",\"id\":\"1360133498\",\"icon\":\"https://img1.doubanio.com/view/photo/s/public/p1360133498.jpg\"},{\"thumb\":\"https://img3.doubanio.com/view/photo/m/public/p1359577252.jpg\",\"image\":\"https://img3.doubanio.com/view/photo/l/public/p1359577252.jpg\",\"cover\":\"https://img3.doubanio.com/view/photo/sqs/public/p1359577252.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/1359577252/\",\"id\":\"1359577252\",\"icon\":\"https://img3.doubanio.com/view/photo/s/public/p1359577252.jpg\"},{\"thumb\":\"https://img3.doubanio.com/view/photo/m/public/p1359559270.jpg\",\"image\":\"https://img3.doubanio.com/view/photo/l/public/p1359559270.jpg\",\"cover\":\"https://img3.doubanio.com/view/photo/sqs/public/p1359559270.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/1359559270/\",\"id\":\"1359559270\",\"icon\":\"https://img3.doubanio.com/view/photo/s/public/p1359559270.jpg\"},{\"thumb\":\"https://img3.doubanio.com/view/photo/m/public/p1359558806.jpg\",\"image\":\"https://img3.doubanio.com/view/photo/l/public/p1359558806.jpg\",\"cover\":\"https://img3.doubanio.com/view/photo/sqs/public/p1359558806.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/1359558806/\",\"id\":\"1359558806\",\"icon\":\"https://img3.doubanio.com/view/photo/s/public/p1359558806.jpg\"},{\"thumb\":\"https://img1.doubanio.com/view/photo/m/public/p1359558167.jpg\",\"image\":\"https://img1.doubanio.com/view/photo/l/public/p1359558167.jpg\",\"cover\":\"https://img1.doubanio.com/view/photo/sqs/public/p1359558167.jpg\",\"alt\":\"https://movie.douban.com/photos/photo/1359558167/\",\"id\":\"1359558167\",\"icon\":\"https://img1.doubanio.com/view/photo/s/public/p1359558167.jpg\"}],\"summary\":\"机器人9号（伊利亚•伍德 Elijah Wood 饰）突然醒来，发现身边的世界充满危机，四处残败，一片末世景象。9号带着一个画有三个奇怪符号的圆形物体逃到街上，幸遇发明家机器人2号（马丁•兰道 Martin Landau 饰）给自己装上了声音，但2号却不幸被机器怪兽抓走。9号找到了老兵1号（克里斯托弗•普卢默 Christopher Plummer 饰）、机械工5号（约翰•雷利 John C. Reilly 饰）、疯癫画家6号（克里斯品•格拉夫 Crispin Glover 饰）和大力士8号（弗雷德•塔塔绍尔 Fred Tatasciore 饰）。9号与5号擅自出行援救2号，危急时被女武士7号（詹妮佛•康纳利 Jennifer Connelly 饰）救下，但无意中9号却令终极机器兽复活。带着自己从哪里来以及生存使命的问题，9号决定想尽办法制服机器兽，拯救全世界……©豆瓣\",\"clips\":[],\"subtype\":\"movie\",\"directors\":[{\"avatars\":{\"small\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1351678808.44.jpg\",\"large\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1351678808.44.jpg\",\"medium\":\"https://img3.doubanio.com/view/celebrity/s_ratio_celebrity/public/p1351678808.44.jpg\"},\"name_en\":\"Shane Acker\",\"name\":\"申·阿克\",\"alt\":\"https://movie.douban.com/celebrity/1276787/\",\"id\":\"1276787\"}],\"comments_count\":9728,\"popular_reviews\":[{\"rating\":{\"max\":5,\"value\":4,\"min\":0},\"title\":\"9哥就是个大阴谋家\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"LaoyeChan\",\"avatar\":\"https://img1.doubanio.com/icon/u2230001-17.jpg\",\"signature\":\"美丽世界的孤儿\",\"alt\":\"https://www.douban.com/people/LaoyeChan/\",\"id\":\"2230001\",\"name\":\"砍脖厮\"},\"summary\":\"电影开始，9哥只是个新来的无名小卒，但它心中的野心计划已经开始。 在它面前的阻碍有，作为团队领袖的1哥，作为自己大哥和情敌的2哥，作为政府武力的胖哥（忘了它是几号了）。 其余的人，艺术家6哥，工人阶级5哥...\",\"alt\":\"https://movie.douban.com/review/2478172/\",\"id\":\"2478172\"},{\"rating\":{\"max\":5,\"value\":4,\"min\":0},\"title\":\"如果卡梅隆来做制片 （剧透在分割线以后）\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"2185280\",\"avatar\":\"https://img3.doubanio.com/icon/u2185280-1.jpg\",\"signature\":\"\",\"alt\":\"https://www.douban.com/people/2185280/\",\"id\":\"2185280\",\"name\":\"imrains\"},\"summary\":\"    真可惜“9”堪比“终结者”的末世场景和机器人，超炫的动作场面，竟然讲了一个这么糊涂的故事... 这是我看完这部电影的总体感觉。      看这部电影的初衷是因为Tim Burton，很喜欢他那些”诡异”的电影所讲述...\",\"alt\":\"https://movie.douban.com/review/2332914/\",\"id\":\"2332914\"},{\"rating\":{\"max\":5,\"value\":3,\"min\":0},\"title\":\"没有了想象力，要技术还有什么用？\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"thatiszhangyan\",\"avatar\":\"https://img1.doubanio.com/icon/u1997010-18.jpg\",\"signature\":\"总得磕磕绊绊地前进吧\",\"alt\":\"https://www.douban.com/people/thatiszhangyan/\",\"id\":\"1997010\",\"name\":\"ZhangYan\"},\"summary\":\"两年前，在北京电影学院国际学生影展上，这个短片被评为了一等奖（打败了一个纽约大学（NYU）的学生拍摄的震惊全场、被老师同学们广为传颂的、得过戛纳短片一等奖的片子）。根据当时的流传（不一定准确），片子的...\",\"alt\":\"https://movie.douban.com/review/2488244/\",\"id\":\"2488244\"},{\"rating\":{\"max\":5,\"value\":4,\"min\":0},\"title\":\"我觉得结尾应该是这样的……\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"avankiss\",\"avatar\":\"https://img3.doubanio.com/icon/u1741835-1.jpg\",\"signature\":\"\",\"alt\":\"https://www.douban.com/people/avankiss/\",\"id\":\"1741835\",\"name\":\"avon\"},\"summary\":\"       如果我以前也曾用“虎头蛇尾”来形容一部电影。那么忘记它吧，因为我也许只是说说而已，除了这一部。 彻头彻尾令人发指的虎头挂个蛇尾，狗尾续个貂身。人神共愤！！！     无论是机器人的造型，特效都是一...\",\"alt\":\"https://movie.douban.com/review/2574051/\",\"id\":\"2574051\"},{\"rating\":{\"max\":5,\"value\":4,\"min\":0},\"title\":\"9个小机器人\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"151268747\",\"avatar\":\"https://img3.doubanio.com/icon/u151268747-10.jpg\",\"signature\":\"\",\"alt\":\"https://www.douban.com/people/151268747/\",\"id\":\"151268747\",\"name\":\"爱吃馒头的宝宝\"},\"summary\":\"科幻悬疑冒险动画，讲述不久的未来，人类制造的机器向人类展开进攻，最终机器将人类屠杀殆尽，另一小队人马却开始了与机器的战争，保护着人类最后的文明。末世情节，虽故事一般，但制作精致，发人深省。 从1号到9...\",\"alt\":\"https://movie.douban.com/review/8696041/\",\"id\":\"8696041\"},{\"rating\":{\"max\":5,\"value\":4,\"min\":0},\"title\":\"引人深思\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"155672072\",\"avatar\":\"https://img3.doubanio.com/icon/u155672072-3.jpg\",\"signature\":\"\",\"alt\":\"https://www.douban.com/people/155672072/\",\"id\":\"155672072\",\"name\":\"快乐卟卟\"},\"summary\":\"电影的故事讲述了在不久的未来，人类制造的机器向人类展开了进攻。建筑被毁、社会也分崩离析。最终，机器将人类屠杀殆尽。另一小队人马却开始了与机器的战争，保护着人类最后的文明。一部引人深思的电影，其实每...\",\"alt\":\"https://movie.douban.com/review/8674804/\",\"id\":\"8674804\"},{\"rating\":{\"max\":5,\"value\":3,\"min\":0},\"title\":\"机器人9号\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"158195982\",\"avatar\":\"https://img3.doubanio.com/icon/u158195982-24.jpg\",\"signature\":\"\",\"alt\":\"https://www.douban.com/people/158195982/\",\"id\":\"158195982\",\"name\":\"工藤新一\"},\"summary\":\"作为创意之作，本片无疑至少在画面上是极度成功的，破败的哥特风格显示了一种动画中很少出现的特异景象，因该比大红大紫的鬼妈妈更有气氛。无法成为经典的原因应该是难以撑起这份颓废的苍白内核。简单的机器反噬...\",\"alt\":\"https://movie.douban.com/review/8662488/\",\"id\":\"8662488\"},{\"rating\":{\"max\":5,\"value\":4,\"min\":0},\"title\":\"第九份灵魂——路线另类虎头蛇尾的电影会是好电影吗？\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"majorjohn\",\"avatar\":\"https://img1.doubanio.com/icon/user_normal.jpg\",\"signature\":\"\",\"alt\":\"https://www.douban.com/people/majorjohn/\",\"id\":\"2371040\",\"name\":\"[已注销]\"},\"summary\":\"路线另类虎头蛇尾的电影会是好电影吗？  最近更新：我在电影院看没有算时间，电影可能真是76分钟，感觉还是挺短的。   在回答这个问题之前，我首先解释下为什么我说它路线另类，虎头蛇尾。  路线另类： 美国动画...\",\"alt\":\"https://movie.douban.com/review/2337210/\",\"id\":\"2337210\"},{\"rating\":{\"max\":5,\"value\":4,\"min\":0},\"title\":\"《机器人9号》：人性分解之九型人格与十二星座理论溯源\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"passionfly\",\"avatar\":\"https://img3.doubanio.com/icon/u1540841-26.jpg\",\"signature\":\"感性也好，理性也好，激情最好\",\"alt\":\"https://www.douban.com/people/passionfly/\",\"id\":\"1540841\",\"name\":\"passionfly\"},\"summary\":\"一、9号机器人星座人格对应列表  机器人及角色 对应12星座 典型性格特征 对应人格九型  1号 退伍兵 天蝎座 狮子座 权威 恐惧 【完美者】一型 2号 发明家 水瓶座 天秤座 实用 承担 【给予者】二型 3号 书管员 双子...\",\"alt\":\"https://movie.douban.com/review/2826017/\",\"id\":\"2826017\"},{\"rating\":{\"max\":5,\"value\":4,\"min\":0},\"title\":\"《9》：小布人的末世预言\",\"subject_id\":\"1764796\",\"author\":{\"uid\":\"LucFrance\",\"avatar\":\"https://img3.doubanio.com/icon/u1978083-2.jpg\",\"signature\":\"春眠不觉小...\",\"alt\":\"https://www.douban.com/people/LucFrance/\",\"id\":\"1978083\",\"name\":\"Luc\"},\"summary\":\"虽说动画在好莱坞的定位，并不止于小朋友们的“寓教于乐”，但当独立的成人动画欲求全面公映时，发行方为了追求票房的最大化，潜在的操作就是“和谐”友爱，降低其暴力程度。为了老少皆宜，保守的剧本不惜套用陈...\",\"alt\":\"https://movie.douban.com/review/2812779/\",\"id\":\"2812779\"}],\"ratings_count\":64720,\"aka\":[\"9：末世决战\",\"九\",\"Number 9\",\"机器人9号\"]}");

/***/ })
]]);
//# sourceMappingURL=../../.sourcemap/mp-weixin/common/vendor.js.map